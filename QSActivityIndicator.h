//
//  QSActivityIndicator.h
//  Qobalt
//
//  Created by Satish Kumar on 19/02/14.
//  Copyright (c) 2014 Satish Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface QSActivityIndicator : UIView

@property (nonatomic, strong) AppDelegate *appDelegate;

+ (void)start;

+ (void)end;

@end
