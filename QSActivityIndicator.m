//
//  QSActivityIndicator.m
//  Qobalt
//
//  Created by Satish Kumar on 19/02/14.
//  Copyright (c) 2014 Satish Kumar. All rights reserved.


#import "QSActivityIndicator.h"

@interface QSActivityIndicator ()

@property (nonatomic, strong) UIActivityIndicatorView *indicator;

@property (nonatomic, strong) UIView *background;

@end

@implementation QSActivityIndicator

static QSActivityIndicator *sharedIndicator = nil;

+ (void)start {
    
    [[self class] sharedIndicator];
    
}

+ (void)sharedIndicator {

    @synchronized(self) {
        
        if (sharedIndicator == nil) {
            
            sharedIndicator = [[self alloc] init];
            
        }
        
	}
	
}

+ (id)allocWithZone: (NSZone *) zone {
    
	@synchronized(self) {
        
        if (sharedIndicator == nil) {
            
            sharedIndicator = [super allocWithZone: zone];
            
			return sharedIndicator;  // assignment and return on first allocation
            
        }
	}
    
	return nil; //on subsequent allocation attempts return nil
    
}

- (id)copyWithZone: (NSZone *) zone {
    
	return self;
}

//-----

- (id)init {

    self = [super init];
    
    if (self) {
        
        _appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        self.frame = CGRectMake(0, 0, _appDelegate.window.frame.size.width, _appDelegate.window.frame.size.height);
        
        [self setBackgroundColor:[UIColor clearColor]];
        
        _background = [[UIView alloc] initWithFrame:CGRectMake(self.frame.size.width/2 - 80/2, self.frame.size.height/2 - 80/2, 80, 80)];
        [_background setBackgroundColor:[UIColor blackColor]];
        [_background setAlpha:0.6];
        [_background setClipsToBounds:YES];
        [[_background layer] setCornerRadius:10.0f];
        [self addSubview:_background];

        _indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [_indicator setFrame:CGRectMake(self.frame.size.width/2 - 30/2, self.frame.size.height/2 - 30/2, 30, 30)];
        [self addSubview:_indicator];
        [_indicator startAnimating];
        
        if (_appDelegate.window.rootViewController) {
         
            [_appDelegate.window.rootViewController.view addSubview:self];

        }
        else {
        
            [_appDelegate.window addSubview:self];

        }
        
    }
    
    return self;
}

+ (void)end {
    
    [sharedIndicator.indicator stopAnimating];
    [sharedIndicator removeFromSuperview];
    sharedIndicator=nil;
}


@end
