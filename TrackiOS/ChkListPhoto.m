//
//  ChkListPhoto.m
//  TrackiOS
//
//  Created by John Paul Ranjith on 05/12/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "ChkListPhoto.h"


@implementation ChkListPhoto

@dynamic checkListID;
@dynamic mainSectionID;
@dynamic mainSectionName;
@dynamic photoObj1;
@dynamic photoObj2;
@dynamic photoObj3;
@dynamic photoObj4;
@dynamic photoObj5;
@dynamic photoSavedLocation;
@dynamic rowID;
@dynamic subSectionID;
@dynamic subSectionName;
@dynamic deletePhotoBtn1;
@dynamic deletePhotoBtn2;
@dynamic deletePhotoBtn3;
@dynamic deletePhotoBtn4;
@dynamic deletePhotoBtn5;

@end
