//
//  ChkListActionPlan.m
//  TrackiOS
//
//  Created by John Paul Ranjith on 02/12/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "ChkListActionPlan.h"


@implementation ChkListActionPlan

@dynamic actionStr;
@dynamic checklistID;
@dynamic desiredOutcome;
@dynamic dueDate;
@dynamic mainSectionID;
@dynamic mainSectionName;
@dynamic responsiblePeople;
@dynamic situation;
@dynamic subSectionID;
@dynamic subSectionName;
@dynamic support;
@dynamic rowID;

@end
