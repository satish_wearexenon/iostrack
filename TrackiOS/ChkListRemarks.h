//
//  ChkListRemarks.h
//  Pods
//
//  Created by John Paul Ranjith on 02/12/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ChkListRemarks : NSManagedObject

@property (nonatomic, retain) NSNumber * checkListID;
@property (nonatomic, retain) NSNumber * mainSectionID;
@property (nonatomic, retain) NSString * mainSectionName;
@property (nonatomic, retain) NSNumber * subSectionID;
@property (nonatomic, retain) NSString * subSectionName;
@property (nonatomic, retain) NSString * remarksStr;
@property (nonatomic, retain) NSNumber * rowID;


@end
