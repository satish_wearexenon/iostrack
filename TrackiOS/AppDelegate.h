//
//  AppDelegate.h
//  TrackiOS
//
//  Created by abhijeet upadhyay on 07/10/14.
//  Copyright (c) 2014 Xenon Automotive. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CustomSplitViewController.h"
#import "ListViewController.h"
#import "MenuViewController.h"
#import "MainViewController.h"
//-------
#import "XeEnterprisLoginViewController.h"
//-------
#import <MessageUI/MessageUI.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate,MFMailComposeViewControllerDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) CustomSplitViewController *customSplitViewController;
@property (assign, nonatomic) BOOL isFirstLaunch;

+ (AppDelegate *)appDelegate;
- (MainViewController *)appBaseViewController;

- (void) addControllers;


- (void) sendEMail;

- (void) moveToUserLogin;
- (void) moveToEnterpriseLogin;


@end
