//
//  GraphView.m
//  CorePlotBarChartExample
//
//  Created by Anthony Perozzo on 8/06/12.
//  Copyright (c) 2012 Gilthonwe Apps. All rights reserved.
//

#import "GraphView.h"

@implementation GraphView
//
//- (void)generateData
//{
//    NSMutableDictionary *dataTemp = [[NSMutableDictionary alloc] init];
//    
//    //Array containing all the dates that will be displayed on the X axis
//    _dates = [NSArray arrayWithObjects:@"2012-05-01", @"2012-05-02", @"2012-05-03",
//             @"2012-05-04", @"2012-05-05", @"2012-05-06", @"2012-05-07", nil];
//    
//    //Dictionary containing the name of the two sets and their associated color
//    //used for the demo
//    sets = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor blueColor], @"Plot 1",
//            [UIColor redColor], @"Plot 2",
//            [UIColor greenColor], @"Plot 3", nil];
//
//    //Generate random data for each set of data that will be displayed for each day
//    //Numbers between 1 and 10
//    for (NSString *date in dates) {
//        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
//        for (NSString *set in sets) {
//            NSNumber *num = [NSNumber numberWithInt:arc4random_uniform(10)+1];
//            [dict setObject:num forKey:set];
//        }
//        [dataTemp setObject:dict forKey:date];
//    }
//    
//    data = [dataTemp copy];
//    [dataTemp release];
//
//    NSLog(@"%@", data);
//}

- (void)generateLayout
{
    //Create graph from theme
	_graph                               = [[CPTXYGraph alloc] initWithFrame:CGRectZero];
    
    CPTColor *your_color = [CPTColor colorWithComponentRed:255 green:255 blue:255 alpha:1];
    _graph.fill = [CPTFill fillWithColor:your_color];
    
	self.hostedGraph                    = _graph;
    _graph.plotAreaFrame.masksToBorder   = YES;
    _graph.paddingLeft                   = 0.0f;
    _graph.paddingTop                    = 0.0f;
	_graph.paddingRight                  = 0.0f;
	_graph.paddingBottom                 = 0.0f;
    
    CPTMutableLineStyle *borderLineStyle    = [CPTMutableLineStyle lineStyle];
	borderLineStyle.lineColor               = [CPTColor whiteColor];
	borderLineStyle.lineWidth               =1.0f;
    _graph.plotAreaFrame.borderLineStyle     = borderLineStyle;
	_graph.plotAreaFrame.paddingTop          = 0.0;
	_graph.plotAreaFrame.paddingRight        = 0.0;
	_graph.plotAreaFrame.paddingBottom       = 10.0;
	_graph.plotAreaFrame.paddingLeft         = 0.0;
    

    _graph.plotAreaFrame.borderWidth = 0;
    _graph.plotAreaFrame.cornerRadius = 0;
    
	//Add plot space
	CPTXYPlotSpace *plotSpace       = (CPTXYPlotSpace *)_graph.defaultPlotSpace;
    plotSpace.delegate              = self;
	plotSpace.yRange                = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(0) 
                                                                   length:CPTDecimalFromInt(10 * _sets.count)];
	plotSpace.xRange                = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(-1) 
                                                                   length:CPTDecimalFromInt(8)];
    
    //Axes
	CPTXYAxisSet *axisSet = (CPTXYAxisSet *)_graph.axisSet;
    
    //X axis
    CPTXYAxis *x                    = axisSet.xAxis;
    x.orthogonalCoordinateDecimal   = CPTDecimalFromInt(0);
	x.majorIntervalLength           = CPTDecimalFromInt(1);
	x.minorTicksPerInterval         = 0;
    x.labelingPolicy                = CPTAxisLabelingPolicyNone; // Remove Y Axis label
    //x.majorGridLineStyle            = majorGridLineStyle;
    x.axisConstraints               = [CPTConstraints constraintWithLowerOffset:0.0];
    
    
    //Y axis
	CPTXYAxis *y            = axisSet.yAxis;
    y.axisConstraints       = [CPTConstraints constraintWithLowerOffset:-3.0];

    
    //Create a bar line style
    CPTMutableLineStyle *barLineStyle   = [[[CPTMutableLineStyle alloc] init] autorelease];
    barLineStyle.lineWidth              = 0.0;
    barLineStyle.lineColor              = [CPTColor whiteColor];
    
    //Plot
    BOOL firstPlot = YES;
    for (NSString *set in [[_sets allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]) {
        CPTBarPlot *plot        = [CPTBarPlot tubularBarPlotWithColor:[CPTColor blueColor] horizontalBars:NO];
        plot.lineStyle          = barLineStyle;
        CGColorRef color        = ((UIColor *)[_sets objectForKey:set]).CGColor;
        plot.fill               = [CPTFill fillWithColor:[CPTColor colorWithCGColor:color]];
        if (firstPlot) {
            plot.barBasesVary   = NO;
            firstPlot           = NO;
        } else {
            plot.barBasesVary   = YES;
        }
        plot.barWidth           = CPTDecimalFromFloat(0.8f);
        plot.barsAreHorizontal  = NO;
        plot.dataSource         = self;
        plot.identifier         = set;
        plot.borderWidth = 0;
        plot.cornerRadius = 0;
        [_graph addPlot:plot toPlotSpace:plotSpace];
    }
}

- (void)createGraph
{
    //Generate data
    //[self generateData];
    
    //Generate layout
    [self generateLayout];
}

//- (void)dealloc
//{
//    [data release];
//    [super dealloc];
//}

#pragma mark - CPTPlotDataSource methods

- (NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot
{
    return _dates.count;
}

- (double)doubleForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index
{
    double num = NAN;
    
    //X Value
    if (fieldEnum == 0) {
        num = index;
    }
    
    else {
        double offset = 0;
        if (((CPTBarPlot *)plot).barBasesVary) {
            for (NSString *set in [[_sets allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]) {
                if ([plot.identifier isEqual:set]) {
                    break;
                }
                offset += [[[_data objectForKey:[_dates objectAtIndex:index]] objectForKey:set] doubleValue];
            }
        }
        
        //Y Value
        if (fieldEnum == 1) {
            num = [[[_data objectForKey:[_dates objectAtIndex:index]] objectForKey:plot.identifier] doubleValue] + offset;
        }
        
        //Offset for stacked bar
        else {
            num = offset;
        }
    }
    
    //NSLog(@"%@ - %d - %d - %f", plot.identifier, index, fieldEnum, num);
    
    return num;
}

@end
