//
//  AppDelegate.m
//  TrackiOS
//
//  Created by abhijeet upadhyay on 07/10/14.
//  Copyright (c) 2014 Xenon Automotive. All rights reserved.
//

#import "AppDelegate.h"
#import "Flurry.h"
#import "User.h"

@interface AppDelegate ()

@property (copy, nonatomic) NSString *localStr;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //NSString *temp = @"abc";
    //self.localStr = [[NSString alloc] init];
    //    NSMutableString *str = [NSMutableString stringWithString:@"Honda"];
    //    CarModel *model = [[CarModel alloc]init];
    //    model.carName = str;
    //    self.localStr = str;
    //    NSLog(@"car = %@ , %@", model.carName , self.localStr);
    //    [str setString:@"Toyota"];
    //    NSLog(@"car = %@ , %@", model.carName, self.localStr);
        NSLog(@"-------");
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    [self.window makeKeyAndVisible];
    
    _isFirstLaunch = YES;
    
    [UIView setAnimationsEnabled:NO];
    
    [MagicalRecord setupCoreDataStack];
    
    if (_isFirstLaunch) {
        
        NSString *paramURL = [[NSUserDefaults standardUserDefaults] objectForKey:@"OEMBaseURL"];
        
        if (paramURL) {
            [self moveToUserLogin];
        }
        else
        {
            [self moveToEnterpriseLogin];
        }
    }
    else
        [self addControllers];
    
    // Flurry API
    [self addFlurryApi];
    
    
    return YES;
}

-(void)addFlurryApi
{
    //Flurry Analytics added for crash, error and sessions analysis.
    [Flurry setCrashReportingEnabled:YES];
    [Flurry startSession:@"R2TG8PSNS2F8D9KQ3HTW"];
    
}

+(AppDelegate *)appDelegate
{
    return (AppDelegate*)[UIApplication sharedApplication].delegate;
}

- (void) moveToEnterpriseLogin
{
    XeEnterprisLoginViewController *homeViewController = [[UIStoryboard enterpriseStoryBoard] instantiateInitialViewController];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:homeViewController];
    [navController.navigationBar setHidden:YES];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    BOOL oldState = [UIView areAnimationsEnabled];
    [UIView setAnimationsEnabled:YES];
    
    if (oldState)
    {
        CGRect newFrame = homeViewController.view.frame;
        newFrame.origin.x -= 200;    // shift left by 200pts
        
        [UIView animateWithDuration:0.3
                         animations:^{
                             homeViewController.view.frame = newFrame;
                         }];
        
    }
    self.window.rootViewController = navController;
}

//Write code to move to User Login page.
- (void) moveToUserLogin
{
    NSLog(@"Please write another function which takes it to user Login controller");
    //  logInStoryBoard
    
    XeUserLoginViewController *loginViewController = [[UIStoryboard enterpriseStoryBoard] instantiateViewControllerWithIdentifier:@"XeUserLoginViewController"];
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:loginViewController];
    [navController.navigationBar setHidden:YES];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    BOOL oldState = [UIView areAnimationsEnabled];
    [UIView setAnimationsEnabled:YES];
    
    if (oldState)
    {
        CGRect newFrame = loginViewController.view.frame;
        newFrame.origin.x -= 200;    // shift left by 200pts
        
        [UIView animateWithDuration:0.3
                         animations:^{
                             loginViewController.view.frame = newFrame;
                         }];
        
    }
    self.window.rootViewController = navController;
    
    
}

// Called when login successful
- (void) addControllers
{
    MainViewController *homeViewController = [[UIStoryboard mainStoryBoard]instantiateInitialViewController];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:homeViewController];
    [navController.navigationBar setHidden:YES];
    
    ListViewController *listViewController = [[UIStoryboard menuStoryBoard] instantiateViewControllerWithIdentifier:@"ListViewController"];
    UINavigationController *navigationControllerListView = [[UINavigationController alloc] initWithRootViewController:listViewController];
    
    MenuViewController *menuViewController = [[UIStoryboard menuStoryBoard] instantiateViewControllerWithIdentifier:@"MenuViewController"];
    UINavigationController *navigationControllerMenuView = [[UINavigationController alloc] initWithRootViewController:menuViewController];
    
    self.customSplitViewController = [[CustomSplitViewController alloc] initWithMenuViewController:navigationControllerMenuView ListViewController:navigationControllerListView DetailViewController:navController];
    self.customSplitViewController.delegate = menuViewController;
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    //    [UIView
    //     transitionWithView:self.window
    //     duration:0.1
    //     options:UIViewAnimationOptionTransitionFlipFromTop
    //     animations:^(void) {
    //         BOOL oldState = [UIView areAnimationsEnabled];
    //         [UIView setAnimationsEnabled:YES];
    //         self.window.rootViewController = self.customSplitViewController;
    //         [UIView setAnimationsEnabled:oldState];
    //     }
    //     completion:nil];
    
    BOOL oldState = [UIView areAnimationsEnabled];
    [UIView setAnimationsEnabled:YES];
    
    if (oldState)
    {
        CGRect newFrame = self.customSplitViewController.view.frame;
        newFrame.origin.x -= 200;    // shift left by 200pts
        
        [UIView animateWithDuration:0.3
                         animations:^{
                             self.customSplitViewController.view.frame = newFrame;
                         }];
    }
    self.window.rootViewController = self.customSplitViewController;
}

- (MainViewController *)appBaseViewController
{
    MainViewController *homeController = nil;
    NSArray *navControllerItems = [(UINavigationController *)self.customSplitViewController.detailViewController viewControllers];
    if ([navControllerItems count] > 0 ) {
        homeController =  [navControllerItems objectAtIndex:0];
    }
    return homeController;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
   // // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    // [COREDATA_MANAGER saveContext];
    //     [[CoreDataLayer coreDataSharedmanager] saveContext];
    //   [[RKManagedObjectStore defaultStore].mainQueueManagedObjectContext saveToPersistentStore:nil];
    
}

- (void) sendEMail
{
    if ([MFMailComposeViewController canSendMail]){
        MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        [controller setSubject:@"Action Plan"];
        //[controller setToRecipients:[NSArray arrayWithObject:eMail]];
        [self.customSplitViewController presentViewController:controller animated:YES completion:nil];
    }
    else{
        UIAlertView *anAlert = [[UIAlertView alloc] initWithTitle:@"error" message:@"No mail account setup on device" delegate:self cancelButtonTitle:nil otherButtonTitles:nil];
        [anAlert addButtonWithTitle:@"Cancel"];
        [anAlert show];
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    if (result == MFMailComposeResultSent)
    {
        NSLog(@"\n\n Email Sent");
        
    }
    if([self respondsToSelector:@selector(dismissViewControllerAnimated:completion:)])
        [self.customSplitViewController dismissViewControllerAnimated:YES completion:nil];
}


@end
