//
//  ChkListPhoto.h
//  TrackiOS
//
//  Created by John Paul Ranjith on 05/12/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ChkListPhoto : NSManagedObject

@property (nonatomic, retain) NSNumber * checkListID;
@property (nonatomic, retain) NSNumber * mainSectionID;
@property (nonatomic, retain) NSString * mainSectionName;
@property (nonatomic, retain) NSData * photoObj1;
@property (nonatomic, retain) NSData * photoObj2;
@property (nonatomic, retain) NSData * photoObj3;
@property (nonatomic, retain) NSData * photoObj4;
@property (nonatomic, retain) NSData * photoObj5;
@property (nonatomic, retain) NSString * photoSavedLocation;
@property (nonatomic, retain) NSNumber * rowID;
@property (nonatomic, retain) NSNumber * subSectionID;
@property (nonatomic, retain) NSString * subSectionName;
@property (nonatomic, retain) NSString * deletePhotoBtn1;
@property (nonatomic, retain) NSString * deletePhotoBtn2;
@property (nonatomic, retain) NSString * deletePhotoBtn3;
@property (nonatomic, retain) NSString * deletePhotoBtn4;
@property (nonatomic, retain) NSString * deletePhotoBtn5;

@end
