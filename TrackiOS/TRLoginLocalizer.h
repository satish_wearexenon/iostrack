//
//  TRLoginLocalizer.h
//  TrackiOS
//
//  Created by satish sharma on 10/13/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

//#ifndef TrackiOS_TRLoginLocalizer_h
//#define TrackiOS_TRLoginLocalizer_h
//
//
//#endif


#define Login                                         @"TRLogin"

#define LOCALIZED_ENTERPRISE_LOGIN_SCREEN_WELCOME_TITLE            NSLocalizedStringWithDefaultValue(@"Welcome to Xenon Track",Login,[NSBundle mainBundle],@"Welcome to Xenon Track",nil)

#define LOCALIZED_ENTERPRISE_LOGIN_SCREEN_PLEASE_ENTER_TITLE            NSLocalizedStringWithDefaultValue(@"Please enter your enterprise code",Login,[NSBundle mainBundle],@"Please enter your enterprise code",nil)

#define LOCALIZED_ENTERPRISE_PLACE_HOLDER             NSLocalizedStringWithDefaultValue(@"Enterprise Code",Login,[NSBundle mainBundle],@"Enterprise Code",nil)

#define LOCALIZED_CONTINUE_BUTTON_TITLE             NSLocalizedStringWithDefaultValue(@"Continue",Login,[NSBundle mainBundle],@"Continue",nil)

#define LOCALIZED_CONFIGURE_DOWNLOAD_TITLE             NSLocalizedStringWithDefaultValue(@"Downloading configure files",Login,[NSBundle mainBundle],@"Downloading configure files",nil)

#define LOCALIZED_LOGIN_WELCOME_TITLE             NSLocalizedStringWithDefaultValue(@"Welcome to Xenon Track",Login,[NSBundle mainBundle],@"Welcome to Xenon Track",nil)

#define LOCALIZED_PLEASE_LOGIN_TITLE             NSLocalizedStringWithDefaultValue(@"Please login to access your account",Login,[NSBundle mainBundle],@"Please login to access your account",nil)


#define LOCALIZED_USER_NAME_PLACE_HOLDER             NSLocalizedStringWithDefaultValue(@"Username",Login,[NSBundle mainBundle],@"Username",nil)

#define LOCALIZED_PASSWORD_PLACE_HOLDER            NSLocalizedStringWithDefaultValue(@"Password",Login,[NSBundle mainBundle],@"Password",nil)

#define LOCALIZED_LOGIN_BUTTON_TITLE            NSLocalizedStringWithDefaultValue(@"login",Login,[NSBundle mainBundle],@"login",nil)

#define LOCALIZED_LOG_OUT_TITLE            NSLocalizedStringWithDefaultValue(@"logout",Login,[NSBundle mainBundle],@"logout",nil)

#define LOCALIZED_FEEDBACK_TITLE            NSLocalizedStringWithDefaultValue(@"Feedback",Login,[NSBundle mainBundle],@"Feedback",nil)

#define LOCALIZED_ABOUT_TITLE            NSLocalizedStringWithDefaultValue(@"About",Login,[NSBundle mainBundle],@"About",nil)

// dealer
#define LOCALIZED_SORT_BY            NSLocalizedStringWithDefaultValue(@"SORT BY:",Login,[NSBundle mainBundle],@"SORT BY:",nil)
#define LOCALIZED_FILTER_DEALER_NAME   NSLocalizedStringWithDefaultValue(@"Dealer Name",Login,[NSBundle mainBundle],@"Dealer Name",nil)
#define LOCALIZED_FILTER_DEALER_CODE   NSLocalizedStringWithDefaultValue(@"Dealer Code",Login,[NSBundle mainBundle],@"Dealer Code",nil)
#define LOCALIZED_FILTER_DEALER_READINESS   NSLocalizedStringWithDefaultValue(@"Readiness",Login,[NSBundle mainBundle],@"Readiness",nil)
#define LOCALIZED_FILTER_DEALER_DATE   NSLocalizedStringWithDefaultValue(@"Update Date",Login,[NSBundle mainBundle],@"Update Date",nil)
#define LOCALIZED_PROGRESS_PERCENTAGE   NSLocalizedStringWithDefaultValue(@"PL: 00% ready",Login,[NSBundle mainBundle],@"PL: 00% ready",nil)
#define LOCALIZED_DEALER_ITEMS_UPDATE   NSLocalizedStringWithDefaultValue(@"items updated",Login,[NSBundle mainBundle],@"items updated",nil)
#define LOCALIZED_DEALER_PHOTOS_ADDED   NSLocalizedStringWithDefaultValue(@"photos added",Login,[NSBundle mainBundle],@"photos added",nil)

//Settings
#define LOCALIZED_SETTINGS_AFTER_SALES   NSLocalizedStringWithDefaultValue(@"After Sales",Login,[NSBundle mainBundle],@"After Sales",nil)
#define LOCALIZED_SETTINGS_NETWORK_DEV   NSLocalizedStringWithDefaultValue(@"Network Dev",Login,[NSBundle mainBundle],@"Network Dev",nil)
#define LOCALIZED_SETTINGS_CHANGE_PASSWORD   NSLocalizedStringWithDefaultValue(@"Change Password",Login,[NSBundle mainBundle],@"Change Password",nil)
#define LOCALIZED_SETTINGS_LOG_OUT   NSLocalizedStringWithDefaultValue(@"Log Out",Login,[NSBundle mainBundle],@"Log Out",nil)
#define LOCALIZED_SETTINGS_XENON_TRACK   NSLocalizedStringWithDefaultValue(@"XENON TRACK",Login,[NSBundle mainBundle],@"XENON TRACK",nil)

//Calendar
#define LOCALIZED_CALENDAR_POCA_DATE   NSLocalizedStringWithDefaultValue(@"POCA Date",Login,[NSBundle mainBundle],@"POCA Date",nil)
#define LOCALIZED_CALENDAR_DUE_DATE   NSLocalizedStringWithDefaultValue(@"Due Date",Login,[NSBundle mainBundle],@"Due Date",nil)

//PopOver
#define LOCALIZED_POPOVER_CANCEL   NSLocalizedStringWithDefaultValue(@"Cancel",Login,[NSBundle mainBundle],@"Cancel",nil)
#define LOCALIZED_POPOVER_OK  NSLocalizedStringWithDefaultValue(@"Ok",Login,[NSBundle mainBundle],@"Ok",nil)
#define LOCALIZED_POPOVER_SAVE  NSLocalizedStringWithDefaultValue(@"Save",Login,[NSBundle mainBundle],@"Save",nil)
#define LOCALIZED_POPOVER_DELETE  NSLocalizedStringWithDefaultValue(@"Delete",Login,[NSBundle mainBundle],@"Delete",nil)

//Dealer ADD Staff
#define LOCALIZED_DEALER_ADD_STAFF_NAME   NSLocalizedStringWithDefaultValue(@"Name",Login,[NSBundle mainBundle],@"Name",nil)
#define LOCALIZED_DEALER_ADD_STAFF_POSITION  NSLocalizedStringWithDefaultValue(@"Position",Login,[NSBundle mainBundle],@"Position",nil)
#define LOCALIZED_DEALER_ADD_STAFF_PHONE  NSLocalizedStringWithDefaultValue(@"Phone",Login,[NSBundle mainBundle],@"Phone",nil)
#define LOCALIZED_DEALER_ADD_STAFF_EMAIL  NSLocalizedStringWithDefaultValue(@"Email",Login,[NSBundle mainBundle],@"Email",nil)

//ERROR
#define LOCALIZED_ERROR_ADD_DEALER_NAME    NSLocalizedStringWithDefaultValue(@"Invalid name",Login,[NSBundle mainBundle],@"Invalid name",nil)
#define LOCALIZED_ERROR_ADD_DEALER_EMAIL   NSLocalizedStringWithDefaultValue(@"Invalid email address",Login,[NSBundle mainBundle],@"Invalid email address",nil)
#define LOCALIZED_ERROR_ADD_DEALER_PHONE   NSLocalizedStringWithDefaultValue(@"7~14 letters",Login,[NSBundle mainBundle],@"7~14 letters",nil)
#define LOCALIZED_ERROR_INVALID   NSLocalizedStringWithDefaultValue(@"Invalid",Login,[NSBundle mainBundle],@"Invalid",nil)

//DealerProfile Add dealer
#define LOCALIZED_ADD_DEALER_TYPE    NSLocalizedStringWithDefaultValue(@"Dealer Type",Login,[NSBundle mainBundle],@"Dealer Type",nil)
#define LOCALIZED_ADD_DEALER_GROUP    NSLocalizedStringWithDefaultValue(@"Dealer Group",Login,[NSBundle mainBundle],@"Dealer Group",nil)
#define LOCALIZED_ADD_DEALER_CODE    NSLocalizedStringWithDefaultValue(@"Dealer Code",Login,[NSBundle mainBundle],@"Dealer Code",nil)
#define LOCALIZED_ADD_DEALER_NAME    NSLocalizedStringWithDefaultValue(@"Dealer Name",Login,[NSBundle mainBundle],@"Dealer Name",nil)
#define LOCALIZED_ADD_DEALER_SIZE    NSLocalizedStringWithDefaultValue(@"Size",Login,[NSBundle mainBundle],@"Size",nil)
#define LOCALIZED_ADD_DEALER_MONTHLY_RETAILING    NSLocalizedStringWithDefaultValue(@"Monthly Retailing",Login,[NSBundle mainBundle],@"Monthly Retailing",nil)
#define LOCALIZED_ADD_DEALER_LOCAL_CARPARK    NSLocalizedStringWithDefaultValue(@"Local Carpark",Login,[NSBundle mainBundle],@"Local Carpark",nil)
#define LOCALIZED_ADD_DEALER_LOYALTY_CUSTOMER    NSLocalizedStringWithDefaultValue(@"Loyalty Customer",Login,[NSBundle mainBundle],@"Loyalty Customer",nil)
#define LOCALIZED_ADD_DEALER_MONTHLY_THROUGHPUT    NSLocalizedStringWithDefaultValue(@"Monthly Throughput",Login,[NSBundle mainBundle],@"Monthly Throughput",nil)
#define LOCALIZED_ADD_DEALER_WORKING_BAY    NSLocalizedStringWithDefaultValue(@"Working Bay",Login,[NSBundle mainBundle],@"Working Bay",nil)
#define LOCALIZED_ADD_DEALER_TECHNICIAN_INFO    NSLocalizedStringWithDefaultValue(@"Technician Information",Login,[NSBundle mainBundle],@"Technician Information",nil)
#define LOCALIZED_ADD_DEALER_TECHNICAL_TURNOVER_RATE    NSLocalizedStringWithDefaultValue(@"Technical Turnover Rate",Login,[NSBundle mainBundle],@"Technical Turnover Rate",nil)
#define LOCALIZED_ADD_DEALER_SERVICE_REVENUE    NSLocalizedStringWithDefaultValue(@"Service Revenue",Login,[NSBundle mainBundle],@"Service Revenue",nil)
#define LOCALIZED_ADD_DEALER_REGION    NSLocalizedStringWithDefaultValue(@"Region",Login,[NSBundle mainBundle],@"Region",nil)
#define LOCALIZED_ADD_DEALER_PROVINCE    NSLocalizedStringWithDefaultValue(@"Province",Login,[NSBundle mainBundle],@"Province",nil)
#define LOCALIZED_ADD_DEALER_CITY    NSLocalizedStringWithDefaultValue(@"City",Login,[NSBundle mainBundle],@"City",nil)
#define LOCALIZED_ADD_DEALER_ZIP_CODE    NSLocalizedStringWithDefaultValue(@"Zip Code",Login,[NSBundle mainBundle],@"Zip Code",nil)
#define LOCALIZED_ADD_DEALER_ADDRESS    NSLocalizedStringWithDefaultValue(@"Address",Login,[NSBundle mainBundle],@"Address",nil)




