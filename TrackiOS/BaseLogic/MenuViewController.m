//
//  MenuViewController.m
//  TrackiOS
//
//  Created by Anish Kumar on 07/10/14.
//  Copyright (c) 2014 Xenon Automotive. All rights reserved.
//

#import "MenuViewController.h"
#import "MenuTableViewCell.h"
#import "ListViewController.h"
#import "TRSettingsViewController.h"

@interface MenuViewController ()
- (IBAction)settingsTapped:(id)sender;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *imageArray;
@property (nonatomic, strong) NSArray *originalImageArray;
@property (nonatomic, strong) NSArray *selectedImages;
@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (nonatomic, strong) UIPopoverController *popVC;
@property (nonatomic, strong) TRSettingsViewController *settingsVC;
@property (nonatomic, strong) NSIndexPath* selectedSettingsIndex;

@end

@implementation MenuViewController

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
//{
//    if ([[segue identifier] isEqualToString:@"popoverSegue"])
//    {
//        _popVC = [(UIStoryboardPopoverSegue*)segue popoverController];
//    }
//}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _selectedSettingsIndex = [NSIndexPath indexPathForRow:0 inSection:0];
    self.navigationController.navigationBarHidden = YES;
    
    _settingsVC = [[UIStoryboard menuStoryBoard]instantiateViewControllerWithIdentifier:@"TRSettingsViewController"];
    _settingsVC.delegate = self;
    
    _popVC = [[UIPopoverController alloc] initWithContentViewController:_settingsVC];
    _popVC.popoverContentSize = CGSizeMake(283, 230.0);

   // [self setNeedsStatusBarAppearanceUpdate];
    
    _originalImageArray = [NSArray arrayWithObjects:@"home_timeline",@"home_dealer", @"home_actionplan", @"home_library", nil];
    //_originalImageArray = [NSArray arrayWithObjects:@"blue",@"blue", @"blue", @"home_library", nil];
    _imageArray = [NSMutableArray arrayWithObjects:@"home_timeline_focused",@"home_dealer", @"home_actionplan", @"home_library", nil];
    //_imageArray = [NSMutableArray arrayWithObjects:@"blue",@"blue", @"blue", @"home_library", nil];
    _selectedImages = [NSArray arrayWithObjects:@"home_timeline_focused",@"home_dealer_focused", @"home_actionplan_focused", @"home_library_focused", nil];
    //_selectedImages = [NSArray arrayWithObjects:@"purple",@"purple", @"purple", @"home_library_focused", nil];


    self.backgroundView.backgroundColor = [UIColor colorWithRed:69.0/255.0 green:73.0/255.0 blue:77.0/255.0 alpha:1.0];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [_tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition: UITableViewScrollPositionNone];
}

//- (UIStatusBarStyle)preferredStatusBarStyle
//{
//    return UIStatusBarStyleLightContent;
//}
#pragma mark - LPThreeViewControllerDelegate

- (void)CustomSplitViewController:(CustomSplitViewController *)customeSplitViewController willShowListViewController:(UIViewController *)listViewController
{
    NSLog(@"willShowListViewController");
}

- (void)CustomSplitViewController:(CustomSplitViewController *)customeSplitViewController didShowListViewController:(UIViewController *)listViewController
{
    NSLog(@"didShowListViewController");
}

- (void)CustomSplitViewController:(CustomSplitViewController *)customeSplitViewController willHideListViewController:(UIViewController *)listViewController
{
    NSLog(@"willHideListViewController");
}

- (void)CustomSplitViewController:(CustomSplitViewController *)customeSplitViewController didHideListViewController:(UIViewController *)listViewController
{
    NSLog(@"didHideListViewController");
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MenuTableViewCell *cell = (MenuTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"MenuTableViewCell" ];
    cell.backgroundColor = [UIColor clearColor];
    NSString *imageName = [_imageArray objectAtIndex:indexPath.row];
    cell.menuImageView.image = [UIImage imageNamed:imageName];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MenuTableViewCell *cell = (MenuTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
    
    cell.backgroundColor = [UIColor clearColor];
    [_imageArray replaceObjectAtIndex:indexPath.row withObject:[_selectedImages objectAtIndex:indexPath.row]];
    NSString *imageName = [_imageArray objectAtIndex:indexPath.row];
    cell.menuImageView.image = [UIImage imageNamed:imageName];
    
    CustomSplitViewController *customeSplitViewController = (CustomSplitViewController*)self.parentViewController.parentViewController;
    [customeSplitViewController showListViewController:self withAnimation:YES withIndex:indexPath.row];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MenuTableViewCell *cell = (MenuTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
    cell.backgroundColor = [UIColor clearColor];

    [_imageArray replaceObjectAtIndex:indexPath.row withObject:[_originalImageArray objectAtIndex:indexPath.row]];
    NSString *imageName = [_imageArray objectAtIndex:indexPath.row];
    cell.menuImageView.image = [UIImage imageNamed:imageName];
}

- (IBAction)settingsTapped:(id)sender
{
    [_popVC presentPopoverFromRect:[(UIButton *)sender frame]  inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (void) hidePop
{
    [_popVC dismissPopoverAnimated:YES];
}

@end
