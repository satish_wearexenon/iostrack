//
//  ListViewController.h
//  TrackiOS
//
//  Created by Anish Kumar on 07/10/14.
//  Copyright (c) 2014 Xenon Automotive. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TrackConstants.h"

@interface ListViewController : UIViewController

-(void)displaySection:(TrackSection)section withSectionData:(NSArray *)sectionArray;

@end
