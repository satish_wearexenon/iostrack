//
//   MainViewController.h
//  TrackiOS
//
//  Created by Anish Kumar on 07/10/14.
//  Copyright (c) 2014 Xenon Automotive. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TrackConstants.h"

@interface  MainViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *topNavBar;
@property (weak, nonatomic) IBOutlet UILabel *navLabel;
@property (weak, nonatomic) IBOutlet UIImageView *cloudImageView;
@property (weak, nonatomic) IBOutlet UIView *progressView;
@property (weak, nonatomic) IBOutlet UIView *progressViewBackground;
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UILabel *backLabel;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

@property (nonatomic, strong) NSString *leftNavigationString;;

@property (weak, nonatomic) IBOutlet UIButton *checkListViewGreenButton;
@property (weak, nonatomic) IBOutlet UIButton *checkListViewRedButton;
@property (weak, nonatomic) IBOutlet UIButton *checkListViewYellowButton;
@property (weak, nonatomic) IBOutlet UIButton *checkListViewGrayButton;
@property (weak, nonatomic) IBOutlet UIButton *checkListUploadPdfButton;

@property (strong, nonatomic) UIViewController *currentDetailController;

//-(void)displaySection:(TrackSection)section withAnimation:(BOOL)animation;
- (void)displaySection:(TrackSubSection)section withAnimation:(BOOL)animation;
- (void)addNavStr:(NSString *)str;
- (void)addNavStr:(NSString *)sectionString regionStr:(NSString*)regionStr;
- (void) proceedBack;
@end
