//
//   MainViewController.m
//  TrackiOS
//
//  Created by Anish Kumar on 07/10/14.
//  Copyright (c) 2014 Xenon Automotive. All rights reserved.
//

#import "MainViewController.h"
#import "ContainerViewController.h"
#import "DealerListViewController.h"
#import "DealerChecklistViewController.h"

@interface  MainViewController ()
@property (nonatomic, strong) UIViewController *currentController;
@property (nonatomic, weak) ContainerViewController *containerViewController;
- (IBAction)backPressed:(id)sender;
- (IBAction)checkListViewButton:(id)sender;
@end

@implementation  MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_backLabel setFont:[UIFont fontWithName:Font_Track_Regular size:23]];
    [_navLabel setFont:[UIFont fontWithName:Font_Track_Regular size:23]];
    [_backButton.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:23]];
    
    _navLabel.textColor = TitleGrayColor;
    [_backButton setTitleColor:TitleGrayColor forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    //NSLog(@"%s", __PRETTY_FUNCTION__);
    
    if ([segue.identifier isEqualToString:@"embedContainer"]) {
        self.containerViewController = segue.destinationViewController;
    }
}

-(void)displaySection:(TrackSubSection)section withAnimation:(BOOL)animation
{
//    if(self.navigationController.presentedViewController != nil)
//        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
//    
//    switch (section) {
//        case eHome:
//        {
//            [self displayHomeViewController:animation];
//            break;
//        }
//        case eReport:
//        {
//            [self displayReportViewController:animation];
//            break;
//        }
//        case eCheckList:
//        {
//            [self displayCheckListViewController:animation];
//            break;
//        }
//        case eAbout:
//        {
//           [self displayAboutViewController:animation];
//            break;
//        }
//        default:
//            break;
//    }
    
    self.checkListViewGreenButton.hidden    = YES;
    self.checkListViewRedButton.hidden      = YES;
    self.checkListViewYellowButton.hidden   = YES;
    self.checkListViewGrayButton.hidden     = YES;
    self.checkListUploadPdfButton.hidden    = YES;
    
    [self.containerViewController swapViewControllers:section];
}

- (void) addNavStr:(NSString *)sectionString regionStr:(NSString*)regionStr
{
    if ([self.containerViewController.currentViewController isKindOfClass: [DealerListViewController class]])
    {
        DealerListViewController *vc = (DealerListViewController*)self.containerViewController.currentViewController;
        [vc populateNavHeader:sectionString regionStr:regionStr];
        
        _leftNavigationString = [NSString stringWithFormat:@"%@-%@-%@", @"Dealer", sectionString, regionStr];
        //vc.backView.hidden = YES;
    }
    // TODO add more classes here
}

- (void) addNavStr:(NSString *)str
{
    _leftNavigationString = str;
    _backView.hidden = YES;
    _backButton.hidden = YES;
    //_navLabel.text = str;
}

- (IBAction)backPressed:(id)sender
{
    NSLog(@"removing %@",self.currentDetailController);
    
    //[self.currentDetailController.parentViewController viewDidAppear:YES];
    //[self.currentDetailController willMoveToParentViewController:nil];
//    
//    NSDictionary* dict = [NSDictionary dictionaryWithObject:
//                          [NSNumber numberWithInt:index]
//                                                     forKey:@"index"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"backPressed"
                                                        object:self
                                                      userInfo:nil];
    
//    [self.currentDetailController.view removeFromSuperview];
//    [self.currentDetailController removeFromParentViewController];
}

- (void) proceedBack
{
    [self.currentDetailController.view removeFromSuperview];
    [self.currentDetailController removeFromParentViewController];
}

- (IBAction)checkListViewButton:(id)sender
{
   
}
@end
