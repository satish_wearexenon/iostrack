//
//  CustomSplitViewController.m
//
//  Created by Luka Penger on 4/21/13.
//  Copyright (c) 2013 LukaPenger. All rights reserved.
//

/*
The MIT License (MIT)
 
Copyright (c) 2013 Luka Penger
 
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
 
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#import "CustomSplitViewController.h"

@interface CustomSplitViewController ()

@property (nonatomic) int selectedIndex;
@end

@implementation CustomSplitViewController

@synthesize delegate = _delegate;

@synthesize menuView = _menuView;
@synthesize listView = _listView;
@synthesize detailView = _detailView;

@synthesize menuViewController = _menuViewController;
@synthesize listViewController = _listViewController;
@synthesize detailViewController = _detailViewController;

@synthesize menuWidthLandscape;
@synthesize menuWidthPortrait;

//MenuViewShadow Settings
@synthesize menuViewShadowColor;
@synthesize menuViewShadowOpacity;
@synthesize menuViewShadowRadius;

//ListViewShadow Settings
@synthesize listViewShadowColor;
@synthesize listViewShadowOpacity;
@synthesize listViewShadowRadius;

@synthesize listViewAnimationDuration;
@synthesize listPortraitClosing;
@synthesize showListViewControllerWhenRotate;
@synthesize gestureListViewOpacity;
@synthesize gestureListViewColor;
@synthesize gestureListViewPaddingTop;

- (id)initWithMenuViewController:(UIViewController*)menuViewController ListViewController:(UIViewController*)listViewController DetailViewController:(UIViewController*)detailViewController;
{
    self = [super init];
    if (self) {
        // Custom initialization
        self.menuViewController=menuViewController;
        self.listViewController=listViewController;
        self.detailViewController=detailViewController;
        
        self.menuWidthLandscape=70.0f;
        self.menuWidthPortrait=70.0f;
        
        self.listWidthLandscape=165.0f;
        self.listWidthPortrait=165.0f;
        
        self.listPortraitClosing=YES;
        self.showListViewControllerWhenRotate=YES;
        
        self.listViewAnimationDuration=0.25;
        self.gestureListViewOpacity=0.3;
        self.gestureListViewColor=[UIColor clearColor];
        self.gestureListViewPaddingTop=44.0f;
    }
    return self;
}

- (void)gestureListViewHandle:(id)sender
{
    [self hideListViewController:self withAnimation:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.menuView = [[UIView alloc] init];
    self.listView = [[UIView alloc] init];
    self.detailView = [[UIView alloc] init];
    
    gestureListView = [[UIView alloc] init];
    
    swipeListView = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(gestureListViewHandle:)];
    [swipeListView setNumberOfTouchesRequired:1];
    [swipeListView setDirection:UISwipeGestureRecognizerDirectionLeft];
    [gestureListView addGestureRecognizer:swipeListView];
    
    tapListView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureListViewHandle:)];
    tapListView.numberOfTapsRequired=1;
    tapListView.numberOfTouchesRequired=1;
    [gestureListView addGestureRecognizer:tapListView];
    
    self.menuView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    self.listView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    self.detailView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    
    [self.view addSubview:self.detailView]; //Layer 1
    [self.view addSubview:gestureListView];
    [self.view addSubview:self.listView]; //Layer 2
    [self.view addSubview:self.menuView]; //Layer 3
    
    [self addChildViewController:self.detailViewController];
    [self addChildViewController:self.listViewController];
    [self addChildViewController:self.menuViewController];
    
    [self.detailView addSubview:self.detailViewController.view];
    [self.listView addSubview:self.listViewController.view];
    [self.menuView addSubview:self.menuViewController.view];
    
    [self hideListViewController:self withAnimation:NO];
    
    //Swipe Gesture
    UISwipeGestureRecognizer * swiperight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeRight)];
    swiperight.direction=UISwipeGestureRecognizerDirectionRight;
    [self.detailView addGestureRecognizer:swiperight];
    
    UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeLeft)];
    swipeleft.direction=UISwipeGestureRecognizerDirectionLeft;
    [self.detailView addGestureRecognizer:swipeleft];
}

- (void)viewDidLayoutSubviews
{
    if(UIInterfaceOrientationIsLandscape(self.interfaceOrientation))    //Landscape
    {
        self.menuView.frame = CGRectMake(0.0f, 0.0f, self.menuWidthLandscape, self.view.bounds.size.height);
        
        [self hideListViewController:self withAnimation:NO];
    } else
    {    //Portrait
        self.menuView.frame = CGRectMake(0.0f, 0.0f, self.menuWidthPortrait, self.view.bounds.size.height);
    }
    
    gestureListView.frame = CGRectMake(0.0f, self.gestureListViewPaddingTop, self.view.bounds.size.width, self.view.bounds.size.height);
    
    self.menuViewController.view.frame=CGRectMake(0.0f, 0.0f, self.menuView.frame.size.width, self.menuView.frame.size.height);
    self.listViewController.view.frame=CGRectMake(0.0f, 0.0f, self.listView.frame.size.width, self.listView.frame.size.height);
    self.detailViewController.view.frame=CGRectMake(70.0f, 0.0f, self.detailView.frame.size.width, self.detailView.frame.size.height);
    
    [super viewDidLayoutSubviews];
}

- (void)showListViewController:(id)sender withAnimation:(BOOL)animation withIndex:(int)index
{
    _selectedIndex = index;
    if ([self.delegate respondsToSelector:@selector(CustomSplitViewController:willShowListViewController:)])
    {
        [self.delegate CustomSplitViewController:self willShowListViewController:self.listViewController];
    }
    
    [self.listViewController viewWillAppear:YES];
    [self.listViewController viewDidAppear:YES];
    
    // Find ListViewController from NavigationController stack
    ListViewController *listViewController = (ListViewController*)[self.listViewController.childViewControllers objectAtIndex:0];
    
    
    [listViewController displaySection:200+index withSectionData:nil];
    
    
    if(animation)
    {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:self.listViewAnimationDuration];
        [UIView setAnimationBeginsFromCurrentState:FALSE];
    }
    
    self.listView.frame=CGRectMake(self.menuView.frame.size.width, 0.0f, self.listWidthPortrait, self.view.bounds.size.height);
    //For Sliding DetailView
    self.detailView.frame = CGRectMake(self.listWidthLandscape, 0.0f, (self.view.bounds.size.width-(self.listWidthLandscape)), self.view.bounds.size.height);
    
    //For DetailView not to be slided
    //self.detailView.frame = CGRectMake(0.0f, 0.0f, (self.view.bounds.size.width-(0)), self.view.bounds.size.height);
    
    gestureListView.backgroundColor=self.gestureListViewColor;
    gestureListView.alpha=self.gestureListViewOpacity;
    
    if(animation)
    {
        [UIView commitAnimations];
    }
    
    if ([self.delegate respondsToSelector:@selector(CustomSplitViewController:didShowListViewController:)])
    {
        [self.delegate CustomSplitViewController:self didShowListViewController:self.listViewController];
    }
}

- (void)hideListViewController:(id)sender withAnimation:(BOOL)animation
{
    if ([self.delegate respondsToSelector:@selector(CustomSplitViewController:willHideListViewController:)])
    {
        [self.delegate CustomSplitViewController:self willHideListViewController:self.listViewController];
    }
    
    [self.listViewController viewDidAppear:YES];
    
    if(animation)
    {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:self.listViewAnimationDuration];
        [UIView setAnimationBeginsFromCurrentState:FALSE];
    }
    
    self.listView.frame=CGRectMake((self.menuView.frame.size.width-(self.menuViewShadowRadius*2))-self.listWidthPortrait, 0.0f, self.listWidthPortrait, self.view.bounds.size.height);
    //self.detailView.frame = CGRectMake(0, 0.0f, (self.view.bounds.size.width-self.menuView.frame.size.width), self.view.bounds.size.height);
    self.detailView.frame = CGRectMake(0, 0.0f, 768.0, self.view.bounds.size.height);
    
    NSLog(@"df = %@", NSStringFromCGRect(self.detailView.frame));


    gestureListView.backgroundColor=[UIColor clearColor];
    gestureListView.alpha=0.0f;
    
    if(animation)
    {
        [UIView commitAnimations];
    }
    
    if ([self.delegate respondsToSelector:@selector(CustomSplitViewController:didHideListViewController:)])
    {
        [self.delegate CustomSplitViewController:self didHideListViewController:self.listViewController];
    }
}

- (void) swipeRight
{
    [self showListViewController:self withAnimation:YES withIndex:_selectedIndex];
}

- (void) swipeLeft
{
    [self hideListViewController:self withAnimation:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
}

@end
