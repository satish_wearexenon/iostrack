//
//  ListViewController.m
//  TrackiOS
//
//  Created by Anish Kumar on 07/10/14.
//  Copyright (c) 2014 Xenon Automotive. All rights reserved.
///

#import "ListViewController.h"
#import "ListTableViewCell.h"
#import "DealerSubMenuRegion.h"
#import "DealerSubMenuReport.h"

#import "ReportSubMenuRegion.h"
#import "ReportSubMenuReport.h"

#import "TimeLineSubMenu.h"




#define kSectionHeaderHeight 55

@interface ListViewController ()

@property (nonatomic, weak)   IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *backGroundView;

@property (nonatomic, strong) NSMutableArray *dealerArray;
@property (nonatomic, strong) NSMutableArray *dealerArrayLocation;

@property (nonatomic, strong) NSMutableArray *libraryArray;
@property (nonatomic, strong) NSMutableArray *libraryArrayLocation;

@property (nonatomic, strong) NSMutableArray *timelineArray;
@property (nonatomic, strong) NSMutableArray *timelineArrayLocation;

@property (nonatomic, strong) NSMutableArray *actionPlanArray;
@property (nonatomic, strong) NSMutableArray *actionPlanArrayLocation;

@property (nonatomic, retain) NSMutableArray *sectionKeys;
@property (nonatomic, retain) NSMutableDictionary *sectionContents;

@property (nonatomic, strong) NSMutableSet *selectedSection0;;
@property (nonatomic, strong) NSMutableSet *selectedSection1;

@property (nonatomic) TrackSection currentSection;

@property (nonatomic, strong) NSMutableArray *indexTopSelectedArray;
@property (nonatomic, strong) NSMutableArray *indexBottomSelectedArray;

@property (nonatomic, strong) NSDictionary *libraryDictionary;
@property (nonatomic, strong) NSDictionary *actionPlaDictionary;
@property (nonatomic, strong) NSDictionary *dealerDictionary;
@property (nonatomic, strong) NSDictionary *timelineDictionary;

@property (nonatomic, strong) NSString *topSectionSelectedStr;
@property (nonatomic, strong) NSString *bottomSectionSelectedStr;

@property (nonatomic) long currentSelectedIndex;
@property (nonatomic, retain) NSIndexPath* checkedIndexPath0;
@property (nonatomic, retain) NSIndexPath* checkedIndexPath1;
@end

@implementation ListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden = YES;
    _currentSelectedIndex = 0;
    
    self.checkedIndexPath0 = [NSIndexPath indexPathForRow:0 inSection:0];
    self.checkedIndexPath1 = [NSIndexPath indexPathForRow:0 inSection:1];
    
  //    _libraryArray = [NSMutableArray arrayWithObjects:@"Pre-Launch", @"Implementation", @"Operation", nil];
  //    _libraryArrayLocation = [NSMutableArray arrayWithObjects:@"All", @"East", @"West", @"North", @"South", nil];
    //----------------------------------
    _libraryArray = [[NSMutableArray alloc] init];
    _libraryArrayLocation = [[NSMutableArray alloc] init];
    
    for (ReportSubMenuReport *stage in (NSArray*)[ReportSubMenuReport findAll]) {
        
        [_libraryArray addObject:stage.reportMenu_stage_name];
        
    }
    for (ReportSubMenuRegion *region in (NSArray*)[ReportSubMenuRegion findAll]) {
        
        [_libraryArrayLocation addObject:region.reportMenu_region_name];
        
    }
    //----------------------------------
    
    _libraryDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:
                          [NSNumber numberWithInt: eSubSectionLibarayPreLaunch], [NSNumber numberWithInt:0],
                          [NSNumber numberWithInt: eSubSectionLibarayImplementation],[NSNumber numberWithInt:1],
                          [NSNumber numberWithInt: eSubSectionLibarayOperation], [NSNumber numberWithInt:2],
                          nil];
    
  //  _actionPlanArray = [NSMutableArray arrayWithObjects:@"Plan1", @"Plan2", @"Plan3", nil];
  //  _actionPlanArrayLocation = [NSMutableArray arrayWithObjects:@"East", @"West", @"North", @"South", nil];
    
    //----------------------------------
    _actionPlanArray = [[NSMutableArray alloc] init];
    _actionPlanArrayLocation = [[NSMutableArray alloc] init];
    
    for (DealerSubMenuReport *stage in (NSArray*)[DealerSubMenuReport findAll]) {
        
        [_actionPlanArray addObject:stage.dealerSubmenuStage_name];
        
    }
    for (DealerSubMenuRegion *region in (NSArray*)[DealerSubMenuRegion findAll]) {
        
        [_actionPlanArrayLocation addObject:region.dealermenu_region_name];
        
    }

    _actionPlaDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:
                          [NSNumber numberWithInt: eSubSectionActionPlan1], [NSNumber numberWithInt:0],
                          [NSNumber numberWithInt: eSubSectionActionPlan2], [NSNumber numberWithInt:1],
                          [NSNumber numberWithInt: eSubSectionActionPlan3], [NSNumber numberWithInt:2],
                          nil];
    
    
    //----------------------------------
    _dealerArray = [[NSMutableArray alloc] init];
    _dealerArrayLocation = [[NSMutableArray alloc] init];
    
    for (DealerSubMenuReport *stage in (NSArray*)[DealerSubMenuReport findAll]) {
        
        [_dealerArray addObject:stage.dealerSubmenuStage_name];
        
    }
    for (DealerSubMenuRegion *region in (NSArray*)[DealerSubMenuRegion findAll]) {
        
        [_dealerArrayLocation addObject:region.dealermenu_region_name];
 
    }
    
  //----------------------------------
    
    //_dealerArray = [NSArray arrayWithObjects:@"Pre-Launch", @"Implementation", @"Operation", nil];
   // _dealerArrayLocation = [NSArray arrayWithObjects:@"East", @"West", @"North", @"South", nil];
    _dealerDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:
                            [NSNumber numberWithInt: eSubSectionDealerList], [NSNumber numberWithInt:0],
                            nil];
    
//    _timelineArray = [NSMutableArray arrayWithObjects:@"Timeline1", @"Timeline2", @"Timeline3", nil];
    //_timelineArrayLocation = [NSMutableArray arrayWithObjects:@"", @"", @"", nil];
    
    //----------------------------------
    _timelineArray = [[NSMutableArray alloc] init];
    
    for (TimeLineSubMenu *stage in (NSArray*)[TimeLineSubMenu findAll]) {
        
        [_timelineArray addObject:stage.timelinemenu_name];
        
    }

    //----------------------------------
    _timelineDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:
                            [NSNumber numberWithInt: eSubSectionTimeline], [NSNumber numberWithInt:0],
                            [NSNumber numberWithInt: eSubSectionTimeline2], [NSNumber numberWithInt:1],
                            [NSNumber numberWithInt: eSubSectionTimeline3], [NSNumber numberWithInt:2],
                            nil];


    _selectedSection0 = [[NSMutableSet alloc] init];
    _selectedSection1 = [[NSMutableSet alloc] init];
    
    _indexTopSelectedArray = [[NSMutableArray alloc] initWithObjects:[NSNumber numberWithInteger:0], [NSNumber numberWithInteger:0], [NSNumber numberWithInteger:0], [NSNumber numberWithInteger:0],  nil];
    _indexBottomSelectedArray = [[NSMutableArray alloc] initWithObjects:[NSNumber numberWithInteger:0], [NSNumber numberWithInteger:0], [NSNumber numberWithInteger:0], [NSNumber numberWithInteger:0],  nil];

    self.backGroundView.backgroundColor = [UIColor colorWithRed:61.0/255.0 green:65.0/255.0 blue:68.0/255.0 alpha:1.0];
}

- (void) viewWillAppear:(BOOL)animated
{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)displaySection:(TrackSection)section withSectionData:(NSArray *)sectionArray
{
    // TODO logic will change when there is API calls available
    
    if (section == _currentSection) {
        return;
    }
    _currentSection = section;
    [_selectedSection0 removeAllObjects];
    [_selectedSection1 removeAllObjects];
    
    switch (section)
    {
        case eSectionTimeline:
        {
            NSMutableArray *keys = [[NSMutableArray alloc] init];
            NSMutableDictionary *contents = [[NSMutableDictionary alloc] init];
            
            NSString *topHeader = @"Timeline";
           // NSString *bottomHeader = @"Region";
            
            [contents setObject:_timelineArray forKey:topHeader];
            //[contents setObject:_timelineArrayLocation forKey:bottomHeader];
            
            [keys addObject:topHeader];
            //[keys addObject:bottomHeader];
            
            [self setSectionKeys:keys];
            [self setSectionContents:contents];
            
            int index = [[_indexTopSelectedArray objectAtIndex:0]intValue];
            [_selectedSection0 addObject:[_timelineArray objectAtIndex:index]];
            
            TrackSubSection subSection = (TrackSubSection)[[_timelineDictionary objectForKey:[NSNumber numberWithInt: index]]intValue];
            
//            index = [[_indexBottomSelectedArray objectAtIndex:0]intValue];
//            [_selectedSection1 addObject:[_timelineArrayLocation objectAtIndex:index]];

            [[APP_DELEGATE appBaseViewController] displaySection:subSection withAnimation:NO];
        }
            break;
            
        case eSectionDealer:
        {
            NSMutableArray *keys = [[NSMutableArray alloc] init];
            NSMutableDictionary *contents = [[NSMutableDictionary alloc] init];
            
            NSString *topHeader = @"Report";
            NSString *bottomHeader = @"Region";
            
            [contents setObject:_dealerArray forKey:topHeader];
            [contents setObject:_dealerArrayLocation forKey:bottomHeader];
            
            [keys addObject:topHeader];
            [keys addObject:bottomHeader];
            
            [self setSectionKeys:keys];
            [self setSectionContents:contents];
            
            int index = [[_indexTopSelectedArray objectAtIndex:1]intValue];
            [_selectedSection0 addObject:[_dealerArray objectAtIndex:index]];
            _topSectionSelectedStr = [_dealerArray objectAtIndex:index];
            
           // TrackSubSection subSection = (TrackSubSection)[[_dealerDictionary objectForKey:[NSNumber numberWithInt: index]]intValue];
            
            index = [[_indexBottomSelectedArray objectAtIndex:1]intValue];
            [_selectedSection1 addObject:[_dealerArrayLocation objectAtIndex:index]];
            
            
            _bottomSectionSelectedStr = [_dealerArrayLocation objectAtIndex:index];
            NSString *navStr = [NSString stringWithFormat:@"Dealer-%@-%@", _topSectionSelectedStr, _bottomSectionSelectedStr];
            [[APP_DELEGATE appBaseViewController] addNavStr:[navStr uppercaseString]];
            [[APP_DELEGATE appBaseViewController] addNavStr:_topSectionSelectedStr regionStr:_bottomSectionSelectedStr];
            [[APP_DELEGATE appBaseViewController] displaySection:eSubSectionDealerList withAnimation:NO];
        }
            break;
            
        case eSectionAction:
        {
            NSMutableArray *keys = [[NSMutableArray alloc] init];
            NSMutableDictionary *contents = [[NSMutableDictionary alloc] init];
            
            NSString *topHeader = @"Action";
            NSString *bottomHeader = @"Region";
            
            [contents setObject:_actionPlanArray forKey:topHeader];
            [contents setObject:_actionPlanArrayLocation forKey:bottomHeader];
            
            [keys addObject:topHeader];
            [keys addObject:bottomHeader];
            
            [self setSectionKeys:keys];
            [self setSectionContents:contents];
            
            int index = [[_indexTopSelectedArray objectAtIndex:2]intValue];
            [_selectedSection0 addObject:[_actionPlanArray objectAtIndex:index]];
            
            TrackSubSection subSection = (TrackSubSection)[[_actionPlaDictionary objectForKey:[NSNumber numberWithInt: index]]intValue];
            
            index = [[_indexBottomSelectedArray objectAtIndex:2]intValue];
            [_selectedSection1 addObject:[_actionPlanArrayLocation objectAtIndex:index]];
            
            //[[APP_DELEGATE appBaseViewController] displaySection:subSection withAnimation:NO];
            [[APP_DELEGATE appBaseViewController] displaySection:eSubSectionActionPlan1 withAnimation:NO];
        }
            break;
        
        case eSectionLibaray:
        {
            
            NSMutableArray *keys = [[NSMutableArray alloc] init];
            NSMutableDictionary *contents = [[NSMutableDictionary alloc] init];
            
            NSString *topHeader = @"Report";
            NSString *bottomHeader = @"Region";
            
            [contents setObject:_libraryArray forKey:topHeader];
            [contents setObject:_libraryArrayLocation forKey:bottomHeader];
            
            [keys addObject:topHeader];
            [keys addObject:bottomHeader];
            
            [self setSectionKeys:keys];
            [self setSectionContents:contents];
            
            int index = [[_indexTopSelectedArray objectAtIndex:3]intValue];
            [_selectedSection0 addObject:[_libraryArray objectAtIndex:index]];
            
            TrackSubSection subSection = (TrackSubSection)[[_libraryDictionary objectForKey:[NSNumber numberWithInt: index]]intValue];
            
            index = [[_indexBottomSelectedArray objectAtIndex:3]intValue];
            [_selectedSection1 addObject:[_libraryArrayLocation objectAtIndex:index]];

            
            [[APP_DELEGATE appBaseViewController] displaySection:subSection withAnimation:NO];
        }
            break;
            
        default:
            break;
    }
    [_tableView reloadData];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 18)];
    /* Create custom view to display section header... */
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(16, 18, tableView.frame.size.width, 18)];
    [label setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    label.textColor = [UIColor colorWithRed:160.0/255.0 green:160.0/255.0 blue:160.0/255.0 alpha:1.0];
    NSString *string =[[self sectionKeys] objectAtIndex:section];
    [label setText:string];
    [view addSubview:label];
    [view setBackgroundColor:[UIColor clearColor]]; 
    return view;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger sections = [[self sectionKeys] count];
    
    return sections;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 54.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *key = [[self sectionKeys] objectAtIndex:section];
    NSArray *contents = [[self sectionContents] objectForKey:key];
    NSInteger rows = [contents count];
    
    return rows;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *key = [[self sectionKeys] objectAtIndex:section];
    
    return key;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ListTableViewCell *cell = (ListTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"ListTableViewCell"];
    cell.backgroundColor = [UIColor clearColor];
    
    NSString *key = [[self sectionKeys] objectAtIndex:[indexPath section]];
    NSArray *contents = [[self sectionContents] objectForKey:key];
    NSString *contentForThisRow = [contents objectAtIndex:[indexPath row]];
    cell.listLabel.text = contentForThisRow;
    cell.backgroundColor = [UIColor clearColor];
    
    [[UITableViewCell appearance] setTintColor:[UIColor whiteColor]];
    //cell.listLabel.highlightedTextColor = [UIColor redColor];
    
//    if (_currentSelectedIndex != indexPath.row)
//    {
//        return cell;
//    }
    if (indexPath.section == 0)
    {
//        if([_selectedSection0 containsObject:contentForThisRow])
//        {
//            cell.backgroundColor = [UIColor clearColor];
//            cell.selected = YES;
//            [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
//        }
//        else
//        {
//            cell.selected = NO;
//            //[tableView deselectRowAtIndexPath:indexPath animated:NO];
//        }
        if([_selectedSection0 containsObject:contentForThisRow])
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            cell.listLabel.textColor = [UIColor whiteColor];
            [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.listLabel.textColor = [UIColor colorWithRed:183.0/255.0 green:183.0/255.0 blue:183.0/255.0 alpha:1.0];
        }
    }
    else
    {

        if([_selectedSection1 containsObject:contentForThisRow])
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            cell.listLabel.textColor = [UIColor whiteColor];
            [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.listLabel.textColor = [UIColor colorWithRed:183.0/255.0 green:183.0/255.0 blue:183.0/255.0 alpha:1.0];
        }
    }
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *key = [[self sectionKeys] objectAtIndex:[indexPath section]];
    NSArray *contents = [[self sectionContents] objectForKey:key];
    NSString *contentForThisRow = [contents objectAtIndex:[indexPath row]];
    
    // ListTableViewCell *cell = (ListTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"ListTableViewCell"];

    _currentSelectedIndex = indexPath.row;
    
    NSString *navStr = @"";
    
    if (indexPath.section == 0)
    {
        [_selectedSection0 removeAllObjects];
        [_selectedSection0 addObject:contentForThisRow];
        
        //if(self.checkedIndexPath)
        if(_selectedSection0)
        {
            ListTableViewCell* uncheckCell = (ListTableViewCell*)[tableView
                                            cellForRowAtIndexPath:self.checkedIndexPath0];
            uncheckCell.accessoryType = UITableViewCellAccessoryNone;
            uncheckCell.listLabel.textColor = [UIColor colorWithRed:183.0/255.0 green:183.0/255.0 blue:183.0/255.0 alpha:1.0];
        }
        ListTableViewCell* cell = (ListTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        cell.listLabel.textColor = [UIColor whiteColor];
        self.checkedIndexPath0 = indexPath;
        
        //TrackSubSection subSection = 0;
        
        switch (_currentSection)
        {
            case eSectionTimeline:
            {
                [_indexTopSelectedArray replaceObjectAtIndex:0 withObject:[NSNumber numberWithInt:indexPath.row]];
               // subSection = (TrackSubSection)[[_timelineDictionary objectForKey:[NSNumber numberWithInt:indexPath.row]]intValue];
            }
                break;
            case eSectionDealer:
            {
                [_indexTopSelectedArray replaceObjectAtIndex:1 withObject:[NSNumber numberWithInt:indexPath.row]];
                //subSection = (TrackSubSection)[[_dealerDictionary objectForKey:[NSNumber numberWithInt:indexPath.row]]intValue];
                //navStr = [NSString stringWithFormat:@"Dealer-%@", [_dealerArray objectAtIndex:indexPath.row]]; //[_dealerArrayLocation objectAtIndex:0]];
                _topSectionSelectedStr = [_dealerArray objectAtIndex:indexPath.row];
                
                navStr = [NSString stringWithFormat:@"Dealer-%@-%@", _topSectionSelectedStr, _bottomSectionSelectedStr];
                
            }
                break;
            case eSectionAction:
                [_indexTopSelectedArray replaceObjectAtIndex:2 withObject:[NSNumber numberWithInt:indexPath.row]];
                //subSection = (TrackSubSection)[[_actionPlaDictionary objectForKey:[NSNumber numberWithInt:indexPath.row]]intValue];
                break;
            case eSectionLibaray:
                [_indexTopSelectedArray replaceObjectAtIndex:3 withObject:[NSNumber numberWithInt:indexPath.row]];
               // subSection = (TrackSubSection)[[_libraryDictionary objectForKey:[NSNumber numberWithInt:indexPath.row]]intValue];
                break;
            default:
                break;
        }
        //[[APP_DELEGATE appBaseViewController] displaySection:subSection withAnimation:NO];
    }
    else
    {
        [_selectedSection1 removeAllObjects];
        [_selectedSection1 addObject:contentForThisRow];
        
        if(_selectedSection1)
        {
            ListTableViewCell* uncheckCell = (ListTableViewCell*)[tableView
                                                                  cellForRowAtIndexPath:self.checkedIndexPath1];
            uncheckCell.accessoryType = UITableViewCellAccessoryNone;
            uncheckCell.listLabel.textColor = [UIColor colorWithRed:183.0/255.0 green:183.0/255.0 blue:183.0/255.0 alpha:1.0];
        }
        ListTableViewCell* cell = (ListTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        cell.listLabel.textColor = [UIColor whiteColor];
        self.checkedIndexPath1 = indexPath;
        
         switch (_currentSection)
         {
             case eSectionTimeline:
                 [_indexBottomSelectedArray replaceObjectAtIndex:0 withObject:[NSNumber numberWithInt:indexPath.row]];
                 break;
             case eSectionDealer:
             {
                 [_indexBottomSelectedArray replaceObjectAtIndex:1 withObject:[NSNumber numberWithInt:indexPath.row]];
                 _bottomSectionSelectedStr = [_dealerArrayLocation objectAtIndex:indexPath.row];
                 navStr = [NSString stringWithFormat:@"Dealer-%@-%@", _topSectionSelectedStr, _bottomSectionSelectedStr];
             }
                 break;
             case eSectionLibaray:
                 [_indexBottomSelectedArray replaceObjectAtIndex:2 withObject:[NSNumber numberWithInt:indexPath.row]];
                 break;
             case eSectionAction:
                 [_indexBottomSelectedArray replaceObjectAtIndex:3 withObject:[NSNumber numberWithInt:indexPath.row]];
                 break;
                 
             default:
                 break;
         }
    }
    //[[APP_DELEGATE appBaseViewController] addNavStr:navStr];
    [[APP_DELEGATE appBaseViewController] addNavStr:_topSectionSelectedStr regionStr:_bottomSectionSelectedStr];
    
    [tableView reloadData];
}

//- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    //return;
//    NSString *key = [[self sectionKeys] objectAtIndex:[indexPath section]];
//    NSArray *contents = [[self sectionContents] objectForKey:key];
//    NSString *contentForThisRow = [contents objectAtIndex:[indexPath row]];
//    
//    if (indexPath.section == 0)
//    {
//        [_selectedSection0 removeObject:contentForThisRow];
//    }
//    else
//    {
//        [_selectedSection1 removeObject:contentForThisRow];
//    }
//}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    for (ListTableViewCell *cell in self.tableView.visibleCells)
    {
        CGFloat hiddenFrameHeight = scrollView.contentOffset.y + kSectionHeaderHeight - cell.frame.origin.y;
        if (hiddenFrameHeight >= 0 || hiddenFrameHeight <= cell.frame.size.height) {
            [cell maskCellFromTop:hiddenFrameHeight];
        }
    }
}

@end
