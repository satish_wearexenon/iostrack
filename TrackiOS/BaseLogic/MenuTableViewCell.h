//
//  MenuTableViewCell.h
//  TrackiOS
//
//  Created by Anish Kumar on 07/10/14.
//  Copyright (c) 2014 Xenon Automotive. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *menuImageView;

@end
