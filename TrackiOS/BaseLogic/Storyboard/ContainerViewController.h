//
//  ContainerViewController.h
//  TrackiOS
//
//  Created by Anish Kumar on 07/10/14.
//  Copyright (c) 2014 Xenon Automotive. All rights reserved.
//

@interface ContainerViewController : UIViewController

@property (nonatomic, strong) UIViewController *currentViewController;

-(void)swapViewControllers:(TrackSubSection)section;

@end
