//
//  ContainerViewController.m
//  TrackiOS
//
//  Created by Anish Kumar on 07/10/14.
//  Copyright (c) 2014 Xenon Automotive. All rights reserved.
//

#import "ContainerViewController.h"
#import "ActionPlanViewController.h"
#import "TLibraryViewController.h"
#import "TTimelineViewController.h"

#import "ActionPlan1ViewController.h"
#import "ActionPlan2ViewController.h"
#import "ActionPlan3ViewController.h"

#import "DealerListViewController.h"

#import "TLibraryPreLaunchViewController.h"
#import "TLibraryImplementationViewController.h"
#import "TLibraryOperationViewController.h"

#import "TTimeline1ViewController.h"
#import "TTimeline2ViewController.h"
#import "TTimeline3ViewController.h"

#import "DealerDetailViewController.h"

#define SegueIdActionPlan @"embedActionPlan"
#define SegueIdActionPlan1 @"embedActionPlan1"
#define SegueIdActionPlan2 @"embedActionPlan2"
#define SegueIdActionPlan3 @"embedActionPlan3"

#define SegueIdDealerList @"embedDealerList"

#define SegueIdLibrary @"embedLibrary"
#define SegueIdLibraryPreLaunch @"embedLibraryPreLaunch"
#define SegueIdLibraryImplementation @"embedLibraryImplementation"
#define SegueIdLibraryOperation @"embedLibraryOperation"

#define SegueIdTimeline @"embedTimeline"
#define SegueIdTimeline1 @"embedTimeline1"
#define SegueIdTimeline2 @"embedTimeline2"
#define SegueIdTimeline3 @"embedTimeline3"

#import "DealerChecklistViewController.h"

@interface ContainerViewController ()

@property (strong, nonatomic) NSString *currentSegueIdentifier;
@property (assign, nonatomic) BOOL transitionInProgress;

@property (strong, nonatomic) ActionPlanViewController *actionPlanViewController;
@property (strong, nonatomic) ActionPlan1ViewController *actionPlan1ViewController;
@property (strong, nonatomic) ActionPlan2ViewController *actionPlan2ViewController;
@property (strong, nonatomic) ActionPlan3ViewController *actionPlan3ViewController;

//@property (strong, nonatomic) DealerViewController *dealerViewController;
@property (strong, nonatomic) DealerListViewController *dealerListViewController;
//@property (strong, nonatomic) DealerListViewController *dealer2ViewController;

@property (strong, nonatomic) TLibraryViewController *libraryViewController;
@property (strong, nonatomic) TLibraryPreLaunchViewController *libraryPreLaunchViewController;
@property (strong, nonatomic) TLibraryImplementationViewController *libraryImplementationViewController;
@property (strong, nonatomic) TLibraryOperationViewController *libraryOperationViewController;

@property (strong, nonatomic) TTimelineViewController *timelineViewController;
@property (strong, nonatomic) TTimeline1ViewController *timeline1ViewController;
@property (strong, nonatomic) TTimeline2ViewController *timeline2ViewController;
@property (strong, nonatomic) TTimeline3ViewController *timeline3ViewController;


@end

@implementation ContainerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.transitionInProgress = NO;
    self.currentSegueIdentifier = SegueIdTimeline;
    [self performSegueWithIdentifier:self.currentSegueIdentifier sender:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    //NSLog(@"%s", __PRETTY_FUNCTION__);

    // Instead of creating new VCs on each seque we want to hang on to existing
    // instances if we have it. Remove the second condition of the following
    // two if statements to get new VC instances instead.
    
    //Timeline
    if ([segue.identifier isEqualToString:SegueIdTimeline]) {
        self.timelineViewController = segue.destinationViewController;
         _currentViewController = _timelineViewController;
    }
   // if ([segue.identifier isEqualToString:SegueIdTimeline1])
    //{
        //self.timeline1ViewController = segue.destinationViewController;
        // _currentViewController = _timeline1ViewController;
   // }
    else if ([segue.identifier isEqualToString:SegueIdTimeline2]) {
        self.timeline2ViewController = segue.destinationViewController;
    }
    else if ([segue.identifier isEqualToString:SegueIdTimeline3]) {
        self.timeline3ViewController = segue.destinationViewController;
    }
    
    //Dealer
//    else if ([segue.identifier isEqualToString:SegueIdDealer]) {
//        self.dealerViewController = segue.destinationViewController;
//    }
    else if ([segue.identifier isEqualToString:SegueIdDealerList]) {
        self.dealerListViewController = segue.destinationViewController;
    }
//    else if ([segue.identifier isEqualToString:SegueIdDealer2]) {
//        self.dealer2ViewController = segue.destinationViewController;
//    }
    
    //Action Plan
    else if ([segue.identifier isEqualToString:SegueIdActionPlan]) {
        self.actionPlanViewController = segue.destinationViewController;
    }
    else if ([segue.identifier isEqualToString:SegueIdActionPlan1]) {
        self.actionPlan1ViewController = segue.destinationViewController;
    }
    else if ([segue.identifier isEqualToString:SegueIdActionPlan2]) {
        self.actionPlan2ViewController = segue.destinationViewController;
    }
    else if ([segue.identifier isEqualToString:SegueIdActionPlan3]) {
        self.actionPlan3ViewController = segue.destinationViewController;
    }
    
    //Library/Report
    else if ([segue.identifier isEqualToString:SegueIdLibrary]) {
        self.libraryViewController = segue.destinationViewController;
    }
    else if ([segue.identifier isEqualToString:SegueIdLibraryPreLaunch]) {
        self.libraryPreLaunchViewController = segue.destinationViewController;
    }
    else if ([segue.identifier isEqualToString:SegueIdLibraryImplementation]) {
        self.libraryImplementationViewController = segue.destinationViewController;
    }
    else if ([segue.identifier isEqualToString:SegueIdLibraryOperation]) {
        self.libraryOperationViewController = segue.destinationViewController;
    }

    // If we're going to the first view controller.
    if ([segue.identifier isEqualToString:SegueIdTimeline])
    {
        // If this is not the first time we're loading this.
        if (self.childViewControllers.count > 0)
        {
            [self swapFromViewController:self.timelineViewController];
        }
        else
        {
            // If this is the very first time we're loading this we need to do
            // an initial load and not a swap.
            [self addChildViewController:segue.destinationViewController];
            UIView* destView = ((UIViewController *)segue.destinationViewController).view;
            [self.view addSubview:destView];
            [segue.destinationViewController didMoveToParentViewController:self];
        }
    }
    else if ([segue.identifier isEqualToString:SegueIdTimeline2])
    {
        [self swapFromViewController:self.timeline2ViewController];
    }
    else if ([segue.identifier isEqualToString:SegueIdTimeline3])
    {
        [self swapFromViewController:self.timeline3ViewController];
    }
    
    // By definition the second view controller will always be swapped with the first one.
    // Dealer
//    else if ([segue.identifier isEqualToString:SegueIdDealer])
//    {
//        [self swapFromViewController:self.dealerViewController];
//    }
    else if ([segue.identifier isEqualToString:SegueIdDealerList])
    {
        [self swapFromViewController:self.dealerListViewController];
    }
//    else if ([segue.identifier isEqualToString:SegueIdDealer2])
//    {
//        [self swapFromViewController:self.dealer2ViewController];
//    }
    
    // Action Plan
    else if ([segue.identifier isEqualToString:SegueIdActionPlan])
    {
        [self swapFromViewController:self.actionPlanViewController];
    }
    else if ([segue.identifier isEqualToString:SegueIdActionPlan1])
    {
        [self swapFromViewController:self.actionPlan1ViewController];
    }
    else if ([segue.identifier isEqualToString:SegueIdActionPlan2])
    {
        [self swapFromViewController:self.actionPlan2ViewController];
    }
    else if ([segue.identifier isEqualToString:SegueIdActionPlan3])
    {
        [self swapFromViewController:self.actionPlan3ViewController];
    }
    
    // Library/ Report
    else if ([segue.identifier isEqualToString:SegueIdLibrary])
    {
        [self swapFromViewController:self.libraryViewController];
    }
    else if ([segue.identifier isEqualToString:SegueIdLibraryPreLaunch])
    {
        [self swapFromViewController:self.libraryPreLaunchViewController];
    }
    else if ([segue.identifier isEqualToString:SegueIdLibraryImplementation])
    {
        [self swapFromViewController:self.libraryImplementationViewController];
    }
    else if ([segue.identifier isEqualToString:SegueIdLibraryOperation])
    {
        [self swapFromViewController:self.libraryOperationViewController];
    }
}

- (void)swapFromViewController:(UIViewController *)toViewController
{
//    MainViewController *parentVC = (MainViewController*)self.parentViewController;
//    parentVC.navLabel.text = @"gg";
    
    
   [_currentViewController willMoveToParentViewController:nil];
    [self addChildViewController:toViewController];
    
    [self transitionFromViewController:_currentViewController
                      toViewController:toViewController
                              duration:0.0
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:nil
                            completion:^(BOOL finished) {
                                [_currentViewController removeFromParentViewController];
                                [toViewController didMoveToParentViewController:self];
                                _currentViewController = toViewController;
                                
                                
                                self.transitionInProgress = NO;
                            }];

}

-(void)swapViewControllers:(TrackSubSection)section
{
    
    if (self.transitionInProgress) {
        return;
    }

    self.transitionInProgress = YES;
    
    switch (section)
    {
//        case eSectionTimeline:
//            self.currentSegueIdentifier = SegueIdTimeline;
//            break;
            
        case eSubSectionTimeline:
            self.currentSegueIdentifier = SegueIdTimeline;
            break;
            
        case eSubSectionTimeline2:
            self.currentSegueIdentifier = SegueIdTimeline2;
            break;
            
        case eSubSectionTimeline3:
            self.currentSegueIdentifier = SegueIdTimeline3;
            break;
            
//        case eSectionDealer:
//            self.currentSegueIdentifier = SegueIdDealer;
//            break;
            
        case eSubSectionDealerList:
            self.currentSegueIdentifier = SegueIdDealerList;
            break;
            
//        case eSubSectionDealer2:
//            self.currentSegueIdentifier = SegueIdDealer2;
//            break;
            
//        case eSectionAction:
//            self.currentSegueIdentifier = SegueIdActionPlan;
//            break;
            
        case eSubSectionActionPlan1:
            self.currentSegueIdentifier = SegueIdActionPlan1;
            break;
            
        case eSubSectionActionPlan2:
            self.currentSegueIdentifier = SegueIdActionPlan2;
            break;
            
        case eSubSectionActionPlan3:
            self.currentSegueIdentifier = SegueIdActionPlan3;
            break;

//        case eSectionLibaray:
//            self.currentSegueIdentifier = SegueIdLibrary;
//            break;
            
        case eSubSectionLibarayPreLaunch:
            self.currentSegueIdentifier = SegueIdLibraryPreLaunch;
            break;
            
        case eSubSectionLibarayImplementation:
            self.currentSegueIdentifier = SegueIdLibraryImplementation;
            break;
            
        case eSubSectionLibarayOperation:
            self.currentSegueIdentifier = SegueIdLibraryOperation;
            break;
            
        default:
            break;
    }
    
    //Timeline
    if (([self.currentSegueIdentifier isEqualToString:SegueIdTimeline]) && self.timelineViewController)
    {
        if (_currentViewController == self.timelineViewController)
        {
            self.transitionInProgress = NO;
            return;
        }
        [self swapFromViewController:self.timelineViewController];
        return;
    }
    else if (([self.currentSegueIdentifier isEqualToString:SegueIdTimeline1]) && self.timeline1ViewController)
    {
        if (_currentViewController == self.timeline1ViewController)
        {
            self.transitionInProgress = NO;
            return;
        }
        [self swapFromViewController:self.timeline1ViewController];
        return;
    }
    else if (([self.currentSegueIdentifier isEqualToString:SegueIdTimeline2]) && self.timeline2ViewController)
    {
        if (_currentViewController == self.timeline2ViewController)
        {
            self.transitionInProgress = NO;
            return;
        }
        [self swapFromViewController:self.timeline2ViewController];
        return;
    }
    else if (([self.currentSegueIdentifier isEqualToString:SegueIdTimeline3]) && self.timeline3ViewController)
    {
        if (_currentViewController == self.timeline3ViewController)
        {
            self.transitionInProgress = NO;
            return;
        }
        [self swapFromViewController:self.timeline3ViewController];
        return;
    }
    
    //Dealer
//    else if (([self.currentSegueIdentifier isEqualToString:SegueIdDealer]) && self.dealerViewController)
//    {
//        if (_currentViewController == self.dealerViewController)
//        {
//            self.transitionInProgress = NO;
//            return;
//        }
//        [self swapFromViewController:self.dealerViewController];
//        return;
//    }
    else if (([self.currentSegueIdentifier isEqualToString:SegueIdDealerList]) && self.dealerListViewController)
    {
        /********************************************************************************/
        //Be default it will trigger dealer listing view when we go from main menu.Need to check ,whether it can be optimized.This had been added to avoid navigation title issue
        //MainViewController *parentVC = [APP_DELEGATE appBaseViewController];
        //if([parentVC.currentDetailController isKindOfClass:[DealerDetailViewController class]]){
        //    [parentVC.currentDetailController.view removeFromSuperview];
        //    [parentVC.currentDetailController removeFromParentViewController];
//        /********************************************************************************/
        
        if (_currentViewController == self.dealerListViewController)
        {
            self.transitionInProgress = NO;
            return;
        }
        [self swapFromViewController:self.dealerListViewController];
        return;
    }
//    else if (([self.currentSegueIdentifier isEqualToString:SegueIdDealer2]) && self.dealer2ViewController)
//    {
//        if (_currentViewController == self.dealer2ViewController)
//        {
//            self.transitionInProgress = NO;
//            return;
//        }
//        [self swapFromViewController:self.dealer2ViewController];
//        return;
//    }
    
    //Action Plan
    else if (([self.currentSegueIdentifier isEqualToString:SegueIdActionPlan]) && self.actionPlanViewController)
    {
        if (_currentViewController == self.actionPlanViewController)
        {
            self.transitionInProgress = NO;
            return;
        }
        [self swapFromViewController:self.actionPlanViewController];
        return;
    }
    else if (([self.currentSegueIdentifier isEqualToString:SegueIdActionPlan1]) && self.actionPlan1ViewController)
    {
        if (_currentViewController == self.actionPlan1ViewController)
        {
            self.transitionInProgress = NO;
            return;
        }
        [self swapFromViewController:self.actionPlan1ViewController];
        return;
    }
    else if (([self.currentSegueIdentifier isEqualToString:SegueIdActionPlan2]) && self.actionPlan2ViewController)
    {
        if (_currentViewController == self.actionPlan2ViewController)
        {
            self.transitionInProgress = NO;
            return;
        }
        [self swapFromViewController:self.actionPlan2ViewController];
        return;
    }
    else if (([self.currentSegueIdentifier isEqualToString:SegueIdActionPlan3]) && self.actionPlan3ViewController)
    {
        if (_currentViewController == self.actionPlan3ViewController)
        {
            self.transitionInProgress = NO;
            return;
        }
        [self swapFromViewController:self.actionPlan3ViewController];
        return;
    }
    
    // Library/ Report
    else if (([self.currentSegueIdentifier isEqualToString:SegueIdLibrary]) && self.libraryViewController)
    {
        if (_currentViewController == self.libraryViewController)
        {
            self.transitionInProgress = NO;
            return;
        }
        [self swapFromViewController:self.libraryViewController];
        return;
    }
    else if (([self.currentSegueIdentifier isEqualToString:SegueIdLibraryPreLaunch]) && self.libraryPreLaunchViewController)
    {
        if (_currentViewController == self.libraryPreLaunchViewController)
        {
            self.transitionInProgress = NO;
            return;
        }
        [self swapFromViewController:self.libraryPreLaunchViewController];
        return;
    }
    else if (([self.currentSegueIdentifier isEqualToString:SegueIdLibraryImplementation]) && self.libraryImplementationViewController)
    {
        if (_currentViewController == self.libraryImplementationViewController)
        {
            self.transitionInProgress = NO;
            return;
        }
        [self swapFromViewController:self.libraryImplementationViewController];
        return;
    }
    else if (([self.currentSegueIdentifier isEqualToString:SegueIdLibraryOperation]) && self.libraryOperationViewController)
    {
        if (_currentViewController == self.libraryOperationViewController)
        {
            self.transitionInProgress = NO;
            return;
        }
        [self swapFromViewController:self.libraryOperationViewController];
        return;
    }

    [self performSegueWithIdentifier:self.currentSegueIdentifier sender:nil];
}

@end
