//
// UIStoryboard + TRACK.m
//  TrackiOS
//
//  Created by Anish Kumar on 07/10/14.
//  Copyright (c) 2014 Xenon Automotive. All rights reserved.
//

#import "UIStoryboard+TRACK.h"

@implementation UIStoryboard (TRACK)

/**
 Method is responsible to return Mainstory Board instance.
 
 @return : id of a main story board
 */

+ (id) enterpriseStoryBoard
{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"XeEnterprisLogin" bundle:nil];
    return storyBoard;
}

+ (id) logInStoryBoard
{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"XeUserLoginViewController" bundle:nil];
    return storyBoard;
}

+ (id) mainStoryBoard
{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    return storyBoard;
}

//MenuStoryboard
+ (id) menuStoryBoard
{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"MenuStoryboard" bundle:nil];
    return storyBoard;
}

@end
