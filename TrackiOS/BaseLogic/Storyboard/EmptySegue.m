//
// EmptySeague.m
//  TrackiOS
//
//  Created by Anish Kumar on 07/10/14.
//  Copyright (c) 2014 Xenon Automotive. All rights reserved.
//


#import "EmptySegue.h"

@implementation EmptySegue

- (void)perform
{
    // The ContainerViewController class handles all of the view controller action.
}

@end
