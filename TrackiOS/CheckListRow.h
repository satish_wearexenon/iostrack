//
//  CheckListRow.h
//  TrackiOS
//
//  Created by John Paul Ranjith on 05/12/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface CheckListRow : NSManagedObject

@property (nonatomic, retain) NSNumber * remarkID;
@property (nonatomic, retain) NSNumber * bestPracticeID;
@property (nonatomic, retain) NSNumber * actionPlanID;
@property (nonatomic, retain) NSNumber * referenceID;
@property (nonatomic, retain) NSNumber * selectedColor;
@property (nonatomic, retain) NSString * rowDescription;
@property (nonatomic, retain) NSData * selectedImageColor;

@end
