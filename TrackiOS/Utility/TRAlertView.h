

@protocol TRAlertViewDelegate;

@interface TRAlertView : UIView <UITextFieldDelegate>

@property (nonatomic, strong) NSURL * urlToOpen;

@property (nonatomic) BOOL showRating;
@property (nonatomic) TrackAlertSection layoutType;
@property (nonatomic ) id <TRAlertViewDelegate> delegate;

@property (nonatomic, strong) UIImageView * backgroundImageView;
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UILabel * messageLabel;
@property (nonatomic, strong) NSMutableArray * buttonArray;
@property (nonatomic, strong) UILabel *headerLabel;
@property (nonatomic, strong) UITextField *passwordField;
@property (nonatomic, strong) UIButton *cancelButton;

@property (nonatomic, strong) UITextField *oldPasswordTextField;
@property (nonatomic, strong) UITextField *nwPasswordTextField;
@property (nonatomic, strong) UITextField *confirmPasswordTextField;

- (id)initWithTitle:(NSString *)inTitle message:(NSString *)inMessage delegate:(id )inDelegate layout:(TrackAlertSection )inLayoutType cancelButtonTitle:(NSString *)inCancelButtonTitle otherButtonTitles:(NSString *)inOtherButtonTitles, ... NS_REQUIRES_NIL_TERMINATION;

- (void)show;
- (void)addButton:(UIButton *)inButton;
@end


@protocol TRAlertViewDelegate <NSObject>
- (void)customAlertView:(TRAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;
- (void) passwordHasChanged;
@end