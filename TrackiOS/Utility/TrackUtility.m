//
//  TrackUtility.m
//  TrackiOS
//
//  Created by Anish Kumar on 26/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "TrackUtility.h"
#import "TrackErrorMessageView.h"

@implementation TrackUtility

+ (TrackUtility *)sharedUtility
{
    static TrackUtility *sharedAccountManagerInstance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        sharedAccountManagerInstance = [[self alloc] init];
    });
    return sharedAccountManagerInstance;
}

#pragma mark - Error messages popovers

- (void)showErrorMessageViewInView:(UIView *)inView withErrorMessage:(NSString *)inErrMsg
{
    //[self hidePasswordStrengthViewInView:inView];
    UIView *view = [inView.superview viewWithTag:(kErrorMessageViewTag + inView.tag)];
    TrackErrorMessageView * errorMessageView = nil;
    
    if ([view isKindOfClass:[TrackErrorMessageView class]]) {
        errorMessageView = (TrackErrorMessageView *)view;
    }
    
    if (errorMessageView == nil) {
        
        errorMessageView = (TrackErrorMessageView *)[[[NSBundle mainBundle] loadNibNamed:@"TrackErrorMessageView" owner:self options:nil] objectAtIndex:0];
        
        errorMessageView.tag = (kErrorMessageViewTag + inView.tag);
        [inView.superview addSubview:errorMessageView];
    }
    
    CGSize textSize = [inErrMsg sizeWithFont:[UIFont fontWithName:Font_Track_Regular size:18]];
    CGRect frame = CGRectMake(0, 0, 160, 35);;
    frame.size.width = MAX(50, (textSize.width + 25));
    frame.origin.y = inView.frame.origin.y - 28;
    frame.origin.x = (inView.frame.size.width - frame.size.width) + inView.frame.origin.x + 5;
    errorMessageView.frame = frame;
    
    errorMessageView.mErrMsgLabel.text = inErrMsg;
}

- (void)hideErrorMessageViewInView:(UIView *)inView
{
    UIView *view = [inView.superview viewWithTag:(kErrorMessageViewTag + inView.tag)];
    if ([view isKindOfClass:[TrackErrorMessageView class]]) {
        [view removeFromSuperview];
    }
}

- (BOOL) isEmailIDValid:(NSString*)inEmailID
{
    //NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    
    //    NSString *emailRegex = @"^((_|\\-){0,1}[^(.@|$|#|%|^|&|\\*|\\+|,|;|€|¥|£|/|\\-|=|:|?|!|'|\"|(|)|{|}|\\[|\\]|<|>|\\\\|_|~)]+)((_|\\-){0,1}[^(.@|$|#|%|^|&|\\*|,|;|€|¥|£|/|\\+|\\-|=|:|?|!|'|\"|(|)|{|}|\\[|\\]|<|>|\\\\|_|~)])+(\\.[^(.@|$|#|%|^|&|\\*|,|;|€|¥|£|/|\\+|\\-|=|:|?|!|'|\"|(|)|{|}|\\[|\\]|<|>|\\\\|_|~)]+)*@([^(.@|$|#|%|^|&|\\*|\\+|,|;|€|¥|£|/|\\-|=|:|?|!|'|\"|(|)|{|}|\\[|\\]|<|>|\\\\|~]+\\.)+([^.@|$|#|%|^|&|\\*|\\+|,|;|€|¥|£|/|=|:|(|)|!|'|\"|~|<|>|{|}|\\[|\\]|_|\\-|\\\\|?|~]{2,4}+)$";
    
    NSString *emailRegex = @"^((_|\\-){0,1}[^(.@|$|#|%|^|&|\\*|\\+|,|;|€|¥|£|/|\\-|=|:|?|!|'|\"|(|)|{|}|\\[|\\]|<|>|\\\\|_|~)]+)((_|\\-){0,1}[^(.@|$|#|%|^|&|\\*|,|;|€|¥|£|/|\\+|\\-|=|:|?|!|'|\"|(|)|{|}|\\[|\\]|<|>|\\\\|_|~)])+(\\.[^(.@|$|#|%|^|&|\\*|,|;|€|¥|£|/|\\+|\\-|=|:|?|!|'|\"|(|)|{|}|\\[|\\]|<|>|\\\\|_|~)]+)*((_|\\-){0,1}[^(.@|$|#|%|^|&|\\*|,|;|€|¥|£|/|\\+|\\-|=|:|?|!|'|\"|(|)|{|}|\\[|\\]|<|>|\\\\|_|~)])*@([^(.@|$|#|%|^|&|\\*|\\+|,|;|€|¥|£|/|\\-|=|:|?|!|'|\"|(|)|{|}|\\[|\\]|<|>|\\\\|~]+\\.)+([^.@|$|#|%|^|&|\\*|\\+|,|;|€|¥|£|/|=|:|(|)|!|'|\"|~|<|>|{|}|\\[|\\]|_|\\-|\\\\|?|~]{2,4}+)$";
    
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:inEmailID];
}

-(BOOL)isValidMobileNumber:(NSString *)withMObileNUmber
{
    NSString *mobileRegex = @"^[0-9]*$";
    NSPredicate *mobileTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", mobileRegex];
    
    return [mobileTest evaluateWithObject:withMObileNUmber];
}

- (BOOL ) stringIsEmpty:(NSString *) aString {
    
    if ((NSNull *) aString == [NSNull null]) {
        return YES;
    }
    
    if (aString == nil) {
        return YES;
    } else if ([aString length] == 0) {
        return YES;
    } else {
        aString = [aString stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if ([aString length] == 0) {
            return YES;
        }
    }
    
    return NO;
}



@end
