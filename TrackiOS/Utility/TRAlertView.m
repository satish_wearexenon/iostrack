

#import "TRAlertView.h"
#import <QuartzCore/QuartzCore.h>

#define kAlertViewPaddingLeft     15
#define kAlertViewPaddingRight    15
#define kAlertViewPaddingTop      15
#define kAlertViewPaddingBottom   15
#define kAlertViewWidth           280
#define kAlertViewMinHeight       120
#define kAlertViewMsgTitleSpacing 5
#define kAlertViewBtnsMsgSpacing  15

#define kAlertViewBtnHeight       35
#define kAlertViewBtnWidth        105
#define kAlertViewBtnsSpacing     20
#define kAlertViewBtnPaddingLeft  25

#define textFieldHorizonPadding     10
#define textFieldVertPadding        18
#define textFieldWidth              269
#define textFieldHeight             29
#define textFieldOriginY            90
#define textFieldOriginX            155

#define labelWidth                  140
#define labelOriginX                8
#define labelOriginY                96
#define labelVertPadding            28
#define labelFontSize               16

#define titleFontSize               25
#define changePasswordViewWidth     444.0
#define changePasswordViewHeight    275.0

#define lineColor [UIColor colorWithRed:178.0/255.0 green:178.0/255.0  blue:178.0/255.0  alpha:1.0]
#define textFieldBorderColor [UIColor colorWithRed:161.0/255.0 green:161.0/255.0  blue:161.0/255.0  alpha:1.0]


@interface TRAlertView ()
{
    UIView   * containerView;
    UIButton * crossCloseBtn;
    
}

- (void)prepareToShow;
- (void)changeLayoutOfButton:(UIButton *)inButton;
- (UIButton *)buttonWithTitle:(NSString *)inBtnTitle;
@end

@implementation TRAlertView

@synthesize layoutType = layoutType;

@synthesize titleLabel = titleLabel;
@synthesize messageLabel = messageLabel;
@synthesize buttonArray = buttonArray;
@synthesize backgroundImageView = backgroundImageView;
@synthesize showRating = showRating;
@synthesize urlToOpen = urlToOpen;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithTitle:(NSString *)inTitle message:(NSString *)inMessage delegate:(id )inDelegate layout:(TrackAlertSection )inLayoutType cancelButtonTitle:(NSString *)inCancelButtonTitle otherButtonTitles:(NSString *)inOtherButtonTitles, ...
{
    self = [super init];
    if (self) {
        
        self.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.3];        
        containerView     = [[UIView alloc] initWithFrame:CGRectZero];
        
        containerView.clipsToBounds = YES;
        containerView.layer.borderWidth = 1.0;
        containerView.layer.cornerRadius = 3.0f;
        
        _delegate = inDelegate;
        layoutType    = inLayoutType;
        showRating    = NO;
        
        switch (inLayoutType) {

            case eControllerypeChangePassword:
            {
                containerView.backgroundColor = [UIColor whiteColor];
                containerView.layer.borderColor  = [[UIColor clearColor] CGColor];
            }
                break;
            case eControllerTypeAlert:
            {
                containerView.backgroundColor = [UIColor redColor];
                containerView.layer.borderColor  = [[UIColor colorWithRed:240.0/255.0 green:165.0/255 blue:26.0/255 alpha:1] CGColor];
            }
                break;
            case eControllerTypeLogin:
            {
                containerView.backgroundColor = [UIColor redColor];
                containerView.layer.borderColor  = [[UIColor colorWithRed:240.0/255.0 green:165.0/255 blue:26.0/255 alpha:1] CGColor];
            }
                break;
            case eControllerTypeError:
            case eControllerTypeLibrary:
            default:
            {
                containerView.backgroundColor = [UIColor yellowColor];
                containerView.layer.borderColor  = [[UIColor colorWithRed:240.0/255.0 green:165.0/255 blue:26.0/255 alpha:1] CGColor];
            }
                break;
        }
        
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
}

- (void)prepareToShow
{
    CGRect frame =  CGRectZero;
    frame.size = CGSizeMake(changePasswordViewWidth, changePasswordViewHeight);
    containerView.frame = frame;
    backgroundImageView.frame = frame;
    [self addSubview:containerView];
    
    //NSInteger btnsCounts = [buttonArray count];
    
    //BOOL hasCrossCloseBtn = btnsCounts == 0;
    
    //Change Password Label
    UILabel *changePasswordLabel = [[UILabel alloc]init];
    NSString *textStr = @"Change Password";
    changePasswordLabel.text = textStr;
    [changePasswordLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:titleFontSize]];
    CGSize expectedLabelSize = [changePasswordLabel.text sizeWithAttributes:
                                @{NSFontAttributeName:
                                      [UIFont systemFontOfSize:titleFontSize]}];
    CGRect frameRelative = CGRectZero;
    float center = (containerView.frame.size.width / 2) - (expectedLabelSize.width / 2);
    frameRelative.origin = CGPointMake(center, 25);
    frameRelative.size   = CGSizeMake(changePasswordViewWidth, expectedLabelSize.height);
    changePasswordLabel.frame = frameRelative;
    [containerView addSubview:changePasswordLabel];
    
    //Old Password Label
    textStr = @"Old Password";
    UILabel *oldPasswordLabel = [self createLabel:textStr withYosition:labelOriginY withFontSize:labelFontSize];
    [containerView addSubview:oldPasswordLabel];
    
    //Old Password Text Field
    float yPosition = textFieldOriginY;
    _oldPasswordTextField = [self createTextField:textStr withYPosition:textFieldOriginY];
    _oldPasswordTextField.delegate = self;
    [containerView addSubview:_oldPasswordTextField];
    
    //New Password Label
    textStr = @"New Password";
    yPosition = oldPasswordLabel.frame.origin.y + oldPasswordLabel.frame.size.height + labelVertPadding;
    UILabel *newPasswordLabel = [self createLabel:textStr withYosition:yPosition withFontSize:labelFontSize];
    [containerView addSubview:newPasswordLabel];
    
    //New Password Text Field
    yPosition = _oldPasswordTextField.frame.origin.y + _oldPasswordTextField.frame.size.height + textFieldVertPadding;
    _nwPasswordTextField = [self createTextField:textStr withYPosition:yPosition];
    _nwPasswordTextField.delegate = self;
    [containerView addSubview:_nwPasswordTextField];
    
    //Confirm Password Label
    textStr = @"Confirm Password";
    yPosition = newPasswordLabel.frame.origin.y + newPasswordLabel.frame.size.height + labelVertPadding;
    UILabel *confirmPasswordLabel = [self createLabel:textStr withYosition:yPosition withFontSize:labelFontSize];
    [containerView addSubview:confirmPasswordLabel];
    
    //Confirm Password Text Field
    yPosition = _nwPasswordTextField.frame.origin.y + _nwPasswordTextField.frame.size.height + textFieldVertPadding;
    _confirmPasswordTextField = [self createTextField:textStr withYPosition:yPosition];
    _confirmPasswordTextField.delegate = self;
    [containerView addSubview:_confirmPasswordTextField];
    
    //Vertical Line Separator
    UIView *verticalLineView = [[UIView alloc]init];
    yPosition = _confirmPasswordTextField.frame.origin.y + _confirmPasswordTextField.frame.size.height + textFieldVertPadding;
    frameRelative.origin = CGPointMake(0, yPosition);
    frameRelative.size   = CGSizeMake(changePasswordViewWidth, 1.0);
    verticalLineView.frame = frameRelative;
    verticalLineView.backgroundColor = lineColor;
    [containerView addSubview:verticalLineView];
    
    //Horizontal Line Separator
    float xPosition = changePasswordViewWidth / 2;
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(xPosition, yPosition, 1.0, 44.0)];
    lineView.backgroundColor = lineColor;
    [containerView addSubview:lineView];
    
    //Cancel Button
    _cancelButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [_cancelButton addTarget:self  action:@selector(cancelTapped:) forControlEvents:UIControlEventTouchUpInside];
    frameRelative.origin = CGPointMake(0, yPosition);
    frameRelative.size   = CGSizeMake(changePasswordViewWidth/2, 44.0);
    [_cancelButton.titleLabel setTextAlignment: NSTextAlignmentCenter];
    [_cancelButton setTitleColor:[UIColor colorWithRed:0/255.0 green:190/255.0 blue:186/255.0 alpha:1.0] forState:UIControlStateNormal];
    [_cancelButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:19]];
    [_cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    _cancelButton.frame = frameRelative;
    [containerView addSubview:_cancelButton];
    
    //Confirm Button
    UIButton *confirmButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [confirmButton addTarget:self  action:@selector(confirmTapped:) forControlEvents:UIControlEventTouchUpInside];
    xPosition = _cancelButton.frame.origin.x + _cancelButton.frame.size.width;
    frameRelative.origin = CGPointMake(xPosition, yPosition);
    frameRelative.size   = CGSizeMake(changePasswordViewWidth/2, 44.0);
    [confirmButton.titleLabel setTextAlignment: NSTextAlignmentCenter];
    [confirmButton setTitle:@"Confirm" forState:UIControlStateNormal];
    [confirmButton setTitleColor:[UIColor colorWithRed:130/255.0 green:138/255.0 blue:144/255.0 alpha:1.0] forState:UIControlStateNormal];
    [confirmButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:19]];
    confirmButton.frame = frameRelative;
    [containerView addSubview:confirmButton];
}

- (UITextField *)createTextField:(NSString *)placeHolderText withYPosition:(float)yPosition
{
    UIImageView *textFieldBorderImageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"TextFieldBorder"]];
    CGRect frameRelative = CGRectZero;
    frameRelative.origin = CGPointMake(textFieldOriginX , yPosition);
    frameRelative.size   = CGSizeMake(textFieldWidth, textFieldHeight);
    textFieldBorderImageView.frame = frameRelative;
    [containerView addSubview:textFieldBorderImageView];
    
    UITextField *textField = [[UITextField alloc]init];
    frameRelative.origin = CGPointMake(textFieldOriginX + 5 , yPosition);
    frameRelative.size   = CGSizeMake(textFieldWidth -5, textFieldHeight);
    textField.frame = frameRelative;
    textField.font = [UIFont systemFontOfSize:15];
    textField.placeholder = placeHolderText;
    textField.autocorrectionType = UITextAutocorrectionTypeNo;
    textField.keyboardType = UIKeyboardTypeDefault;
    textField.returnKeyType = UIReturnKeyDone;
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    textField.borderStyle = UITextBorderStyleNone;
    
    //Border Layer
//    textField.layer.cornerRadius = 0.0f;
//    textField.layer.masksToBounds = YES;
//    textField.layer.borderColor = [textFieldBorderColor CGColor];
//    textField.layer.borderWidth = 1.0f;
//    textField.delegate = self;

    return textField;
}

- (UILabel *)createLabel:(NSString *)labelText withYosition:(float)yPosition withFontSize:(float)fontSize
{
    UILabel *label = [[UILabel alloc]init];
    label.textAlignment = NSTextAlignmentRight;
    label.text = labelText;
    [label setFont:[UIFont fontWithName:@"Helvetica" size:fontSize]];
    CGSize expectedLabelSize = [label.text sizeWithAttributes:
                                @{NSFontAttributeName:
                                      [UIFont systemFontOfSize:fontSize]}];
    CGRect frameRelative = CGRectZero;
    frameRelative.origin = CGPointMake(labelOriginX, yPosition);
    frameRelative.size   = CGSizeMake(labelWidth, expectedLabelSize.height);
    label.frame = frameRelative;
    
    return label;
}

- (void)show
{
    [self prepareToShow];
    
    UIWindow * activeWindow = [APP_DELEGATE window];
    self.frame = [activeWindow bounds];
    self.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.3];
    
    [activeWindow addSubview:self];
    containerView.center = self.center;
    containerView.transform = CGAffineTransformMakeScale( 0.001, 0.001);
    
    NSTimeInterval duration = kTransitionDuration/2;
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^ {
                       containerView.transform = CGAffineTransformMakeScale( 1.1, 1.1);
                     }
                     completion:^(BOOL finished) {
                         [UIView animateWithDuration:duration/2
                                               delay:0
                                             options:UIViewAnimationOptionCurveEaseOut
                                          animations:^ {
                                              containerView.transform = CGAffineTransformMakeScale (0.9, 0.9);                                              
                                          }
                                          completion:^(BOOL finished) {
                                              [UIView animateWithDuration:duration/2
                                                                    delay:0
                                                                  options:UIViewAnimationOptionCurveEaseOut
                                                               animations:^ {
                                                                   containerView.transform = CGAffineTransformMakeScale (1.0, 1.0);
                                                               }
                                                               completion:nil ]; }]; }];
    
}

#pragma mark - Buttons Action Methods 

- (void)cancelTapped:(UIButton *)inTappedBtn
{
//    CGRect frame = CGRectZero;
//    frame.origin = CGPointMake(containerView.frame.origin.x + inTappedBtn.frame.origin.x, containerView.frame.origin.y +  inTappedBtn.frame.origin.y);
//    frame.size   = CGSizeMake(inTappedBtn.frame.size.width, inTappedBtn.frame.size.height);
//    
//    [UIView animateWithDuration:0.25
//                          delay:0
//                        options:UIViewAnimationOptionCurveEaseOut
//                     animations:^ {
//                         inTappedBtn.frame = inTappedBtn.bounds;
//                         containerView.frame = frame;
//                     }
//                     completion:^(BOOL isFinish){[self removeFromSuperview];}];
    
//    [UIView animateWithDuration:0.1 animations:^(void) {
//        self.transform = CGAffineTransformScale(self.transform, 0.1, 0.1);
//    }
//                     completion:^(BOOL isFinish){[self removeFromSuperview];}];
    
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionTransitionCurlUp
                     animations:^ {
                         self.alpha = 0;
                     }
                     completion:^(BOOL isFinish){
                         [self removeFromSuperview];
                         self.alpha = 1;
                     }];
}

- (void)confirmTapped:(UIButton *)inTappedBtn
{
    if (_oldPasswordTextField.text.length > 4)
    {
        if (_nwPasswordTextField.text.length > 4)
        {
        if ([_nwPasswordTextField.text isEqual: _confirmPasswordTextField.text])
        {
            if ([_delegate respondsToSelector:@selector(passwordHasChanged)])
            {
                [_delegate passwordHasChanged];
            }
            
            [UIView animateWithDuration:0.5
                                  delay:0
                                options:UIViewAnimationOptionCurveEaseOut
                             animations:^ {
                                 self.alpha = 0;
                             }
                             completion:^(BOOL isFinish){
                                 [self removeFromSuperview];
                                 self.alpha = 1;
                             }];
        }
        
    }
    }
    
    
}

- (void)didTappedButton:(UIButton *)inTappedBtn
{
    if ([_delegate respondsToSelector:@selector(customAlertView:clickedButtonAtIndex:)]) {
        [_delegate customAlertView:self clickedButtonAtIndex:[buttonArray indexOfObject:inTappedBtn]];
    }
    
    [self removeFromSuperview];
    
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^ {
                         self.alpha = 0;
                     }
                     completion:^(BOOL isFinish){
                         [self removeFromSuperview];
                         self.alpha = 1;
                     }];
    
}

#pragma mark - Buttons Creation Methods

- (UIButton *)crossCloseButton
{
    UIButton *crossBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [crossBtn setImage:[UIImage imageNamed:@"cross_btn.png"] forState:UIControlStateNormal];
    [crossBtn sizeToFit];    
    [crossBtn addTarget:self action:@selector(didTappedCrossButton:) forControlEvents:UIControlEventTouchUpInside];
    return crossBtn;
}

- (UIButton *)buttonWithTitle:(NSString *)inBtnTitle
{
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.titleLabel.textColor = [UIColor whiteColor];
   // btn.titleLabel.font = [[FTDUtilities sharedUtility] fontForType:eCustomFontTypeAlertButton];
    //[btn setBackgroundImage:[self buttonBackgroundImageForLayout:layoutType] forState:UIControlStateNormal];
    [btn setTitle:inBtnTitle forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(didTappedButton:) forControlEvents:UIControlEventTouchUpInside];
    
    return btn;
}

- (void)changeLayoutOfButton:(UIButton *)inButton
{
    
}

//- (UIImage *)buttonBackgroundImageForLayout:(TrackAlertSection)inLayoutType
//{
//    //UIImage * bgImage = [UIImage imageNamed:@"alert_btn.png"];
//    switch (inLayoutType) {
//            
//        case eControllerTypeDealer:
//        {
//            bgImage = [UIImage imageNamed:@"tips_alert_btn.png"];
//        }
//            break;
//        case eControllerLayoutTypeHome:
//        case eControllerLayoutTypeJournal:
//        default:
//        {
//            
//        }
//            break;
//    }
//    return bgImage;
//}

//- (UILabel *)ratingInfoLabel
//{
//    CGRect frame = CGRectMake(0.0, 0.0, kAlertViewWidth, 40.0);
//    UILabel * infoLabel = [[UILabel alloc] initWithFrame:frame];
//    
//    infoLabel.font = [[FTDUtilities sharedUtility] fontForType:eCustomFontTypeAlertMessage];
//    infoLabel.textColor = [UIColor whiteColor];
//    infoLabel.backgroundColor = [UIColor clearColor];
//    infoLabel.textAlignment   = UITextAlignmentCenter;
//    infoLabel.numberOfLines   = 0;
//    infoLabel.text = @"1 - Poor  2 - Fair  3 - Neutral \n 4 - Good  5 - Great";
//    
//    return infoLabel;
//}

@end







/*
 
 -(void)showAlertView{
 
 
 self.transform = CGAffineTransformMakeScale( 0.001, 0.001);
 mContainerView.frame = [self bounds];
 [UIView beginAnimations:nil context:nil];
 [UIView setAnimationDuration:kTransitionDuration/1.5];
 [UIView setAnimationDelegate:self];
 [UIView setAnimationDidStopSelector:@selector(bounce1AnimationStopped)];
 self.transform = CGAffineTransformMakeScale( 1.1, 1.1);
 mContainerView.frame = [self bounds];
 [UIView commitAnimations];
 }
 
 - (void)bounce1AnimationStopped
 {
 [UIView beginAnimations:nil context:nil];
 [UIView setAnimationDuration:kTransitionDuration/2];
 [UIView setAnimationDelegate:self];
 [UIView setAnimationDidStopSelector:@selector(bounce2AnimationStopped)];
 self.transform = CGAffineTransformMakeScale (0.9, 0.9);
 mContainerView.frame = [self bounds];
 [UIView commitAnimations];
 }
 
 
 
 - (void)bounce2AnimationStopped
 {
 [UIView beginAnimations:nil context:nil];
 [UIView setAnimationDuration:kTransitionDuration/2];
 [UIView setAnimationDelegate:self];
 
 self.transform =  CGAffineTransformIdentity;
 mContainerView.frame = [self bounds];
 [UIView commitAnimations];
 }
 
 */

