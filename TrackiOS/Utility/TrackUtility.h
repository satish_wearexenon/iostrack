//
//  TrackUtility.h
//  TrackiOS
//
//  Created by Anish Kumar on 26/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TrackUtility : NSObject

- (void) showErrorMessageViewInView:(UIView *)inView withErrorMessage:(NSString *)inErrMsg;
- (void) hideErrorMessageViewInView:(UIView *)inView;
- (BOOL) isEmailIDValid:(NSString*)inEmailID;
- (BOOL) isValidMobileNumber:(NSString *)withMObileNUmber;
- (BOOL) stringIsEmpty:(NSString *) aString;

+ (TrackUtility *)sharedUtility;
@end
