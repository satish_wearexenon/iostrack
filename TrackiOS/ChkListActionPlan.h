//
//  ChkListActionPlan.h
//  TrackiOS
//
//  Created by John Paul Ranjith on 02/12/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ChkListActionPlan : NSManagedObject

@property (nonatomic, retain) NSString * actionStr;
@property (nonatomic, retain) NSNumber * checklistID;
@property (nonatomic, retain) NSString * desiredOutcome;
@property (nonatomic, retain) NSString * dueDate;
@property (nonatomic, retain) NSNumber * mainSectionID;
@property (nonatomic, retain) NSString * mainSectionName;
@property (nonatomic, retain) NSString * responsiblePeople;
@property (nonatomic, retain) NSString * situation;
@property (nonatomic, retain) NSNumber * subSectionID;
@property (nonatomic, retain) NSString * subSectionName;
@property (nonatomic, retain) NSString * support;
@property (nonatomic, retain) NSNumber * rowID;

@end
