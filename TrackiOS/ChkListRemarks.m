//
//  ChkListRemarks.m
//  Pods
//
//  Created by John Paul Ranjith on 02/12/14.
//
//

#import "ChkListRemarks.h"


@implementation ChkListRemarks

@dynamic checkListID;
@dynamic mainSectionID;
@dynamic mainSectionName;
@dynamic subSectionID;
@dynamic subSectionName;
@dynamic remarksStr;
@dynamic rowID;

@end
