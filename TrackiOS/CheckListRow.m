//
//  CheckListRow.m
//  TrackiOS
//
//  Created by John Paul Ranjith on 05/12/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "CheckListRow.h"


@implementation CheckListRow

@dynamic remarkID;
@dynamic bestPracticeID;
@dynamic actionPlanID;
@dynamic referenceID;
@dynamic selectedColor;
@dynamic rowDescription;
@dynamic selectedImageColor;

@end
