

#import <Foundation/Foundation.h>


typedef enum  {
    eSectionTimeline = 200,
    
    eSectionDealer,
    
    eSectionAction,
    
    eSectionLibaray,
    
    eSectionLogin
}TrackSection;

typedef enum
{
    eSubSectionTimeline = 300,
    eSubSectionTimeline1,
    eSubSectionTimeline2,
    eSubSectionTimeline3,
    
    eSubSectionDealerList,
    //eSubSectionDealer2,
    
    eSubSectionActionPlan1,
    eSubSectionActionPlan2,
    eSubSectionActionPlan3,
    
    eSubSectionLibarayPreLaunch,
    eSubSectionLibarayImplementation,
    eSubSectionLibarayOperation
}TrackSubSection;


typedef enum  {
    eControllerypeChangePassword = 0,
    eControllerTypeLogin,
    eControllerTypeError,
    eControllerTypeLibrary,
    eControllerTypeDealer,
    eAlertTypeWithOk,
    eAlertTypeWithOkCancel,
    eAlertTypeWithNetworkConnection,
    eAlertTypeWithErrorEmail
}TrackAlertSection;

typedef enum  {
    eCameraOptionPopOver = 0,
    eCheckListSectionPopOver,
    eDealerPopupGroup,
    eDealerPopupStaff
}PopOverType;

typedef enum  {
    eGreen = 0,
    eYellow,
    eRed,
    eGray
}ColorSelector;

typedef enum  {
    eDealerAdd = 110,
    eDealerEdit
}AddActionType;


#define kTransitionDuration 0.4f
#define kErrorMessageViewTag 245

#define kZipTextFieldMinLimit 6
#define kZipTextFieldMaxLimit 8
#define kPhoneNumberTextFieldMinLimit 7
#define kNumberTextFieldMinLimit 1
#define kPhoneNumberTextFieldMaxLimit 14
#define kNameTextFieldLimit 30
#define kEmailTextFieldLimit 65

