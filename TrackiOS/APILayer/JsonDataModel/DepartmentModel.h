//
//  DepartmentModel.h
//  DataSynchResearch
//
//  Created by abhijeet upadhyay on 18/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "JSONModel.h"

@protocol DepartmentModel
@end

@interface DepartmentModel : JSONModel

@property (strong, nonatomic) NSNumber* department_id;
@property (strong, nonatomic) NSString* department_name;

@end
