//
//  ActionPlanModel.h
//  TrackiOS
//
//  Created by satish sharma on 12/16/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "JSONModel.h"

@protocol ActionPlanModel
@end

@interface ActionPlanModel : JSONModel
@property (nonatomic, strong) NSString<Optional> * action;
@property (nonatomic, strong) NSNumber<Optional> * action_plan_id;
@property (nonatomic, strong) NSString<Optional> * checklist_name;
@property (nonatomic, strong) NSString<Optional> * desired_outcome;
@property (nonatomic, strong) NSString<Optional> * due_date;
@property (nonatomic, strong) NSNumber<Optional> * item_data_id;
@property (nonatomic, strong) NSString<Optional> * situation;
@property (nonatomic, strong) NSString<Optional> * status;
@property (nonatomic, strong) NSString<Optional> * summary;
@property (nonatomic, strong) NSString<Optional> * support;
@property (nonatomic, strong) NSArray<Optional> *responsible_persons_id;
@end
