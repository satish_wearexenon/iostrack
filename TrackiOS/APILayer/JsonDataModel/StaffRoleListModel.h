//
//  StaffRoleListModel.h
//  TrackiOS
//
//  Created by abhijeet upadhyay on 01/12/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "JSONModel.h"
@protocol StaffRoleListModel

@end
@interface StaffRoleListModel : JSONModel

@property (strong, nonatomic) NSNumber * role_id;
@property (strong, nonatomic) NSString * role_name;
@end
