//
//  UserModel.h
//  DataSynchResearch
//
//  Created by abhijeet upadhyay on 18/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

//{
//    "email": "frodo@nineleaps.com",
//    "first_name": "frodo",
//    "middle_name": "",
//    "last_name": "",
//    "departments": : 4,
//                    "region_name": "east",
//                 [
//                    {
//                        "department_id": 2,
//                        "department_name": "After Sales"
//                    },
//                    {
//                        "department_id": 3,
//                        "department_name": "Network Development"
//                    }
//                    ],
//    "regions": [
//                {
//                    "region_id"   "department_id": 2
//                },
//                {
//                    "region_id": 10,
//                    "region_name": "west",
//                    "department_id": 3
//                }
//                ],
//    "roles": [
//              "Regional Manager"
//              ],
//    "token": "a8a0bcdcf8a012762ed0dd41321067cc02597d7d53bbb141cb81dfeb623afe01"
//}

#import <Foundation/Foundation.h>
#import "JSONModel.h"
#import "DepartmentModel.h"
#import "RegionModel.h"

@interface UserModel : JSONModel

@property (strong, nonatomic) NSString* email;
@property (strong, nonatomic) NSString* first_name;
@property (strong, nonatomic) NSString* middle_name;
@property (strong, nonatomic) NSString* last_name;
@property (strong, nonatomic) NSString* token;

@property (strong, nonatomic) NSArray<DepartmentModel>* departments;
@property (strong, nonatomic) NSArray<RegionModel>* regions;


@end
