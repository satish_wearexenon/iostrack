//
//  StaffRoleModel.h
//  TrackiOS
//
//  Created by abhijeet upadhyay on 01/12/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "JSONModel.h"
#import "StaffRoleListModel.h"

@interface StaffRoleModel : JSONModel


@property (strong, nonatomic) NSArray<StaffRoleListModel>* staff_roles_list;

@end
