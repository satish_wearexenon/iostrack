//
//  DealerDetailModel.h
//  TrackiOS
//
//  Created by abhijeet upadhyay on 21/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"
#import "DealerGraphModel.h"

@protocol DealerDetailModel
@end

@interface DealerDetailModel : JSONModel

//"dealer_city" = Bangalore;
//"dealer_code" = ANOO1;
//"dealer_id" = 2;
//"dealer_name" = "Alex_dealerName";
//"dealer_province" = province;
//"dealer_readiness" = 68;
//"dealer_region_id" = 1;
//"dealer_stage" = "pre-launch";
//"dealer_stage_checklist" = PL;
//"dealer_stage_id" = 2;
//"dealer_street_address" = "street_address";
//"dealer_type_id" = 2002;
//"first_car_date" = "2014-05-01";
//"last_updated" = 34567765;
//"start_of_operation" = "2014-06-01";

@property (strong, nonatomic) NSNumber<Optional>* dealer_id;
@property (strong, nonatomic) NSString<Optional>* dealer_code;
@property (strong, nonatomic) NSString<Optional>* dealer_name;
@property (strong, nonatomic) NSNumber<Optional>* dealer_region_id;
//@property (strong, nonatomic) NSString* dealer_region;
@property (strong, nonatomic) NSNumber<Optional>* dealer_stage_id;
@property (strong, nonatomic) NSString<Optional>* dealer_stage;
@property (strong, nonatomic) NSString<Optional>* dealer_stage_checklist;
//@property (strong, nonatomic) NSString* dealer_type;
@property (strong, nonatomic) NSNumber<Optional>* dealer_type_id;
@property (strong, nonatomic) NSString<Optional>* dealer_street_address;
@property (strong, nonatomic) NSString<Optional>* dealer_province;
@property (strong, nonatomic) NSString<Optional>* dealer_city;
//@property (strong, nonatomic) NSString* representative_person;
@property (strong, nonatomic) NSString<Optional>* first_car_date;
@property (strong, nonatomic) NSString<Optional>* start_of_operation;
@property (strong, nonatomic) NSString<Optional>* last_updated;
@property (strong, nonatomic) NSString<Optional>* dealer_readiness;


@property (strong, nonatomic) NSArray<DealerGraphModel>* dealer_stage_graph_values;
@property (strong, nonatomic) NSArray *latest_action_details;
@property (strong, nonatomic) NSArray *responsible_person;

@end
