//
//  DealerGraphModel.h
//  TrackiOS
//
//  Created by abhijeet upadhyay on 21/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

//  "total": 15,
//  "y": 2,
//  "r": 2,
//  "g": 8

@protocol DealerGraphModel
@end

@interface DealerGraphModel : JSONModel

@property (strong, nonatomic) NSString* total;
@property (strong, nonatomic) NSString* y;
@property (strong, nonatomic) NSString* r;
@property (strong, nonatomic) NSString* g;

@end
