//
//  DealerModel.h
//  TrackiOS
//
//  Created by abhijeet upadhyay on 21/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

#import "DealerDetailModel.h"
//{
//    "dealer_list" =     (
//                         {
//                             "dealer_city" = Bangalore;
//                             "dealer_code" = ANOO1;
//                             "dealer_id" = 2;
//                             "dealer_name" = "Alex_dealerName";
//                             "dealer_province" = province;
//                             "dealer_readiness" = 68;
//                             "dealer_region_id" = 1;
//                             "dealer_stage" = "pre-launch";
//                             "dealer_stage_checklist" = PL;
//                             "dealer_stage_graph_values" =             (
//                                                                        {
//                                                                            g = 2;
//                                                                            r = 3;
//                                                                            total = 5;
//                                                                            y = 0;
//                                                                        },
//                                                                        {
//                                                                            g = 0;
//                                                                            r = 0;
//                                                                            total = 1;
//                                                                            y = 1;
//                                                                        }
//                                                                        );
//                             "dealer_stage_id" = 2;
//                             "dealer_street_address" = "street_address";
//                             "dealer_type_id" = 2002;
//                             "first_car_date" = "2014-05-01";
//                             "last_updated" = 34567765;
//                             "latest_action_details" =             (
//                                                                    "9 items updated ",
//                                                                    " 3 photos added"
//                                                                    );
//                             "responsible_person" =             (
//                                                                 123,
//                                                                 124,
//                                                                 125
//                                                                 );
//                             "start_of_operation" = "2014-06-01";
//                         }
//                         );
//}
@interface DealerModels : JSONModel

@property (strong, nonatomic) NSArray<DealerDetailModel>* dealer_list;


@end
