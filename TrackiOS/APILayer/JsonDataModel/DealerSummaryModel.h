//
//  DealerSummaryModel.h
//  TrackiOS
//
//  Created by abhijeet upadhyay on 27/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

//"action_plans" = 4;
//"best_practices" = 5;
//"total_issues" = 3;

#import "JSONModel.h"

@interface DealerSummaryModel : JSONModel

@property (strong, nonatomic) NSNumber* action_plans;
@property (strong, nonatomic) NSNumber* best_practices;
@property (strong, nonatomic) NSNumber* total_issues;

@end
