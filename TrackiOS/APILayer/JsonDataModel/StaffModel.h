//
//  StaffModel.h
//  TrackiOS
//
//  Created by abhijeet upadhyay on 28/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "JSONModel.h"
#import "StaffDetailModel.h"

@interface StaffModel : JSONModel

@property (strong, nonatomic) NSArray <StaffDetailModel>* staff_list;

@end
