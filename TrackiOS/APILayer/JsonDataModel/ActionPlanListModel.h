//
//  ActionPlanListModel.h
//  TrackiOS
//
//  Created by satish sharma on 12/16/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "JSONModel.h"
#import "ActionPlanModel.h"

@interface ActionPlanListModel : JSONModel

@property (strong, nonatomic) NSArray <ActionPlanModel>* actionPlans;

@end
