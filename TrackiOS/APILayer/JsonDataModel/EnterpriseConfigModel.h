//
//  EnterpriseConfigModel.h
//  TrackiOS
//
//  Created by abhijeet upadhyay on 21/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

@interface EnterpriseConfigModel : JSONModel

@property (strong, nonatomic) NSString* ImageURL;
@property (strong, nonatomic) NSString* parameter_url;
@property (strong, nonatomic) NSString* total_image_files;

@end
