//
//  TimeLineMenuModel.m
//  TrackiOS
//
//  Created by abhijeet upadhyay on 27/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "TimeLineMenuModel.h"

@implementation TimeLineMenuModel


+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"submenu_timeline_id": @"timeline_id",
                                                       @"submenu_timeline_name": @"name",
                                                       }];
}

@end
