//
//  DealerMenuModel.h
//  TrackiOS
//
//  Created by abhijeet upadhyay on 27/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "JSONModel.h"
#import "LibraryMenuModel.h"
#import "TimeLineMenuModel.h"
#import "DealerMenuModel.h"
#import "ReportMenuModel.h"

@interface MenuModel : JSONModel


@property (strong, nonatomic) DealerMenuModel* menu_dealer;

@property (strong, nonatomic) ReportMenuModel* menu_report;

@property (strong, nonatomic) NSArray<LibraryMenuModel>* menu_library;
@property (strong, nonatomic) NSArray<TimeLineMenuModel>* menu_timeline;

@end
