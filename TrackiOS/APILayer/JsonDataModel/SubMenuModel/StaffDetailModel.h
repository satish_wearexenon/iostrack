//
//  StaffDetailModel.h
//  TrackiOS
//
//  Created by abhijeet upadhyay on 28/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "JSONModel.h"
@protocol StaffDetailModel
@end

@interface StaffDetailModel : JSONModel


@property (strong, nonatomic) NSNumber* dealer_id;
@property (strong, nonatomic) NSString* email;
@property (strong, nonatomic) NSString* first_name;
@property (strong, nonatomic) NSString* last_name;
@property (strong, nonatomic) NSString* middle_name;
@property (strong, nonatomic) NSString* office_phone_number;
@property (strong, nonatomic) NSString* residence_phone_number;
@property (strong, nonatomic) NSNumber* staff_id;
@property (strong, nonatomic) NSNumber* staff_role_id;

@end
