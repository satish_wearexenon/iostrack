//
//  LibraryMenuModel.h
//  TrackiOS
//
//  Created by abhijeet upadhyay on 27/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "JSONModel.h"
@protocol LibraryMenuModel
@end

@interface LibraryMenuModel : JSONModel

@end
