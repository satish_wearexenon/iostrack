//
//  DealerMenuModel.h
//  TrackiOS
//
//  Created by abhijeet upadhyay on 27/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "JSONModel.h"
#import "DealerMenuRegionModel.h"
#import "DealerMenuReportModel.h"

@interface DealerMenuModel : JSONModel

@property (strong, nonatomic) NSArray<DealerMenuRegionModel>* regions;
@property (strong, nonatomic) NSArray<DealerMenuReportModel>* reports;

@end
