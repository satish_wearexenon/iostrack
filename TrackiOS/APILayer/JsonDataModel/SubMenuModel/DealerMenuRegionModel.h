//
//  DealerMenuRegionModel.h
//  TrackiOS
//
//  Created by abhijeet upadhyay on 27/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "JSONModel.h"


@protocol DealerMenuRegionModel
@end

@interface DealerMenuRegionModel : JSONModel


@property (strong, nonatomic) NSNumber* region_id;
@property (strong, nonatomic) NSString* name;

@end
