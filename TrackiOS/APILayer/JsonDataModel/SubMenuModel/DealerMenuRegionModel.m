//
//  DealerMenuRegionModel.m
//  TrackiOS
//
//  Created by abhijeet upadhyay on 27/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "DealerMenuRegionModel.h"

@implementation DealerMenuRegionModel


+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"region_id": @"region_id",
                                                       @"region_name": @"name",
                                                       }];
}
@end
