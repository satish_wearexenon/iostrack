//
//  ReportMenuModel.h
//  TrackiOS
//
//  Created by abhijeet upadhyay on 27/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "JSONModel.h"

#import "ReportMenuRegionModel.h"
#import "ReportsMenuReportModel.h"

@interface ReportMenuModel : JSONModel

@property (strong, nonatomic) NSArray<ReportMenuRegionModel>* regions;

@property (strong, nonatomic) NSArray<ReportsMenuReportModel>* reports;

@end
