//
//  DealerMenuReportModel.h
//  TrackiOS
//
//  Created by abhijeet upadhyay on 27/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "JSONModel.h"

@protocol DealerMenuReportModel
@end

@interface DealerMenuReportModel : JSONModel

@property (strong, nonatomic) NSNumber* checkilst_id;
@property (strong, nonatomic) NSString* checklist_name;
@property (strong, nonatomic) NSNumber* stage_id;
@property (strong, nonatomic) NSString* stage_name;

@end
