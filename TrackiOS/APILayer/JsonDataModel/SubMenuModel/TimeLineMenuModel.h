//
//  TimeLineMenuModel.h
//  TrackiOS
//
//  Created by abhijeet upadhyay on 27/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "JSONModel.h"

@protocol TimeLineMenuModel
@end

@interface TimeLineMenuModel : JSONModel

@property (strong, nonatomic) NSNumber* timeline_id;
@property (strong, nonatomic) NSString* name;

@end
