//
//  SubMenuModel.h
//  TrackiOS
//
//  Created by abhijeet upadhyay on 27/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "JSONModel.h"
#import "MenuModel.h"
@interface SubMenuModel : JSONModel


@property (strong, nonatomic)MenuModel* submenu;
@end
