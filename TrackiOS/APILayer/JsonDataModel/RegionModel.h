//
//  RegionModel.h
//  DataSynchResearch
//
//  Created by abhijeet upadhyay on 18/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "JSONModel.h"
@protocol RegionModel
@end

@interface RegionModel : JSONModel
@property (strong, nonatomic) NSNumber* region_id;
@property (strong, nonatomic) NSString* region_name;
@property (strong, nonatomic) NSString* department_id;
@end
