//
//  EnterpriseConfigModel.m
//  TrackiOS
//
//  Created by abhijeet upadhyay on 21/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "EnterpriseConfigModel.h"

@implementation EnterpriseConfigModel

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"image_info.image_url": @"ImageURL",
                                                       @"parameter_url": @"parameter_url",
                                                       @"image_info.total_image_files": @"total_image_files"
                                                       }];
}

@end
