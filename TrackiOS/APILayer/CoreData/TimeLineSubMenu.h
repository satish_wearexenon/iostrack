//
//  TimeLineSubMenu.h
//  TrackiOS
//
//  Created by abhijeet upadhyay on 27/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface TimeLineSubMenu : NSManagedObject

@property (nonatomic, retain) NSNumber * timelinemenu_id;
@property (nonatomic, retain) NSString * timelinemenu_name;

@end
