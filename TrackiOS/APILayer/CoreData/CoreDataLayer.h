//
//  CoreDataLayer.h
//  TrackiOS
//
//  Created by abhijeet upadhyay on 30/10/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CoreDataLayer : NSObject


/**
 Core Data related Properties
 
 managedObjectContext :Query work(fetch/update etc) for the context
 managedObjectModel : Database Schema, How data is getting stored, DB model, Entities etc.
 persistentStoreCoordinator : Defines the Srote Type for the CoreData as sqllight.
 
 */

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

/**
 Core Data Healper functions
 */

- (NSURL *)applicationDocumentsDirectory;

+ (id)coreDataSharedmanager;

@end
