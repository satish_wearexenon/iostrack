//
//  DealerUpdateStatus.m
//  TrackiOS
//
//  Created by abhijeet upadhyay on 27/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "DealerUpdateStatus.h"


@implementation DealerUpdateStatus

@dynamic dealer_id;
@dynamic iteamsadded;
@dynamic addedphotos;

@end
