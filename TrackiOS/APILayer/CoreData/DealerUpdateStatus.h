//
//  DealerUpdateStatus.h
//  TrackiOS
//
//  Created by abhijeet upadhyay on 27/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface DealerUpdateStatus : NSManagedObject

@property (nonatomic, retain) NSNumber * dealer_id;
@property (nonatomic, retain) NSString * iteamsadded;
@property (nonatomic, retain) NSString * addedphotos;

@end
