//
//  Dealer.h
//  TrackiOS
//
//  Created by abhijeet upadhyay on 28/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DealerStaff, DealerStageGraph, DealerSummary, DealerUpdateStatus, Staff;

@interface Dealer : NSManagedObject

@property (nonatomic, retain) NSString * city;
@property (nonatomic, retain) NSString * country;
@property (nonatomic, retain) NSString * date_created;
@property (nonatomic, retain) NSString * dealer_code;
@property (nonatomic, retain) NSNumber * dealer_group_id;
@property (nonatomic, retain) NSNumber * dealer_id;
@property (nonatomic, retain) NSString * dealer_readiness;
@property (nonatomic, retain) NSNumber * dealer_region_id;
@property (nonatomic, retain) NSString * dealer_stage_checklist;
@property (nonatomic, retain) NSNumber * dealer_type_id;
@property (nonatomic, retain) NSString * first_car_date;
@property (nonatomic, retain) NSString * first_name;
@property (nonatomic, retain) NSString * last_name;
@property (nonatomic, retain) NSString * last_updated;
@property (nonatomic, retain) NSNumber * mark_as_inactive;
@property (nonatomic, retain) NSNumber * mark_for_delete;
@property (nonatomic, retain) NSString * middle_name;
@property (nonatomic, retain) NSString * office_phone_number;
@property (nonatomic, retain) NSString * province;
@property (nonatomic, retain) NSString * residence_phone_number;
@property (nonatomic, retain) NSString * responcible_person;
@property (nonatomic, retain) NSString * stage;
@property (nonatomic, retain) NSString * start_of_operation;
@property (nonatomic, retain) NSString * street_address;
@property (nonatomic, retain) NSString * zip_code;
@property (nonatomic, retain) NSSet *dealerStaff;
@property (nonatomic, retain) NSSet *dealerStageGraph;
@property (nonatomic, retain) DealerSummary *dealerSummary;
@property (nonatomic, retain) DealerUpdateStatus *dealerUpdate;
@property (nonatomic, retain) NSSet *dealerTotalStaff;
@end

@interface Dealer (CoreDataGeneratedAccessors)

- (void)addDealerStaffObject:(DealerStaff *)value;
- (void)removeDealerStaffObject:(DealerStaff *)value;
- (void)addDealerStaff:(NSSet *)values;
- (void)removeDealerStaff:(NSSet *)values;

- (void)addDealerStageGraphObject:(DealerStageGraph *)value;
- (void)removeDealerStageGraphObject:(DealerStageGraph *)value;
- (void)addDealerStageGraph:(NSSet *)values;
- (void)removeDealerStageGraph:(NSSet *)values;

- (void)addDealerTotalStaffObject:(Staff *)value;
- (void)removeDealerTotalStaffObject:(Staff *)value;
- (void)addDealerTotalStaff:(NSSet *)values;
- (void)removeDealerTotalStaff:(NSSet *)values;

@end
