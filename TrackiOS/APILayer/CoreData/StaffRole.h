//
//  StaffRole.h
//  TrackiOS
//
//  Created by abhijeet upadhyay on 01/12/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Staff;

@interface StaffRole : NSManagedObject

@property (nonatomic, retain) NSNumber * role_id;
@property (nonatomic, retain) NSString * role_name;
@property (nonatomic, retain) Staff *staffRoleStaff;

@end
