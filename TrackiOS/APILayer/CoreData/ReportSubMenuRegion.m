//
//  ReportSubMenuRegion.m
//  TrackiOS
//
//  Created by abhijeet upadhyay on 27/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "ReportSubMenuRegion.h"


@implementation ReportSubMenuRegion

@dynamic reportMenu_region_id;
@dynamic reportMenu_region_name;

@end
