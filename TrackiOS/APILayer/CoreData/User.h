//
//  User.h
//  TrackiOS
//
//  Created by abhijeet upadhyay on 20/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Role, UserDepartment, UserRegion;

@interface User : NSManagedObject

@property (nonatomic, retain) NSString * avatar_link;
@property (nonatomic, retain) NSString * created_date;
@property (nonatomic, retain) NSString * device_token;
@property (nonatomic, retain) NSString * device_type;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * email_token;
@property (nonatomic, retain) NSNumber * email_token_expirty;
@property (nonatomic, retain) NSString * emailid;
@property (nonatomic, retain) NSString * first_name;
@property (nonatomic, retain) NSString * ios_token;
@property (nonatomic, retain) NSNumber * ios_token_unset_on;
@property (nonatomic, retain) NSString * last_name;
@property (nonatomic, retain) NSNumber * marked_for_delete;
@property (nonatomic, retain) NSString * middle_name;
@property (nonatomic, retain) NSString * modified_date;
@property (nonatomic, retain) NSString * office_phone_number;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSString * password_salt;
@property (nonatomic, retain) NSString * residence_phone_number;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSNumber * user_id;
@property (nonatomic, retain) NSString * user_name;
@property (nonatomic, retain) NSSet *userDepartment;
@property (nonatomic, retain) NSSet *userRegion;
@property (nonatomic, retain) NSSet *userRole;
@end

@interface User (CoreDataGeneratedAccessors)

- (void)addUserDepartmentObject:(UserDepartment *)value;
- (void)removeUserDepartmentObject:(UserDepartment *)value;
- (void)addUserDepartment:(NSSet *)values;
- (void)removeUserDepartment:(NSSet *)values;

- (void)addUserRegionObject:(UserRegion *)value;
- (void)removeUserRegionObject:(UserRegion *)value;
- (void)addUserRegion:(NSSet *)values;
- (void)removeUserRegion:(NSSet *)values;

- (void)addUserRoleObject:(Role *)value;
- (void)removeUserRoleObject:(Role *)value;
- (void)addUserRole:(NSSet *)values;
- (void)removeUserRole:(NSSet *)values;

@end
