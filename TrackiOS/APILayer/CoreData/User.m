//
//  User.m
//  TrackiOS
//
//  Created by abhijeet upadhyay on 20/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "User.h"
#import "Role.h"
#import "UserDepartment.h"
#import "UserRegion.h"


@implementation User

@dynamic avatar_link;
@dynamic created_date;
@dynamic device_token;
@dynamic device_type;
@dynamic email;
@dynamic email_token;
@dynamic email_token_expirty;
@dynamic emailid;
@dynamic first_name;
@dynamic ios_token;
@dynamic ios_token_unset_on;
@dynamic last_name;
@dynamic marked_for_delete;
@dynamic middle_name;
@dynamic modified_date;
@dynamic office_phone_number;
@dynamic password;
@dynamic password_salt;
@dynamic residence_phone_number;
@dynamic status;
@dynamic user_id;
@dynamic user_name;
@dynamic userDepartment;
@dynamic userRegion;
@dynamic userRole;

@end
