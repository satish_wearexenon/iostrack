//
//  ReportSubMenuReport.m
//  TrackiOS
//
//  Created by abhijeet upadhyay on 27/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "ReportSubMenuReport.h"


@implementation ReportSubMenuReport

@dynamic reportMenu_checklist_id;
@dynamic reportMenu_checklist_name;
@dynamic reportMenu_stage_id;
@dynamic reportMenu_stage_name;

@end
