//
//  ReportSubMenuReport.h
//  TrackiOS
//
//  Created by abhijeet upadhyay on 27/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ReportSubMenuReport : NSManagedObject

@property (nonatomic, retain) NSNumber * reportMenu_checklist_id;
@property (nonatomic, retain) NSString * reportMenu_checklist_name;
@property (nonatomic, retain) NSNumber * reportMenu_stage_id;
@property (nonatomic, retain) NSString * reportMenu_stage_name;

@end
