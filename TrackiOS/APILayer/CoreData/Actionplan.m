//
//  Actionplan.m
//  TrackiOS
//
//  Created by satish sharma on 12/16/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "Actionplan.h"
#import "ResponsiblePersonAP.h"


@implementation Actionplan

@dynamic action;
@dynamic action_plan_id;
@dynamic date_created;
@dynamic date_modified;
@dynamic desired_outcome;
@dynamic due_date;
@dynamic item_data_id;
@dynamic marked_for_delete;
@dynamic responsibility;
@dynamic situation;
@dynamic status;
@dynamic summary;
@dynamic support;
@dynamic checklist_name;
@dynamic responsibleActionPlan;

@end
