//
//  Role.m
//  TrackiOS
//
//  Created by abhijeet upadhyay on 18/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "Role.h"


@implementation Role

@dynamic created_date;
@dynamic modified_date;
@dynamic role_id;
@dynamic role_name;

@end
