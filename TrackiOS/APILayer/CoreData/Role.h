//
//  Role.h
//  TrackiOS
//
//  Created by abhijeet upadhyay on 18/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Role : NSManagedObject

@property (nonatomic, retain) NSString * created_date;
@property (nonatomic, retain) NSString * modified_date;
@property (nonatomic, retain) NSNumber * role_id;
@property (nonatomic, retain) NSString * role_name;

@end
