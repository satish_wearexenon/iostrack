//
//  DealerSubMenuReport.m
//  TrackiOS
//
//  Created by abhijeet upadhyay on 27/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "DealerSubMenuReport.h"


@implementation DealerSubMenuReport

@dynamic dealerSubmenuChecklist_id;
@dynamic dealerSubmenuStage_Id;
@dynamic dealerSubmenuChecklist_name;
@dynamic dealerSubmenuStage_name;

@end
