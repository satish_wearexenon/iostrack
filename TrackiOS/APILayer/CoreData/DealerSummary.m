//
//  DealerSummary.m
//  TrackiOS
//
//  Created by abhijeet upadhyay on 27/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "DealerSummary.h"


@implementation DealerSummary

@dynamic dealer_id;
@dynamic actionplan;
@dynamic bestpractice;
@dynamic totalissues;

@end
