//
//  DealerSubMenuRegion.m
//  TrackiOS
//
//  Created by abhijeet upadhyay on 27/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "DealerSubMenuRegion.h"


@implementation DealerSubMenuRegion

@dynamic dealermenu_region_id;
@dynamic dealermenu_region_name;

@end
