//
//  DealerStaff.h
//  TrackiOS
//
//  Created by abhijeet upadhyay on 26/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface DealerStaff : NSManagedObject

@property (nonatomic, retain) NSNumber * staff_id;
@property (nonatomic, retain) NSNumber * dealer_id;

@end
