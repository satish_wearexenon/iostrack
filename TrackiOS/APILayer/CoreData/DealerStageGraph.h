//
//  DealerStageGraph.h
//  TrackiOS
//
//  Created by abhijeet upadhyay on 26/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface DealerStageGraph : NSManagedObject

@property (nonatomic, retain) NSNumber * dealer_id;
@property (nonatomic, retain) NSString * total;
@property (nonatomic, retain) NSString * r;
@property (nonatomic, retain) NSString * y;
@property (nonatomic, retain) NSString * g;

@end
