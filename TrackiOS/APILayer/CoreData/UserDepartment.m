//
//  UserDepartment.m
//  TrackiOS
//
//  Created by abhijeet upadhyay on 20/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "UserDepartment.h"


@implementation UserDepartment

@dynamic date_created;
@dynamic date_modified;
@dynamic department_id;
@dynamic mark_for_delete;
@dynamic user_id;

@end
