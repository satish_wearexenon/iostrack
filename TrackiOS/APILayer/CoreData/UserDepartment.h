//
//  UserDepartment.h
//  TrackiOS
//
//  Created by abhijeet upadhyay on 20/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface UserDepartment : NSManagedObject

@property (nonatomic, retain) NSString * date_created;
@property (nonatomic, retain) NSString * date_modified;
@property (nonatomic, retain) NSNumber * department_id;
@property (nonatomic, retain) NSNumber * mark_for_delete;
@property (nonatomic, retain) NSNumber * user_id;

@end
