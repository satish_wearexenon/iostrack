//
//  Staff.m
//  TrackiOS
//
//  Created by abhijeet upadhyay on 01/12/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "Staff.h"


@implementation Staff

@dynamic date_created;
@dynamic date_modified;
@dynamic dealer_id;
@dynamic email;
@dynamic first_name;
@dynamic last_name;
@dynamic mark_for_delete;
@dynamic middle_name;
@dynamic office_phone_number;
@dynamic residence_phone_number;
@dynamic staff_id;
@dynamic staff_role_id;
@dynamic staff_updated;

@end
