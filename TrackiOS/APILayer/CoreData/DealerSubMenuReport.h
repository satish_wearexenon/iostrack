//
//  DealerSubMenuReport.h
//  TrackiOS
//
//  Created by abhijeet upadhyay on 27/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface DealerSubMenuReport : NSManagedObject

@property (nonatomic, retain) NSNumber * dealerSubmenuChecklist_id;
@property (nonatomic, retain) NSNumber * dealerSubmenuStage_Id;
@property (nonatomic, retain) NSString * dealerSubmenuChecklist_name;
@property (nonatomic, retain) NSString * dealerSubmenuStage_name;

@end
