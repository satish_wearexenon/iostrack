//
//  UserRegion.m
//  TrackiOS
//
//  Created by abhijeet upadhyay on 20/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "UserRegion.h"


@implementation UserRegion

@dynamic date_created;
@dynamic date_modified;
@dynamic region_id;
@dynamic user_id;

@end
