//
//  ReportSubMenuRegion.h
//  TrackiOS
//
//  Created by abhijeet upadhyay on 27/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ReportSubMenuRegion : NSManagedObject

@property (nonatomic, retain) NSNumber * reportMenu_region_id;
@property (nonatomic, retain) NSString * reportMenu_region_name;

@end
