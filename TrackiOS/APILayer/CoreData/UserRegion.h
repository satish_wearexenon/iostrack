//
//  UserRegion.h
//  TrackiOS
//
//  Created by abhijeet upadhyay on 20/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface UserRegion : NSManagedObject

@property (nonatomic, retain) NSString * date_created;
@property (nonatomic, retain) NSString * date_modified;
@property (nonatomic, retain) NSNumber * region_id;
@property (nonatomic, retain) NSNumber * user_id;

@end
