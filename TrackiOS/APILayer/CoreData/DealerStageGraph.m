//
//  DealerStageGraph.m
//  TrackiOS
//
//  Created by abhijeet upadhyay on 26/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "DealerStageGraph.h"


@implementation DealerStageGraph

@dynamic dealer_id;
@dynamic total;
@dynamic r;
@dynamic y;
@dynamic g;

@end
