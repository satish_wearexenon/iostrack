//
//  DealerSubMenuRegion.h
//  TrackiOS
//
//  Created by abhijeet upadhyay on 27/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface DealerSubMenuRegion : NSManagedObject

@property (nonatomic, retain) NSNumber * dealermenu_region_id;
@property (nonatomic, retain) NSString * dealermenu_region_name;

@end
