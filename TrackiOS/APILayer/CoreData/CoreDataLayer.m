//
//  CoreDataLayer.m
//  TrackiOS
//
//  Created by abhijeet upadhyay on 30/10/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "CoreDataLayer.h"

@implementation CoreDataLayer

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;


+(id)coreDataSharedmanager {
    
    static CoreDataLayer *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    return sharedManager;
}

- (id)init {
    if (self = [super init]) {
    
    
    }
    return self;
}

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "Xenon.TrackiOS" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

-(void)setUpRestKitCoreData{

}

@end
