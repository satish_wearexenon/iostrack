//
//  Actionplan.h
//  TrackiOS
//
//  Created by satish sharma on 12/16/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ResponsiblePersonAP;

@interface Actionplan : NSManagedObject

@property (nonatomic, retain) NSString * action;
@property (nonatomic, retain) NSNumber * action_plan_id;
@property (nonatomic, retain) NSString * date_created;
@property (nonatomic, retain) NSString * date_modified;
@property (nonatomic, retain) NSString * desired_outcome;
@property (nonatomic, retain) NSString * due_date;
@property (nonatomic, retain) NSNumber * item_data_id;
@property (nonatomic, retain) NSNumber * marked_for_delete;
@property (nonatomic, retain) NSString * responsibility;
@property (nonatomic, retain) NSString * situation;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSString * summary;
@property (nonatomic, retain) NSString * support;
@property (nonatomic, retain) NSString * checklist_name;
@property (nonatomic, retain) NSSet *responsibleActionPlan;
@end

@interface Actionplan (CoreDataGeneratedAccessors)

- (void)addResponsibleActionPlanObject:(ResponsiblePersonAP *)value;
- (void)removeResponsibleActionPlanObject:(ResponsiblePersonAP *)value;
- (void)addResponsibleActionPlan:(NSSet *)values;
- (void)removeResponsibleActionPlan:(NSSet *)values;

@end
