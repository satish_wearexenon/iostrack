//
//  Staff.h
//  TrackiOS
//
//  Created by abhijeet upadhyay on 01/12/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Staff : NSManagedObject

@property (nonatomic, retain) NSString * date_created;
@property (nonatomic, retain) NSString * date_modified;
@property (nonatomic, retain) NSNumber * dealer_id;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * first_name;
@property (nonatomic, retain) NSString * last_name;
@property (nonatomic, retain) NSNumber * mark_for_delete;
@property (nonatomic, retain) NSString * middle_name;
@property (nonatomic, retain) NSString * office_phone_number;
@property (nonatomic, retain) NSString * residence_phone_number;
@property (nonatomic, retain) NSNumber * staff_id;
@property (nonatomic, retain) NSNumber * staff_role_id;
@property (nonatomic, retain) NSNumber * staff_updated;

@end
