//
//  Dealer.m
//  TrackiOS
//
//  Created by abhijeet upadhyay on 28/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "Dealer.h"
#import "DealerStaff.h"
#import "DealerStageGraph.h"
#import "DealerSummary.h"
#import "DealerUpdateStatus.h"
#import "Staff.h"


@implementation Dealer

@dynamic city;
@dynamic country;
@dynamic date_created;
@dynamic dealer_code;
@dynamic dealer_group_id;
@dynamic dealer_id;
@dynamic dealer_readiness;
@dynamic dealer_region_id;
@dynamic dealer_stage_checklist;
@dynamic dealer_type_id;
@dynamic first_car_date;
@dynamic first_name;
@dynamic last_name;
@dynamic last_updated;
@dynamic mark_as_inactive;
@dynamic mark_for_delete;
@dynamic middle_name;
@dynamic office_phone_number;
@dynamic province;
@dynamic residence_phone_number;
@dynamic responcible_person;
@dynamic stage;
@dynamic start_of_operation;
@dynamic street_address;
@dynamic zip_code;
@dynamic dealerStaff;
@dynamic dealerStageGraph;
@dynamic dealerSummary;
@dynamic dealerUpdate;
@dynamic dealerTotalStaff;

@end
