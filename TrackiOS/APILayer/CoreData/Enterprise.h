//
//  Enterprise.h
//  TrackiOS
//
//  Created by abhijeet upadhyay on 28/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Enterprise : NSManagedObject

@property (nonatomic, retain) NSString * imageURL;
@property (nonatomic, retain) NSString * parameterURL;
@property (nonatomic, retain) NSString * total_conf_files;

@end
