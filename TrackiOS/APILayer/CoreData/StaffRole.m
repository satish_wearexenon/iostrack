//
//  StaffRole.m
//  TrackiOS
//
//  Created by abhijeet upadhyay on 01/12/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "StaffRole.h"
#import "Staff.h"


@implementation StaffRole

@dynamic role_id;
@dynamic role_name;
@dynamic staffRoleStaff;

@end
