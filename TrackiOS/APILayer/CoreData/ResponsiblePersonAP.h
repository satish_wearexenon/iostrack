//
//  ResponsiblePersonAP.h
//  TrackiOS
//
//  Created by abhijeet upadhyay on 16/12/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ResponsiblePersonAP : NSManagedObject

@property (nonatomic, retain) NSNumber * responsible_person_id;
@property (nonatomic, retain) NSNumber * action_plan_id;

@end
