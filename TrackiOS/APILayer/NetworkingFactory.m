//
//  NetworkingFactory.m
//  TrackiOS
//
//  Created by abhijeet upadhyay on 04/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "NetworkingFactory.h"
#import "User.h"
#import "CoreData+MagicalRecord.h"

#import "AFHTTPRequestOperationManager.h"
#import "UserModel.h"
#import "JSONModelLib.h"
#import "User.h"
#import "UserDepartment.h"
#import "UserRegion.h"
#import "DealerModels.h"
#import "EnterpriseConfigModel.h"
#import "Enterprise.h"

#import "DealerStageGraph.h"
#import "DealerDetailModel.h"
#import "DealerStaff.h"
#import "SubMenuModel.h"
#import "DealerSubMenuRegion.h"
#import "ReportSubMenuReport.h"
#import "DealerSubMenuReport.h"
#import "TimeLineSubMenu.h"
#import "ReportSubMenuRegion.h"
#import "DealerSummaryModel.h"
#import "DealerSummary.h"
#import "DealerUpdateStatus.h"
#import "StaffModel.h"
#import "Staff.h"
#import "StaffRoleModel.h"
#import "StaffRole.h"

#import "ActionPlanModel.h"
#import "ActionPlanListModel.h"
#import "Actionplan.h"
#import "ResponsiblePersonAP.h"

static AFHTTPRequestOperationManager *manager;

@implementation NetworkingFactory


- (id)init {
    if (self = [super init]) {
        
        
       // [self loadService];
       // [self loginServiceCall];
        
    }
    return self;
}

+(id)networkingSharedmanager{
    //static NetworkingFactory *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
       // sharedManager = [[self alloc] init];
        // manager = [AFHTTPRequestOperationManager manager];
    });
    return manager;
}
#pragma -
#pragma GET API Calls

//User Information call
+(void)loginServiceCall:(NSString*)userName withPassword:(NSString*)password withSuccessBlock:(LoginServiceCall)loginSuccess{
    

    NSString *uRLString = [NSString stringWithFormat:@"http://trackoem2.cloudapp.net/v1.0/app/auth/login"];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    NSDictionary *params = @ {@"username" :userName, @"password" :password, @"device_token" :@"any_string"};
    
    
   // [[manager operationQueue] cancelAllOperations];
   
    if (manager==nil) {
        manager = [AFHTTPRequestOperationManager manager];
        
    }
    
    [MagicalRecord setupCoreDataStack];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];

    [manager POST:uRLString parameters:params
         success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"response object = %@", responseObject);
         UserModel *userModel = [[UserModel alloc]initWithDictionary:responseObject error:nil];
        
         User *user = [User createEntity];
         user.first_name = userModel.first_name;
         user.email = userModel.email;
         user.middle_name = userModel.middle_name;
         user.last_name = userModel.last_name;
         user.ios_token = userModel.token;
         //user.password = password;
        
         [[NSUserDefaults standardUserDefaults] setObject:userModel.token forKey:@"token"];
         [[NSUserDefaults standardUserDefaults] synchronize];
         
         for (DepartmentModel *depObj  in userModel.departments)
         {
             UserDepartment *dept = [UserDepartment createEntity];
             //dept.dapartment_name = depObj.department_name;
             dept.department_id = depObj.department_id;
             [user addUserDepartmentObject:dept];
         }
         
         for (RegionModel *regionObj  in userModel.regions)
         {
             UserRegion *region = [UserRegion createEntity];
             //dept.dapartment_name = depObj.department_name;
             region.region_id = regionObj.region_id;
             [user addUserRegionObject:region];
         }
        
         
         [[NSManagedObjectContext defaultContext] saveWithOptions:MRSaveWithoutOptions completion:^(BOOL success, NSError *error) {
             
             loginSuccess(true);
             
             
         }];
    }
         failure:
     ^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"%@",error);
         
         TRAlertView *av = [[TRAlertView alloc] initWithTitle:@"Alert"
                                                      message:@"Please provide correct login credentials"
                                                     delegate:self
                                                       layout:eAlertTypeWithOk
                                                containsEmail:NO
                                                      dateStr:nil
                                            cancelButtonTitle:nil
                                            otherButtonTitles:@"OK", nil];
         [av show];
         
         [QSActivityIndicator end];
         loginSuccess(false);
     }];
}

//Change Password

+ (void)changePasswordFromOldPaddword:(NSString*)oldPasword toNewPassword:(NSString*)newPassword withSuccess:(ChangePassword)success{

    NSString *uRLString = [NSString stringWithFormat:@"http://trackoem2.cloudapp.net/v1.0/app/auth/changePassword"];
    
    NSDictionary *params = @ {@"old_password" :oldPasword, @"new_password" :newPassword};
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
    
    [manager.requestSerializer setValue:token forHTTPHeaderField:@"Authorization"];
    
    [manager PUT:uRLString parameters:params
         success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"response object = %@", responseObject);
         
        success(true);
     }
     
         failure:
     ^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Change Password not successful");
         success(false);
     }];

}
//Logout API call with success
+ (void)logOutWithSuccess:(LogOut)success{
    
    NSString *uRLString = [NSString stringWithFormat:@"http://trackoem2.cloudapp.net/v1.0/app/auth/logout"];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
    
    [manager.requestSerializer setValue:token forHTTPHeaderField:@"Authorization"];
    
    [manager GET:uRLString parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"response object = %@", responseObject);
         
         success(true);
     }
     
         failure:
     ^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Logout not successful");
         success(false);
     }];

}

//Config Service call
+ (void)configServiceCall:(ConfigServiceCall)configSuccess{

    NSString *uRLString = @"http://trackoem2.cloudapp.net/v1.0/app/config";
    
     manager.requestSerializer = [AFJSONRequestSerializer serializer];
 //   [[manager operationQueue] cancelAllOperations];
    
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
    
    [manager.requestSerializer setValue:token forHTTPHeaderField:@"Authorization"];
    
    [manager GET:uRLString parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
          NSLog(@"response object = %@", responseObject);
          SubMenuModel *subConfigModel = [[SubMenuModel alloc]initWithDictionary:responseObject error:nil];
         
         //Dealer Submenu
         for (DealerMenuRegionModel *dmRegionModel in subConfigModel.submenu.menu_dealer.regions) {
             
             DealerSubMenuRegion *dealerRegion = [DealerSubMenuRegion createEntity];
             
             dealerRegion.dealermenu_region_id =  dmRegionModel.region_id;
             dealerRegion.dealermenu_region_name = dmRegionModel.name;
         }
         
         for (DealerMenuReportModel *dmReportModel in subConfigModel.submenu.menu_dealer.reports) {
             
             DealerSubMenuReport *dealerReports = [DealerSubMenuReport createEntity];
             
             dealerReports.dealerSubmenuChecklist_id = dmReportModel.checkilst_id;
             dealerReports.dealerSubmenuChecklist_name = dmReportModel.checklist_name;
             dealerReports.dealerSubmenuStage_Id = dmReportModel.stage_id;
             dealerReports.dealerSubmenuStage_name = dmReportModel.stage_name;
         }
        
         //Report Menu
         for (ReportMenuRegionModel *dmReportModel in subConfigModel.submenu.menu_report.regions) {
             
             ReportSubMenuRegion *dealerReport = [ReportSubMenuRegion createEntity];
    
             dealerReport.reportMenu_region_id =  dmReportModel.region_id;
             dealerReport.reportMenu_region_name = dmReportModel.name;
         }
         
         for (ReportsMenuReportModel *rmReportModel in subConfigModel.submenu.menu_report.reports) {
             
             ReportSubMenuReport *reportReports = [ReportSubMenuReport createEntity];
             
             reportReports.reportMenu_checklist_id = rmReportModel.checkilst_id;
             reportReports.reportMenu_checklist_name = rmReportModel.checklist_name;
             reportReports.reportMenu_stage_id = rmReportModel.stage_id;
             reportReports.reportMenu_stage_name = rmReportModel.stage_name;
         }
         
         //Time Line Menu
         for (TimeLineMenuModel *tlModel in subConfigModel.submenu.menu_timeline) {
             
             TimeLineSubMenu *timeLineMenu = [TimeLineSubMenu createEntity];
             timeLineMenu.timelinemenu_id =  tlModel.timeline_id;
             timeLineMenu.timelinemenu_name = tlModel.name;
         }

         [[NSManagedObjectContext defaultContext] saveWithOptions:MRSaveWithoutOptions completion:^(BOOL success, NSError *error) {
             
             configSuccess(true);
             
              NSLog(@"region object 1= %ld", (unsigned long)[DealerSubMenuRegion findAll].count);
              NSLog(@"region object 2 = %@", [DealerSubMenuRegion findFirst].dealermenu_region_name);
              NSLog(@"report object = %ld", (unsigned long)[DealerSubMenuReport findAll].count);
              NSLog(@"time line object = %ld", (unsigned long)[TimeLineSubMenu findAll].count);
              NSLog(@"report menu report object = %ld", (unsigned long)[ReportSubMenuReport findAll].count);
        }];
         
     }
         failure:
     ^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Config Response Not connected");
         [QSActivityIndicator end];
         TRAlertView *av = [[TRAlertView alloc] initWithTitle:@"Alert"
                                                      message:@"Config Response Not connected"
                                                     delegate:self
                                                       layout:eAlertTypeWithOk
                                                containsEmail:NO
                                                      dateStr:nil
                                            cancelButtonTitle:nil
                                            otherButtonTitles:@"OK", nil];
         [av show];

         configSuccess(false);
     }];

}

+ (void)getBaseURLforOEMcall:(NSString*)eKey withSuccess:(BaseURLCall)urlSuccess
{
    [QSActivityIndicator start];
    
    manager = [AFHTTPRequestOperationManager manager];
    NSString *uRLString = @"http://tracksuperadmin.cloudapp.net/v1.0/enterpriseInfo?enterprise_code";
    
    [manager GET:[NSString stringWithFormat:@"%@=%@",uRLString,eKey] parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSMutableDictionary *response = (NSMutableDictionary *)responseObject;
         
         NSLog(@"response object: %@",response);
        //After Logout usage
         if (response) {
             [[NSUserDefaults standardUserDefaults] setObject:[response objectForKey:@"enterprise_url"] forKey:@"OEMBaseURL"];
             [[NSUserDefaults standardUserDefaults] setObject:[response objectForKey:@"enterprise_secret_key"] forKey:@"EnterprisesecretKey"];
         }
         
       // [QSActivityIndicator end];
         
         urlSuccess(true);
     } failure:
     ^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Enterprise Config not connected");
         [QSActivityIndicator end];
         TRAlertView *av = [[TRAlertView alloc] initWithTitle:@"Alert"
                                                      message:@"Please provide correct code"
                                                     delegate:self
                                                       layout:eAlertTypeWithOk
                                                containsEmail:NO
                                                      dateStr:nil
                                            cancelButtonTitle:nil
                                            otherButtonTitles:@"OK", nil];
         [av show];
         
         urlSuccess(false);
         
     }];

}

//Enterprise Config Call
+ (void)enterpriseConfigServiceCall:(NSString*)eKey withSuccess:(EnterpriseConfigServiceCall)configSuccess {

     [MagicalRecord setupCoreDataStack];
    
    [QSActivityIndicator start];
    
    if (manager==nil) {
        manager = [AFHTTPRequestOperationManager manager];

    }
    
   // NSString *uRLString = @"http://trackoem2.cloudapp.net/v1.0/enterprise/config";
    
    NSMutableString *uRLString = [NSMutableString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"OEMBaseURL"]];
    [uRLString appendString:@"/v1.0/enterprise/config"];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
 //   [[manager operationQueue] cancelAllOperations];
    
    // Need to use eKey param for setValue once we receive.
    [manager.requestSerializer setValue:eKey forHTTPHeaderField:@"enterprise_secret_key"];
    
    [manager GET:uRLString parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSLog(@"response object = %@", responseObject);
         EnterpriseConfigModel *eConfigModel = [[EnterpriseConfigModel alloc]initWithDictionary:responseObject error:nil];
         

         Enterprise *eConfig  =  [Enterprise createEntity];
         eConfig.imageURL = eConfigModel.ImageURL;
         eConfig.total_conf_files = eConfigModel.total_image_files;
         eConfig.parameterURL = eConfigModel.parameter_url;
         
         //After Logout usage
         [[NSUserDefaults standardUserDefaults] setObject:eConfigModel.ImageURL forKey:@"ImageURL"];
         [[NSUserDefaults standardUserDefaults] setObject:eConfigModel.parameter_url forKey:@"ParamURL"];
         
         
         [[NSManagedObjectContext defaultContext] saveWithOptions:MRSaveWithoutOptions completion:^(BOOL success, NSError *error) {
             
             configSuccess(true);
              NSLog(@"Enterprise object = %@", [Enterprise findFirst].parameterURL);
             [QSActivityIndicator end];
        }];
         
    }
         failure:
     ^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Enterprise Config not connected, operation:%@, error:%@",operation,error);
          [QSActivityIndicator end];
         TRAlertView *av = [[TRAlertView alloc] initWithTitle:@"Alert"
                                                      message:@"Please provide correct code"
                                                     delegate:self
                                                       layout:eAlertTypeWithOk
                                                containsEmail:NO
                                                      dateStr:nil
                                            cancelButtonTitle:nil
                                            otherButtonTitles:@"OK", nil];
         [av show];
      [QSActivityIndicator end];
         configSuccess(false);
     }];
}

//Dealer Listing call
+ (void)dealerListingServiceCall:(DealerListingServiceCall)dealerListingSuccess{

    NSString *uRLString = @"http://trackoem2.cloudapp.net/v1.0/app/dealers";
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [[manager operationQueue] cancelAllOperations];
    
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
    
    [manager.requestSerializer setValue:token forHTTPHeaderField:@"Authorization"];
    
    [manager GET:uRLString parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"response object = %@", responseObject);
         DealerModels *dealerModel = [[DealerModels alloc]initWithDictionary:responseObject error:nil];
         
         
         
         //general deatail of dealer.
         for (DealerDetailModel *ddmodel in dealerModel.dealer_list) {
             
             Dealer *dealer = [Dealer createEntity];
             dealer.dealer_id = ddmodel.dealer_id;
             dealer.dealer_code = ddmodel.dealer_code;
             dealer.first_name = ddmodel.dealer_name;
             dealer.dealer_region_id = ddmodel.dealer_region_id;
             dealer.stage = ddmodel.dealer_stage; //Stage ID use will be taken care of afterwards.
             dealer.dealer_stage_checklist = ddmodel.dealer_stage_checklist;
             dealer.dealer_type_id = ddmodel.dealer_type_id;
             dealer.street_address = ddmodel.dealer_street_address;
             dealer.province = ddmodel.dealer_province;
             dealer.city = ddmodel.dealer_city;
             dealer.first_car_date = ddmodel.first_car_date;
             dealer.start_of_operation = ddmodel.start_of_operation;
             dealer.last_updated = ddmodel.last_updated;
             dealer.dealer_readiness = ddmodel.dealer_readiness;
             
             //Graph deatail of dealer
             for (DealerGraphModel *graphModel in ddmodel.dealer_stage_graph_values){
                 
                 DealerStageGraph *graph = [DealerStageGraph createEntity];
                 graph.total = graphModel.total;
                 graph.y = graphModel.y;
                 graph.g = graphModel.g;
                 graph.r = graphModel.r;
                 graph.dealer_id = ddmodel.dealer_id;
                 
                 [dealer addDealerStageGraphObject:graph];
             }
             
             //Staff deails of dealer
             for (NSNumber *staffID in ddmodel.responsible_person){
             
                 DealerStaff *staff = [DealerStaff createEntity];
                 staff.dealer_id = ddmodel.dealer_id;
                 staff.staff_id = staffID;
                 
                 [dealer addDealerStaffObject:staff];
             }
             
          
            //Update Iteams
             DealerUpdateStatus *updateStatus = [DealerUpdateStatus createEntity];
             if (ddmodel.latest_action_details.count == 2) {
                 updateStatus.addedphotos = [ddmodel.latest_action_details objectAtIndex:1];
                 updateStatus.iteamsadded = [ddmodel.latest_action_details objectAtIndex:0];
                 updateStatus.dealer_id = ddmodel.dealer_id;
                 
                 
             }
             else
             {
                 updateStatus.addedphotos = @"0";
                 updateStatus.iteamsadded = @"0";
                 updateStatus.dealer_id = ddmodel.dealer_id;
             }
             NSLog(@"Dealer ID in update status = %@", updateStatus.dealer_id);
             
             
          
             //Call for summary
             [NetworkingFactory dealerSummaryServiceCall: dealer.dealer_id withSuccessBlock:^(BOOL success) {
                 if (success) {
                     NSLog(@"ALL COOL GOT dealer summary");
                     
                 }
             }];
             
             //Call for staff listing
             
             [NetworkingFactory dealerStaffListServiceCall:dealer withSuccessBlock:^(BOOL success) {
                 if (success) {
                     NSLog(@"ALL COOL GOT dealer staff listing");
                 }
            }];
        }
         
         
         
         [[NSManagedObjectContext defaultContext] saveWithOptions:MRSaveWithoutOptions completion:^(BOOL success, NSError *error) {
             
             NSLog(@"Dealer object = %@", [Dealer findFirst].dealer_stage_checklist);
             NSLog(@"Count graph data  = %ld", (unsigned long)[DealerStageGraph findAll].count);
             NSLog(@"Count staff data = %ld", (unsigned long)[DealerStaff findAll].count);
             NSLog(@"Count dealer daler ID = %@", [DealerUpdateStatus findFirst].dealer_id);
             
             dealerListingSuccess(true);
             
         }];
    }
         failure:
     ^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Dealer Response Not connected");
         dealerListingSuccess(false);
         
     }];

}

//Dealer Summary
//Call has been done in dealer listing only
+ (void)dealerSummaryServiceCall:(NSNumber*)dealerID withSuccessBlock:(DealerSummaryServiceCall)summarySuccess{

   
    
    NSString *uRLString = [NSString stringWithFormat:@"http://trackoem2.cloudapp.net/v1.0/app/dealers/%@/summary",dealerID];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
     
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
    
    [manager.requestSerializer setValue:token forHTTPHeaderField:@"Authorization"];
    
    [manager GET:uRLString parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
          NSLog(@"response object = %@", responseObject);
         DealerSummaryModel *dealerModel = [[DealerSummaryModel alloc]initWithDictionary:responseObject error:nil];
         
         DealerSummary *dlSummary = [DealerSummary createEntity];
         dlSummary.actionplan = dealerModel.action_plans;
         dlSummary.dealer_id = dealerID;
         dlSummary.bestpractice = dealerModel.best_practices;
         dlSummary.totalissues = dealerModel.total_issues;
         
         [[NSManagedObjectContext defaultContext] saveWithOptions:MRSaveWithoutOptions completion:^(BOOL success, NSError *error) {
             
             NSLog(@"Dealer Summary = %@", [DealerSummary findFirst].bestpractice);
             NSLog(@"Dealer id = %@", [DealerSummary findFirst].dealer_id);
             
             summarySuccess(true);
             
         }];
         
        }
         
     failure:
         ^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Dealer Summary Response Not connected");
             summarySuccess(false);
         }];

}

//Staff Listing
+ (void)dealerStaffListServiceCall:(Dealer*)dealer withSuccessBlock:(DealerStaffListServiceCall)staffSuccess{

    NSString *uRLString = [NSString stringWithFormat:@"http://trackoem2.cloudapp.net/v1.0/app/dealers/%@/dealerStaff",dealer.dealer_id];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
    
    [manager.requestSerializer setValue:token forHTTPHeaderField:@"Authorization"];
    
    [manager GET:uRLString parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"Staff response object = %@", responseObject);
         
//         //[Staff  MR_truncateAll];
//         NSArray *array = [Staff findAll];
//         for (Staff *staff in array) {
//             [staff MR_deleteEntity];
//         }
         
         //[dealer removeDealerStaff:[NSSet setWithArray:[Staff findAll]]];
         
//         //TODO Delete old data
//         NSArray *dealerStaffArray = [DealerStaff findAll];
//         for (DealerStaff *dealerStaff in dealerStaffArray)
//         {
//             [dealer removeDealerStaffObject:dealerStaff];
//         }
//         NSArray *arrayStaff = [Staff findAll];
//         for (Staff *staff in arrayStaff)
//         {
//             [staff deleteEntity];
//             [staff MR_deleteEntity];
//         }
//         
//         [[NSManagedObjectContext defaultContext] saveToPersistentStoreAndWait];
         
//         [[NSManagedObjectContext defaultContext] saveWithOptions:MRSaveWithoutOptions completion:^(BOOL success, NSError *error) {
//             
//            // NSLog(@"Staff first name = %@", [Staff findFirst].first_name);
//             NSLog(@"Staff  = %ld", (unsigned long)[Staff findAll].count);
//             
//             staffSuccess(true);
//             
//         }];
         
        StaffModel *dealerModel = [[StaffModel alloc]initWithDictionary:responseObject error:nil];
         
        for (StaffDetailModel *sdmodel in dealerModel.staff_list) {
             Staff *staff = [Staff createEntity];
             staff.dealer_id = sdmodel.dealer_id;
             staff.email = sdmodel.email;
             staff.first_name = sdmodel.first_name;
             staff.middle_name = sdmodel.middle_name;
             staff.office_phone_number = sdmodel.office_phone_number;
             staff.residence_phone_number = sdmodel.residence_phone_number;
             staff.staff_id = sdmodel.staff_id;
             staff.staff_role_id = sdmodel.staff_role_id;
             staff.staff_updated = [NSNumber numberWithBool:false];
            
             [dealer addDealerTotalStaffObject:staff];
             
         }
         [[NSManagedObjectContext defaultContext] saveWithOptions:MRSaveWithoutOptions completion:^(BOOL success, NSError *error) {
             
             //NSLog(@"Staff first name = %@", [Staff findFirst].first_name);
             NSLog(@"Staff  = %ld", (unsigned long)[Staff findAll].count);
             
             staffSuccess(true);
             
         }];
    }
     
    failure:
     ^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Dealer staff Response Not connected");
         staffSuccess(false);
     }];
}

//Staff Roles

+ (void)staffRoleAPICall:(StaffRolesServiceCall)staffRolesSuccess{
    
    NSString *uRLString = [NSString stringWithFormat:@"http://trackoem2.cloudapp.net/v1.0/app/staffRoles"];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
    
    [manager.requestSerializer setValue:token forHTTPHeaderField:@"Authorization"];
    
    [manager GET:uRLString parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         //StaffRoleModel
         NSLog(@"response object = %@", responseObject);
         
         StaffRoleModel *dealerModel = [[StaffRoleModel alloc] initWithDictionary:responseObject error:nil];
         
         for (StaffRoleListModel *srlModel in dealerModel.staff_roles_list) {
             
             StaffRole *staffRole = [StaffRole createEntity];
             
             staffRole.role_id = srlModel.role_id;
             staffRole.role_name = srlModel.role_name;
         }
         
         [[NSManagedObjectContext defaultContext] saveWithOptions:MRSaveWithoutOptions completion:^(BOOL success, NSError *error) {
             
             NSLog(@"Staff Role Role name = %@", [StaffRole findFirst].role_name);
             NSLog(@"Staff  = %ld", (unsigned long)[StaffRole findAll].count);
             
             staffRolesSuccess(true);
             
         }];
    }
     
         failure:
     ^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Dealer staff Response Not connected");
         staffRolesSuccess(false);
     }];
}

// Action plan
+ (void)actionPlanGETAPICall:(NSNumber*)dealerID withSuccessCallBack:(ActionPlanGETAPICall)actionPlanGetSuccess
{
    NSString *uRLString = [NSString stringWithFormat:@"http://trackoem2.cloudapp.net/v1.0/app/actionPlan?dealer_id=%@",dealerID];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
    
    [manager.requestSerializer setValue:token forHTTPHeaderField:@"Authorization"];
    
    [manager GET:uRLString parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"Action Plan response object = %@", responseObject);
         
         
        ActionPlanListModel  *actionPlanlistModel = [[ActionPlanListModel alloc] initWithDictionary:responseObject error:nil];
 
         for (ActionPlanModel *actionpPlanmodel in actionPlanlistModel.actionPlans) {
             
             Actionplan *actionplan = [Actionplan createEntity];
             actionplan.action = actionpPlanmodel.action;
             actionplan.action_plan_id = actionpPlanmodel.action_plan_id;
            
             actionplan.desired_outcome = actionpPlanmodel.desired_outcome;
             actionplan.due_date = actionpPlanmodel.due_date;
             actionplan.item_data_id = actionpPlanmodel.item_data_id;
             actionplan.status = actionpPlanmodel.status;
             actionplan.situation = actionpPlanmodel.situation;
             actionplan.summary = actionpPlanmodel.summary;
             actionplan.support = actionpPlanmodel.support;
             actionplan.checklist_name= actionpPlanmodel.checklist_name;
             
             for (id responseAP in actionpPlanmodel.responsible_persons_id) {
                 
                 ResponsiblePersonAP *responseId = [ResponsiblePersonAP createEntity];
                 responseId.action_plan_id = actionpPlanmodel.action_plan_id;
                 responseId.responsible_person_id = (NSNumber*)responseAP;
                 [actionplan addResponsibleActionPlanObject:responseId];
             }
         }
         
         [[NSManagedObjectContext defaultContext] saveWithOptions:MRSaveWithoutOptions completion:^(BOOL success, NSError *error) {
             
             //NSLog(@"Staff first name = %@", [Staff findFirst].first_name);
             NSLog(@"Actionplan  = %ld", (unsigned long)[Actionplan findAll].count);
             NSLog(@"Actionplan action = %@", [Actionplan findFirst].action);
             NSLog(@"Actionplan summary = %@", [Actionplan findFirst].summary);
             NSLog(@"Actionplan status = %@", [Actionplan findFirst].status);

             actionPlanGetSuccess(true);
             
         }];
         
         actionPlanGetSuccess(true);
     }
     
         failure:
     ^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Dealer staff Response Not connected");
         actionPlanGetSuccess(false);
     }];

}


#pragma -
#pragma POST API calls

+ (void)staffPostAPICall:(NSNumber*)dealerID withStaffDictionary:(NSDictionary*)staffDicParam withSuccessCallBack:(StaffPostAPICall)staffPostSuccess{

    NSString *uRLString = [NSString stringWithFormat:@"http://trackoem2.cloudapp.net/v1.0/app/dealers/%@/dealerStaff",dealerID];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
    
    [manager.requestSerializer setValue:token forHTTPHeaderField:@"Authorization"];
    
    [manager POST:uRLString parameters:staffDicParam
          success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         staffPostSuccess(true);
     }
     
          failure:
     ^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Dealer staff Response Not connected");
         staffPostSuccess(false);
     }];
}

#pragma -
#pragma PUT API calls

+ (void)staffPUTAPICall:(NSNumber*)dealerID withStaffID:(NSNumber*)staffID withStaffDictionary:(NSDictionary*)staffDicParam withSuccessCallBack:(StaffPUTAPICall)staffPutSuccess{

    NSString *uRLString = [NSString stringWithFormat:@"http://trackoem2.cloudapp.net/v1.0/app/dealers/%@/dealerStaff/%@",dealerID,staffID];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
    
    [manager.requestSerializer setValue:token forHTTPHeaderField:@"Authorization"];
    
    [manager PUT:uRLString parameters:staffDicParam
         success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         staffPutSuccess(true);
     }
     
         failure:
     ^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Dealer staff Response Not connected");
         staffPutSuccess(false);
     }];
}

+ (void)staffPUTDeleteAPICall:(NSNumber*)dealerID withStaffID:(NSNumber*)staffID withSuccessCallBack:(staffPUTDeleteAPICall)staffPutDeleteSuccess
{
    NSString *uRLString = [NSString stringWithFormat:@"http://trackoem2.cloudapp.net/v1.0/app/dealers/%@/dealerStaff/%@",dealerID,staffID];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
    
    [manager.requestSerializer setValue:token forHTTPHeaderField:@"Authorization"];
    
    [manager DELETE:uRLString parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"Staff Delete: response object = %@", responseObject);
         staffPutDeleteSuccess(true);
     }
         failure:
     ^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Staff Delete not successful");
         staffPutDeleteSuccess(false);
     }];
}


@end
