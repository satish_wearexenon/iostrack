//
//  NetworkingFactory.h
//  TrackiOS
//
//  Created by abhijeet upadhyay on 04/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Dealer.h"

typedef void (^BaseURLCall)(BOOL) ;
typedef void (^LoginServiceCall)(BOOL) ;
typedef void(^ConfigServiceCall) (BOOL);
typedef void(^EnterpriseConfigServiceCall) (BOOL);
typedef void(^DealerListingServiceCall) (BOOL);
typedef void(^DealerSummaryServiceCall) (BOOL) ;
typedef void(^DealerStaffListServiceCall) (BOOL) ;
typedef void (^CheckListServiceCall)(BOOL) ;
typedef void(^ChangePassword) (BOOL) ;
typedef void(^LogOut) (BOOL) ;
typedef void(^StaffRolesServiceCall) (BOOL) ;
typedef void(^StaffPostAPICall) (BOOL);
typedef void(^StaffPUTAPICall) (BOOL);
typedef void(^staffPUTDeleteAPICall) (BOOL);
typedef void(^ActionPlanGETAPICall) (BOOL);



@interface NetworkingFactory : NSObject{


}


/**
 Singleton Access for Networking single point access
 */

+ (id)networkingSharedmanager;

/**
 Base URL API service call with success block call back.
 */

+ (void)getBaseURLforOEMcall:(NSString*)eKey withSuccess:(BaseURLCall)urlSuccess;

/**
 Login API service call with success block call back.
 */
+ (void)loginServiceCall:(NSString*)userName withPassword:(NSString*)password withSuccessBlock:(LoginServiceCall)loginSuccess;

/**
 After login config API service call with success block call back.
 */
+ (void)configServiceCall:(ConfigServiceCall)configSuccess;

/**
 Enterprise config (befire login) service API call with success call back.
 */
+ (void)enterpriseConfigServiceCall:(NSString*)eKey withSuccess:(EnterpriseConfigServiceCall)configSuccess;

/**
 Dealer Listing API call with success call back.
 */
+ (void)dealerListingServiceCall:(DealerListingServiceCall)dealerListingSuccess;

/**
 Dealer Summary API call with success call back.
 */
+ (void)dealerSummaryServiceCall:(NSNumber*)dealerID withSuccessBlock:(DealerSummaryServiceCall)summarySuccess;

/**
 Dealer Staff Listing API call with success call back.
 */
+ (void)dealerStaffListServiceCall:(Dealer*)dealer withSuccessBlock:(DealerStaffListServiceCall)staffSuccess;

/**
 Change Password API call with success call back.
 */
+ (void)changePasswordFromOldPaddword:(NSString*)oldPasword toNewPassword:(NSString*)newPassword withSuccess:(ChangePassword)success;

/**
Logout API call with success call back.
*/

+ (void)logOutWithSuccess:(LogOut)success;

/**
 Staff Roles API call with success call back.
 */
+ (void)staffRoleAPICall:(StaffRolesServiceCall)staffRolesSuccess;

/**
 Staff POST API call with success call back.
 */
+ (void)staffPostAPICall:(NSNumber*)dealerID withStaffDictionary:(NSDictionary*)staffDicParam withSuccessCallBack:(StaffPostAPICall)staffPostSuccess;

/**
 Staff PUT API call with success call back.
 */
+ (void)staffPUTAPICall:(NSNumber*)dealerID withStaffID:(NSNumber*)staffID withStaffDictionary:(NSDictionary*)staffDicParam withSuccessCallBack:(StaffPUTAPICall)staffPutSuccess;

/**
 Staff PUT API call with success call back.
 */
+ (void)staffPUTDeleteAPICall:(NSNumber*)dealerID withStaffID:(NSNumber*)staffID withSuccessCallBack:(StaffPUTAPICall)staffPutSuccess;

/**
 Actionplan GET API call with success call back.
 */
+ (void)actionPlanGETAPICall:(NSNumber*)dealerID withSuccessCallBack:(ActionPlanGETAPICall)actionPlanGetSuccess;


@end
