//
//  TLibraryPreLaunchViewController.m
//  TrackiOS
//
//  Created by Anish Kumar on 15/10/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "TLibraryPreLaunchViewController.h"
#import "TTLibraryCell.h"
#import "QSPDFPreviewViewController.h"

@interface TLibraryPreLaunchViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong) QSPDFPreviewViewController *pdfPreviewVC;


@end

@implementation TLibraryPreLaunchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.view setBackgroundColor:ViewBackgroundColor];

//
//    MainViewController *parentVC = [APP_DELEGATE appBaseViewController];
//    parentVC.navLabel.text = @"Pre Launch";
    
    [_tableView setDataSource:self];
    [_tableView setDelegate:self];
    [_tableView setBackgroundColor:[UIColor clearColor]];
    [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [_tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidAppear:(BOOL)animated
{
    MainViewController *parentVC = [APP_DELEGATE appBaseViewController];
    parentVC.navLabel.text = @"Pre Launch";
    
    //parentVC.backView.hidden = YES;
    parentVC.backButton.hidden = YES;
    parentVC.navLabel.hidden = NO;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - UItableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    return 153;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  
       TTLibraryCell *celllib = (TTLibraryCell*)[tableView dequeueReusableCellWithIdentifier:@"TTLibraryCell"];
   
       [celllib setBackgroundColor:[UIColor clearColor]];
       [celllib setSelectionStyle:UITableViewCellSelectionStyleNone];

       celllib.titleLabel.text = @"Guideline: People";
       celllib.detailLabel.text = @"Guideline for checklist....";
       celllib.dateTimeLabel.text = @"2014/12/10 14.56";

      return celllib;
  
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
 
    NSLog(@"didDeselectRowAtIndexPath");
    
    NSMutableString *url = [[NSMutableString alloc] init];
//    [url appendString:@"http://preqobalt.qorosauto.com/GetServiceImage"];
//    [url appendFormat:@"/%@",_empId];
//    [url appendFormat:@"/%@",_appDelegate.strCustomerNo];
//    [url appendFormat:@"/%@",_appDelegate.serviceOrderNumber];
//    [url appendString:@"/CL01"];
//    [url appendString:@"/pdf"];
//    [url appendFormat:@"/%@",_appDelegate.serviceOrderNumber];
//    [url appendString:@"_CL01.pdf"];
    //http://developer.apple.com/iphone/library/documentation/UIKit/Reference/UIWebView_Class/UIWebView_Class.pdf
     [url appendString:@"http://developer.apple.com/iphone/library/documentation/UIKit/Reference/UIWebView_Class/UIWebView_Class.pdf"];
    
    
    _pdfPreviewVC = [[QSPDFPreviewViewController alloc] initWithNibName:@"QSPDFPreviewViewController" bundle:nil];
    
    _pdfPreviewVC.url = url;
    
    [APP_DELEGATE.customSplitViewController presentViewController:_pdfPreviewVC animated:YES completion:nil];
}

- (IBAction)tableCellButtonTapped:(id)sender
{
}
@end
