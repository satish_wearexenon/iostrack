//
//  TLibraryImplementationViewController.m
//  TrackiOS
//
//  Created by Anish Kumar on 15/10/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "TLibraryImplementationViewController.h"

@interface TLibraryImplementationViewController ()

@end

@implementation TLibraryImplementationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    MainViewController *parentVC = [APP_DELEGATE appBaseViewController];
//    parentVC.navLabel.text = @"Implementation";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidAppear:(BOOL)animated
{
    MainViewController *parentVC = [APP_DELEGATE appBaseViewController];
    parentVC.navLabel.text = @"Implementation";
    
    //parentVC.backView.hidden = YES;
    parentVC.backButton.hidden = YES;
    parentVC.navLabel.hidden = NO;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
