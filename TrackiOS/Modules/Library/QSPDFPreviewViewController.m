//
//  QSPDFPreviewViewController.m
//  Qobalt
//
//  Created by satish sharma on 6/25/14.
//  Copyright (c) 2014 Satish Kumar. All rights reserved.
//

#import "QSPDFPreviewViewController.h"
#import "QSActivityIndicator.h"

@interface QSPDFPreviewViewController ()

@property (nonatomic, strong) UIActivityIndicatorView *indicator;
@property (nonatomic, strong) UIView *background;

@end

@implementation QSPDFPreviewViewController

static QSActivityIndicator *sharedIndicator = nil;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    
    _PrintButton.enabled = NO;
    _PrintButton.hidden = YES;
    
    _emailButton.enabled = NO;
    _emailButton.hidden = YES;
    
   // UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(10, 10, 200, 200)];
    //http://preqobalt.qorosauto.com/GetServiceImage/EMP000005/CU000253/SO001618/CL01/pdf/SO001618_CL01.pdf
    
    //http://developer.apple.com/iphone/library/documentation/UIKit/Reference/UIWebView_Class/UIWebView_Class.pdf
    
}

-(void) viewWillAppear:(BOOL)animated
{
    [_cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [_cancelButton.titleLabel setTextColor:CustomTextColor];
    [_cancelButton.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:14]];
    [_cancelButton setBackgroundImage:[UIImage imageNamed:@"cancel_button"] forState:UIControlStateNormal];
}

-(void)viewDidAppear:(BOOL)animated
{
    //[QSActivityIndicator start];
    
    NSLog(@"\nURL To Open PDF: %@",_url);
    
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@",_url]];
    NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    _webView.delegate = self;
   // [_webView addSubview:activity];
    //_webView.backgroundColor = [UIColor blackColor];
    [_webView loadRequest:request];
    
   // [QSActivityIndicator start];
    
    [self addActivityIndicator];
}

-(void)webViewDidStartLoad:(UIWebView *)webView
{
   // [QSActivityIndicator start];
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
  // [QSActivityIndicator end];
     [self.indicator stopAnimating];
     [self.background removeFromSuperview];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    //
    [self.indicator stopAnimating];
    
    [self.background removeFromSuperview];
}
- (IBAction)cancelPreview:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)addActivityIndicator
{
 //   self.view.frame = CGRectMake(0, 0, _appDelegate.window.frame.size.width, _appDelegate.window.frame.size.height);
    
   // [self.view setBackgroundColor:[UIColor clearColor]];
    
    _background = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 - 80/2, self.view.frame.size.height/2 - 80/2, 80, 80)];
    [_background setBackgroundColor:[UIColor blackColor]];
    [_background setAlpha:0.6];
    [_background setClipsToBounds:YES];
    [[_background layer] setCornerRadius:10.0f];
    [self.view addSubview:_background];
    
    _indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [_indicator setFrame:CGRectMake(self.view.frame.size.width/2 - 30/2, self.view.frame.size.height/2 - 30/2, 30, 30)];
     [_indicator startAnimating];
    
    [self.view addSubview:_indicator];
}

- (IBAction)PrintPDF:(id)sender
{
    
}

- (IBAction)EmailPDF:(id)sender
{
  //  [_appDelegate sendEmail:@"" cc:@"" subject:[NSString stringWithFormat:@"%@",LOCALIZED_VEHICLE_HEALTH_CHECK] body:kMailBody attachment:_url];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
