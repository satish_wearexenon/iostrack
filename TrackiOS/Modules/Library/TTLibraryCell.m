//
//  TTLibraryCell.m
//  TrackiOS
//
//  Created by satish sharma on 12/9/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "TTLibraryCell.h"

@implementation TTLibraryCell

- (void)awakeFromNib {
    // Initialization code

    [self.contentView setBackgroundColor:ViewBackgroundColor];
    
    [_titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:23]];
    [_detailLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    
    [_dateTimeLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    [_detailLabel setNumberOfLines:3];
    
    //set color
    _titleLabel.textColor = DarkGrayTextColor;
    _detailLabel.textColor = LightGrayTextColor;
    _dateTimeLabel.textColor = CustomTextColor;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
