//
//  TLibraryPreLaunchViewController.h
//  TrackiOS
//
//  Created by Anish Kumar on 15/10/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TLibraryPreLaunchViewController : UIViewController

- (IBAction)tableCellButtonTapped:(id)sender;

@end
