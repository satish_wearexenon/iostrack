//
//  QSPDFPreviewViewController.h
//  Qobalt
//
//  Created by satish sharma on 6/25/14.
//  Copyright (c) 2014 Satish Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QSPDFPreviewViewController : UIViewController <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

@property (weak, nonatomic) IBOutlet UIButton *PrintButton;

@property (weak, nonatomic) IBOutlet UIButton *emailButton;

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (strong, nonatomic)  NSMutableString *url;





- (IBAction)cancelPreview:(id)sender;

- (IBAction)PrintPDF:(id)sender;

- (IBAction)EmailPDF:(id)sender;



@end

