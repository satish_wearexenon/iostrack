//
//  ChecklistColorTableViewCell.h
//  TrackiOS
//
//  Created by Anish Kumar on 14/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChecklistColorTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *colorImageView;

@end
