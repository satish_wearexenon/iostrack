//
//  PhotoDisplayViewController.m
//  TrackiOS
//
//  Created by John Paul Ranjith on 08/12/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "PhotoDisplayViewController.h"
#import "PageViewController.h"

@interface PhotoDisplayViewController ()

@end

@implementation PhotoDisplayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.imageArray = [[NSMutableArray alloc] init];
    [self.imageArray addObject:[UIImage imageNamed:@"defaultCarImage.png"]];
    [self.imageArray addObject:[UIImage imageNamed:@"defaultCarImage.png"]];
    [self.imageArray addObject:[UIImage imageNamed:@"defaultCarImage.png"]];
    [self.imageArray addObject:[UIImage imageNamed:@"defaultCarImage.png"]];
    [self.imageArray addObject:[UIImage imageNamed:@"defaultCarImage.png"]];
    
    self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    
    self.pageController.dataSource = self;
    [[self.pageController view] setFrame:[[self view] bounds]];
    
    PageViewController *initialViewController = [self viewControllerAtIndex:0];
    
    NSArray *viewControllers = [NSArray arrayWithObject:initialViewController];
    
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    [self addChildViewController:self.pageController];
    [[self view] addSubview:[self.pageController view]];
    [self.pageController didMoveToParentViewController:self];

    
}


- (PageViewController *)viewControllerAtIndex:(NSUInteger)index {
   
    PageViewController *childViewController = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"PageViewController"];

    childViewController.imageObj = [self.imageArray objectAtIndex:index];
    childViewController.pageNo = [NSString stringWithFormat:@"%lu/%lu",(unsigned long)index+1,(unsigned long)[self.imageArray count]];
    childViewController.index = index;
    
    return childViewController;
    
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(PageViewController *)viewController index];
    
    if (index == 0) {
        return nil;
    }
    
    // Decrease the index by 1 to return
    index--;
    
    return [self viewControllerAtIndex:index];
    
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(PageViewController *)viewController index];
    
    index++;
    
    if (index == 5) {
        return nil;
    }
    
    return [self viewControllerAtIndex:index];
    
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    // The number of items reflected in the page indicator.
    return 5;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    // The selected item reflected in the page indicator.
    return 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
