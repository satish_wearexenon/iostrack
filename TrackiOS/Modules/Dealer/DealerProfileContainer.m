//
//  DealerProfileContainer.m
//  TrackiOS
//
//  Created by satish sharma on 11/4/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "DealerProfileContainer.h"

#define SegueIdentifierFirst  @"summaryEmbed"
#define SegueIdentifierSecond @"actionPlanEmbed"
#define SegueIdentifierThird  @"bestPracticeEmbed"
#define SegueIdentifierFour   @"dealerInfoEmbed"
#import "DealerSummaryViewController.h"
#import "DealerActionPlanViewController.h"
#import "DealerBestPracticeViewController.h"
#import "DealerInfoViewController.h"

@interface DealerProfileContainer ()
@property (strong, nonatomic) DealerSummaryViewController *summaryVC;
@property (strong, nonatomic) DealerActionPlanViewController *actionPlanVC;
@property (strong, nonatomic) DealerBestPracticeViewController *bestPracticeVC;
@property (strong, nonatomic) DealerInfoViewController *infoVC;
@end


@implementation DealerProfileContainer

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.currentSegueIdentifier = SegueIdentifierFirst;
    
    [self performSegueWithIdentifier:self.currentSegueIdentifier sender:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:SegueIdentifierFirst])
    {
        _summaryVC = segue.destinationViewController;
        _summaryVC.parent = _parent;
        
        if (self.childViewControllers.count > 0) {
        [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:segue.destinationViewController];
       
        }
        else {
        [self addChildViewController:segue.destinationViewController];
        ((UIViewController *)segue.destinationViewController).view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        [self.view addSubview:((UIViewController *)segue.destinationViewController).view];
        [segue.destinationViewController didMoveToParentViewController:self];
        }
    }
    else if ([segue.identifier isEqualToString:SegueIdentifierSecond])
    {
        _actionPlanVC = segue.destinationViewController;
        _actionPlanVC.parent = _parent;
        
        [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:segue.destinationViewController];
    }
    else if ([segue.identifier isEqualToString:SegueIdentifierThird])
    {
         _bestPracticeVC = segue.destinationViewController;
         _bestPracticeVC.parent = _parent;
        [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:segue.destinationViewController];
    }else if ([segue.identifier isEqualToString:SegueIdentifierFour])
    {
        _infoVC = segue.destinationViewController;
        _infoVC.selectedDealer = _selectedDealer;
        [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:segue.destinationViewController];
    }
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

-(void)swapFromViewController:(UIViewController*)fromViewController toViewController:(UIViewController*)toViewController{
    
    toViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    [fromViewController willMoveToParentViewController:nil];
    [self addChildViewController:toViewController];
    [self transitionFromViewController:fromViewController toViewController:toViewController duration:0.0 options:UIViewAnimationOptionTransitionCrossDissolve animations:nil completion:^(BOOL finished) {
        [fromViewController removeFromParentViewController];
        [toViewController didMoveToParentViewController:self];
    }];
    
}

-(void) swapViewController
{
   // self.currentSegueIdentifier = (self.currentSegueIdentifier == SegueIdentifierFirst) ? SegueIdentifierSecond : SegueIdentifierFirst;
    [self performSegueWithIdentifier:self.currentSegueIdentifier sender:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
