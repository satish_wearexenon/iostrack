//
//  DealerStageCell.m
//  TrackiOS
//
//  Created by satish sharma on 11/4/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "DealerStageCell.h"
#import "DealerExportPopOverVC.h"


@implementation DealerStageCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        
    }
    
    return self;
}

- (void)awakeFromNib {
    // Initialization code
    [self.contentView setBackgroundColor:ViewBackgroundColor];
    
    //set font for uicontrol
    [_dealerStageLabel setFont:[UIFont fontWithName:Font_Track_Regular size:23]];
    [_itemLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    [_noofPhotoLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    [_dateTimeLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    [_percentageLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    
    
    
    //set color for uicontrol
    _dealerStageLabel.textColor = DarkGrayTextColor;
    _itemLabel.textColor = LightGrayTextColor;
    _noofPhotoLabel.textColor = LightGrayTextColor;
    _dateTimeLabel.textColor = CustomTextColor;
    _percentageLabel.textColor = CustomTextColor;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)exportPDFButtonClick:(id)sender
{
    NSLog(@"DealerStageCell: exportPDFButtonClick");

    DealerExportPopOverVC *viewController = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"DealerExportPopOverVC"];

    _popVC = [[UIPopoverController alloc] initWithContentViewController:viewController];

    _popVC.popoverContentSize = CGSizeMake(189.0, 98.0);

    [_popVC presentPopoverFromRect:[(UIButton *)sender frame]  inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];

}



@end
