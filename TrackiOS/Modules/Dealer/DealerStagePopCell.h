//
//  DealerStagePopCell.h
//  TrackiOS
//
//  Created by satish sharma on 11/5/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 The DealerStageCell class is used to maintain the table view cell
 */
@interface DealerStagePopCell : UITableViewCell
/**
 Properties for the UI Controls
 */
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *checkImage;

@end
