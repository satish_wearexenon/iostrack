//
//  DataPopOverVC.h
//  TrackiOS
//
//  Created by John Paul Ranjith on 14/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol DataPopOverVCDelegate <NSObject>

-(void)dataSelectedCallBack:(id)data;

@end


/**
 The DataPopOverVC class is used to maintain a generic class 
 */
@interface DataPopOverVC : UIViewController
{
    
}
@property (strong, nonatomic) IBOutlet UITableView *popOverTable;

@property (nonatomic, strong)  NSMutableArray *dataArray;
@property (nonatomic, assign)  PopOverType viewType;


@property (nonatomic) id <DataPopOverVCDelegate> delegate;



@end
