//
//  DealerBestPracticeViewController.h
//  TrackiOS
//
//  Created by satish sharma on 11/4/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DealerDetailViewController.h"

/**
 The DealerBestPracticeViewController class is present the Dealer Best Practice view
 */
@interface DealerBestPracticeViewController : UIViewController  <UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong) UIPopoverController *popVC;

@property (strong, nonatomic) DealerDetailViewController *parent;

@end
