//
//  DealerCheckListTableViewCell.m
//  TrackiOS
//
//  Created by John Paul Ranjith on 04/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "DealerCheckListTableViewCell.h"

@implementation DealerCheckListTableViewCell

- (void)awakeFromNib
{
    [_contentLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    _contentLabel.textColor = DarkGrayTextColor;
    
    [_remarksBtn.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    [_photoBtn.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    [_bestPracticeBtn.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    [_referenceButton.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    
    [_remarksBtn setTitleColor:DarkGrayTextColor forState:UIControlStateNormal];
    [_photoBtn setTitleColor:DarkGrayTextColor forState:UIControlStateNormal];
    [_bestPracticeBtn setTitleColor:DarkGrayTextColor forState:UIControlStateNormal];
    [_referenceButton setTitleColor:DarkGrayTextColor forState:UIControlStateNormal];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
