//
//  DealerActionPlanPopover.m
//  TrackiOS
//
//  Created by satish sharma on 11/25/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "DealerActionPlanPopover.h"
#import "CheckListResponsiblePeopleVC.h"
#import "CalenderViewController.h"

@interface DealerActionPlanPopover () <UITextViewDelegate, checkListResponsiblePeopleDelegate, CalenderViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *responseScroll;

@property (strong, nonatomic) CheckListResponsiblePeopleVC *checkListResponsible;
@property (strong, nonatomic) CalenderViewController *calendarVC;

@property (strong, nonatomic) NSMutableArray *resPersonArray;

@end

@implementation DealerActionPlanPopover

- (void) viewDidAppear:(BOOL)animated
{
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void) viewWillAppear:(BOOL)animated
{
    [self.view setBackgroundColor:LightGrayBackgroundColor];
    
    [_titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:25.0f]];
    [_titleLabel setTextColor:DarkGrayTextColor];
    
    // set title text color
    [_implementationLabel setTextColor:LightGrayTextColor];
    [_situationLabel setTextColor:LightGrayTextColor];
    [_actionLabel setTextColor:LightGrayTextColor];
    [_outcomeLabel setTextColor:LightGrayTextColor];
    [_supportLabel setTextColor:LightGrayTextColor];
    [_resPersonLabel setTextColor:LightGrayTextColor];
    [_dueDateLabel setTextColor:LightGrayTextColor];
    
    //set font
    [_implementationLabel setFont:[UIFont fontWithName:Font_Track_Regular size:23.0f]];
    [_situationLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16.0f]];
    [_actionLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16.0f]];
    [_outcomeLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16.0f]];
    [_supportLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16.0f]];
    [_resPersonLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16.0f]];
    [_dueDateLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16.0f]];
    
    /*
     [_situationLabel setText:@""];
     [_actionLabel setText:@""];
     [_outcomeLabel setText:@""];
     [_supportLabel setText:@""];
     [_resPersonLabel setText:@""];
     [_dueDateLabel setText:@""];
     */
    
    //set textview text color
    [_situationTextview setTextColor:DarkGrayTextColor];
    [_actionTextview setTextColor:DarkGrayTextColor];
    [_outcomeTextview setTextColor:DarkGrayTextColor];
    [_supportTextview setTextColor:DarkGrayTextColor];
    [_dateLabel setTextColor:OrangeTextColor];
    
    [_implementationTextview setBackgroundColor:LightGrayBackgroundColor];
    [_situationTextview setBackgroundColor:LightGrayBackgroundColor];
    [_actionTextview setBackgroundColor:LightGrayBackgroundColor];
    [_outcomeTextview setBackgroundColor:LightGrayBackgroundColor];
    [_supportTextview setBackgroundColor:LightGrayBackgroundColor];
    [_dateLabel setBackgroundColor:LightGrayBackgroundColor];
    
//    [_implementationTextview setText:@""];
//    [_situationTextview setText:@""];
//    [_actionTextview setText:@""];
//    [_outcomeTextview setText:@""];
//    [_supportTextview setText:@""];
    
    [_implementationTextview setFont:[UIFont fontWithName:Font_Track_Regular size:16.0f]];
    [_situationTextview setFont:[UIFont fontWithName:Font_Track_Regular size:16.0f]];
    [_actionTextview setFont:[UIFont fontWithName:Font_Track_Regular size:16.0f]];
    [_outcomeTextview setFont:[UIFont fontWithName:Font_Track_Regular size:16.0f]];
    [_supportTextview setFont:[UIFont fontWithName:Font_Track_Regular size:16.0f]];
    [_dateLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16.0f]];
    
    [_implementationTextview setTextContainerInset:UIEdgeInsetsMake(0, 16, 0, 16)];
    [_situationTextview setTextContainerInset:UIEdgeInsetsMake(0, 16, 0, 16)];
    [_actionTextview setTextContainerInset:UIEdgeInsetsMake(0, 16, 0, 16)];
    [_outcomeTextview setTextContainerInset:UIEdgeInsetsMake(0, 16, 0, 16)];
    [_supportTextview setTextContainerInset:UIEdgeInsetsMake(0, 16, 0, 16)];
    // [_dateLabel setTextContainerInset:UIEdgeInsetsMake(0, 16, 0, 16)];
    
    
    [_situationTextview setDelegate:self];
    [_actionTextview setDelegate:self];
    [_outcomeTextview setDelegate:self];
    [_supportTextview setDelegate:self];
    
    // Do any additional setup after loading the view.
    [_viewChecklistBtn setTitleColor:CustomTextColor forState:UIControlStateNormal];
    [_viewChecklistBtn.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16.0f]];

    [_sendMailBtn setTitleColor:CustomTextColor forState:UIControlStateNormal];
    [_sendMailBtn.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16.0f]];

    [_saveBtn setTitleColor:CustomTextColor forState:UIControlStateNormal];
    [_saveBtn.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:25]];
    
    [_deleteButton setTitleColor:CustomTextColor forState:UIControlStateNormal];
    [_deleteButton.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:25]];
    
    [_completeButton setTitleColor:CustomTextColor forState:UIControlStateNormal];
    [_completeButton.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:25]];
    
    [_cancelBtn setTitleColor:CustomTextColor forState:UIControlStateNormal];
    [_cancelBtn.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:25]];
    
    [_sendMailBtn.imageView setFrame:CGRectMake(3, 2, 20, 20)];
    
    [_saveBtn setBackgroundImage:[UIImage imageNamed:@"custom_button_bg"] forState:UIControlStateNormal];
    //[_saveBtn setBackgroundImage:[UIImage imageNamed:@"01_login_button_press"] forState:UIControlStateSelected];
    
}

/**
 Method to cancel the edited data
 */
-(IBAction)cancelAction:(id)sender
{
    [self.delegate parentCallBack:nil];
    
}
/**
 Method to save the edited data
 */
-(IBAction)saveAction:(id)sender
{
    TRAlertView *av = [[TRAlertView alloc] initWithTitle:@"Info"
                                                 message:@"Action plan saved successfully."
                                                delegate:self
                                                  layout:eAlertTypeWithOk
                                           containsEmail:NO
                                                 dateStr:nil
                                       cancelButtonTitle:nil
                                       otherButtonTitles:@"OK", nil];
    [av show];
    [self.delegate parentCallBack:nil];
}

/**
 Method to complete the edited data
 */
-(IBAction)completeAction:(id)sender
{
    
}

/**
 Method to delete the edited data
 */
-(IBAction)deleteAction:(id)sender
{
    
}

/**
 Method to send Email the edited data
 */
-(IBAction)sendMailAction:(id)sender
{
   // [APP_DELEGATE sendEMail];
}

/**
 Method to viewc Check list the edited data
 */
-(IBAction)viewCheckListAction:(id)sender
{
    [self.delegate viewCheckListCallback:sender];
}

-(void)setdata:(Actionplan*)actionpln;
{
    self.situationTextview.text = actionpln.situation;
    self.actionTextview.text = actionpln.action;
    self.outcomeTextview.text = actionpln.desired_outcome;
    self.supportTextview.text = actionpln.support;
    self.dateLabel.text = actionpln.due_date;

}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"checkListResponsiblePeopleVC"]) {
        
        _checkListResponsible = [segue destinationViewController];
        _checkListResponsible.delegate = self;
    }else if ([segue.identifier isEqualToString:@"calenderViewController"])
    {
        _calendarVC = [segue destinationViewController];
        _calendarVC.delegate = self;
        _calendarVC.titleString = LOCALIZED_CALENDAR_DUE_DATE;
    }
}

/**
 Delegate Method to add responsible Person
 */
- (void) addNumberOfResponsiblePerson:(NSArray *)personArray
{
    _resPersonArray = (NSMutableArray *)personArray;
    NSLog(@"personArray: %@",personArray);
    
    _responseScroll.contentSize = CGSizeMake(([personArray count]*100), 40);
    _responseScroll.showsVerticalScrollIndicator = NO;
    _responseScroll.showsHorizontalScrollIndicator =NO;
    
    [self removeAllSubviews];
    
    for (int i =0; i<[personArray count]; i++) {
        UIButton *personButton;
        personButton = [UIButton buttonWithType:UIButtonTypeCustom];
        personButton.frame = CGRectMake(20 + 102*i, 5, 100, 30);
        personButton.tag = (i+111);
        [personButton setTitle:[personArray objectAtIndex:i] forState:UIControlStateNormal];
        [personButton setBackgroundImage:[UIImage imageNamed:@"shadow_btn_bg"] forState:UIControlStateNormal];
        [personButton addTarget:self action:@selector(deleteResponsiblePerson:) forControlEvents:UIControlEventTouchUpInside];
        [_responseScroll addSubview:personButton];
    }
    
    NSLog(@"addNumberOfResponsiblePerson");
}

-(void) deleteResponsiblePerson:(id) sender
{
    UIButton *btn = (UIButton *)sender;
    
    [self removeAllSubviews];
    
    if (btn.tag>=0) {
        [_resPersonArray removeObjectAtIndex:(btn.tag - 111)];
        
    }
    
    NSLog(@"addNumberOfResponsiblePerson");
    
    [self addNumberOfResponsiblePerson:_resPersonArray];
    
}

- (void) removeAllSubviews
{
    NSArray *array = [_responseScroll subviews];
    
    for (int i = 0; i< [array count]; i++) {
        
        if ([[array objectAtIndex:i] isKindOfClass:[UIButton class]]) {
            UIButton *btn1 = [array objectAtIndex:i];
            [btn1 removeFromSuperview];
        }
        
    }
    
}

//UITextview delegate methods
- (void)textViewDidBeginEditing:(UITextView *)textView {
    
    NSLog(@"textViewDidBeginEditing");
}

//UITextview delegate methods
- (void)textViewDidEndEditing:(UITextView *)textView{
    
    NSLog(@"textViewDidEndEditing %@",textView.text);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSUInteger newLength = [textView.text length] + [text length] - range.length;
    return (newLength > 300) ? NO : YES;
}

- (void) didDateSelected:(NSString *) date
{
    _dateLabel.text = date;
}


@end
