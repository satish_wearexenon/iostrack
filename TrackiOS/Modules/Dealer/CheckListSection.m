//
//  CheckListSection.m
//  CorePlot-CocoaTouch
//
//  Created by John Paul Ranjith on 05/12/14.
//
//

#import "CheckListSection.h"
#import "CheckListRow.h"


@implementation CheckListSection

@dynamic mainSectionID;
@dynamic subSectionID;
@dynamic rowID;
@dynamic checkListID;
@dynamic mainSectionName;
@dynamic subSectionName;
@dynamic checkListRow;

@end
