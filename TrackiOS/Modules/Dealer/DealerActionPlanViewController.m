//
//  DealerActionPlanViewController.m
//  TrackiOS
//
//  Created by satish sharma on 11/4/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "DealerActionPlanViewController.h"
#import "DealerActionPlanCell.h"
#import "DealerActionPlanPopover.h"
#import "DealerChecklistViewController.h"
#import "Actionplan.h"

@interface DealerActionPlanViewController ()  <DealerActionPlanPopoverDelegate>

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;
@property (weak, nonatomic) IBOutlet UIButton *exportButton;

@property (strong, nonatomic) IBOutlet UITableView *actionListTableView;

@property (strong, nonatomic) NSMutableArray *actionPlanList;

- (IBAction)exportPDFButtonClick:(id)sender;

- (IBAction)tableButtonTapped:(id)sender;

@end

@implementation DealerActionPlanViewController

- (void) viewDidAppear:(BOOL)animated
{
    // Listen to back Notification Handler
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(backPressedLocalAction)  name:@"backPressed" object:nil];
}

- (void) viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"backPressed" object:nil];
}

- (void) backPressedLocalAction
{
    MainViewController *parentVC = [APP_DELEGATE appBaseViewController];
    [parentVC proceedBack];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated
{


    _actionPlanList = (NSMutableArray*)[[Actionplan findAll] mutableCopy];

    
    [self.view setBackgroundColor:ViewBackgroundColor];
    [_actionListTableView setBackgroundColor:ViewBackgroundColor];
    [_actionListTableView setDelegate:self];
    [_actionListTableView setDataSource:self];
    [_actionListTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [_nameLabel setFont:[UIFont fontWithName:Font_Track_Regular size:23]];
    [_detailLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    _nameLabel.textColor = DarkGrayTextColor;
    _detailLabel.textColor = LightGrayTextColor;
    
    //_nameLabel.text = @"name";
    //_detailLabel.text = @"details";
    
    

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_actionPlanList count];
    //return 10;

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 153.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"DealerActionPlanCell";
    
    DealerActionPlanCell *cell = (DealerActionPlanCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[DealerActionPlanCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    //set value
    
    Actionplan *actionpln = [_actionPlanList objectAtIndex:indexPath.row];
    
    cell.titleLabel.text = actionpln.situation;
    cell.detailLabel.text = actionpln.action;
    cell.stageLabel.text = actionpln.checklist_name;
    cell.dateLabel.text = actionpln.due_date;
    cell.exportButton.tag = indexPath.row;

    return cell;
    // Configure the cell...
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)tableButtonTapped:(id)sender
{
    UIButton *button = (UIButton*)sender;
    
    NSLog(@"DealerActionPlanCell: exportPDFButtonClick");
    CGRect positionRect;
    
    DealerActionPlanPopover *viewController = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"DealerActionPlanPopover"];
    viewController.delegate= self;
   
    
    positionRect = CGRectMake(self.view.frame.origin.x - 20, 30, 675, 928);
    _popVC = [[UIPopoverController alloc] initWithContentViewController:viewController];
    
    _popVC.popoverContentSize = CGSizeMake(675, 928);
    [_popVC presentPopoverFromRect:positionRect  inView:self.view permittedArrowDirections:0 animated:YES];
    
     Actionplan *actionpln = [_actionPlanList objectAtIndex:button.tag];
     [viewController setdata:actionpln];
}

-(void)parentCallBack:(id)data
{
    [_popVC dismissPopoverAnimated:YES];
}

//DealerActionPlanCell

- (IBAction)exportPDFButtonClick:(id)sender
{
    
}

- (IBAction)checkMarkButtonClick:(id)sender
{
    
}

-(void)viewCheckListCallback:(id) sender
{
    NSLog(@"DealerBestPracticeViewController: viewCheckListCallback");
    
    [_popVC dismissPopoverAnimated:YES];
    
    DealerChecklistViewController *viewController = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"DealerChecklistViewController"];
    
    //MainViewController *parentVC = [APP_DELEGATE appBaseViewController];
    
    [_parent addChildViewController:viewController];
    [viewController didMoveToParentViewController:_parent];
    [_parent.view addSubview:viewController.view];
    
}

@end
