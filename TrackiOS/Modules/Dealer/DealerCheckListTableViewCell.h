//
//  DealerCheckListTableViewCell.h
//  TrackiOS
//
//  Created by John Paul Ranjith on 04/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 The DealerCheckListTableViewCell class is used to maintain the table view cell
 */
@interface DealerCheckListTableViewCell : UITableViewCell
{
    
}

/**
 This property holds a reference of check list table cell
 */
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UIButton *remarksBtn;
@property (weak, nonatomic) IBOutlet UIButton *photoBtn;
@property (weak, nonatomic) IBOutlet UIButton *bestPracticeBtn;
@property (weak, nonatomic) IBOutlet UIButton *referenceButton;
@property (weak, nonatomic) IBOutlet UIButton *colorSelectorBtn;
@property (weak, nonatomic) IBOutlet UIButton *actionPlanBtn;

@property (nonatomic, strong)  NSIndexPath *index;

@end
