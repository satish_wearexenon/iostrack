//
//  DataPopOverCell.h
//  TrackiOS
//
//  Created by John Paul Ranjith on 04/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 The DataPopOverCell class is used to maintain the table view cell
 */
@interface DataPopOverCell : UITableViewCell
{
    
}
@property (weak, nonatomic) IBOutlet UILabel *dataLabel;

@end
