//
//  CapturePhotoVCViewController.m
//  TrackiOS
//
//  Created by John Paul Ranjith on 14/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "CapturePhotoVCViewController.h"
#import "DataPopOverVC.h"




@interface CapturePhotoVCViewController ()

@end

@implementation CapturePhotoVCViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)showCameraView:(NSString*)data
{
    self.picker = [[UIImagePickerController alloc] init];
    self.picker.allowsEditing = YES;

    self.picker.modalPresentationStyle = UIModalPresentationFullScreen;
    
    self.picker.delegate = self;
    
    NSLog(@"selected data %@",data);
    
    if([data isEqualToString:@"Use Camera"])
    {
        self.picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    else
    {
        self.picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    }
    
    NSLog(@"parent %@",NSStringFromClass([self.view class]));
    
    [self.parentVC presentViewController:self.picker animated:YES completion:NULL];

}




- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo 
{
    NSLog(@"tes");
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    [_delegate capturedPhoto:[info objectForKey:@"UIImagePickerControllerOriginalImage"]];
    [self.picker dismissViewControllerAnimated:YES completion:nil];
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self.picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
