//
//  DealerBestPracticePopover.m
//  TrackiOS
//
//  Created by satish sharma on 11/26/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "DealerBestPracticePopover.h"

@interface DealerBestPracticePopover ()
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UILabel *implementationLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;

@end

@implementation DealerBestPracticePopover

- (void) viewDidAppear:(BOOL)animated
{
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = LightGrayBackgroundColor;
    
    [_cancelBtn setTitleColor:CustomTextColor forState:UIControlStateNormal];
    [_saveBtn setTitleColor:CustomTextColor forState:UIControlStateNormal];
    [_viewChecklistBtn setTitleColor:CustomTextColor forState:UIControlStateNormal];

     _headerLabel.textColor = DarkGrayTextColor;
    [_headerLabel setFont:[UIFont fontWithName:Font_Track_Regular size:25]];

    [_implementationLabel setFont:[UIFont fontWithName:Font_Track_Regular size:23.0f]];
    [_titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16.0f]];
    [_descLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16.0f]];

    [_implementationTextView setFont:[UIFont fontWithName:Font_Track_Regular size:16.0f]];
    [_descTextView setFont:[UIFont fontWithName:Font_Track_Regular size:16.0f]];
    [_titleTextView setFont:[UIFont fontWithName:Font_Track_Regular size:16.0f]];

    [_implementationLabel setTextColor:LightGrayTextColor];
    [_titleLabel setTextColor:LightGrayTextColor];
    [_descLabel setTextColor:LightGrayTextColor];
    
    [_cancelBtn.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:25]];
    [_saveBtn.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    [_viewChecklistBtn.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    
    
     _implementationTextView.layer.borderWidth = 1.0;
     _implementationTextView.layer.borderColor = [[UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:0.5] CGColor];
     _implementationTextView.textContainerInset = UIEdgeInsetsMake(10.0, 20.0, 10.0, 20.0);

    _titleTextView.layer.borderWidth = 1.0;
    _titleTextView.layer.borderColor = [[UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:0.5] CGColor];
    _titleTextView.textContainerInset = UIEdgeInsetsMake(10.0, 20.0, 10.0, 20.0);
    
    _descTextView.layer.borderWidth = 1.0;
    _descTextView.layer.borderColor = [[UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:0.5] CGColor];
    _descTextView.textContainerInset = UIEdgeInsetsMake(10.0, 20.0, 10.0, 20.0);
    
    _deletePhotoBtn1.hidden = YES;
    _deletePhotoBtn2.hidden = YES;
    _deletePhotoBtn3.hidden = YES;
    _deletePhotoBtn4.hidden = YES;
    _deletePhotoBtn5.hidden = YES;
}

//Method to save the edited data
-(IBAction)saveAction:(id)sender
{
    TRAlertView *av = [[TRAlertView alloc] initWithTitle:@"Info"
                                                 message:@"Best practice saved successfully."
                                                delegate:self
                                                  layout:eAlertTypeWithOk
                                           containsEmail:NO
                                                 dateStr:nil
                                       cancelButtonTitle:nil
                                       otherButtonTitles:@"OK", nil];
    [av show];
    [self.delegate parentCallBack:nil];
}


-(IBAction)deletePhotoAction:(id)sender
{
    [self disableDeleteBtn:sender];
}


//Method to cancel the edited data
-(IBAction)cancelAction:(id)sender
{
    [self.delegate parentCallBack:nil];
}

-(void)presentDataPopOver:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    
    selectedButton = btn;
    
    DataPopOverVC *dataPopOverVC = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"DataPopOverVC"];
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    [array addObject:@"Use Camera"];
    [array addObject:@"Choose from Album"];
    
    dataPopOverVC.delegate = self;
    
    _popVC = [[UIPopoverController alloc] initWithContentViewController:dataPopOverVC];
    
    _popVC.popoverContentSize = CGSizeMake(200, 100.0);
    
    CGRect positionRect = CGRectMake(btn.frame.origin.x, btn.frame.origin.y, btn.frame.size.width, btn.frame.size.height);
    
    [_popVC presentPopoverFromRect:positionRect  inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    
    dataPopOverVC.viewType = eCameraOptionPopOver;
    dataPopOverVC.dataArray = array;
    
}

-(void)dataSelectedCallBack:(id)data
{
    NSLog(@"selected data %@",data);
    
    NSString *selectedData = [NSString stringWithFormat:@"%@",data];
    
    photoVC = [[CapturePhotoVCViewController alloc] init];
    photoVC.delegate = self;
    photoVC.parentVC = self;
    [photoVC showCameraView:selectedData];
    [_popVC dismissPopoverAnimated:YES];
}

-(void)capturedPhoto:(UIImage*)imageData
{
    NSLog(@"captued...");
    [selectedButton setImage:imageData forState:UIControlStateNormal];
    [self enableDeleteBtn:selectedButton];
}

-(void) enableDeleteBtn:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    
    if(btn == _photoBtn1){
        _deletePhotoBtn1.hidden= NO;
        _photoBtn1.userInteractionEnabled = NO;
    }
    else if(btn == _photoBtn2)
    {
        _deletePhotoBtn2.hidden= NO;
        _photoBtn2.userInteractionEnabled = NO;
    }
    else if(btn == _photoBtn3)
    {
        _deletePhotoBtn3.hidden= NO;
        _photoBtn3.userInteractionEnabled = NO;
    }
    else if(btn == _photoBtn4)
    {
        _deletePhotoBtn4.hidden= NO;
        _photoBtn4.userInteractionEnabled = NO;
    }
    else if(btn == _photoBtn5)
    {
        _deletePhotoBtn5.hidden= NO;
        _photoBtn5.userInteractionEnabled = NO;
    }
}

-(void) disableDeleteBtn:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    
    if(btn == _deletePhotoBtn1){
        _deletePhotoBtn1.hidden= YES;
        [_photoBtn1 setImage:[UIImage imageNamed:@"CheckListAddPhotoBtn.png"] forState:UIControlStateNormal];
    }
    else if(btn == _deletePhotoBtn2){
        _deletePhotoBtn2.hidden= YES;
        [_photoBtn2 setImage:[UIImage imageNamed:@"CheckListAddPhotoBtn.png"] forState:UIControlStateNormal];
    }
    else if(btn == _deletePhotoBtn3){
        _deletePhotoBtn3.hidden= YES;
        [_photoBtn3 setImage:[UIImage imageNamed:@"CheckListAddPhotoBtn.png"] forState:UIControlStateNormal];
    }
    else if(btn == _deletePhotoBtn4){
        _deletePhotoBtn4.hidden= YES;
        [_photoBtn4 setImage:[UIImage imageNamed:@"CheckListAddPhotoBtn.png"] forState:UIControlStateNormal];
    }
    else if(btn == _deletePhotoBtn5){
        _deletePhotoBtn5.hidden= YES;
        [_photoBtn5 setImage:[UIImage imageNamed:@"CheckListAddPhotoBtn.png"] forState:UIControlStateNormal];
    }
}

/**
 Method to capture photo
 */
-(IBAction)capturePhotoAction:(id)sender
{
    NSLog(@"capturePhotoAction");
    [self presentDataPopOver:sender];
}

//Textview delegate method
- (void)textViewDidBeginEditing:(UITextView *)textView {
    
    NSLog(@"textViewDidBeginEditing");
}

//Textview delegate method
- (void)textViewDidEndEditing:(UITextView *)textView{
    
    NSLog(@"textViewDidEndEditing %@",textView.text);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
*/

/**
 Method to viewc Check list the edited data
 */
-(IBAction)viewCheckListAction:(id)sender
{
     NSLog(@"DealerBestPracticePopover: viewCheckListAction");
    
    [_delegate viewCheckListCallback:sender];
}
@end
