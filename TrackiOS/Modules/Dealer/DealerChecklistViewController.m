//
//  DealerChecklistViewController.m
//  TrackiOS
//
//  Created by John Paul Ranjith on 04/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "DealerChecklistViewController.h"
#import "DealerCheckListTableViewCell.h"
#import "CheckListRemarksVC.h"
#import "DealerExportPopOverVC.h"
#import "CheckListColorSelectorVC.h"
#import "CheckListActionPlanVC.h"
#import "CheckListRemarksHistoryVC.h"
#import "CheckListPhotoVC.h"
#import "CheckListBestPractiseVC.h"
#import "CheckListReferenceVC.h"

#import "CheckListRowData.h"
#import "CheckListSectionData.h"

#import "UIImage+ProportionalFill.h"
#import "UIImage+Tint.h"

#import "ChkListBestPractice.h"
#import "ChkListRemarks.h"

#import "CheckListSection.h"
#import "CheckListRow.h"


@interface DealerChecklistViewController ()
- (IBAction)tableButtonTapped:(id)sender;
@end

@implementation DealerChecklistViewController

- (void) backPressedLocalAction
{
    MainViewController *parentVC = [APP_DELEGATE appBaseViewController];
    [parentVC proceedBack];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    remark = [ChkListRemarks createEntity];
//    photoData = [ChkListPhoto createEntity];
//    bestPractData = [ChkListBestPractice createEntity];
//    actionPlanData = [ChkListActionPlan createEntity];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(backPressedLocalAction)  name:@"backPressed" object:nil];


    mainSectionArray = [[NSMutableArray alloc] init];
    
    MainViewController *parentVC = [APP_DELEGATE appBaseViewController];
    
    [parentVC.checkListUploadPdfButton addTarget:self action:@selector(checkListUploadPdfAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [parentVC.checkListViewGreenButton addTarget:self action:@selector(checkListViewButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [parentVC.checkListViewRedButton addTarget:self action:@selector(checkListViewButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [parentVC.checkListViewYellowButton addTarget:self action:@selector(checkListViewButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [parentVC.checkListViewGrayButton addTarget:self action:@selector(checkListViewButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    //Need to remove after api integration
    [self hardCodedDataNew];
    
    [self getMainSectionHeader];
    
    checkListGreenBtnCount  = 0;
    checkListYellowBtnCount = 0;
    checkListRedBtnCount    = 0;
    checkListGrayBtnCount   = 0;
    
    [self getCheckListBtnCount];
    
    //[self insertCheckListData];
    
    [self fetchCheckListData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//Method to initialize the default values
- (void) viewDidAppear:(BOOL)animated
{
    // Listen to back Notification Handler
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(backPressedLocalAction)  name:@"backPressed" object:nil];
    
    MainViewController *parentVC = [APP_DELEGATE appBaseViewController];
    
    parentViewControler = parentVC.currentDetailController;
    parentViewControlerTitle = parentVC.backButton.titleLabel.text;
    
    parentVC.navLabel.hidden = YES;
    parentVC.backView.hidden = NO;
    parentVC.backButton.hidden = NO;
    
    [parentVC.backButton setTitle:[@"CHECK LIST" uppercaseString] forState:UIControlStateNormal];
    
    parentVC.currentDetailController = self;
    
    parentVC.checkListViewGreenButton.hidden    = NO;
    parentVC.checkListViewRedButton.hidden      = NO;
    parentVC.checkListViewYellowButton.hidden   = NO;
    parentVC.checkListViewGrayButton.hidden     = NO;
    parentVC.checkListUploadPdfButton.hidden    = NO;
    
    [parentVC.checkListViewGreenButton setTitle:[NSString stringWithFormat:@"%ld",(long)checkListGreenBtnCount] forState:UIControlStateNormal];
    [parentVC.checkListViewRedButton setTitle:[NSString stringWithFormat:@"%ld",(long)checkListRedBtnCount] forState:UIControlStateNormal];
    [parentVC.checkListViewYellowButton setTitle:[NSString stringWithFormat:@"%ld",(long)checkListYellowBtnCount] forState:UIControlStateNormal];
    [parentVC.checkListViewGrayButton setTitle:[NSString stringWithFormat:@"%ld",(long)checkListGrayBtnCount] forState:UIControlStateNormal];
    
    parentVC.checkListViewGreenButton.backgroundColor   = CheckListGreenBtnColor
    parentVC.checkListViewRedButton.backgroundColor     = CheckListRedBtnColor
    parentVC.checkListViewYellowButton.backgroundColor  = CheckListYellowBtnColor
    parentVC.checkListViewGrayButton.backgroundColor    = CheckListGrayBtnColor
    
    [super viewDidAppear:animated];
}

//Method to remove unwanted objects
- (void) viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"backPressed" object:nil];
    
    MainViewController *parentVC = [APP_DELEGATE appBaseViewController];
    
    parentVC.currentDetailController = parentViewControler;
    //parentVC.backLabel.text = parentViewControlerTitle;
    [parentVC.backButton setTitle:parentViewControlerTitle forState:UIControlStateNormal];
    
    parentVC.checkListViewGreenButton.hidden    = YES;
    parentVC.checkListViewRedButton.hidden      = YES;
    parentVC.checkListViewYellowButton.hidden   = YES;
    parentVC.checkListViewGrayButton.hidden     = YES;
    parentVC.checkListUploadPdfButton.hidden    = YES;
    
    //Removing the targets while disappear
    [parentVC.checkListUploadPdfButton removeTarget:self action:@selector(checkListUploadPdfAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [parentVC.checkListViewGreenButton removeTarget:self action:@selector(checkListViewButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [parentVC.checkListViewRedButton removeTarget:self action:@selector(checkListViewButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [parentVC.checkListViewYellowButton removeTarget:self action:@selector(checkListViewButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [parentVC.checkListViewGrayButton removeTarget:self action:@selector(checkListViewButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [super viewDidDisappear:animated];
}

//Method to trigger different pop over controllers
- (void)checkListViewButtonAction:(UIButton *)inTappedBtn
{
    NSLog(@"checkListViewButtonAction");
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return UIDeviceOrientationIsPortrait(toInterfaceOrientation);
}

//Method to trigger upload pdf action
- (void)checkListUploadPdfAction:(UIButton *)inTappedBtn
{
    NSLog(@"checkListUploadPdfAction");
    
    DealerExportPopOverVC *viewController = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"DealerExportPopOverVC"];
    
    _popVC = [[UIPopoverController alloc] initWithContentViewController:viewController];
    
    _popVC.popoverContentSize = CGSizeMake(189.0, 98.0);
    
    CGRect positionRect = CGRectMake(inTappedBtn.frame.origin.x,inTappedBtn.frame.origin.y-85, inTappedBtn.frame.size.width, inTappedBtn.frame.size.height);
    
    [_popVC presentPopoverFromRect:positionRect inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

//Method to trigger different pop over controllers
- (IBAction)tableButtonTapped:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    
    CGRect buttonFrameInTableView = [btn convertRect:btn.bounds toView:self.checkListTable];
    NSIndexPath *path = [self.checkListTable indexPathForRowAtPoint:buttonFrameInTableView.origin];
    
    DealerCheckListTableViewCell *clickedCell = (DealerCheckListTableViewCell*)[self.checkListTable cellForRowAtIndexPath:path];
    //DealerCheckListTableViewCell *clickedCell = (DealerCheckListTableViewCell *)[[[sender superview] superview ]superview];
    
    NSLog(@"btn tag %ld",(long)[sender tag]);//btn.tag);
    
    NSLog(@"btn name %@",btn.titleLabel.text);
    
    CGRect positionRect;
    
    NSIndexPath *indexPath = [self.checkListTable indexPathForCell:clickedCell];
    
    CheckListSectionData *dataClass = [checkLstArray objectAtIndex:indexPath.section];
    NSMutableArray *array = dataClass.rowData;
    //CheckListRowData *rowData = [array objectAtIndex:btn.tag];
    CheckListRowData *rowData = [array objectAtIndex:indexPath.row];
    
    if(btn == clickedCell.remarksBtn)
    {
//        NSIndexPath *indexPath = [self.checkListTable indexPathForCell:clickedCell];
//        
//        CheckListSectionData *dataClass = [checkLstArray objectAtIndex:indexPath.section];
//        NSMutableArray *array = dataClass.rowData;
//        CheckListRowData *rowData = [array objectAtIndex:btn.tag];

        
        ChkListRemarks *sendingObj;
        
        for (ChkListRemarks *remData in [ChkListRemarks findAll]) {
            NSLog(@"remData.remarksStr %@-%@-%@-%@",remData.mainSectionID,remData.subSectionID,remData.rowID,remData.remarksStr);
            if(remData.mainSectionID == [NSNumber numberWithInt:dataClass.mainSection] && remData.subSectionID == [NSNumber numberWithInt:dataClass.subSection] && remData.rowID == [NSNumber numberWithInt:rowData.rowID] && remData.remarksStr != nil)
            {
                sendingObj = (ChkListRemarks*)remData;
                NSLog(@"sendingObj %@",sendingObj.remarksStr);
                break;
            }
        }
        if(sendingObj == nil){
            sendingObj = [ChkListRemarks createEntity];
            sendingObj.mainSectionID =[NSNumber numberWithInt:dataClass.mainSection];
            sendingObj.subSectionID =[NSNumber numberWithInt:dataClass.subSection];
            sendingObj.rowID =[NSNumber numberWithInt:rowData.rowID];
        }
        

        if(rowData.hasRemarkHistory)
        {
            CheckListRemarksHistoryVC *viewController = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"CheckListRemarksHistoryVC"];
            viewController.delegate= self;
            viewController.remarkData = sendingObj;
            
            positionRect = CGRectMake(self.view.frame.origin.x - 25, 90,675, 654);
            
            
            _popVC = [[UIPopoverController alloc] initWithContentViewController:viewController];
            
            [_popVC setDelegate:self];
            _popVC.popoverContentSize = CGSizeMake(675, 654);
            [_popVC presentPopoverFromRect:positionRect  inView:self.view permittedArrowDirections:0 animated:YES];
        }
        else
        {
           CheckListRemarksVC *viewController = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"CheckListRemarksVC"];
            
            viewController.delegate= self;
            viewController.mainSectionID = [NSNumber numberWithInteger:dataClass.mainSection];
            viewController.subSectionID = [NSNumber numberWithInteger:dataClass.subSection];
            viewController.rowID = [NSNumber numberWithInteger:rowData.rowID];
            viewController.remarkData = sendingObj;
            
            positionRect = CGRectMake(self.view.frame.origin.x - 25, 235, 675, 332);
            
            _popVC = [[UIPopoverController alloc] initWithContentViewController:viewController];
             [_popVC setDelegate:self];
            _popVC.popoverContentSize = CGSizeMake(675, 332);
            [_popVC presentPopoverFromRect:positionRect  inView:self.view permittedArrowDirections:0 animated:YES];
        }
    }
    if(btn == clickedCell.photoBtn)
    {
        
        ChkListPhoto *sendingObj;
        
        for (ChkListPhoto *remData in [ChkListPhoto findAll]) {
            //NSLog(@"remData.remarksStr %@-%@-%@-%@",remData.mainSectionID,remData.subSectionID,remData.rowID,remData.remarksStr);
            if(remData.mainSectionID == [NSNumber numberWithInt:dataClass.mainSection] && remData.subSectionID == [NSNumber numberWithInt:dataClass.subSection] && remData.rowID == [NSNumber numberWithInt:rowData.rowID])
            {
                sendingObj = (ChkListPhoto*)remData;
                //NSLog(@"sendingObj %@",sendingObj.remarksStr);
                break;
            }
        }
        if(sendingObj == nil){
            sendingObj = [ChkListPhoto createEntity];
            sendingObj.mainSectionID =[NSNumber numberWithInt:dataClass.mainSection];
            sendingObj.subSectionID =[NSNumber numberWithInt:dataClass.subSection];
            sendingObj.rowID =[NSNumber numberWithInt:rowData.rowID];
        }
        
        
        
        CheckListPhotoVC *viewController = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"CheckListPhotoVC"];
        viewController.delegate= self;
        viewController.photoData = sendingObj;
        positionRect = CGRectMake(self.view.frame.origin.x - 25, 235, 675, 331);
        
        _popVC = [[UIPopoverController alloc] initWithContentViewController:viewController];
        [_popVC setDelegate:self];
        _popVC.popoverContentSize = CGSizeMake(675, 331);
        [_popVC presentPopoverFromRect:positionRect  inView:self.view permittedArrowDirections:0 animated:YES];
  
    }
    if(btn == clickedCell.bestPracticeBtn && ![btn.titleLabel.text isEqualToString:@"Action Plan"])
    {
        ChkListBestPractice *sendingObj;
        
        for (ChkListBestPractice *remData in [ChkListBestPractice findAll]) {
            if(remData.mainSectionID == [NSNumber numberWithInt:dataClass.mainSection] && remData.subSectionID == [NSNumber numberWithInt:dataClass.subSection] && remData.rowID == [NSNumber numberWithInt:rowData.rowID])
            {
                sendingObj = (ChkListBestPractice*)remData;
                break;
            }
        }
        if(sendingObj == nil){
            sendingObj = [ChkListBestPractice createEntity];
            sendingObj.mainSectionID =[NSNumber numberWithInt:dataClass.mainSection];
            sendingObj.subSectionID =[NSNumber numberWithInt:dataClass.subSection];
            sendingObj.rowID =[NSNumber numberWithInt:rowData.rowID];
        }

        
        CheckListBestPractiseVC *viewController = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"CheckListBestPractiseVC"];
        viewController.delegate= self;
        viewController.bestPractData = sendingObj;
        positionRect = CGRectMake(self.view.frame.origin.x - 25, 235, 675, 635);
        
        _popVC = [[UIPopoverController alloc] initWithContentViewController:viewController];
        [_popVC setDelegate:self];
        _popVC.popoverContentSize = CGSizeMake(675, 635);
        [_popVC presentPopoverFromRect:positionRect  inView:self.view permittedArrowDirections:0 animated:YES];
        
    }
    if(btn == clickedCell.referenceButton)
    {
        CheckListReferenceVC *viewController = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"CheckListReferenceVC"];
        viewController.delegate= self;
        viewController.view.clipsToBounds         = YES;
        
        positionRect = CGRectMake(self.view.frame.origin.x - 25, 300, 675, 242);
        _popVC = [[UIPopoverController alloc] initWithContentViewController:viewController];
        
        _popVC.popoverContentSize = CGSizeMake(675, 242);
        [_popVC presentPopoverFromRect:positionRect  inView:self.view permittedArrowDirections:0 animated:YES];
    }
    else if([btn.titleLabel.text isEqualToString:@"Action Plan"])
    {
        ChkListActionPlan *sendingObj;
        
        for (ChkListActionPlan *remData in [ChkListActionPlan findAll]) {
            if(remData.mainSectionID == [NSNumber numberWithInt:dataClass.mainSection] && remData.subSectionID == [NSNumber numberWithInt:dataClass.subSection] && remData.rowID == [NSNumber numberWithInt:rowData.rowID])
            {
                sendingObj = (ChkListActionPlan*)remData;
                break;
            }
        }
        if(sendingObj == nil){
            sendingObj = [ChkListActionPlan createEntity];
            sendingObj.mainSectionID =[NSNumber numberWithInt:dataClass.mainSection];
            sendingObj.subSectionID =[NSNumber numberWithInt:dataClass.subSection];
            sendingObj.rowID =[NSNumber numberWithInt:rowData.rowID];
        }
        
        CheckListActionPlanVC *viewController = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"CheckListActionPlanVC"];
        viewController.delegate= self;
        viewController.actionPlanData = sendingObj;
        positionRect = CGRectMake(self.view.frame.origin.x - 30, 30, 675, 928);
        _popVC = [[UIPopoverController alloc] initWithContentViewController:viewController];
        
        _popVC.popoverContentSize = CGSizeMake(675, 928);
        [_popVC presentPopoverFromRect:positionRect  inView:self.view permittedArrowDirections:0 animated:YES];
        
        //[self CheckListActionPlanButtonClick:sender];
    }
}

//Method to remove the current pop over
-(void)parentCallBack:(id)data
{
    [_popVC dismissPopoverAnimated:YES];
}


-(void)fetchCheckListData
{
    
    for (ChkListBestPractice *bestPractData in (NSArray*)[ChkListBestPractice findAll]) {
        
        NSLog(@"bestPractData title %@",bestPractData.title);
        
    }
    
}

-(void)insertCheckListData
{
    ChkListBestPractice *bestPrac = [ChkListBestPractice createEntity];
    
    bestPrac.title = @"Testing";
    
    [[NSManagedObjectContext defaultContext] saveWithOptions:MRSaveWithoutOptions completion:^(BOOL success, NSError *error) {
        
        NSLog(@"Inserted");
        
    }];
    
}


//Method to invoke action plan view
- (IBAction)CheckListActionPlanButtonClick:(id)sender
{
  CheckListActionPlanVC   *viewController = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"CheckListActionPlanVC"];

    viewController.modalPresentationStyle = UIModalTransitionStyleCrossDissolve;
    
    [APP_DELEGATE.customSplitViewController presentViewController:viewController animated:NO completion:^{
        CGPoint centerPoint = CGPointMake([[UIScreen mainScreen] bounds].size.width/2, [[UIScreen mainScreen] bounds].size.height/2);
       
        viewController.view.superview.center = centerPoint;
        viewController.view.superview.layer.cornerRadius    = 7.0f;
        viewController.view.superview.clipsToBounds         = YES;
        viewController.view.hidden=NO;
        
    }];
}

//Method to invoke color selector
-(IBAction)checkListColorSelector:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    
    CGRect buttonFrameInTableView = [btn convertRect:btn.bounds toView:self.checkListTable];
    NSIndexPath *path = [self.checkListTable indexPathForRowAtPoint:buttonFrameInTableView.origin];
    
    //NSIndexPath *path = [NSIndexPath indexPathForRow:btn.tag inSection:0]; //[NSIndexPath indexPathWithIndex:btn.tag];
                         
    DealerCheckListTableViewCell *cell = (DealerCheckListTableViewCell*)[self.checkListTable cellForRowAtIndexPath:path];
    //DealerCheckListTableViewCell *cell = (DealerCheckListTableViewCell *)[[[sender superview] superview ]superview];
    
    CheckListColorSelectorVC *checkListColorViewController = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"CheckListColorSelectorVC"];

    checkListColorViewController.delegate = self;
    checkListColorViewController.selectedBtn = cell.colorSelectorBtn;
    checkListColorViewController.cellObj = cell;
    checkListColorViewController.index = path;
    
    _popVC = [[UIPopoverController alloc] initWithContentViewController:checkListColorViewController];
    
    _popVC.popoverContentSize = CGSizeMake(105, 142);
    NSLog(@"cell = %@", NSStringFromCGRect(cell.frame));
    
    CGRect positionRect = CGRectMake(38 , 25, 30, 30);
    
    NSInteger totalNoRow = [self.checkListTable numberOfRowsInSection:path.section];//first get total rows in that section by current indexPath.
    
    if(path.row == totalNoRow -1 || path.row == totalNoRow -2){
        [_popVC presentPopoverFromRect:positionRect  inView:btn permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
    }
    else{
        [_popVC presentPopoverFromRect:positionRect  inView:btn permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    }
}

//Method to update the checklstArray when editing
-(void)updateEditedData : (NSIndexPath*)indexpath : (ColorSelector)color : (UIImage*)imageSelected
{
    CheckListSectionData *sectionData = [checkLstArray objectAtIndex:indexpath.section];
    NSMutableArray *rowDataArray = sectionData.rowData;
    CheckListRowData *rowData = [rowDataArray objectAtIndex:indexpath.row];
    rowData.selectedColor = color;
    rowData.selectedImageColor = imageSelected;
    
    [sectionData.rowData replaceObjectAtIndex:indexpath.row withObject:rowData];
    [checkLstArray replaceObjectAtIndex:indexpath.section withObject:sectionData];
    
    [_popVC dismissPopoverAnimated:YES];
    
    //Need to enable once api is done
    [self insertCheckListTableData : sectionData.mainSection :sectionData.subSection : rowData.rowID : color : imageSelected];
    
    [self.checkListTable reloadData];
}

//Method to trigger the edited data
-(void)selectedColor:(UIImage*)imageSelected :(id)path :(NSIndexPath*)indexpath
{
    NSIndexPath *index = (NSIndexPath*)path;
    [self updateEditedData:index : indexpath.row : imageSelected];
    [self getCheckListBtnCount];
}

//Delegate method to render table view header
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    //Need to change the condition according to the received data
    CheckListSectionData *checkListSection = [checkLstArray objectAtIndex:section];
    
    if([checkListSection.sectionType isEqualToString:@"mainSection"])
    {
        UIView *mainSectionView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 50)];
        mainSectionBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width - 20, 50)];
        [mainSectionView addSubview:mainSectionBackgroundView];
        UILabel *mainSectionLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 16, tableView.frame.size.width, 18)];
        [mainSectionLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
        mainSectionLabel.textColor = [UIColor whiteColor];
        
        NSString *headerTitle = checkListSection.mainSectionTitle;
        [mainSectionLabel setText:headerTitle];
        
        [mainSectionBackgroundView addSubview:mainSectionLabel];
        //[mainSectionBackgroundView setBackgroundColor:CheckListTblMainSectionColor];
        mainSectionBackgroundView.backgroundColor = CheckListTblMainSectionColor;
        
        
        UIView *subSectionView = [[UIView alloc] initWithFrame:CGRectMake(0, 50, tableView.frame.size.width -20, 33)];
        
        UILabel *subSectionLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 8, tableView.frame.size.width, 18)];
        [subSectionLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
        subSectionLabel.textColor = [UIColor whiteColor];
        
        [subSectionLabel setText:checkListSection.subSectionTitle];
        
        [subSectionView addSubview:subSectionLabel];
        
        //[subSectionView setBackgroundColor:CheckListTblSubSectionColor];
        subSectionView.backgroundColor = CheckListTblSubSectionColor;
        
        
        [mainSectionBackgroundView addSubview:subSectionView];
        
        UIButton *selectButton = [[UIButton alloc] initWithFrame:CGRectMake(mainSectionBackgroundView.frame.size.width-35, mainSectionBackgroundView.frame.origin.y+10, 30, 30)];
        
        [selectButton setImage:[UIImage imageNamed:@"rightButtonArrow.png"] forState:UIControlStateNormal];
        
        [selectButton setImage:[self fetchCustomizedColoredImage:selectButton :WhiteBGColor] forState:UIControlStateNormal];
        
        selectButton.tag = section;
        [selectButton addTarget:self action:@selector(sectionMenuTapped:) forControlEvents:UIControlEventTouchUpInside];
        [mainSectionBackgroundView addSubview:selectButton];
        
        return mainSectionView;
    }
    else//Need to change the condition according to the received data
    {
        UIView *subSectionView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 33)];
        subSectionView.backgroundColor = [ UIColor clearColor];
        UIView *subSectionBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width - 20, 33)];
        [subSectionView addSubview:subSectionBackgroundView];
        
        UILabel *subSectionLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 8, tableView.frame.size.width, 18)];
        [subSectionLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
        subSectionLabel.textColor = [UIColor whiteColor];
        
        [subSectionLabel setText:checkListSection.subSectionTitle];
        
        [subSectionBackgroundView addSubview:subSectionLabel];
        //[subSectionBackgroundView setBackgroundColor:[UIColor colorWithRed:144.0/255.0 green:144.0/255.0 blue:144.0/255.0 alpha:1.0]];
        subSectionBackgroundView.backgroundColor = CheckListTblSubSectionColor;
        
        return subSectionView;
    }
}

//Method to avoid dismissing popover when the user tap outside
-(BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    return NO;
}

//Method to trigger section menu
-(IBAction)sectionMenuTapped:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    
    DataPopOverVC *dataPopOverVC = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"DataPopOverVC"];
    
    dataPopOverVC.delegate = self;
    
    _popVC = [[UIPopoverController alloc] initWithContentViewController:dataPopOverVC];
    
    
    
    _popVC.popoverContentSize = CGSizeMake(200, 100.0);
    
    CGRect positionRect = CGRectMake(btn.frame.origin.x-2, btn.frame.origin.y-3, btn.frame.size.width, btn.frame.size.height);
    [_popVC presentPopoverFromRect:positionRect inView:btn.superview permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    
    dataPopOverVC.viewType = eCheckListSectionPopOver;
    dataPopOverVC.dataArray = mainSectionArray;
    
}

//Method to reload the section header
-(void)dataSelectedCallBack:(id)data
{
    NSIndexPath *index = (NSIndexPath*)data;

    [self.checkListTable scrollToRowAtIndexPath:index
                               atScrollPosition:UITableViewScrollPositionTop
                                       animated:YES];
    [_popVC dismissPopoverAnimated:YES];
}

//Method to decide number of sections
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [checkLstArray count];
}

//Method to decide the height of header
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    //Need to change the condition according to the received data
    CheckListSectionData *checkListSection = [checkLstArray objectAtIndex:section];
    
    if([checkListSection.sectionType isEqualToString:@"mainSection"])
        return 83.0f;
    else
        return 33.0f;
}

#pragma mark - UItableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSMutableArray *rowArray = [self getSubSectionArrayContent : section];
    return [rowArray count];
}

-(UIImage *) getImageWithTintedColor:(UIImage *)image withTint:(UIColor *)color withIntensity:(float)alpha {
    CGSize size = image.size;
    
    UIGraphicsBeginImageContextWithOptions(size, FALSE, 2);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    [image drawAtPoint:CGPointZero blendMode:kCGBlendModeNormal alpha:1.0];
    
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextSetBlendMode(context, kCGBlendModeOverlay);
    CGContextSetAlpha(context, alpha);
    
    CGContextFillRect(UIGraphicsGetCurrentContext(), CGRectMake(CGPointZero.x, CGPointZero.y, image.size.width, image.size.height));
    
    UIImage * tintedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return tintedImage;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //DealerCheckListTableViewCell *checkListCell = (DealerCheckListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"dealerCheckListCellId"];
    
    static NSString *cellIdentifier = @"dealerCheckListCellId";
    
    DealerCheckListTableViewCell *checkListCell = (DealerCheckListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (checkListCell == nil)
    {
        checkListCell = [[DealerCheckListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    checkListCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    checkListCell.remarksBtn.tag = indexPath.row;
    checkListCell.photoBtn.tag = indexPath.row;
    checkListCell.bestPracticeBtn.tag = indexPath.row;
    checkListCell.colorSelectorBtn.tag = indexPath.row;
    checkListCell.index = indexPath;
    
    checkListCell.backgroundColor = [UIColor clearColor];
    
    CheckListSectionData *dataClass = [checkLstArray objectAtIndex:indexPath.section];
    
    NSMutableArray *array = dataClass.rowData;
    
    CheckListRowData *rowData = [array objectAtIndex:indexPath.row];
    
    [checkListCell.colorSelectorBtn setImage:rowData.selectedImageColor forState:UIControlStateNormal];
    
    //NSLog(@"rowData selected color %d",rowData.selectedColor);
    if(rowData.selectedColor == eGreen)
    {
        checkListCell.bestPracticeBtn.hidden = NO;
        checkListCell.remarksBtn.hidden = NO;
        checkListCell.photoBtn.hidden = NO;
        
        [checkListCell.remarksBtn setTitleColor:CustomTextColor forState:UIControlStateNormal];
        [checkListCell.photoBtn setTitleColor:CustomTextColor forState:UIControlStateNormal];
        [checkListCell.bestPracticeBtn setTitleColor:CustomTextColor forState:UIControlStateNormal];
        [checkListCell.referenceButton setTitleColor:CustomTextColor forState:UIControlStateNormal];
        
        
        [checkListCell.remarksBtn setTitle:@"Remark" forState:UIControlStateNormal];
        [checkListCell.photoBtn setTitle:@"Photo" forState:UIControlStateNormal];
        [checkListCell.bestPracticeBtn setTitle:@"Best Practice" forState:UIControlStateNormal];
        
        
        [checkListCell.bestPracticeBtn setImage:[self fetchCustomizedColoredImage:checkListCell.bestPracticeBtn:CheckListIconColor] forState:UIControlStateNormal];
        [checkListCell.remarksBtn setImage:[self fetchCustomizedColoredImage:checkListCell.remarksBtn:CheckListIconColor] forState:UIControlStateNormal];
        [checkListCell.photoBtn setImage:[self fetchCustomizedColoredImage:checkListCell.photoBtn:CheckListIconColor] forState:UIControlStateNormal];
    }
    else if(rowData.selectedColor == eRed || rowData.selectedColor == eYellow)
    {
        checkListCell.bestPracticeBtn.hidden = NO;
        checkListCell.remarksBtn.hidden = NO;
        checkListCell.photoBtn.hidden = NO;
        
        [checkListCell.remarksBtn setTitle:@"Remark" forState:UIControlStateNormal];
        [checkListCell.photoBtn setTitle:@"Photo" forState:UIControlStateNormal];
        [checkListCell.bestPracticeBtn setTitle:@"Action Plan" forState:UIControlStateNormal];
        
        [checkListCell.bestPracticeBtn setImage:[UIImage imageNamed:@"CheckListActionPlanBtn.png"] forState:UIControlStateNormal];
        
        [checkListCell.bestPracticeBtn setImage:[self fetchCustomizedColoredImage:checkListCell.bestPracticeBtn:CheckListIconColor] forState:UIControlStateNormal];
        [checkListCell.remarksBtn setImage:[self fetchCustomizedColoredImage:checkListCell.remarksBtn:CheckListIconColor] forState:UIControlStateNormal];
        [checkListCell.photoBtn setImage:[self fetchCustomizedColoredImage:checkListCell.photoBtn:CheckListIconColor] forState:UIControlStateNormal];
        
        
        [checkListCell.remarksBtn setTitleColor:CustomTextColor forState:UIControlStateNormal];
        [checkListCell.photoBtn setTitleColor:CustomTextColor forState:UIControlStateNormal];
        [checkListCell.bestPracticeBtn setTitleColor:CustomTextColor forState:UIControlStateNormal];
        [checkListCell.referenceButton setTitleColor:CustomTextColor forState:UIControlStateNormal];
    }
    else{
        checkListCell.bestPracticeBtn.hidden = YES;
        checkListCell.remarksBtn.hidden = YES;
        checkListCell.photoBtn.hidden = YES;
        [checkListCell.referenceButton setTitle:rowData.referenceTitle forState:UIControlStateNormal];
        [checkListCell.referenceButton setTitleColor:DarkGrayTextColor forState:UIControlStateNormal];
    }
    
    
//Refreshing from local db need to chnage after api integaration
//    for (CheckListSection *remData in [CheckListSection findAll]) {
//        NSLog(@"rem %@-%@-%@",remData.mainSectionID,remData.subSectionID,remData.rowID);
//        
//        if(remData.mainSectionID == [NSNumber numberWithInt:dataClass.mainSection] && remData.subSectionID == [NSNumber numberWithInt:dataClass.subSection] && remData.rowID == [NSNumber numberWithInt:rowData.rowID])
//            {
//                [checkListCell.colorSelectorBtn setImage:rowData.selectedImageColor forState:UIControlStateNormal];
//            }
//    }
//Need to change Refreshing from local db need to chnage after api integaration
        
    
    return checkListCell;
    
}

//Method to change the color of UIImage
-(UIImage*)fetchCustomizedColoredImage:(UIButton*)imgBtn:(UIColor*)colorName
{
    UIImage *oldImage = [imgBtn.imageView.image imageScaledToFitSize:imgBtn.imageView.frame.size];
    UIImage *image = [oldImage imageTintedWithColor:colorName];
    
    return image;
}

- (void)tableView:(UITableView *)tableView didEndDisplayingHeaderView:(UIView *)view forSection:(NSInteger)section
{
    NSLog(@"hiding section");
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    NSLog(@"displaying section");
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

//Need to remove after api integration
-(NSMutableArray*)getHardCodedRowData
{
    NSMutableArray *rowDataArray = [[NSMutableArray alloc] init];
    
    CheckListRowData *rowData1 = [[CheckListRowData alloc] init];
    rowData1.bestPracticeOrActionPlanTitle = @"";
    rowData1.referenceTitle = @"Reference";
    rowData1.selectedImageColor = [UIImage imageNamed:@"CheckListColorSelectorGrayBtn"];
    rowData1.selectedColor = 3;
    rowData1.rowID = 1;
    
    [rowDataArray addObject:rowData1];
    
    CheckListRowData *rowData2 = [[CheckListRowData alloc] init];
    rowData2.bestPracticeOrActionPlanTitle = @"";
    rowData2.referenceTitle = @"Reference";
    rowData2.selectedImageColor = [UIImage imageNamed:@"CheckListColorSelectorGrayBtn"];
    rowData2.selectedColor = 3;
    rowData2.rowID = 2;
    
    [rowDataArray addObject:rowData2];
    
    CheckListRowData *rowData3 = [[CheckListRowData alloc] init];
    rowData3.bestPracticeOrActionPlanTitle = @"";
    rowData3.referenceTitle = @"Reference";
    rowData3.selectedImageColor = [UIImage imageNamed:@"CheckListColorSelectorGrayBtn"];
    rowData3.selectedColor = 3;
    rowData3.rowID = 3;
    
    [rowDataArray addObject:rowData3];
    
    CheckListRowData *rowData4 = [[CheckListRowData alloc] init];
    rowData4.bestPracticeOrActionPlanTitle = @"";
    rowData4.referenceTitle = @"Reference";
    rowData4.selectedImageColor = [UIImage imageNamed:@"CheckListColorSelectorGrayBtn"];
    rowData4.selectedColor = 3;
    rowData4.rowID = 4;
    
    [rowDataArray addObject:rowData4];
    
    CheckListRowData *rowData5 = [[CheckListRowData alloc] init];
    rowData5.bestPracticeOrActionPlanTitle = @"";
    rowData5.referenceTitle = @"Reference";
    rowData5.selectedImageColor = [UIImage imageNamed:@"CheckListColorSelectorGrayBtn"];
    rowData5.selectedColor = 3;
    rowData5.rowID = 5;
    
    [rowDataArray addObject:rowData5];
    
    return rowDataArray;
    
}

//Need to remove after api integration
-(NSMutableArray*)getHardCodedRowData2
{
    NSMutableArray *rowDataArray1 = [[NSMutableArray alloc] init];
    
    CheckListRowData *rowData11 = [[CheckListRowData alloc] init];
    rowData11.bestPracticeOrActionPlanTitle = @"";
    rowData11.referenceTitle = @"Reference";
    rowData11.selectedImageColor = [UIImage imageNamed:@"CheckListColorSelectorGrayBtn"];
    rowData11.hasRemarkHistory = NO;
    
    rowData11.selectedColor = 3;
    rowData11.rowID = 1;
    
    [rowDataArray1 addObject:rowData11];
    
    CheckListRowData *rowData22 = [[CheckListRowData alloc] init];
    rowData22.bestPracticeOrActionPlanTitle = @"";
    rowData22.referenceTitle = @"Reference";
    rowData22.selectedImageColor = [UIImage imageNamed:@"CheckListColorSelectorGrayBtn"];
    rowData11.hasRemarkHistory = YES;
    
    rowData22.selectedColor = 3;
    rowData22.rowID = 2;
    [rowDataArray1 addObject:rowData22];
    
    return rowDataArray1;
}

//Need to remove after api integration
-(void)hardCodedDataNew
{
    checkLstArray = [[NSMutableArray alloc] init];
    
    CheckListSectionData *sectionData1 = [[CheckListSectionData alloc] init];
    sectionData1.mainSection = 0;
    sectionData1.subSection = 0;
    sectionData1.mainSectionTitle = @"SPARE PARTS";
    sectionData1.subSectionTitle = @"Management Check";
    sectionData1.sectionType = @"mainSection";
    sectionData1.rowData = [self getHardCodedRowData];
    
    [checkLstArray addObject:sectionData1];
    
    CheckListSectionData *sectionData2 = [[CheckListSectionData alloc] init];
    sectionData2.mainSection = 1;
    sectionData2.subSection = 1;
    sectionData2.mainSectionTitle = @"SPARE PARTS";
    sectionData2.subSectionTitle = @"Other Check";
    sectionData2.sectionType = @"subSection";
    sectionData2.rowData =[self getHardCodedRowData];
    
    [checkLstArray addObject:sectionData2];

    CheckListSectionData *sectionData3 = [[CheckListSectionData alloc] init];
    sectionData3.mainSection = 2;
    sectionData3.subSection = 0;
    sectionData3.mainSectionTitle = @"OTHER PARTS";
    sectionData3.subSectionTitle = @"Management Check";
    sectionData3.sectionType = @"mainSection";

    sectionData3.rowData =[self getHardCodedRowData2];

    [checkLstArray addObject:sectionData3];
    
    CheckListSectionData *sectionData4 = [[CheckListSectionData alloc] init];
    sectionData4.mainSection = 3;
    sectionData4.subSection = 1;
    sectionData4.mainSectionTitle = @"OTHER PARTS";
    sectionData4.subSectionTitle = @"Other Check";
    sectionData4.sectionType = @"subSection";
    sectionData4.rowData =[self getHardCodedRowData];

    [checkLstArray addObject:sectionData4];
    
}

//Need to get main section header
-(void)getMainSectionHeader
{
    CheckListSectionData *data;
    for (data in checkLstArray) {
        if(![mainSectionArray containsObject:data.mainSectionTitle])
            [mainSectionArray addObject:data.mainSectionTitle];
    }
    NSLog(@"getMainSectionHeader %@",mainSectionArray);
}

//Need to get number of main section
-(NSInteger)getNumberOfMainSection
{
    return [mainSectionArray count];
}

//Need to get main section array
-(NSMutableArray*)getMainSectionArray
{
    NSMutableArray *sectionArr = [[NSMutableArray alloc] init];
    
    CheckListSectionData *data;
    for (data in checkLstArray) {
        if([data.sectionType isEqualToString:@"mainSection"] )
            [sectionArr addObject:data];
    }
    return sectionArr;
}

//Need to get sub section array count based on main section
-(NSMutableArray*)getSubSectionArrayContent : (NSInteger)mainSection
{
    NSMutableArray *subSectionArr = [[NSMutableArray alloc] init];
    
    CheckListSectionData *data;
    for (data in checkLstArray) {
        if(data.mainSection == mainSection)
            subSectionArr = data.rowData;
    }
    return subSectionArr;
}

//Method to refresh the checklist button count
-(void)getCheckListBtnCount
{
    CheckListSectionData    *sectionData;
    CheckListRowData        *rowData;
    
    checkListGreenBtnCount  = 0;
    checkListYellowBtnCount = 0;
    checkListRedBtnCount    = 0;
    checkListGrayBtnCount   = 0;
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    for (sectionData in checkLstArray) {
        array = sectionData.rowData;
        
        for (rowData in array) {
            if(rowData.selectedColor == eGreen)
            {
                checkListGreenBtnCount = checkListGreenBtnCount+1;
            }
            else if(rowData.selectedColor == eYellow)
            {
                checkListYellowBtnCount = checkListYellowBtnCount+1;
            }
            else if(rowData.selectedColor == eRed)
            {
                checkListRedBtnCount = checkListRedBtnCount+1;
            }
            else if(rowData.selectedColor == eGray)
            {
                checkListGrayBtnCount = checkListGrayBtnCount+1;
            }
        }
    }
    
    MainViewController *parentVC = [APP_DELEGATE appBaseViewController];
    [parentVC.checkListViewGreenButton setTitle:[NSString stringWithFormat:@"%ld",(long)checkListGreenBtnCount] forState:UIControlStateNormal];
    [parentVC.checkListViewYellowButton setTitle:[NSString stringWithFormat:@"%ld",(long)checkListYellowBtnCount] forState:UIControlStateNormal];
    [parentVC.checkListViewRedButton setTitle:[NSString stringWithFormat:@"%ld",(long)checkListRedBtnCount] forState:UIControlStateNormal];
    [parentVC.checkListViewGrayButton setTitle:[NSString stringWithFormat:@"%ld",(long)checkListGrayBtnCount] forState:UIControlStateNormal];
}
    
-(void) insertCheckListTableData : (NSInteger)mainSectionID : (NSInteger)subSectionID : (NSInteger)rowID : (ColorSelector)color : (UIImage*)imageSelected
    {
        CheckListSection *sendingObj;
        
        for (CheckListSection *remData in [CheckListSection findAll]) {
            NSLog(@"remData.remarksStr %@-%@-%@",remData.mainSectionID,remData.subSectionID,remData.rowID);
            if(remData.mainSectionID == [NSNumber numberWithInt:mainSectionID] && remData.subSectionID == [NSNumber numberWithInt:subSectionID] && remData.rowID == [NSNumber numberWithInt:rowID])
            {
                sendingObj = (CheckListSection*)remData;
                break;
            }
        }
        
        if(sendingObj == nil){
            sendingObj = [CheckListSection createEntity];
            sendingObj.mainSectionID =[NSNumber numberWithInteger:mainSectionID];
            sendingObj.subSectionID =[NSNumber numberWithInteger:subSectionID];
            sendingObj.rowID =[NSNumber numberWithInteger:rowID];
        }
        
        CheckListRow *rowData = [CheckListRow createEntity];
        rowData.selectedColor = [NSNumber numberWithInt:color];
        rowData.selectedImageColor = UIImagePNGRepresentation(imageSelected);
        
        
        sendingObj.checkListRow = rowData;
        
        [[NSManagedObjectContext defaultContext] saveWithOptions:MRSaveWithoutOptions completion:^(BOOL success, NSError *error) {
            NSLog(@"saved successfully");
            NSLog(@"checklist... %@",[CheckListSection findAll]);
        }];
        
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
