//
//  checkListResponsiblePeopleVC.h
//  TrackiOS
//
//  Created by satish sharma on 11/19/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol checkListResponsiblePeopleDelegate <NSObject>

- (void) addNumberOfResponsiblePerson:(NSArray *)personArray;

@end

@interface CheckListResponsiblePeopleVC : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UISearchBar *searchSearchbar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) id <checkListResponsiblePeopleDelegate> delegate;

@end
