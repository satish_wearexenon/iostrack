//
//  DataPopOverCell.m
//  TrackiOS
//
//  Created by John Paul Ranjith on 04/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "DataPopOverCell.h"

@implementation DataPopOverCell

- (void)awakeFromNib
{
    [self.dataLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    self.dataLabel.textColor = CustomTextColor;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
