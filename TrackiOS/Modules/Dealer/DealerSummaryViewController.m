//
//  DealerSummaryViewController.m
//  TrackiOS
//
//  Created by satish sharma on 11/4/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "DealerSummaryViewController.h"
#import "DealerStageCell.h"
#import "DealerPopOverVC.h"
#import "DealerExportPopOverVC.h"
#import "DealerChecklistViewController.h"
#import "DealerDetailViewController.h"
#import "DealerSummary.h"
#import "Dealer.h"
#import "DealerUpdateStatus.h"

typedef enum {
    TableViewTypeStage = 0,         //No output
    TableViewTypeStageExportPDF,     //Default, outputs only critical log events
    TableViewTypeStageList            //Debug level, outputs critical and main log events
}TableViewType;

@interface DealerSummaryViewController () <DealerPopOverVCDelegate>

@property (weak, nonatomic) IBOutlet UIView *dealerHeaderView;
@property (weak, nonatomic) IBOutlet UILabel *dealerAreaLabel;
@property (weak, nonatomic) IBOutlet UIImageView *dealerImage;
@property (weak, nonatomic) IBOutlet UILabel *dealerNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dealerCodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *regionLabel;
@property (weak, nonatomic) IBOutlet UIButton *dealerStageButton;
@property (weak, nonatomic) IBOutlet UIButton *exportPdfButton;

@property (weak, nonatomic) IBOutlet UILabel *totalissueTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalIssueNo;
@property (weak, nonatomic) IBOutlet UILabel *actionPlanNoLabel;
@property (weak, nonatomic) IBOutlet UILabel *actionPlanDeatailLabel;
@property (weak, nonatomic) IBOutlet UILabel *bestPracticeDetailsLAbel;
@property (weak, nonatomic) IBOutlet UILabel *bestPracticeNoLabel;

@property (strong, nonatomic) NSMutableArray *stageDetailArray;
@property (strong, nonatomic) NSMutableDictionary *stageDetailDict;

@property (strong, nonatomic) NSMutableArray *stagefilterArray;



@property (nonatomic, strong) UIPopoverController *popVC;

@property (strong, nonatomic) IBOutlet UITableView *stageTableView;

- (IBAction)dealerStageButtonClick:(id)sender;
- (IBAction)exportPDFButtonClick:(id)sender;
- (IBAction)barGraphClick:(id)sender;

@end

@implementation DealerSummaryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
       
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidAppear:(BOOL)animated
{
    // Listen to back Notification Handler
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(backPressedLocalAction)  name:@"backPressed" object:nil];
}

- (void) viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"backPressed" object:nil];
}

- (void) backPressedLocalAction
{
    MainViewController *parentVC = [APP_DELEGATE appBaseViewController];
    [parentVC proceedBack];
}

-(void) viewWillAppear:(BOOL)animated
{
    // MainViewController *parentVC = [APP_DELEGATE appBaseViewController];
    //parentVC.currentDetailController = self;
    
    //_dealerid
    
    
    DealerSummary *summary = [DealerSummary MR_findFirstByAttribute:@"dealer_id" withValue:[NSNumber numberWithInt:1001]];
    
    NSLog(@"Dealer Summary = %@", [DealerSummary findFirst].bestpractice);
    NSLog(@"Dealer id = %@", [DealerSummary findFirst].dealer_id);
//    _totalIssueNo.text = [NSString stringWithFormat:@"%@",summary.totalissues];
//    _actionPlanNoLabel.text = [NSString stringWithFormat:@"%@",summary.actionplan];
//    _bestPracticeNoLabel.text = [NSString stringWithFormat:@"%@",summary.bestpractice];
    _totalIssueNo.text = [NSString stringWithFormat:@"%@",[DealerSummary findFirst].totalissues];
    _actionPlanNoLabel.text = [NSString stringWithFormat:@"%@",[DealerSummary findFirst].actionplan];
    _bestPracticeNoLabel.text = [NSString stringWithFormat:@"%@",[DealerSummary findFirst].bestpractice];
    
    _stageDetailArray = [[NSMutableArray alloc] init];
    
    Dealer *dealerData = [Dealer MR_findFirstByAttribute:@"dealer_id" withValue:[NSNumber numberWithInt:1002]];
    DealerUpdateStatus *dealerStatus = [DealerUpdateStatus MR_findFirstByAttribute:@"dealer_id" withValue:[NSNumber numberWithInt:1002]];
    //Dummy data
    NSLog(@"Dealer = %@", [DealerSummary findFirst].bestpractice);
    
    _stageDetailDict = [[NSMutableDictionary alloc] init];
    [_stageDetailDict setValue:dealerData.stage forKey:@"Stage"];
    [_stageDetailDict setValue:dealerStatus.iteamsadded forKey:@"Item"];
    [_stageDetailDict setValue:dealerStatus.addedphotos forKey:@"Photo"];
    [_stageDetailDict setValue:dealerData.first_car_date forKey:@"dateTime"];
    [_stageDetailDict setValue:[NSString stringWithFormat:@"PL:%@%@ ready",dealerData.dealer_readiness,@"%"] forKey:@"percentage"];
    [_stageDetailArray addObject:_stageDetailDict];
    
//    _stageDetailDict = [[NSMutableDictionary alloc] init];
//    [_stageDetailDict setValue:@"Implementation" forKey:@"Stage"];
//    [_stageDetailDict setValue:@"2 item uploded" forKey:@"Item"];
//    [_stageDetailDict setValue:@"17 photo added" forKey:@"Photo"];
//    [_stageDetailDict setValue:@"2014/10/18 15:30" forKey:@"dateTime"];
//    [_stageDetailDict setValue:@"PL:40%ready" forKey:@"percentage"];
//    [_stageDetailArray addObject:_stageDetailDict];
//    
//    _stageDetailDict = [[NSMutableDictionary alloc] init];
//    [_stageDetailDict setValue:@"Operation" forKey:@"Stage"];
//    [_stageDetailDict setValue:@"7 item uploded" forKey:@"Item"];
//    [_stageDetailDict setValue:@"4 photo added" forKey:@"Photo"];
//    [_stageDetailDict setValue:@"2014/10/18 15:30" forKey:@"dateTime"];
//    [_stageDetailDict setValue:@"PL:38%ready" forKey:@"percentage"];
//    [_stageDetailArray addObject:_stageDetailDict];
//    
//    _stageDetailDict = [[NSMutableDictionary alloc] init];
//    [_stageDetailDict setValue:@"Additional" forKey:@"Stage"];
//    [_stageDetailDict setValue:@"5 item uploded" forKey:@"Item"];
//    [_stageDetailDict setValue:@"12 photo added" forKey:@"Photo"];
//    [_stageDetailDict setValue:@"2014/10/18 15:30" forKey:@"dateTime"];
//    [_stageDetailDict setValue:@"PL:55%ready" forKey:@"percentage"];
//    [_stageDetailArray addObject:_stageDetailDict];
    //------------------------------------------------------------
    
    _stagefilterArray = [NSMutableArray arrayWithArray:_stageDetailArray];
    //
    [self.view setBackgroundColor:ViewBackgroundColor];
    [_stageTableView setBackgroundColor:ViewBackgroundColor];
    [_stageTableView setDelegate:self];
    [_stageTableView setDataSource:self];
    [_stageTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [_stageTableView setScrollEnabled:NO];
    
    //set font
    [_dealerAreaLabel setFont:[UIFont fontWithName:Font_Track_Regular size:23]];
    [_dealerNameLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    [_dealerCodeLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    [_regionLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    [_dealerStageButton setTitleColor:CustomTextColor forState:UIControlStateNormal];
    [_dealerStageButton.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];

    [_totalIssueNo setFont:[UIFont fontWithName:Font_Track_Regular size:50.0f]];
    [_totalissueTextLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16.0f]];
    [_actionPlanNoLabel setFont:[UIFont fontWithName:Font_Track_Regular size:50.0f]];
    [_actionPlanDeatailLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16.0f]];
    [_bestPracticeNoLabel setFont:[UIFont fontWithName:Font_Track_Regular size:50.0f]];
    [_bestPracticeDetailsLAbel setFont:[UIFont fontWithName:Font_Track_Regular size:16.0f]];
    
    //set TextColor
    _dealerAreaLabel.textColor = DarkGrayTextColor;
    _dealerNameLabel.textColor = LightGrayTextColor;
    _dealerCodeLabel.textColor = CustomTextColor;
    _regionLabel.textColor = CustomTextColor;
    
    _totalIssueNo.textColor = OrangeTextColor;
    _actionPlanNoLabel.textColor = CustomTextColor;
    _bestPracticeNoLabel.textColor = CustomTextColor;
    
    _totalissueTextLabel.textColor = ButtonDisabledTextColor;
    _actionPlanDeatailLabel.textColor = ButtonDisabledTextColor;
    _bestPracticeDetailsLAbel.textColor = ButtonDisabledTextColor;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UIbutton Action
- (IBAction)dealerStageButtonClick:(id)sender {
    NSLog(@"dealerStageButtonClick");

    DealerPopOverVC *viewController = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"DealerPopOverVC"];
    viewController.delegate = self;

    viewController.noOfStageArray = [[NSMutableArray alloc] initWithObjects:@"Pre-launch",@"Implementation",@"Operation",@"Additional", nil];
    viewController.checkMarkArray = [[NSMutableArray alloc] initWithObjects:[NSNumber numberWithBool:YES],[NSNumber numberWithBool:YES],[NSNumber numberWithBool:YES],[NSNumber numberWithBool:YES], nil];

    [viewController.checkMarkArray replaceObjectAtIndex:[self currentStage:sender withTitle:viewController.noOfStageArray] withObject:[NSNumber numberWithBool:NO]];

    _popVC = [[UIPopoverController alloc] initWithContentViewController:viewController];

    _popVC.popoverContentSize = CGSizeMake(274, 174.0);

     UIButton *button = (UIButton *)sender;

    [_popVC presentPopoverFromRect:CGRectMake(button.frame.origin.x + 95, button.frame.origin.y, button.frame.size.width, button.frame.size.height)  inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

- (IBAction)exportPDFButtonClick:(id)sender {

    NSLog(@"exportPDFButtonClick");

    DealerExportPopOverVC *viewController = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"DealerExportPopOverVC"];

    _popVC = [[UIPopoverController alloc] initWithContentViewController:viewController];
    _popVC.popoverContentSize = CGSizeMake(189.0, 98.0);
    UIButton *button = (UIButton *)sender;
    [_popVC presentPopoverFromRect:CGRectMake(button.frame.origin.x + 19, button.frame.origin.y, button.frame.size.width, button.frame.size.height)  inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

- (void) hidePopOver
{
    [_popVC dismissPopoverAnimated:NO];
    _popVC = nil;
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_stagefilterArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"DealerStageCells";
    
    DealerStageCell *cell = (DealerStageCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
        if (cell == nil) {
            cell = [[DealerStageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            
        }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    //set value
    [cell.dealerStageLabel setText:[[[_stagefilterArray objectAtIndex:indexPath.row] valueForKey:@"Stage"] uppercaseString]];
    [cell.itemLabel setText:[[_stagefilterArray objectAtIndex:indexPath.row] valueForKey:@"Item"]];
    
    [cell.noofPhotoLabel setText:[[_stagefilterArray objectAtIndex:indexPath.row] valueForKey:@"Photo"]];
    
    [cell.dateTimeLabel setText:[[_stagefilterArray objectAtIndex:indexPath.row] valueForKey:@"dateTime"]];
    
    [cell.percentageLabel setText:[[_stagefilterArray objectAtIndex:indexPath.row] valueForKey:@"percentage"]];
    
    return cell;
    // Configure the cell...

}

#pragma mark - Delegate method
//--- delegate conformation ----
-(void) updateButtonTitle:(NSString *) title
{
    [_dealerStageButton setTitle:[NSString stringWithFormat:@"Stage: %@",title] forState:UIControlStateNormal];
    
  /*
    for (int i = 0; i<[_stageDetailArray count]; i++) {
        if ([[[_stageDetailArray objectAtIndex:i] valueForKey:@"Stage"] isEqualToString:title]) {
            
             _stagefilterArray = [NSMutableArray arrayWithObject:[_stageDetailArray objectAtIndex:i]];
        }
    }
    [_stageTableView reloadData];
   */
}

//-- getCurrent stage ---
-(int) currentStage:(id) sender withTitle:(NSArray *)array
{
    int j = 0;
    UIButton *button = (UIButton *)sender;
    [button titleForState:UIControlStateNormal];

    NSMutableString *title=[NSMutableString stringWithString:[button titleForState:UIControlStateNormal]];;
     NSRange temprange=NSMakeRange(0, 7);
    [title replaceCharactersInRange:temprange withString:@""];

        for (int i = 0; i<[array count]; i++) {
            if ([title isEqualToString:[array objectAtIndex:i]]) {
               
                j=i;
            }
        }
    return j;
}

- (IBAction)barGraphClick:(id)sender
{
    
  //  DealerDetailViewController *parentviewController = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"DealerDetailViewController"];
    
    DealerChecklistViewController *viewController = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"DealerChecklistViewController"];
    
    //MainViewController *parentVC = [APP_DELEGATE appBaseViewController];
    
    [_parent addChildViewController:viewController];
    [viewController didMoveToParentViewController:_parent];
    [_parent.view addSubview:viewController.view];

}

@end
