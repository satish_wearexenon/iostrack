//
//  DealerInfoViewController.m
//  TrackiOS
//
//  Created by satish sharma on 11/4/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "DealerInfoViewController.h"
#import "CalenderViewController.h"
#import "DealerInfoAddDealerStaffVC.h"
#import "JVFloatLabeledTextField.h"
#import "CalenderViewController.h"
#import "DealerGroupPopVC.h"
#import "DealerAddInfoTableViewCell.h"
#import "Staff.h"
#import "StaffRole.h"
#import "StaffModel.h"
#import "UITextField+Validation.h"

@interface DealerInfoViewController () <CalenderViewControllerDelegate, DealerGroupPopVCDelegate, DealerInfoAddDealerStaffVCDelegate>

enum DealerInfoTextFieldTag
{
    eDealerTypeTextFieldTag = 8000,
    eDealerGroupTextFieldTag = 8001,
    eDealerCodeTextFieldTag = 8002,
    eDealerNameTextFieldTag = 8003,
    eDateTextFieldTag = 8004,
    eSizeTextFieldTag = 8005,
    eMonthlyRetailingTextFieldTag = 8006,
    eLocalCarparkTextFieldTag = 8007,
    eLoyaltyCustomerTextFieldTag = 8008,
    eMonthlyThroughputTextFieldTag = 8009,
    eWorkingBayTextFieldTag = 8010,
    eTechnicalInformationTextFieldTag = 8011,
    eTechTurnoverRateTextFieldTag = 8012,
    eServiceRevenueTextFieldTag = 8013,
    eRegionTypeTextFieldTag = 8014,
    eProvinceTextFieldTag = 8015,
    eCityTypeTextFieldTag = 8016,
    eZipTypeTextFieldTag = 8017,
    eAddressTypeTextFieldTag = 8018
};

@property (strong, nonatomic) CalenderViewController *calendarVC;

@property (weak, nonatomic) IBOutlet UIButton *dateButton;

@property (nonatomic, strong) UIPopoverController *addInfoPopoverControl;
@property (nonatomic, strong) UIPopoverController *datePopoverControl;
@property (nonatomic, strong) UIPopoverController *groupPopoverControl;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;

@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *dealerTypeTextField;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *dealerGroupTextField;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *dealerCodeTextField;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *dealerNameTextField;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *sizeTextField;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *dateTextField;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *monthlyRetailingTextField;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *localCarparkTextField;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *loyaltyCustomerTextField;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *monthlyThroughputTextField;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *workingBayTextField;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *technicalInformationTextField;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *techTurnoverRateTextField;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *serviceRevenueTextField;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *regionTextField;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *provinceTextField;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *cityTextField;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *zipTextField;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *addressTextField;
@property (nonatomic, strong) UITextField *activeTextField;

@property (nonatomic, strong) NSIndexPath* selectedIndexPath;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) DealerInfoAddDealerStaffVC *addInfoVC;

@property (nonatomic, strong) IBOutletCollection(JVFloatLabeledTextField) NSArray *textFields;

@property (nonatomic, strong) NSMutableArray *dealerStaffArray;

@property (nonatomic, strong) NSMutableArray *selectedIndexArray;

@property (nonatomic, strong) NSIndexPath *selectedEditItem;
@property (nonatomic, strong)DealerGroupPopVC *groupVC;

@property (nonatomic) BOOL isEditionDone;

- (IBAction)addDealerStaffTapped:(id)sender;
- (IBAction)saveAction:(id)sender;
@end

@implementation DealerInfoViewController

- (void) viewDidAppear:(BOOL)animated
{
    // Listen to back Notification Handler
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(backPressedLocalAction)  name:@"backPressed" object:nil];
}

- (void) viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"backPressed" object:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    NSDate *currentDate = [NSDate date];
    //NSString *date = [[self dateFormate] stringFromDate:currentDate];
    //NSString *dateLabelString = [NSString stringWithFormat:@"%@: %@ %@",LOCALIZED_CALENDAR_POCA_DATE, date, @">"];
    [self dateForButton:currentDate];
    //[_dateButton setTitle:dateLabelString forState:UIControlStateNormal];

    _addInfoVC = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"DealerInfoAddDealerStaffVC"];
    _addInfoVC.delegate = self;
    _addInfoVC.addType = eDealerAdd;
    _addInfoPopoverControl = [[UIPopoverController alloc] initWithContentViewController:_addInfoVC];
    
    CalenderViewController *calendarVC = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"calenderViewController"];
    calendarVC.delegate = self;
    _datePopoverControl = [[UIPopoverController alloc] initWithContentViewController:calendarVC];
    
    _groupVC = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"DealerInfoStaffPositionVC"];
    _groupVC.positionArray = [NSArray arrayWithObjects:@"Group 1",@"Group 2",@"Group 3",@"Group 4",@"Group 5",@"Group 6",nil];
    _groupVC.delegate = self;
    _groupVC.popoverType = eDealerPopupGroup;
    _groupPopoverControl = [[UIPopoverController alloc] initWithContentViewController:_groupVC];
    
    // Textfield Placeholders
    _dealerTypeTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LOCALIZED_ADD_DEALER_TYPE
                                                                       attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    _dealerGroupTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LOCALIZED_ADD_DEALER_GROUP
                                                                                 attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    _dealerCodeTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LOCALIZED_ADD_DEALER_CODE
                                                                                 attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    _dealerNameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LOCALIZED_ADD_DEALER_NAME
                                                                                 attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    _sizeTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LOCALIZED_ADD_DEALER_SIZE
                                                                                 attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    _dateTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LOCALIZED_CALENDAR_POCA_DATE
                                                                                 attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    _monthlyRetailingTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LOCALIZED_ADD_DEALER_MONTHLY_RETAILING
                                                                                 attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    _localCarparkTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LOCALIZED_ADD_DEALER_LOCAL_CARPARK
                                                                                 attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    _loyaltyCustomerTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LOCALIZED_ADD_DEALER_LOYALTY_CUSTOMER
                                                                                 attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    _monthlyThroughputTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LOCALIZED_ADD_DEALER_MONTHLY_THROUGHPUT
                                                                                 attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    _workingBayTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LOCALIZED_ADD_DEALER_WORKING_BAY
                                                                                 attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    _technicalInformationTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LOCALIZED_ADD_DEALER_TECHNICIAN_INFO
                                                                                 attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    _techTurnoverRateTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LOCALIZED_ADD_DEALER_TECHNICAL_TURNOVER_RATE
                                                                                 attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    _serviceRevenueTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LOCALIZED_ADD_DEALER_SERVICE_REVENUE
                                                                                 attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    _regionTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LOCALIZED_ADD_DEALER_REGION
                                                                                     attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    _provinceTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LOCALIZED_ADD_DEALER_PROVINCE
                                                                                     attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    _cityTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LOCALIZED_ADD_DEALER_CITY
                                                                                     attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    _zipTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LOCALIZED_ADD_DEALER_ZIP_CODE
                                                                                     attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    _addressTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LOCALIZED_ADD_DEALER_ADDRESS
                                                                                     attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    
    _dealerTypeTextField.text = @"TYPE";
    _regionTextField.text = @"South";
    
    // Set Textfields Color and font
    for (JVFloatLabeledTextField *textField in _textFields)
    {
        textField.font = [UIFont fontWithName:Font_Track_Regular size:18];
        textField.floatingLabel.font = [UIFont fontWithName:Font_Track_Regular size:12];
        textField.floatingLabelTextColor = LightGrayTextColor;
    }
    
    UIButton *buttonDate = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonDate addTarget:self action:@selector(addDateTapped:) forControlEvents:UIControlEventTouchUpInside];
    buttonDate.frame = CGRectMake(0, 0, 195, 55);
    [_dateTextField addSubview:buttonDate];
    
    UIButton *buttonDealerGroup = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonDealerGroup addTarget:self action:@selector(addGroupTapped:) forControlEvents:UIControlEventTouchUpInside];
    buttonDealerGroup.frame = CGRectMake(0, 0, 320, 55);
    [_dealerGroupTextField addSubview:buttonDealerGroup];
    
    //_cityTextField.floatingLabelActiveTextColor = [UIColor redColor];
    //_cityTextField.attributedPlaceholder =
    
    _dealerGroupTextField.text = [_groupVC.positionArray objectAtIndex:0];
    _dealerStaffArray = [NSMutableArray array];
    _selectedIndexArray = [NSMutableArray array];
    
    _dealerStaffArray = [[Staff findAll]mutableCopy];
    
//    for (Staff *staff in _dealerStaffArray)
//    {
//        NSLog(@"name = %@", staff.first_name);
//    }
    
    _selectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    
    [_saveButton setTitle:LOCALIZED_POPOVER_SAVE forState:UIControlStateNormal];
    [_saveButton setTitleColor:ButtonDisabledTextColor forState:UIControlStateNormal];
    [_saveButton.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:25]];
    
    _isEditionDone = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//-(void) viewWillAppear:(BOOL)animated
//{
//    [self.view setBackgroundColor:ViewBackgroundColor];
//    
//}

#pragma mark - TextField Delegate callbacks

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.activeTextField=textField;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSUInteger newLength = (textField.text.length - range.length) + string.length;
    
    NSString *testString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    testString = [testString stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    BOOL isValidField = YES;
    if([testString length] == 0)
    {
        isValidField = NO;
    }
    else
    {
        NSString *nameCharactersString = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ-' ";
        
        switch (textField.tag)
        {
            //case eDealerTypeTextFieldTag:
            //case eDealerGroupTextFieldTag:
            case eDealerCodeTextFieldTag:
            case eDealerNameTextFieldTag:
            //case eDateTextFieldTag:
            //case eLocalCarparkTextFieldTag:
            //case eLoyaltyCustomerTextFieldTag:
            //case eWorkingBayTextFieldTag:
            //case eTechnicalInformationTextFieldTag:
           // case eRegionTypeTextFieldTag:
           // case eProvinceTextFieldTag:
           // case eCityTypeTextFieldTag:
           // case eAddressTypeTextFieldTag:
            {
//                if(nil != textField && [testString length] > 0 )
//                {
//                    isValidField = [self checkValidityOfString:testString againstCharacters:nameCharactersString];
//                }
                 if(testString.length > kNameTextFieldLimit)
                {
                    isValidField = NO;
                    
                }
                //isValidField = ([self validateRegistrationValues] && isValidField) ? YES : NO;
                break;
            }

//            case eSizeTextFieldTag:
//            case eMonthlyRetailingTextFieldTag:
//            case eMonthlyThroughputTextFieldTag:
//            case eTechTurnoverRateTextFieldTag:
//            case eServiceRevenueTextFieldTag:
//            case eZipTypeTextFieldTag:
//            {
//                if ([[TrackUtility sharedUtility] isValidMobileNumber:testString])
//                {
//                    isValidField = (testString.length >= kNumberTextFieldMinLimit ) ? YES : NO;
//                    isValidField = ([self validateRegistrationValues] && isValidField) ? YES : NO;
//                }
//                else
//                    isValidField = NO;
//                
//                
//                break;
//            }
//            default:
//                break;
        }
    }
    //TODO Valid save button
    
//    if(isValidField)
//    {23
//        _saveButton.userInteractionEnabled = YES;
//        [_saveButton setTitleColor:CustomTextColor forState:UIControlStateNormal];
//        
//    }
//    else
//    {
//        _saveButton.userInteractionEnabled = NO;
//        [_saveButton setTitleColor:ButtonDisabledTextColor forState:UIControlStateNormal];
//    }
    
    //[self validateTextFieldValues];
    
    switch (textField.tag) {
//        case eDealerTypeTextFieldTag:
//        {
//            return [self isTextFieldEditable:textField withNewLength:newLength replacementString:string errorMsg:@"eDealerTypeTextFieldTag Limit" withLimit:kNameTextFieldLimit];
//            break;
//        }
//        case eDealerGroupTextFieldTag:
//        {
//            return [self isTextFieldEditable:textField withNewLength:newLength replacementString:string errorMsg:@"eDealerTypeTextFieldTag Limit" withLimit:kNameTextFieldLimit];
//            break;
//        }
        case eDealerCodeTextFieldTag:
        {
            return [self isTextFieldEditable:textField withNewLength:newLength replacementString:string errorMsg:@"eDealerTypeTextFieldTag Limit" withLimit:kNameTextFieldLimit];
            break;
        }
        case eDealerNameTextFieldTag:
        {
            return [self isTextFieldEditable:textField withNewLength:newLength replacementString:string errorMsg:@"eDealerTypeTextFieldTag Limit" withLimit:kNameTextFieldLimit];
            break;
        }
//        case eDateTextFieldTag:
//        {
//            return [self isTextFieldEditable:textField withNewLength:newLength replacementString:string errorMsg:@"eDealerTypeTextFieldTag Limit" withLimit:kNameTextFieldLimit];
//            break;
//        }
//        case eSizeTextFieldTag:
//        {
//            return [self isTextFieldEditable:textField withNewLength:newLength replacementString:string errorMsg:@"eDealerTypeTextFieldTag Limit" withLimit:kNameTextFieldLimit];
//            break;
//        }
//        case eMonthlyRetailingTextFieldTag:
//        {
//            return [self isTextFieldEditable:textField withNewLength:newLength replacementString:string errorMsg:@"eDealerTypeTextFieldTag Limit" withLimit:kNameTextFieldLimit];
//            break;
//        }
//        case eLocalCarparkTextFieldTag:
//        {
//            return [self isTextFieldEditable:textField withNewLength:newLength replacementString:string errorMsg:@"eDealerTypeTextFieldTag Limit" withLimit:kNameTextFieldLimit];
//            break;
//        }
//        case eLoyaltyCustomerTextFieldTag:
//        {
//            return [self isTextFieldEditable:textField withNewLength:newLength replacementString:string errorMsg:@"eDealerTypeTextFieldTag Limit" withLimit:kNameTextFieldLimit];
//            break;
//        }
//        case eMonthlyThroughputTextFieldTag:
//        {
//            return [self isTextFieldEditable:textField withNewLength:newLength replacementString:string errorMsg:@"eDealerTypeTextFieldTag Limit" withLimit:kNameTextFieldLimit];
//            break;
//        }
//        case eWorkingBayTextFieldTag:
//        {
//            return [self isTextFieldEditable:textField withNewLength:newLength replacementString:string errorMsg:@"eDealerTypeTextFieldTag Limit" withLimit:kNameTextFieldLimit];
//            break;
//        }
//        case eTechnicalInformationTextFieldTag:
//        {
//            return [self isTextFieldEditable:textField withNewLength:newLength replacementString:string errorMsg:@"eDealerTypeTextFieldTag Limit" withLimit:kNameTextFieldLimit];
//            break;
//        }
//        case eTechTurnoverRateTextFieldTag:
//        {
//            return [self isTextFieldEditable:textField withNewLength:newLength replacementString:string errorMsg:@"eDealerTypeTextFieldTag Limit" withLimit:kNameTextFieldLimit];
//            break;
//        }
//        case eServiceRevenueTextFieldTag:
//        {
//            return [self isTextFieldEditable:textField withNewLength:newLength replacementString:string errorMsg:@"eDealerTypeTextFieldTag Limit" withLimit:kNameTextFieldLimit];
//            break;
//        }
//        case eRegionTypeTextFieldTag:
//        {
//            return [self isTextFieldEditable:textField withNewLength:newLength replacementString:string errorMsg:@"eDealerTypeTextFieldTag Limit" withLimit:kNameTextFieldLimit];
//            break;
//        }
//        case eProvinceTextFieldTag:
//        {
//            return [self isTextFieldEditable:textField withNewLength:newLength replacementString:string errorMsg:@"eDealerTypeTextFieldTag Limit" withLimit:kNameTextFieldLimit];
//            break;
//        }
//        case eCityTypeTextFieldTag:
//        {
//            return [self isTextFieldEditable:textField withNewLength:newLength replacementString:string errorMsg:@"eDealerTypeTextFieldTag Limit" withLimit:kNameTextFieldLimit];
//            break;
//        }
//        case eZipTypeTextFieldTag:
//        {
//            return [self isTextFieldEditable:textField withNewLength:newLength replacementString:string errorMsg:@"eDealerTypeTextFieldTag Limit" withLimit:kNameTextFieldLimit];
//            break;
//        }
//        case eAddressTypeTextFieldTag:
//        {
//            return [self isTextFieldEditable:textField withNewLength:newLength replacementString:string errorMsg:@"eDealerTypeTextFieldTag Limit" withLimit:kNameTextFieldLimit];
//            break;
//        }
        default:
            break;
    }
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    _isEditionDone = NO;
    NSString *text = textField.text;
    NSString *newString = [text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    textField.text=newString;
    
    JVFloatLabeledTextField *textF = (JVFloatLabeledTextField*)textField;
    NSString *errorStr = [NSString stringWithFormat:@"%@: %@",LOCALIZED_ERROR_INVALID, [textField.attributedPlaceholder string]];
    
    //Remove the error view from the active textfield if textfield length == max length.
    switch (_activeTextField.tag)
    {
        //case eDealerTypeTextFieldTag:
        //case eDealerGroupTextFieldTag:
        case eDealerCodeTextFieldTag:
        case eDealerNameTextFieldTag:
        //case eDateTextFieldTag:
        //case eLocalCarparkTextFieldTag:
        //case eLoyaltyCustomerTextFieldTag:
        //case eWorkingBayTextFieldTag:
        //case eTechnicalInformationTextFieldTag:
       // case eRegionTypeTextFieldTag:
        //case eProvinceTextFieldTag:
        //case eCityTypeTextFieldTag:
        //case eAddressTypeTextFieldTag:
        {
            if(_activeTextField.text.length == kNameTextFieldLimit)
            {
                textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[textField.attributedPlaceholder string]
                                                                                 attributes:@{NSForegroundColorAttributeName: [UIColor blackColor]}];
                textF.floatingLabelTextColor = LightGrayTextColor;
                textF.floatingLabel.text = [textField.attributedPlaceholder string];
            }
            break;
        }
//        case eSizeTextFieldTag:
//        case eMonthlyRetailingTextFieldTag:
//        case eMonthlyThroughputTextFieldTag:
//        case eTechTurnoverRateTextFieldTag:
//        case eServiceRevenueTextFieldTag:
//        
//        {
//            if(_activeTextField.text.length >= kNumberTextFieldMinLimit)
//            {
//                textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[textField.attributedPlaceholder string]
//                                                                                  attributes:@{NSForegroundColorAttributeName: [UIColor blackColor]}];
//                textF.floatingLabelTextColor = LightGrayTextColor;
//                textF.floatingLabel.text = [textField.attributedPlaceholder string];
//            }
//            
//            break;
//        }
//        case eZipTypeTextFieldTag:
//        {
//            if(_activeTextField.text.length >= kZipTextFieldMinLimit && _activeTextField.text.length < kZipTextFieldMaxLimit)
//            {
//                textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[textField.attributedPlaceholder string]
//                                                                                  attributes:@{NSForegroundColorAttributeName: [UIColor blackColor]}];
//                textF.floatingLabelTextColor = LightGrayTextColor;
//                textF.floatingLabel.text = [textField.attributedPlaceholder string];
//            }
//            else
//            {
//                if(![textField requiredText] )
//                {
//                    textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[textField.attributedPlaceholder string]
//                                                                                      attributes:@{NSForegroundColorAttributeName: [UIColor redColor]}];
//                }
//                
//                else if(!(textField.text.length >= kZipTextFieldMinLimit) && (textField.text.length <= kZipTextFieldMaxLimit))
//                {
//                    textF.floatingLabelTextColor = [UIColor redColor];
//                    textF.floatingLabel.text = errorStr;
//                }
//            }
//            
//            break;
//        }
            
        default:
            break;
    }
    
    switch (textField.tag)
    {
       // case eDealerTypeTextFieldTag:
        //case eDealerGroupTextFieldTag:
        case eDealerCodeTextFieldTag:
        case eDealerNameTextFieldTag:
        //case eDateTextFieldTag:
        //case eLocalCarparkTextFieldTag:
       // case eLoyaltyCustomerTextFieldTag:
       // case eWorkingBayTextFieldTag:
       // case eTechnicalInformationTextFieldTag:
        //case eRegionTypeTextFieldTag:
        //case eProvinceTextFieldTag:
       // case eCityTypeTextFieldTag:
        //case eAddressTypeTextFieldTag:
        {
            if(![textField requiredText] )
            {
                textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[textField.attributedPlaceholder string]
                                                                                             attributes:@{NSForegroundColorAttributeName: [UIColor redColor]}];
            }

            else if (![self checkIfStringContainsAtLeastTwoCharacters:textField.text andAnySpecialCharacter:@"-' "])
            {
                textF.floatingLabelTextColor = [UIColor redColor];
                textF.floatingLabel.text = errorStr;
            }
//            else if(![self validateRegistrationValues:textField])
//            {
//                textF.floatingLabelTextColor = [UIColor redColor];
//                textF.floatingLabel.text = errorStr;
//            }
            else if([self validateTextFieldTextForSalutations:textField.text])
            {
                textF.floatingLabelTextColor = [UIColor redColor];
                textF.floatingLabel.text = errorStr;
            }
//            else
//            {
//                textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[textField.attributedPlaceholder string]
//                                                                                  attributes:@{NSForegroundColorAttributeName: [UIColor blackColor]}];
//                textF.floatingLabelTextColor = LightGrayTextColor;
//                textF.floatingLabel.text = [textField.attributedPlaceholder string];
//            }
            break;
        }
            
//        case eSizeTextFieldTag:
//        case eMonthlyRetailingTextFieldTag:
//        case eMonthlyThroughputTextFieldTag:
//        case eTechTurnoverRateTextFieldTag:
//        case eServiceRevenueTextFieldTag:
//        {
//            if(![textField requiredText] )
//            {
//                textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[textField.attributedPlaceholder string]
//                                                                                  attributes:@{NSForegroundColorAttributeName: [UIColor redColor]}];
//            }
//            else if(![self validateRegistrationValues:textField])
//            {
//                textF.floatingLabelTextColor = [UIColor redColor];;
//                textF.floatingLabel.text = errorStr;
//            }
//            
//            break;
//        }
//        case eZipTypeTextFieldTag:
//        {
//            
//            
//            break;
//        }

        default:
            break;
    }
    
    [self validateTextFieldValues];
    [textField resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


//Custom

-(void)validateTextFieldValues
{
    BOOL isValid = YES;
    
    {
        if([self validateRegistrationValues] && ![self validateForMaxLimitOfField])
            isValid = YES;
        else
            isValid = NO;
    }
    if(isValid)
    {
        _saveButton.userInteractionEnabled=YES;
        [_saveButton setTitleColor:CustomTextColor forState:UIControlStateNormal];
    }
    else
    {
        _saveButton.userInteractionEnabled=NO;
        [_saveButton setTitleColor:ButtonDisabledTextColor forState:UIControlStateNormal];
    }
}

-(BOOL)validateTextFieldTextForSalutations:(NSString *)inString
{
    BOOL isContainSalutation = NO;
    
    //Common Salutations and Titles: Dr, Mr, Mrs, Ms, Sr, Jr, Miss, PHD, BA, Sr, Jr, and, 1st, 2nd, 3rd,  4th, 5 th
    
    NSArray * salutationTitlesArray = [NSArray arrayWithObjects:@"Dr",@"Mr",@"Mrs",@"Ms",@"Sr",@"Jr",@"Miss",@"PHD",@"BA",@"1st",@"2nd",@"3rd",@"4th",@"5th", nil];
    
    //Sepearate strings by space
    NSArray *wordsAndEmptyStrings = [inString componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSArray *words = [wordsAndEmptyStrings filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"length > 0"]];
    
    for (NSString* eachWord in words)
    {
        for (NSString *commonStr in salutationTitlesArray)
        {
            if([eachWord caseInsensitiveCompare:commonStr]== NSOrderedSame)
            {
                isContainSalutation=YES;
                break;
            }
        }
        
    }
    
    return isContainSalutation;
}

-(BOOL)validateForMaxLimitOfField
{
    BOOL didCrossMaxLimit = NO;
    
    for (JVFloatLabeledTextField *textField in _textFields)
    {
        switch (textField.tag)
        {
            case eDealerTypeTextFieldTag:
            case eDealerGroupTextFieldTag:
            case eDealerCodeTextFieldTag:
            case eDealerNameTextFieldTag:
            case eDateTextFieldTag:
            case eLocalCarparkTextFieldTag:
            case eLoyaltyCustomerTextFieldTag:
            case eWorkingBayTextFieldTag:
            case eTechnicalInformationTextFieldTag:
            case eRegionTypeTextFieldTag:
            case eProvinceTextFieldTag:
            case eCityTypeTextFieldTag:
            case eAddressTypeTextFieldTag:
            {
                didCrossMaxLimit = (textField.text.length > kNameTextFieldLimit)? YES:NO;
                break;
                
            }
            case eSizeTextFieldTag:
            case eMonthlyRetailingTextFieldTag:
            case eMonthlyThroughputTextFieldTag:
            case eTechTurnoverRateTextFieldTag:
            case eServiceRevenueTextFieldTag:
            {
                didCrossMaxLimit = !((textField.text.length >= kNumberTextFieldMinLimit) ) ? YES:NO;
                break;
            }
            case eZipTypeTextFieldTag:
            {
                didCrossMaxLimit = !((textField.text.length >= kZipTextFieldMinLimit) && (textField.text.length <= kZipTextFieldMaxLimit)) ? YES:NO;
                break;
            }
                
                
            default:
                break;
        }
    }
    
    return didCrossMaxLimit;
}

-(BOOL)validateRegistrationValues:(UITextField *)withTextField
{
    BOOL isValidField=YES;
    // Handle checking against first name and last name for the presence of these characters.
    NSString *nameCharactersString = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ-' ";
    
    
    {
        switch (withTextField.tag)
        {
           // case eDealerTypeTextFieldTag:
            //case eDealerGroupTextFieldTag:
            case eDealerCodeTextFieldTag:
            case eDealerNameTextFieldTag:
            //case eDateTextFieldTag:
            //case eLocalCarparkTextFieldTag:
            //case eLoyaltyCustomerTextFieldTag:
            //case eWorkingBayTextFieldTag:
            //case eTechnicalInformationTextFieldTag:
            //case eRegionTypeTextFieldTag:
            //case eProvinceTextFieldTag:
           // case eCityTypeTextFieldTag:
           // case eAddressTypeTextFieldTag:
            {
                if([withTextField.text length] == 0)
                {
                    isValidField = NO;
                }
                else if(nil != withTextField && [withTextField.text length]>0 )
                {
                    isValidField = [self checkValidityOfString:withTextField.text againstCharacters:nameCharactersString];
                }
                else if(withTextField.text.length > kNameTextFieldLimit)
                {
                    isValidField = NO;
                }
                break;
            }
//            case eSizeTextFieldTag:
//            case eMonthlyRetailingTextFieldTag:
//            case eMonthlyThroughputTextFieldTag:
//            case eTechTurnoverRateTextFieldTag:
//            case eServiceRevenueTextFieldTag:
//            case eZipTypeTextFieldTag:
//            {
//                isValidField = (nil != withTextField && [withTextField.text length]>0) ? YES : NO;
//                isValidField = [[TrackUtility sharedUtility] isValidMobileNumber:withTextField.text] && isValidField;
//                
//                break;
//            }
            default:
                break;
        }
    }
    
    return isValidField;
}

- (BOOL)validateRegistrationValues
{
    BOOL isValidField=YES;
    // Handle checking against first name and last name for the presence of these characters.
    NSString *nameCharactersString = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ-' ";

    for (JVFloatLabeledTextField *textField in _textFields)
    {
        switch (textField.tag)
        {
            
            //case eDealerTypeTextFieldTag:
            //case eDealerGroupTextFieldTag:
            case eDealerCodeTextFieldTag:
            case eDealerNameTextFieldTag:
            //case eDateTextFieldTag:
            //case eLocalCarparkTextFieldTag:
            //case eLoyaltyCustomerTextFieldTag:
            //case eWorkingBayTextFieldTag:
            //case eTechnicalInformationTextFieldTag:
            //case eRegionTypeTextFieldTag:
            //case eProvinceTextFieldTag:
            //case eCityTypeTextFieldTag:
            //case eAddressTypeTextFieldTag:
            {
                if([textField.text length] == 0)
                {
                    isValidField = NO;
                    return NO;
                }
                
//                else if(nil != textField && [textField.text length]>0 )
//                {
//                    isValidField = [self checkValidityOfString:textField.text againstCharacters:nameCharactersString];
//                    if (isValidField == NO) {
//                        return NO;
//                    }
//                }
                else if(textField.text.length > kNameTextFieldLimit)
                {
                    isValidField = NO;
                    return NO;
                }
                else if (![self checkIfStringContainsAtLeastTwoCharacters:textField.text andAnySpecialCharacter:@"-' "])
                {
                    isValidField = NO;
                }
                else if([self validateTextFieldTextForSalutations:textField.text])
                {
                    isValidField = NO;
                }
                break;
            }
                
//            case eSizeTextFieldTag:
//            case eMonthlyRetailingTextFieldTag:
//            case eMonthlyThroughputTextFieldTag:
//            case eTechTurnoverRateTextFieldTag:
//            case eServiceRevenueTextFieldTag:
//            {
//                isValidField = (nil != textField && [textField.text length]>0) ? YES : NO;
//                isValidField = [[TrackUtility sharedUtility] isValidMobileNumber:textField.text] && isValidField;
//                
//                if (isValidField == NO) {
//                    return NO;
//                }
//                
//                break;
//            }
//            case eZipTypeTextFieldTag:
//            {
//                BOOL isValidField = (textField.text.length >= kZipTextFieldMinLimit && textField.text.length < kZipTextFieldMaxLimit);
//                isValidField = [[TrackUtility sharedUtility] isValidMobileNumber:textField.text] && isValidField;
//                if (isValidField == NO) {
//                    return NO;
//                }
//            }
            default:
                break;
        }
    }
    
    return isValidField;
}

-(BOOL)isTextFieldEditable:(UITextField *)textField withNewLength:(NSUInteger)newLength replacementString:(NSString *)string errorMsg:(NSString *)errorMessage withLimit:(NSUInteger)withLimit
{
    BOOL isEditable=YES;
    
    JVFloatLabeledTextField *textF = (JVFloatLabeledTextField*)textField;
    NSString *errorStr = [NSString stringWithFormat:@"%@: %@",LOCALIZED_ERROR_INVALID, [textField.attributedPlaceholder string]];

    if(textField == _sizeTextField || textField == _monthlyRetailingTextField || textField == _monthlyThroughputTextField || textField == _techTurnoverRateTextField || textField == _techTurnoverRateTextField || textField == _zipTextField)
    {
        int maxLimit= 14;
        
        NSCharacterSet* numberCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; ++i)
        {
            unichar c = [string characterAtIndex:i];
            if (![numberCharSet characterIsMember:c] || (newLength > maxLimit))
            {
                textF.floatingLabelTextColor = [UIColor redColor];
                textF.floatingLabel.text = errorStr;
                return NO;
            }
        }
        textF.floatingLabelTextColor = LightGrayTextColor;
        textF.floatingLabel.text = [textField.attributedPlaceholder string];
        return YES;
    }
    else if(textField == _dealerNameTextField || textField == _dealerCodeTextField)
    {
        
        if ([string isEqualToString:@""])
        {
            if (withLimit >=textField.text.length)
            {
                textF.floatingLabelTextColor = LightGrayTextColor;
                textF.floatingLabel.text = [textField.attributedPlaceholder string];
            }
            else
            {
                textF.floatingLabelTextColor = [UIColor redColor];
                textF.floatingLabel.text = errorStr;
            }
            return YES;
        }
        if(newLength <= withLimit)
        {
            textF.floatingLabelTextColor = LightGrayTextColor;
            textF.floatingLabel.text = [textField.attributedPlaceholder string];
            return YES;
        }
        else if([string length]>1)
        {
            textF.floatingLabelTextColor = [UIColor redColor];
            textF.floatingLabel.text = errorStr;
            return YES;
        }
        else if(newLength > withLimit)
        {
            textF.floatingLabelTextColor = [UIColor redColor];
            textF.floatingLabel.text = errorStr;
            return NO;
        }
        
    }
    return isEditable;
}

/*
 Method to check whether the string contains the specified characters in it.
 @param:
 instring , the string to be checked against the characters
 inCharactersString, string of sharacters to be checked for the presence in the string inString.
 */
-(BOOL)checkValidityOfString:(NSString *)inString againstCharacters:(NSString *)inCharactersString
{
    BOOL isValid = YES;
    NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:inCharactersString] invertedSet];
    
    if ([inString rangeOfCharacterFromSet:set].location != NSNotFound)
    {
        isValid = NO;
        
    }
    return isValid;
}

-(BOOL)checkIfStringContainsAtLeastTwoCharacters:(NSString*)inString andAnySpecialCharacter:(NSString*)specialCharacter
{
    BOOL isValid = YES;
    if ([inString rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:specialCharacter]].location != NSNotFound)
    {
        NSString *alphabetSet = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ";
        NSCharacterSet *alphabetCharacterSet = [NSCharacterSet characterSetWithCharactersInString:alphabetSet];;
        int index = 0;
        int numberOfCharacters = 0;
        for (index =0; index < inString.length; index++)
        {
            unichar c = [inString characterAtIndex:index];
            NSString *string = [NSString stringWithCharacters:&c length:1];
            if ([string rangeOfCharacterFromSet:alphabetCharacterSet].location != NSNotFound)
                numberOfCharacters ++;
        }
        if(numberOfCharacters < 2)
            isValid = NO;
    }
    return isValid;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
//    if ([segue.identifier isEqualToString:@"calenderViewController"])
//    {
//        _calendarVC = [segue destinationViewController];
//        _calendarVC.delegate = self;
//        _calendarVC.titleString = LOCALIZED_CALENDAR_POCA_DATE;
//    }
}

- (void) didDateSelected:(NSString *) date
{
//    //_dateLabel.text = date;
//    NSString *dateLabelString = [NSString stringWithFormat:@"%@ %@", date, @"    >"];
//    //[_dateButton setTitle:dateLabelString forState:UIControlStateNormal];
//    _dateTextField.text = dateLabelString;
}


- (void) didDateChange:(NSDate *) date
{
    [self dateForButton:date];
}

- (void) dateForButton:(NSDate*)date
{
    NSString *dateStr = [[self dateFormate] stringFromDate:date];
    NSString *dateLabelString = [NSString stringWithFormat:@"%@     >", dateStr];
   // [_dateButton setTitle:dateLabelString forState:UIControlStateNormal];
    _dateTextField.text = dateLabelString;
}

- (NSDateFormatter *) dateFormate
{
    //E MMM d yyyy
    //NSString *formatString = [NSDateFormatter dateFormatFromTemplate:@"E MMM d yyyy" options:0 locale:[NSLocale currentLocale]];
    NSString *formatString = [NSDateFormatter dateFormatFromTemplate:@"yyyy MMM d" options:NULL locale:[NSLocale currentLocale]];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:formatString];
    
    return dateFormatter;
}

- (IBAction)addDateTapped:(id)sender
{
    [_activeTextField resignFirstResponder];
    _datePopoverControl.popoverContentSize = CGSizeMake(319, 272);
    [_datePopoverControl presentPopoverFromRect:[sender bounds] inView:sender  permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
}

- (IBAction)addGroupTapped:(id)sender
{
    [_activeTextField resignFirstResponder];
    _groupVC.checkedIndexPath = _selectedIndexPath;
    _groupPopoverControl.popoverContentSize = CGSizeMake(348, 264);
    [_groupPopoverControl presentPopoverFromRect:[sender bounds] inView:sender  permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

- (void) didPositionChange:(NSString *) position
{
     NSString *positionStr = [NSString stringWithFormat:@"%@      >", position];
    _dealerGroupTextField.text = positionStr;
}

//- (void) didPositionChangeId:(NSInteger) index
//{
//
//}

- (IBAction)addDealerStaffTapped:(id)sender
{
    _addInfoVC = nil;
    _addInfoPopoverControl = nil;
    
    _addInfoVC = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"DealerInfoAddDealerStaffVC"];
    _addInfoVC.delegate = self;
    _addInfoVC.addType = eDealerAdd;
    _addInfoPopoverControl = [[UIPopoverController alloc] initWithContentViewController:_addInfoVC];
    
    //_addInfoVC.addType = eDealerAdd;
    [self showPopOver];
    
    //DealerInfoAddDealerStaffVC *modalController = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"DealerInfoAddDealerStaffVC"];
    //[self presentModalViewController:viewController animated:YES];
   // [self presentViewController:viewController animated:YES completion:nil];
    
//   // UIViewController* modalController = [[UIViewController alloc]init];
//    modalController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//    modalController.modalPresentationStyle =  UIModalPresentationFormSheet;
//    
//    CGPoint frameSize = CGPointMake([[UIScreen mainScreen] bounds].size.width*0.95f, [[UIScreen mainScreen] bounds].size.height*0.95f);
//    CGRect screenRect = [[UIScreen mainScreen] bounds];
//    CGFloat screenWidth = screenRect.size.width;
//    CGFloat screenHeight = screenRect.size.height;
//    
//    // Resizing for iOS 8
//    //modalController.preferredContentSize = CGSizeMake(675, 462);//CGSizeMake(frameSize.x, frameSize.y);
//    // Resizing for <= iOS 7
//   // modalController.view.superview.frame = CGRectMake(20, 20, 675, 462);
//    //modalController.view.superview.frame = CGRectMake((screenWidth - frameSize.x)/2, (screenHeight - frameSize.y)/2, frameSize.x, frameSize.y);
//    
//    UIViewController *vc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
//    [self presentViewController:modalController animated:YES completion:nil];

}

- (IBAction)saveAction:(id)sender
{
    _isEditionDone = YES;
}

- (void) showPopOver
{
    CGRect positionRect = CGRectMake(self.view.frame.origin.x - 25, 188, 675, 331);
    _addInfoPopoverControl.popoverContentSize = CGSizeMake(675, 384);
    [_addInfoPopoverControl presentPopoverFromRect:positionRect inView:self.view permittedArrowDirections:NO animated:YES];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_dealerStaffArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSDictionary *staffDictionary = [_dealerStaffArray objectAtIndex:indexPath.row];
    Staff *staff =  [_dealerStaffArray objectAtIndex:indexPath.row];
    StaffRole *staffRole = [StaffRole MR_findFirstByAttribute:@"role_id" withValue: staff.staff_role_id];
    
    DealerAddInfoTableViewCell *cell = (DealerAddInfoTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"dealerAddInfoTableViewCell"];
    UIButton *addBtn = [cell editButton];
    [addBtn addTarget:self action:@selector(editCellTapped:) forControlEvents:UIControlEventTouchUpInside];

    cell.backgroundColor = [UIColor clearColor];
    cell.dealerStaffName.text = staff.first_name; //[staffDictionary objectForKey:@"addName"];
    cell.dealerStaffPosition.text = staffRole.role_name;//[staffDictionary objectForKey:@"addPosition"];
    cell.dealerStaffPhone.text = staff.office_phone_number;//[staffDictionary objectForKey:@"addPhone"];
    cell.dealerStaffEmail.text = staff.email;//[staffDictionary objectForKey:@"addEmail"];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if(self.checkedIndexPath)
//    {
//        UITableViewCell* uncheckCell = [tableView cellForRowAtIndexPath:self.checkedIndexPath];
//        uncheckCell.accessoryType = UITableViewCellAccessoryNone;
//    }
//    UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
//    cell.accessoryType = UITableViewCellAccessoryCheckmark;
//    [[UITableViewCell appearance] setTintColor:CustomTextColor];
//    self.checkedIndexPath = indexPath;
//    cell.backgroundColor = [UIColor clearColor];
//    
//    
//    if ([_delegate respondsToSelector:@selector(didPositionChange:)])
//    {
//        [_delegate didPositionChange:[_positionArray objectAtIndex:indexPath.row ]];
//    }
}


- (void) dataAdded:(NSDictionary *)data selectedIndex:(NSIndexPath*)selectedIndex
{
    [_dealerStaffArray addObject:data];
    [_tableView reloadData];
    [_addInfoPopoverControl dismissPopoverAnimated:YES];
    [_selectedIndexArray addObject:selectedIndex];
    //_addInfoPopoverControl = nil;
}

- (void) dataAdded:(NSDictionary *)data
{
    // Send data to server
    NSDictionary *staffParams = @ {
                                @"first_name" : [data objectForKey:@"addName"],
                                @"middle_name" :@"",
                                @"last_name" : @"",
                                @"office_phone_number" : [data objectForKey:@"addPhone"],
                                @"residence_phone_number" : @"",
                                @"email" : [data objectForKey:@"addEmail"],
                                @"staff_role_id" : [data objectForKey:@"addId"]};
    
    Staff *staffNew = [Staff createEntity];
    staffNew.dealer_id = _selectedDealer.dealer_id;
    staffNew.email = [data objectForKey:@"addEmail"];
    staffNew.first_name = [data objectForKey:@"addName"];
    staffNew.middle_name = @"";
    staffNew.office_phone_number = [data objectForKey:@"addPhone"];
    staffNew.residence_phone_number = @"";
    staffNew.staff_id = 0000;
    staffNew.staff_role_id = [data objectForKey:@"addId"];
    staffNew.staff_updated = [NSNumber numberWithBool:true];
    
    [_selectedDealer addDealerTotalStaffObject:staffNew];
    
    [[NSManagedObjectContext defaultContext] saveWithOptions:MRSaveWithoutOptions completion:^(BOOL success, NSError *error) {
        
        NSLog(@"Staff first name = %@", [Staff findAll]);
        NSLog(@"Staff  = %ld", (unsigned long)[Staff findAll].count);
        
        //staffSuccess(true);
        [_dealerStaffArray removeAllObjects];
        _dealerStaffArray = [[Staff findAll] mutableCopy];
        [_tableView reloadData];
        
    }];

//    [NetworkingFactory staffPostAPICall:_selectedDealer.dealer_id withStaffDictionary:staffParams withSuccessCallBack:^(BOOL succcess) {
//
//      NSLog(@"ALL COOL POSTED Staff Roles");
//        
//        // Receive data
//        [NetworkingFactory dealerStaffListServiceCall:_selectedDealer withSuccessBlock:^(BOOL success) {
//            if (success)
//            {
//                NSLog(@"ALL COOL GOT dealer staff listing");
//                //[[NSManagedObjectContext defaultContext] deleteEntity];
//                BOOL success = [staff MR_deleteEntity];
//                if (success)
//                    NSLog(@"dataDeleted");
//                
//                [_dealerStaffArray removeAllObjects];
//                _dealerStaffArray = [[Staff findAll] mutableCopy];
//                [_tableView reloadData];
//            }
//        }];
//    }];
    [_addInfoPopoverControl dismissPopoverAnimated:YES];
}

- (void) dataEdited:(NSDictionary *)data selectedIndex:(NSIndexPath*)selectedIndex
{
    [_dealerStaffArray replaceObjectAtIndex:_selectedEditItem.row withObject:data];
    [_tableView reloadData];
    [_addInfoPopoverControl dismissPopoverAnimated:YES];
    [_selectedIndexArray replaceObjectAtIndex:_selectedEditItem.row withObject:selectedIndex];
}

- (void) dataEdited:(NSDictionary *)data
{
//    [_dealerStaffArray replaceObjectAtIndex:_selectedEditItem.row withObject:data];
//    [_tableView reloadData];
//    [_addInfoPopoverControl dismissPopoverAnimated:YES];
    
    Staff *staff =  [_dealerStaffArray objectAtIndex:_selectedEditItem.row];
    
//    // Send data to server
//    NSDictionary *staffParams = @ {
//        @"first_name" : [data objectForKey:@"addName"],
//        @"middle_name" :@"",
//        @"last_name" : @"",
//        @"office_phone_number" : [data objectForKey:@"addPhone"],
//        @"residence_phone_number" : @"",
//        @"email" : [data objectForKey:@"addEmail"],
//        @"staff_role_id" : [data objectForKey:@"addId"]};
    

    //Staff *staffNew = [Staff createEntity];
    staff.dealer_id = _selectedDealer.dealer_id;
    staff.email = [data objectForKey:@"addEmail"];
    staff.first_name = [data objectForKey:@"addName"];
    staff.middle_name = @"";
    staff.office_phone_number = [data objectForKey:@"addPhone"];
    staff.residence_phone_number = @"";
    staff.staff_id = staff.staff_id;
    staff.staff_role_id = [data objectForKey:@"addId"];
    staff.staff_updated = [NSNumber numberWithBool:true];
    
    [_selectedDealer addDealerTotalStaffObject:staff];
    
    [[NSManagedObjectContext defaultContext] saveWithOptions:MRSaveWithoutOptions completion:^(BOOL success, NSError *error) {
        
        NSLog(@"Staff first name = %@", [Staff findAll]);
        NSLog(@"Staff  = %ld", (unsigned long)[Staff findAll].count);
        
        //staffSuccess(true);
        [_dealerStaffArray removeAllObjects];
        _dealerStaffArray = [[Staff findAll] mutableCopy];
        [_tableView reloadData];
        
    }];
    //TODO
    //Call synch API for POST
//    
////    [NetworkingFactory staffPUTAPICall:_selectedDealer.dealer_id withStaffID:staff.staff_id withStaffDictionary:staffParams withSuccessCallBack:^(BOOL success) {
//    
//    
//    [NetworkingFactory staffPostAPICall:_selectedDealer.dealer_id withStaffDictionary:staffParams withSuccessCallBack:^(BOOL success) {
//        
//    //[NetworkingFactory staffPostAPICall:_selectedDealer.dealer_id withStaffDictionary:staffParams withSuccessCallBack:^(BOOL succcess) {
//        
////        NSLog(@"Staff Role: Data Updated");
////        
////        // Receive data
////        [NetworkingFactory dealerStaffListServiceCall:_selectedDealer withSuccessBlock:^(BOOL success) {
////            if (success)
////            {
////                NSLog(@"ALL COOL GOT dealer staff listing");
////                //[[NSManagedObjectContext defaultContext] deleteEntity];
//////                BOOL success = [staff MR_deleteEntity];
//////                if (success)
//////                    NSLog(@"dataDeleted");
////                
////                [_dealerStaffArray removeAllObjects];
////                _dealerStaffArray = [[Staff findAll] mutableCopy];
////                [_tableView reloadData];
////            }
////        }];
//    }];
    [_addInfoPopoverControl dismissPopoverAnimated:YES];
}

- (void) dataDeleted
{
     Staff *staff =  [_dealerStaffArray objectAtIndex:_selectedEditItem.row];
    
    [NetworkingFactory staffPUTDeleteAPICall:_selectedDealer.dealer_id withStaffID:staff.staff_id withSuccessCallBack:^(BOOL success) {
        
        if (success) {
            NSLog(@"Staff Role: Data Deleted = %@", staff.staff_id);
            
            // Receive data
            [NetworkingFactory dealerStaffListServiceCall:_selectedDealer withSuccessBlock:^(BOOL success) {
                if (success)
                {
                    NSLog(@"ALL COOL GOT dealer staff listing");
                    
                    [_dealerStaffArray removeAllObjects];
                    _dealerStaffArray = [[Staff findAll] mutableCopy];
                    [_tableView reloadData];
                }
            }];
        }
        else
        {
            NSLog(@"Error on deleting");
        }
        
    }];
    [_addInfoPopoverControl dismissPopoverAnimated:YES];
}

-(IBAction)editCellTapped:(id)sender
{
    UIButton *editButton = (UIButton *)sender;
    CGPoint location = [editButton.superview convertPoint:editButton.center toView:_tableView];
    NSIndexPath *indexPath = [_tableView indexPathForRowAtPoint:location];
    Staff *staff =  [_dealerStaffArray objectAtIndex:indexPath.row];
    
    _addInfoVC = nil;
    _addInfoPopoverControl = nil;
    
    _addInfoVC = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"DealerInfoAddDealerStaffVC"];
    _addInfoVC.delegate = self;
    _addInfoVC.addType = eDealerEdit;
    //_addInfoVC.staffDictionary = [_dealerStaffArray objectAtIndex:indexPath.row];
    //_addInfoVC.selectedIndex = [_selectedIndexArray objectAtIndex:indexPath.row];
    
    _addInfoVC.selectedStaff = [_dealerStaffArray objectAtIndex:indexPath.row];
    _addInfoVC.selectedID = staff.staff_role_id;
    
    _selectedEditItem = indexPath;
    _addInfoPopoverControl = [[UIPopoverController alloc] initWithContentViewController:_addInfoVC];
    
    [self showPopOver];
}

- (void) didPositionChange:(NSString *) position selectedIndex:(NSIndexPath*)selectedIndex
{
    _dealerGroupTextField.text = position;
    _selectedIndexPath = selectedIndex;
}

- (void) backPressedLocalAction
{
    if (_isEditionDone)
    {
        MainViewController *parentVC = [APP_DELEGATE appBaseViewController];
        [parentVC proceedBack];
    }
    else
    {
        NSLog(@"Show Alert");
        TRAlertView *av = [[TRAlertView alloc] initWithTitle:@"Info"
                                                     message:@"Do you want to discard the changes."
                                                    delegate:self
                                                      layout:eAlertTypeWithOk
                                               containsEmail:NO
                                                     dateStr:nil
                                           cancelButtonTitle:@"Cancel"
                                           otherButtonTitles:@"OK", nil];
        [av show];
    }
}

- (void)customAlertView:(TRAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
//    if(buttonIndex == 0)
//    {
//        NSLog(@"Cancel");
//        [self dismissViewControllerAnimated:YES completion:nil];
//    }
     if(buttonIndex == 1)
    {
        NSLog(@"OK");
        MainViewController *parentVC = [APP_DELEGATE appBaseViewController];
        [parentVC proceedBack];
    }
}

@end
