//
//  DealerAddInfoTableViewCell.h
//  TrackiOS
//
//  Created by Anish Kumar on 01/12/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DealerAddInfoTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *dealerStaffName;
@property (weak, nonatomic) IBOutlet UILabel *dealerStaffPosition;
@property (weak, nonatomic) IBOutlet UILabel *dealerStaffPhone;
@property (weak, nonatomic) IBOutlet UILabel *dealerStaffEmail;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *labelsOutlet;
@property (weak, nonatomic) IBOutlet UIButton *editButton;

//- (IBAction)editAction:(id)sender;

@end
