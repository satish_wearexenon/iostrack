//
//  PhotoDisplayViewController.h
//  TrackiOS
//
//  Created by John Paul Ranjith on 08/12/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoDisplayViewController : UIViewController<UIPageViewControllerDataSource
>
{
    
}

@property (strong, nonatomic) UIPageViewController *pageController;
@property (nonatomic, strong)  NSMutableArray *imageArray;
@end
