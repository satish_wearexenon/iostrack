//
//  Dealer1ViewController.m
//  TrackiOS
//
//  Created by Anish Kumar on 15/10/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "DealerListViewController.h"
#import "DealerDetailViewController.h"
#import "TRLoginLocalizer.h"
#import "DealerListTableViewCell.h"
#import "DealerModel.h"
#import "Dealer.h"
#import "DealerSubMenuRegion.h"
#import "DealerUpdateStatus.h"
#import "DealerChecklistViewController.h"

@interface DealerListViewController ()
- (IBAction)detailTapped:(id)sender;
- (IBAction)filterTouchedDown:(id)sender;
- (IBAction)filterDragExit:(id)sender;
- (IBAction)filterTouchCancel:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *sortByLabel;
@property (strong, nonatomic) IBOutlet UIView *filterBackView;
@property (weak, nonatomic) IBOutlet UIButton *filterNameButton;
@property (weak, nonatomic) IBOutlet UIButton *filterCodeButton;
@property (weak, nonatomic) IBOutlet UIButton *filterReadinessButton;
@property (weak, nonatomic) IBOutlet UIButton *filterDateButton;

@property (weak, nonatomic) IBOutlet UIImageView *nameUpArrow;
@property (weak, nonatomic) IBOutlet UIImageView *nameDownArrow;
@property (weak, nonatomic) IBOutlet UIImageView *codeUpArrow;
@property (weak, nonatomic) IBOutlet UIImageView *codeDownArrow;
@property (weak, nonatomic) IBOutlet UIImageView *readinessUpArrow;
@property (weak, nonatomic) IBOutlet UIImageView *readinessDownArrow;
@property (weak, nonatomic) IBOutlet UIImageView *dateUpArrow;
@property (weak, nonatomic) IBOutlet UIImageView *dateDownArrow;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic) BOOL arrowSelectedUp;
@property (nonatomic) int previousButtonSelectedTag;
@property (nonatomic) BOOL filterFullyTouched;

////Dictionary
//@property (nonatomic, strong) NS

//Arrays
@property (strong, nonatomic) NSArray *dealerLocationArray;
@property (strong, nonatomic) NSArray *dealerCodeArray;
@property (strong, nonatomic) NSArray *dealerDateArray;
@property (strong, nonatomic) NSArray *dealerProgresseArray;
@property (strong, nonatomic) NSArray *dealerContactNameArray;
@property (strong, nonatomic) NSArray *dealerItemUpdateCountArray;
@property (strong, nonatomic) NSArray *dealerPhotoUpdateCountArray;
@property (strong, nonatomic) NSArray *dealerDirectionArray;
@property (strong, nonatomic) NSArray *dealerProgresseAttributedArray;
@property (strong, nonatomic) NSArray *dealerStageArray;

@property (strong, nonatomic) NSMutableArray *graphImageArray;

@property (strong, nonatomic) NSMutableArray *dealerArray;

@property (strong, nonatomic) NSMutableArray *filterArray;

- (IBAction)filterButtonTapped:(id)sender;

@end

@implementation DealerListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _dealerArray = [NSMutableArray array];
    _graphImageArray = [NSMutableArray array];
    _previousButtonSelectedTag = _filterNameButton.tag;
    _filterFullyTouched = NO;

    //graph
    
    for (int i = 0 ; i < 12; i++)
    {
        //_graphView = [GraphView new];
        //[self.view addSubview:_graphView];
        _graphView.delegate = self;

        
        NSMutableDictionary *dataTemp = [[NSMutableDictionary alloc] init];
        
        //Array containing all the dates that will be displayed on the X axis
        NSArray *dataSetArray = [NSArray arrayWithObjects:
                                 @"DataSet1", @"DataSet2", @"DataSet3",
                                 @"DataSet4", @"DataSet5", nil];
        
        //Dictionary containing the name of the two sets and their associated color
        //used for the demo
        NSDictionary *sets = [NSDictionary dictionaryWithObjectsAndKeys:
                              [UIColor colorWithRed:154.0/255.0 green:204.0/255.0 blue:104.0/255.0 alpha:1], @"Plot 1",
                              [UIColor colorWithRed:255.0/255.0 green:102.0/255.0 blue:102.0/255.0 alpha:1], @"Plot 2",
                              [UIColor colorWithRed:194.0/255.0 green:194.0/255.0 blue:194.0/255.0 alpha:1], @"Plot 3", nil];
        
        //Generate random data for each set of data that will be displayed for each day
        //Numbers between 1 and 10
        for (NSString *dataSetObject in dataSetArray)
        {
    
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            int count = 0;
            int total = 0;
            int plot3Value = 0;
            for (NSString *set in sets)
            {
                count++;
                NSNumber *num = [NSNumber numberWithInt:arc4random_uniform(10)+1];
                if (![set isEqualToString:@"Plot 3"] )
                {
                    //[dict setObject:[NSNumber numberWithInt:(30 - total)] forKey:set];
                    if ([dataSetObject isEqualToString:@"DataSet1"] && i==0)
                    {
                        [dict setObject:[NSNumber numberWithInt:0] forKey:set];
                        total += 0;
                    }
                    else
                    {
                        [dict setObject:num forKey:set];
                        total += [num intValue];
                    }
                }
                else if ( count == 3)
                {
                    //if (plot3Value != 0)
                    {
                        [dict setObject:[NSNumber numberWithInt:(20 - total)] forKey:set];
                    }
                    //else
                    {
                    
                    }
                    
                }
                else
                    plot3Value = [num intValue];
                    
            }
            [dataTemp setObject:dict forKey:dataSetObject];
        }
        
        NSDictionary *data = [dataTemp copy];
        
        NSLog(@"%@", data);

        _graphView.dataSetNameArray = dataSetArray;
        _graphView.sets = sets;
        _graphView.data = data;
        [_graphView createGraph];
    }
    
    //NSLog(@"Dealer object = %@", [Dealer findFirst].dealer_stage_checklist);
    //NSLog(@"Count graph data  = %d", [DealerStageGraph findAll].count);
    //NSLog(@"Count staff data = %d", [DealerStaff findAll].count);
   // NSLog(@"Dealer codes = %@", [Dealer find)
    NSArray *dealerDataArray = [Dealer findAll];
    
//    for (Dealer *dealer in dealerDataArray)
//    {
//        //dealer
//    }
    
//    _dealerLocationArray = [NSArray arrayWithObjects:@"LocationA",@"LocationB",@"LocationC",@"LocationD",@"LocationE",@"LocationF", @"LocationA",@"LocationB",@"LocationC",@"LocationD",@"LocationE",@"LocationF",nil];
//    _dealerCodeArray = [NSArray arrayWithObjects:@"CodeF",@"CodeE",@"CodeA",@"CodeB",@"CodeD",@"CodeX",@"CodeF",@"CodeE",@"CodeA",@"CodeB",@"CodeD",@"CodeX",nil];
//    _dealerDateArray = [NSArray arrayWithObjects:@"01/12/2014",@"02/12/2014",@"03/12/2014",@"04/12/2014",@"05/12/2014",@"06/12/2014",@"01/12/2014",@"02/12/2014",@"03/12/2014",@"04/12/2014",@"05/12/2014",@"06/12/2014", nil];
//    _dealerProgresseArray = [NSArray arrayWithObjects:@"10",@"11",@"12",@"13",@"14",@"15",@"10",@"11",@"12",@"13",@"14",@"15", nil];
//    _dealerProgresseAttributedArray = _dealerProgresseArray;
//    _dealerContactNameArray = [NSArray arrayWithObjects:@"ContactA",@"ContactX",@"ContactN",@"ContactD",@"ContactE",@"ContactP",@"ContactA",@"ContactX",@"ContactN",@"ContactD",@"ContactE",@"ContactP", nil];
//    _dealerItemUpdateCountArray = [NSArray arrayWithObjects:@"8",@"3",@"21",@"12",@"2",@"5",@"8",@"3",@"21",@"12",@"2",@"5", nil];
//    _dealerPhotoUpdateCountArray = [NSArray arrayWithObjects:@"2",@"1",@"9",@"19",@"4",@"59",@"2",@"1",@"9",@"19",@"4",@"59", nil];
//    _dealerDirectionArray = [NSArray arrayWithObjects:@"East",@"East",@"South",@"North",@"East",@"West",@"East",@"West",@"South",@"North",@"East",@"South", nil];
//    _dealerStageArray = [NSArray arrayWithObjects:@"Pre-Launch", @"Pre-Launch", @"Implementation", @"Implementation", @"Pre-Launch", @"Operation",@"Operation", @"Operation",@"Pre-Launch", @"Pre-Launch", @"Pre-Launch", @"Operation",nil];
    
    //for (int i = 0; i < [_dealerLocationArray count]; i++)
    for (Dealer *dealer in dealerDataArray)
    {
//        NSDictionary *dealerDict = [[NSDictionary alloc] initWithObjectsAndKeys:
//                                    [_dealerLocationArray objectAtIndex:i], @"location",
//                                    [_dealerCodeArray objectAtIndex:i], @"code",
//                                    [_dealerDateArray objectAtIndex:i], @"date",
//                                    [self percentageAttributedString:[_dealerProgresseAttributedArray objectAtIndex:i]], @"progressAttributed",
//                                    [_dealerProgresseArray objectAtIndex:i], @"progress",
//                                    [_dealerContactNameArray objectAtIndex:i], @"contactName",
//                                    [NSString stringWithFormat:@"%@ %@", [_dealerItemUpdateCountArray objectAtIndex:i], LOCALIZED_DEALER_ITEMS_UPDATE], @"itemUpdateCount",
//                                    [NSString stringWithFormat:@"%@ %@", [_dealerPhotoUpdateCountArray objectAtIndex:i], LOCALIZED_DEALER_PHOTOS_ADDED], @"photoUpdateCount",
//                                    [_dealerDirectionArray objectAtIndex:i], @"direction",
//                                    [_dealerStageArray objectAtIndex:i], @"stage",nil];
        
        DealerSubMenuRegion *subMenuRegion = [DealerSubMenuRegion MR_findFirstByAttribute:@"dealermenu_region_id" withValue: dealer.dealer_region_id];
        DealerUpdateStatus *dealerUpdateStatus = [DealerUpdateStatus MR_findFirstByAttribute:@"dealer_id" withValue: dealer.dealer_id];
        NSLog(@"Status count = %@", [DealerUpdateStatus findFirst].dealer_id);
        
        BOOL isDateInvalid = [[TrackUtility sharedUtility] stringIsEmpty:dealer.start_of_operation];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd "];
        NSString *formattedDateString = @"";
        
        if (isDateInvalid)
        {
            formattedDateString = [dateFormatter stringFromDate:[NSDate date]];
        }
        else
        {
            long long lastModified = [dealer.start_of_operation longLongValue];
            NSDate *remoteModified = [NSDate dateWithTimeIntervalSince1970:lastModified/1000]; // /1000 remove trailing 4
            formattedDateString = [dateFormatter stringFromDate:remoteModified];
        }
        
        NSDictionary *dealerDict = [[NSDictionary alloc] initWithObjectsAndKeys:
                                    dealer.city, @"location",
                                    dealer.dealer_code, @"code",
                                    dealer.first_name, @"contactName",
                                    formattedDateString , @"date",
                                    //dealer.start_of_operation, @"date",
                                    dealer.stage, @"stage",
                                    [self percentageAttributedString:dealer.dealer_readiness], @"progressAttributed",
                                    dealer.dealer_readiness, @"progress",
                                    dealerUpdateStatus.iteamsadded , @"itemUpdateCount",
                                    dealerUpdateStatus.addedphotos , @"photoUpdateCount",
                                    subMenuRegion.dealermenu_region_name, @"direction",
                                    dealer.dealer_id , @"dealerID",
                                    dealer , @"dealer",
                                    nil];
        
        NSLog(@"dealerDict = %@", dealerDict);
        
        DealerModel *dealerModel = [[DealerModel alloc] initWithDictionary:dealerDict];
        [_dealerArray addObject:dealerModel];
    }
    
    //filterArray
    //NSPredicate *stagePred = [NSPredicate predicateWithFormat:@"stage = %@", @"pre-launch"];
    NSPredicate *stagePred = [NSPredicate predicateWithFormat:@"stage CONTAINS[cd] %@", @"operation"];
    NSPredicate *regionPred = [NSPredicate predicateWithFormat:@"direction CONTAINS[cd] %@", @"south"];
    NSPredicate *compoundedPred = [NSCompoundPredicate andPredicateWithSubpredicates:@[stagePred, regionPred]];
    _filterArray = [NSMutableArray arrayWithArray:[_dealerArray filteredArrayUsingPredicate:compoundedPred]];
   // _filterArray = [_dealerArray filteredArrayUsingPredicate:compoundedPred];
    
   // _dealerArray = [NSMutableArray arrayWithArray:[Dealer findAll]];
    
    // Sort Label
    _sortByLabel.text = LOCALIZED_SORT_BY;
    _sortByLabel.textColor = LightGrayTextColor;
    [_sortByLabel setFont:[UIFont fontWithName:Font_Track_Regular size:21]];
    
    // FILTER BUTTONS TEXT
    [_filterNameButton setTitle:LOCALIZED_FILTER_DEALER_NAME forState:UIControlStateNormal];
    [_filterCodeButton setTitle:LOCALIZED_FILTER_DEALER_CODE forState:UIControlStateNormal];
    [_filterReadinessButton setTitle:LOCALIZED_FILTER_DEALER_READINESS forState:UIControlStateNormal];
    [_filterDateButton setTitle:LOCALIZED_FILTER_DEALER_DATE forState:UIControlStateNormal];
    
    // FILTER BUTTONS FONT
    [_filterNameButton.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    [_filterCodeButton.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    [_filterReadinessButton.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    [_filterDateButton.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    
    _filterNameButton.selected = YES;
    _nameUpArrow.image = [UIImage imageNamed:@"arrowUpSelected"];
    
    _arrowSelectedUp = YES;
    _previousButtonSelectedTag = [_filterNameButton tag] - 2000;
    
    [_tableView reloadData];
}

- (void) imageCreated:(UIImage *)image
{
    [_graphImageArray addObject:image];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated
{
   // MainViewController *parentVC = [APP_DELEGATE appBaseViewController];
    //parentVC.currentDetailController = self;
}
- (void) viewDidAppear:(BOOL)animated
{
    MainViewController *parentVC = [APP_DELEGATE appBaseViewController];
    //parentVC.currentDetailController = self;
    
    parentVC.navLabel.text = parentVC.leftNavigationString;
    //parentVC.backView.hidden = YES;
    parentVC.backButton.hidden = YES;
    parentVC.navLabel.hidden = NO;
    _currentNavString = parentVC.leftNavigationString;

    
    //[_tableView reloadData];
}

- (void) populateNavHeader:(NSString *)sectionString regionStr:(NSString*)regionStr
{
    MainViewController *parentVC = [APP_DELEGATE appBaseViewController];
    parentVC.navLabel.text = [[NSString stringWithFormat:@"%@-%@-%@", @"Dealer", sectionString, regionStr] uppercaseString];
    _currentNavString = [[NSString stringWithFormat:@"%@-%@-%@", @"Dealer", sectionString, regionStr] uppercaseString];
    
    //NSPredicate *stagePred = [NSPredicate predicateWithFormat:@"stage = %@", sectionString];
    NSPredicate *stagePred = [NSPredicate predicateWithFormat:@"stage CONTAINS[cd] %@", sectionString];
    //NSPredicate *regionPred = [NSPredicate predicateWithFormat:@"direction = %@", regionStr];
    NSPredicate *regionPred = [NSPredicate predicateWithFormat:@"direction CONTAINS[cd] %@", regionStr];
    NSPredicate *compoundedPred = [NSCompoundPredicate andPredicateWithSubpredicates:@[stagePred, regionPred]];
    [_filterArray removeAllObjects];
    _filterArray = [NSMutableArray arrayWithArray:[_dealerArray filteredArrayUsingPredicate:compoundedPred]];
    //NSLog(@"filterArray = %@", _filterArray);
    [_tableView reloadData];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)detailViewTapped:(id)sender
{
}


- (IBAction)detailTapped:(id)sender
{
    DealerDetailViewController *viewController = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"DealerDetailViewController"];
    
    [self addChildViewController:viewController];
    [viewController didMoveToParentViewController:self];
    [self.view addSubview:viewController.view];
}

- (IBAction)filterTouchedDown:(id)sender
{
    _filterNameButton.selected = NO;
    _filterCodeButton.selected = NO;
    _filterReadinessButton.selected = NO;
    _filterDateButton.selected = NO;
    
    _filterFullyTouched = NO;
}

- (IBAction)filterDragExit:(id)sender
{
    if (!_filterFullyTouched)
    {
        UIButton *randomButton = (UIButton *)[_filterBackView viewWithTag:_previousButtonSelectedTag + 2000];
        randomButton.selected = YES;
    }
}

- (IBAction)filterTouchCancel:(id)sender
{
    if (!_filterFullyTouched)
    {
        UIButton *randomButton = (UIButton *)[_filterBackView viewWithTag:_previousButtonSelectedTag + 2000];
        randomButton.selected = YES;
    }
}

- (IBAction)filterButtonTapped:(id)sender
{
    _filterNameButton.selected = NO;
    _filterCodeButton.selected = NO;
    _filterReadinessButton.selected = NO;
    _filterDateButton.selected = NO;
    
    _filterFullyTouched = YES;
    
    int tag = [sender tag] - 2000;
    if (_previousButtonSelectedTag != tag)
    {
        _arrowSelectedUp = NO;
        [self resetArrows];
    }
    
    _previousButtonSelectedTag = tag;
    
    switch (tag)
    {
        case 0:
        {
            _filterNameButton.selected = YES;
            
            if (_arrowSelectedUp)
            {
                _arrowSelectedUp = NO;
                _nameUpArrow.image =   [UIImage imageNamed:@"arrowUpUnselected"];
                _nameDownArrow.image = [UIImage imageNamed:@"arrowDownSelected"];
                
                NSSortDescriptor *nameDescriptor = [NSSortDescriptor
                                                     sortDescriptorWithKey:@"location"
                                                     ascending:NO
                                                     selector:@selector(compare:)];
                NSArray *descriptors = @[nameDescriptor];
                [_filterArray sortUsingDescriptors:descriptors];
                [_tableView reloadData];
            }
            else
            {
                _arrowSelectedUp = YES;
                _nameUpArrow.image =   [UIImage imageNamed:@"arrowUpSelected"];
                _nameDownArrow.image = [UIImage imageNamed:@"arrowDownUnselected"];
                
                NSSortDescriptor *nameDescriptor = [NSSortDescriptor
                                                    sortDescriptorWithKey:@"location"
                                                    ascending:YES
                                                    selector:@selector(compare:)];
                NSArray *descriptors = @[nameDescriptor];
                [_filterArray sortUsingDescriptors:descriptors];
                [_tableView reloadData];
            }
            
        }
            break;
        case 1:
        {
            _filterCodeButton.selected = YES;
            
            if (_arrowSelectedUp)
            {
                _arrowSelectedUp = NO;
                _codeUpArrow.image =   [UIImage imageNamed:@"arrowUpUnselected"];
                _codeDownArrow.image = [UIImage imageNamed:@"arrowDownSelected"];
                
                NSSortDescriptor *nameDescriptor = [NSSortDescriptor
                                                    sortDescriptorWithKey:@"code"
                                                    ascending:NO
                                                    selector:@selector(compare:)];
                NSArray *descriptors = @[nameDescriptor];
                [_filterArray sortUsingDescriptors:descriptors];
                [_tableView reloadData];
            }
            else
            {
                _arrowSelectedUp = YES;
                _codeUpArrow.image =   [UIImage imageNamed:@"arrowUpSelected"];
                _codeDownArrow.image = [UIImage imageNamed:@"arrowDownUnselected"];
                
                NSSortDescriptor *nameDescriptor = [NSSortDescriptor
                                                    sortDescriptorWithKey:@"code"
                                                    ascending:YES
                                                    selector:@selector(compare:)];
                NSArray *descriptors = @[nameDescriptor];
                [_filterArray sortUsingDescriptors:descriptors];
                [_tableView reloadData];

            }
        }
            break;
        case 2:
        {
            _filterReadinessButton.selected = YES;
            
            if (_arrowSelectedUp)
            {
                _arrowSelectedUp = NO;
                _readinessUpArrow.image =   [UIImage imageNamed:@"arrowUpUnselected"];
                _readinessDownArrow.image = [UIImage imageNamed:@"arrowDownSelected"];
                
                NSSortDescriptor *nameDescriptor = [NSSortDescriptor
                                                    sortDescriptorWithKey:@"progress"
                                                    ascending:NO
                                                    selector:@selector(compare:)];
                NSArray *descriptors = @[nameDescriptor];
                [_filterArray sortUsingDescriptors:descriptors];
                [_tableView reloadData];

            }
            else
            {
                _arrowSelectedUp = YES;
                _readinessUpArrow.image =   [UIImage imageNamed:@"arrowUpSelected"];
                _readinessDownArrow.image = [UIImage imageNamed:@"arrowDownUnselected"];
                
                NSSortDescriptor *nameDescriptor = [NSSortDescriptor
                                                    sortDescriptorWithKey:@"progress"
                                                    ascending:YES
                                                    selector:@selector(compare:)];
                NSArray *descriptors = @[nameDescriptor];
                [_filterArray sortUsingDescriptors:descriptors];
                [_tableView reloadData];
            }
        }
            break;
        case 3:
        {
            _filterDateButton.selected = YES;
            
            if (_arrowSelectedUp)
            {
                _arrowSelectedUp = NO;
                _dateUpArrow.image =   [UIImage imageNamed:@"arrowUpUnselected"];
                _dateDownArrow.image = [UIImage imageNamed:@"arrowDownSelected"];
                
                NSSortDescriptor *nameDescriptor = [NSSortDescriptor
                                                    sortDescriptorWithKey:@"date"
                                                    ascending:NO
                                                    selector:@selector(compare:)];
                NSArray *descriptors = @[nameDescriptor];
                [_filterArray sortUsingDescriptors:descriptors];
                [_tableView reloadData];
            }
            else
            {
                _arrowSelectedUp = YES;
                _dateUpArrow.image =   [UIImage imageNamed:@"arrowUpSelected"];
                _dateDownArrow.image = [UIImage imageNamed:@"arrowDownUnselected"];
                
                NSSortDescriptor *nameDescriptor = [NSSortDescriptor
                                                    sortDescriptorWithKey:@"date"
                                                    ascending:YES
                                                    selector:@selector(compare:)];
                NSArray *descriptors = @[nameDescriptor];
                [_filterArray sortUsingDescriptors:descriptors];
                [_tableView reloadData];
            }
        }
            break;
            
        default:
            break;
    }
}

- (void) resetArrows
{
    _nameUpArrow.image =   [UIImage imageNamed:@"arrowUpUnselected"];
    _nameDownArrow.image = [UIImage imageNamed:@"arrowDownUnselected"];
    _codeUpArrow.image =   [UIImage imageNamed:@"arrowUpUnselected"];
    _codeDownArrow.image = [UIImage imageNamed:@"arrowDownUnselected"];
    _readinessUpArrow.image =   [UIImage imageNamed:@"arrowUpUnselected"];
    _readinessDownArrow.image = [UIImage imageNamed:@"arrowDownUnselected"];
    _dateUpArrow.image =   [UIImage imageNamed:@"arrowUpUnselected"];
    _dateDownArrow.image = [UIImage imageNamed:@"arrowDownUnselected"];
}

#pragma mark - UItableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_filterArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DealerListTableViewCell *dealerCell = (DealerListTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"dealerListCellId"];
    [dealerCell setBackgroundColor:[UIColor clearColor]];
    
    DealerModel *modelObj = [_filterArray objectAtIndex:indexPath.row];
   
    dealerCell.dealerLocationLabel.text = modelObj.location;
    dealerCell.dealerCodeLabel.text = modelObj.code;
    dealerCell.dealerProgressLabel.attributedText = modelObj.progressAttributed;
    dealerCell.dealerDateLabel.text = modelObj.date;
    dealerCell.dealerContactsLabel.text = modelObj.contactName;
    dealerCell.dealerItemsUpdateLabel.text = modelObj.itemUpdateCount;
    dealerCell.dealerPhotoUpateLabel.text = modelObj.photoUpdateCount;
    dealerCell.dealerDirectionLabel.text = modelObj.direction;
    dealerCell.graphImageView.image = [_graphImageArray objectAtIndex:indexPath.row];
    
    UIButton *graphBtn = [dealerCell grpahButton];
    [graphBtn addTarget:self action:@selector(graphButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    return dealerCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    //DealerListTableViewCell *dealerCell = (DealerListTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
    //[dealerCell setBackgroundColor:[UIColor redColor]];
    //dealerCell.contentView.backgroundColor = [UIColor whiteColor];
    DealerModel *modelObj = [_filterArray objectAtIndex:indexPath.row];
    
    DealerDetailViewController *viewController = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"DealerDetailViewController"];
    viewController.selectedDealer = modelObj.dealer;
    
    [self addChildViewController:viewController];
    [viewController didMoveToParentViewController:self];
    [self.view addSubview:viewController.view];
}

- (NSMutableAttributedString *) percentageAttributedString:(NSString*) percentage
{
    NSRange range = [LOCALIZED_PROGRESS_PERCENTAGE rangeOfString:@"00"];
    int initialPoint = 0;
    if (range.location != NSNotFound)
    {
        initialPoint = range.location;
    }
    int sizeOfStr = [percentage length];
    NSString *replacedStr = [LOCALIZED_PROGRESS_PERCENTAGE stringByReplacingOccurrencesOfString:@"00" withString:percentage];
    
    NSMutableAttributedString *progressStr =  [[NSMutableAttributedString alloc]init];
    [[progressStr mutableString] setString:replacedStr];
    [progressStr beginEditing];
    [progressStr addAttribute:NSFontAttributeName
                        value:[UIFont fontWithName:Font_Track_Bold size:16]
                        range:NSMakeRange(initialPoint, sizeOfStr++)];
    [progressStr endEditing];
    return progressStr;
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(IBAction)graphButtonTapped:(id)sender
{
    UIButton *button = (UIButton *)sender;
    CGPoint location = [button.superview convertPoint:button.center toView:_tableView];
    NSIndexPath *indexPath = [_tableView indexPathForRowAtPoint:location];
    DealerModel *modelObj = [_filterArray objectAtIndex:indexPath.row];
    
    DealerChecklistViewController *viewController = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"DealerChecklistViewController"];
    viewController.selectedDealer = modelObj.dealer;
    [self addChildViewController:viewController];
    [viewController didMoveToParentViewController:self];
    [self.view addSubview:viewController.view];
    
//    DealerChecklistViewController *viewController = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"DealerChecklistViewController"];
//    viewController.selectedDealer = modelObj.dealer;
//    [_parent addChildViewController:viewController];
//    [viewController didMoveToParentViewController:_parent];
//    [_parent.view addSubview:viewController.view];

    
}
@end
