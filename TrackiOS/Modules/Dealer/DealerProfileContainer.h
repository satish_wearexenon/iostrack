//
//  DealerProfileContainer.h
//  TrackiOS
//
//  Created by satish sharma on 11/4/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Dealer.h"

@class DealerDetailViewController;

/**
 The DealerProfileContainer class is a Container for Dealer profile screen
 */

@interface DealerProfileContainer : UIViewController

@property (strong, nonatomic) NSString *currentSegueIdentifier;

@property (strong, nonatomic) DealerDetailViewController *parent;
//@property (nonatomic, strong) NSNumber *currentSelectedDealerID;
@property (nonatomic, strong) Dealer *selectedDealer;


/**
 Method to swap the View
 */

-(void) swapViewController;
@end
