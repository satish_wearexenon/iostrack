//
//  CheckListBestPractiseVC.m
//  TrackiOS
//
//  Created by John Paul Ranjith on 06/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "CheckListBestPractiseVC.h"
#import "CheckListPhotoViewController.h"

@interface CheckListBestPractiseVC ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation CheckListBestPractiseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = LightGrayBackgroundColor;
    
    [self.cancelBtn setTitleColor:CustomTextColor forState:UIControlStateNormal];
    [self.saveBtn setTitleColor:CustomTextColor forState:UIControlStateNormal];
    _titleLabel.textColor = DarkGrayTextColor;

    _cancelBtn.tag = 1000;
    
    [_cancelBtn.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:25]];
    [_saveBtn.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    [_titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:25]];
    
    _titleTextView.layer.borderWidth = 1.0;
    _titleTextView.layer.borderColor = CheckListTextViewBorderColor;
    _titleTextView.textContainerInset = UIEdgeInsetsMake(10.0, 20.0, 10.0, 20.0);
    
    _descTextView.layer.borderWidth = 1.0;
    _descTextView.layer.borderColor = CheckListTextViewBorderColor;
    _descTextView.textContainerInset = UIEdgeInsetsMake(10.0, 20.0, 10.0, 20.0);
    
    _deletePhotoBtn1.hidden = YES;
    _deletePhotoBtn2.hidden = YES;
    _deletePhotoBtn3.hidden = YES;
    _deletePhotoBtn4.hidden = YES;
    _deletePhotoBtn5.hidden = YES;
    
    _saveBtn.enabled = NO;
    
     photoArray = [[NSMutableArray alloc] init];
    
    
    //Displaying images from local DB
    //NSMutableArray *array = [self fetchBestPractDataArray];
    //if([array count]>0){
    //    ChkListBestPractice *bestPractDat = [array objectAtIndex:0];
    if(self.bestPractData.photoObj1 != nil){
        
        [self.photoBtn1 setImage:[UIImage imageWithData:self.bestPractData.photoObj1] forState:UIControlStateNormal];
        
        //Logic to enable delete btn for the retrieved image from local db
        if([self.bestPractData.deletePhotoBtn1 isEqualToString:@"YES"])
            _deletePhotoBtn1.hidden= NO;
        else
            _deletePhotoBtn1.hidden= YES;
    }
    if(self.bestPractData.photoObj2 != nil){
        
        [self.photoBtn2 setImage:[UIImage imageWithData:self.bestPractData.photoObj2] forState:UIControlStateNormal];
        
        //Logic to enable delete btn for the retrieved image from local db
        if([self.bestPractData.deletePhotoBtn2 isEqualToString:@"YES"])
            _deletePhotoBtn2.hidden= NO;
        else
            _deletePhotoBtn2.hidden= YES;
        
    }
    if(self.bestPractData.photoObj3 != nil){
        
        [self.photoBtn3 setImage:[UIImage imageWithData:self.bestPractData.photoObj3] forState:UIControlStateNormal];
        
        //Logic to enable delete btn for the retrieved image from local db
        if([self.bestPractData.deletePhotoBtn3 isEqualToString:@"YES"])
            _deletePhotoBtn3.hidden= NO;
        else
            _deletePhotoBtn3.hidden= YES;
    }
    if(self.bestPractData.photoObj4 != nil){
        
        [self.photoBtn4 setImage:[UIImage imageWithData:self.bestPractData.photoObj4] forState:UIControlStateNormal];
        
        //Logic to enable delete btn for the retrieved image from local db
        if([self.bestPractData.deletePhotoBtn4 isEqualToString:@"YES"])
            _deletePhotoBtn4.hidden= NO;
        else
            _deletePhotoBtn4.hidden= YES;
    }
    if(self.bestPractData.photoObj5 != nil){
        
        [self.photoBtn5 setImage:[UIImage imageWithData:self.bestPractData.photoObj5] forState:UIControlStateNormal];
        
        //Logic to enable delete btn for the retrieved image from local db
        if([self.bestPractData.deletePhotoBtn5 isEqualToString:@"YES"])
            _deletePhotoBtn5.hidden= NO;
        else
            _deletePhotoBtn5.hidden= YES;
    }

    self.titleTextView.text = self.bestPractData.title;
    self.descTextView.text = self.bestPractData.descriptionStr;
        
    //}
}

//Method to save the edited data
 -(IBAction)saveAction:(id)sender
{
    [self.titleTextView resignFirstResponder];
    [self.descTextView resignFirstResponder],
    
    [self insertBestPractData];
}


- (void)customAlertView:(TRAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;
{
    UIButton *btn = (UIButton*)buttonSender;
    if(btn.tag == 1000)
    {
        if(buttonIndex == 0)
        {
            NSLog(@"Cancel");
            [self.delegate parentCallBack:nil];
            
        }
        else if(buttonIndex == 1)
        {
            if(_saveBtn.enabled){
                [self insertBestPractData];
                [self.delegate parentCallBack:nil];
            }
        }
    }
    else{
        
        if(buttonIndex == 0)
        {
            NSLog(@"Cancel");
            [self.delegate parentCallBack:nil];
            
        }
        else if(buttonIndex == 1)
        {
            _saveBtn.enabled = YES;
            [self disableDeleteBtn:buttonSender];
        }
    }
}

-(IBAction)deletePhotoAction:(id)sender
{
    TRAlertView *av = [[TRAlertView alloc] initWithTitle:@"Info"
                                                 message:@"Do you want to delete the photo."
                                                delegate:self
                                                  layout:eAlertTypeWithOk
                                           containsEmail:NO
                                                 dateStr:nil
                                       cancelButtonTitle:@"Cancel"
                                       otherButtonTitles:@"OK", nil];
    [av show];
    
    buttonSender = sender;
    
}


//Method to cancel the edited data
-(IBAction)cancelAction:(id)sender
{
    buttonSender = sender;
    
    if(_saveBtn.enabled)
    {
        TRAlertView *av = [[TRAlertView alloc] initWithTitle:@"Info"
                                                     message:@"Do you want to save the changes."
                                                    delegate:self
                                                      layout:eAlertTypeWithOk
                                               containsEmail:NO
                                                     dateStr:nil
                                           cancelButtonTitle:@"Cancel"
                                           otherButtonTitles:@"OK", nil];
        [av show];
    }
    else
    {
        [self.delegate parentCallBack:nil];
    }

}

-(void)presentDataPopOver:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    
    selectedButton = btn;
    
    DataPopOverVC *dataPopOverVC = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"DataPopOverVC"];
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    [array addObject:@"Use Camera"];
    [array addObject:@"Choose from Album"];
    
    dataPopOverVC.delegate = self;
    
    _popVC = [[UIPopoverController alloc] initWithContentViewController:dataPopOverVC];
    
    _popVC.popoverContentSize = CGSizeMake(200, 100.0);
    
    CGRect positionRect = CGRectMake(btn.frame.origin.x, btn.frame.origin.y, btn.frame.size.width, btn.frame.size.height);
    
    [_popVC presentPopoverFromRect:positionRect  inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    
    dataPopOverVC.viewType = eCameraOptionPopOver;
    dataPopOverVC.dataArray = array;
    
}

-(void)dataSelectedCallBack:(id)data
{
    [_popVC dismissPopoverAnimated:YES];
    
    NSLog(@"selected data %@",data);
    
    NSString *selectedData = [NSString stringWithFormat:@"%@",data];
    
    photoVC = [[CapturePhotoVCViewController alloc] init];
    photoVC.delegate = self;
    photoVC.parentVC = self;
    [photoVC showCameraView:selectedData];
    
}

-(void)capturedPhoto:(UIImage*)imageData
{
    _saveBtn.enabled = YES;
    
    NSLog(@"captued...");
    [selectedButton setImage:imageData forState:UIControlStateNormal];
    [self enableDeleteBtn:selectedButton];
    
    if(![photoArray containsObject:selectedButton])
        [photoArray addObject:selectedButton];
    
}

-(void) enableDeleteBtn:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    
    if(btn == _photoBtn1){
        _deletePhotoBtn1.hidden= NO;
        self.bestPractData.deletePhotoBtn1 = @"YES";
    }
    else if(btn == _photoBtn2)
    {
        _deletePhotoBtn2.hidden= NO;
        self.bestPractData.deletePhotoBtn2 = @"YES";
    }
    else if(btn == _photoBtn3)
    {
        _deletePhotoBtn3.hidden= NO;
        self.bestPractData.deletePhotoBtn3 = @"YES";
    }
    else if(btn == _photoBtn4)
    {
        _deletePhotoBtn4.hidden= NO;
        self.bestPractData.deletePhotoBtn4 = @"YES";
    }
    else if(btn == _photoBtn5)
    {
        _deletePhotoBtn5.hidden= NO;
        self.bestPractData.deletePhotoBtn5 = @"YES";
    }
}

-(void) disableDeleteBtn:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    
    if(btn == _deletePhotoBtn1){
        _deletePhotoBtn1.hidden= YES;
        [_photoBtn1 setImage:[UIImage imageNamed:@"CheckListAddPhotoBtn.png"] forState:UIControlStateNormal];
        self.bestPractData.deletePhotoBtn1 = @"NO";
        [photoArray removeObject:_photoBtn1];
    }
    else if(btn == _deletePhotoBtn2){
        _deletePhotoBtn2.hidden= YES;
        [_photoBtn2 setImage:[UIImage imageNamed:@"CheckListAddPhotoBtn.png"] forState:UIControlStateNormal];
        self.bestPractData.deletePhotoBtn2 = @"NO";
        [photoArray removeObject:_photoBtn2];
    }
    else if(btn == _deletePhotoBtn3){
        _deletePhotoBtn3.hidden= YES;
        [_photoBtn3 setImage:[UIImage imageNamed:@"CheckListAddPhotoBtn.png"] forState:UIControlStateNormal];
        self.bestPractData.deletePhotoBtn3 = @"NO";
        [photoArray removeObject:_photoBtn3];
    }
    else if(btn == _deletePhotoBtn4){
        _deletePhotoBtn4.hidden= YES;
        [_photoBtn4 setImage:[UIImage imageNamed:@"CheckListAddPhotoBtn.png"] forState:UIControlStateNormal];
        self.bestPractData.deletePhotoBtn4 = @"NO";
        [photoArray removeObject:_photoBtn4];
    }
    else if(btn == _deletePhotoBtn5){
        _deletePhotoBtn5.hidden= YES;
        [_photoBtn5 setImage:[UIImage imageNamed:@"CheckListAddPhotoBtn.png"] forState:UIControlStateNormal];
        self.bestPractData.deletePhotoBtn5 = @"NO";
        [photoArray removeObject:_photoBtn5];
    }
}

/**
 Method to capture photo
 */
-(IBAction)capturePhotoAction:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    
    //if(btn.imageView.image == [UIImage imageNamed:@"CheckListAddPhotoBtn.png"])
    if(![photoArray containsObject:btn])
    {
        NSLog(@"no image");
        [self presentDataPopOver:sender];
    }
    else{
        NSLog(@"image");
        CheckListPhotoViewController *viewController = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"CheckListPhotoViewController"];
        viewController.capturedImage = btn.imageView.image;
        viewController.modalPresentationStyle = UIModalPresentationFullScreen;
        [self presentViewController:viewController animated:YES completion:NULL];
    }
    
    
    NSLog(@"capturePhotoAction");
}

//Textview delegate method
- (void)textViewDidBeginEditing:(UITextView *)textView {
    
    NSLog(@"textViewDidBeginEditing");
    _saveBtn.enabled = YES;
}

//Textview delegate method
- (void)textViewDidEndEditing:(UITextView *)textView{
    
    NSLog(@"textViewDidEndEditing %@",textView.text);
    
}

//Method to insert best Practice array
-(void)insertBestPractData
{
    self.bestPractData.title = self.titleTextView.text;
    self.bestPractData.descriptionStr = self.descTextView.text;
    
    self.bestPractData.photoObj1 = UIImagePNGRepresentation(self.photoBtn1.imageView.image);
    self.bestPractData.photoObj2 = UIImagePNGRepresentation(self.photoBtn2.imageView.image);
    self.bestPractData.photoObj3 = UIImagePNGRepresentation(self.photoBtn3.imageView.image);
    self.bestPractData.photoObj4 = UIImagePNGRepresentation(self.photoBtn4.imageView.image);
    self.bestPractData.photoObj5 = UIImagePNGRepresentation(self.photoBtn5.imageView.image);
    
    
    [[NSManagedObjectContext defaultContext] saveWithOptions:MRSaveWithoutOptions completion:^(BOOL success, NSError *error) {
        
        NSLog(@"Inserted");
        TRAlertView *av = [[TRAlertView alloc] initWithTitle:@"Info"
                                                     message:@"Best practice saved successfully."
                                                    delegate:self
                                                      layout:eAlertTypeWithOk
                                               containsEmail:NO
                                                     dateStr:nil
                                           cancelButtonTitle:nil
                                           otherButtonTitles:@"OK", nil];
        [av show];
        [self.delegate parentCallBack:nil];
    }];
    
    
}

//Method to fetch best Practice array
-(NSMutableArray*)fetchBestPractDataArray
{
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    for (ChkListBestPractice *bestPract in (NSArray*)[ChkListBestPractice findAll]) {
        
        if(self.bestPractData.mainSectionID == bestPract.mainSectionID && self.bestPractData.subSectionID == bestPract.subSectionID && self.bestPractData.rowID == bestPract.rowID)
        {
            if(bestPract.photoObj1 != nil || bestPract.photoObj2 != nil || bestPract.photoObj3 != nil || bestPract.photoObj4 != nil || bestPract.photoObj5 != nil)
                [array addObject:bestPract];
                NSLog(@"photo %@",bestPract);
                break;
        }
        
    }
    return array;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
