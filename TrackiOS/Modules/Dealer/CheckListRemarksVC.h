//
//  CheckListRemarksVC.h
//  TrackiOS
//
//  Created by John Paul Ranjith on 06/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChkListRemarks.h"
#import "TRAlertView.h"

@protocol CheckListRemarksVCDelegate <NSObject>

-(void)parentCallBack:(id)data;


@end


/**
 The CheckListRemarksVC class is present the remarks pop over view
 */
@interface CheckListRemarksVC : UIViewController<UITextViewDelegate,TRAlertViewDelegate>
{
    id buttonSender;
}

/**
 This property holds a reference of pop over view
 */
@property (nonatomic, assign) BOOL isPreviousHistExists;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UITextView *remarksTextView;

@property (nonatomic) id <CheckListRemarksVCDelegate> delegate;

@property (nonatomic, assign)  BOOL isEditing;


@property(nonatomic,assign) NSNumber *mainSectionID;
@property(nonatomic,assign) NSNumber *subSectionID;
@property(nonatomic,assign) NSNumber *rowID;

@property(nonatomic) ChkListRemarks *remarkData;

/**
 Method to save the edited data
 */
-(IBAction)saveAction:(id)sender;

/**
 Method to cancel the edited data
 */
-(IBAction)cancelAction:(id)sender;

@end
