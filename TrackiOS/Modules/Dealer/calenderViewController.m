//
//  calenderViewController.m
//  TrackiOS
//
//  Created by satish sharma on 11/21/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "CalenderViewController.h"

@interface CalenderViewController ()

@end

@implementation CalenderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated
{
    [_titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:17.0f]];
    [_titleLabel setTextColor:[UIColor blackColor]];
    _titleLabel.text = _titleString;
    
    [_dateLabel setFont:[UIFont fontWithName:Font_Track_Regular size:17.0f]];
    [_dateLabel setTextColor:OrangeTextColor];
    
    [_datePicker addTarget:self action:@selector(selectDate:)
     forControlEvents:UIControlEventValueChanged];
    
    NSDate *defaultDate = [_datePicker date];
    _dateLabel.text = [[self dateFormate] stringFromDate:defaultDate];
    

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)selectDate:(id)sender {
   
    NSDate *defaultDate = [_datePicker date];
    NSString *date = [[self dateFormate] stringFromDate:defaultDate];
    _dateLabel.text = date;
    
    if ([_delegate respondsToSelector:@selector(didDateSelected:)]) {
        [_delegate didDateSelected:date];
    }
    //[_delegate didDateSelected:date];
    if ([_delegate respondsToSelector:@selector(didDateChange:)]) {
        [_delegate didDateChange:defaultDate];
    }
    
    //[_delegate didDateChange:defaultDate];
}

-(NSDateFormatter *) dateFormate
{
     //E MMM d yyyy
    NSString *formatString = [NSDateFormatter dateFormatFromTemplate:@"E MMM d yyyy" options:0 locale:[NSLocale currentLocale]];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:formatString];
    
    return dateFormatter;
}

@end
