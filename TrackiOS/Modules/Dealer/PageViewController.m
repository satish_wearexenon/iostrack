//
//  PageViewController.m
//  TrackiOS
//
//  Created by John Paul Ranjith on 08/12/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "PageViewController.h"

@interface PageViewController ()

@end

@implementation PageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
    [_cancelBtn setTitleColor:CustomTextColor forState:UIControlStateNormal];
    [_saveBtn setTitleColor:CustomTextColor forState:UIControlStateNormal];
    _screenNumber.textColor = WhiteBGColor;
    
    [_cancelBtn.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:25]];
    [_saveBtn.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:25]];
    [_screenNumber setFont:[UIFont fontWithName:Font_Track_Regular size:25]];
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.screenNumber.text = _pageNo;
    self.imageViewObj.image = self.imageObj;
    _saveBtn.hidden         = NO;
    _cancelBtn.hidden       = NO;
    _dividerImg.hidden      = NO;
    _screenNumber.hidden    = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    _saveBtn.hidden         = YES;
    _cancelBtn.hidden       = YES;
    _dividerImg.hidden      = YES;
    _screenNumber.hidden    = YES;
}

-(IBAction)cancelAction:(id)sender
{
    NSLog(@"cancel");
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(IBAction)saveAction:(id)sender
{
    NSLog(@"save");
    UIImageWriteToSavedPhotosAlbum(self.imageViewObj.image,nil,nil,nil);
    TRAlertView *av = [[TRAlertView alloc] initWithTitle:@"Info"
                                                 message:@"Saved to album successfully."
                                                delegate:self
                                                  layout:eAlertTypeWithOk
                                           containsEmail:NO
                                                 dateStr:nil
                                       cancelButtonTitle:nil
                                       otherButtonTitles:@"OK", nil];
    [av show];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
