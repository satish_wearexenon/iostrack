//
//  DealerPopOverVC.m
//  TrackiOS
//
//  Created by satish sharma on 11/5/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "DealerPopOverVC.h"
#import "DealerStagePopCell.h"

typedef enum{
   eSelectedCell   = 0,
   eUnselectedCell = 1
}cellType;

@interface DealerPopOverVC () <UITableViewDataSource, UITableViewDelegate>
{
    BOOL isSelected;
}
@property (weak, nonatomic) IBOutlet UITableView *stagePopTableView;


@end

@implementation DealerPopOverVC

- (void)viewDidLoad {
    [super viewDidLoad];
   
    // Do any additional setup after loading the view.
}

-(void) viewWillAppear:(BOOL)animated
{
    
    [_stagePopTableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    [_stagePopTableView setDataSource:self];
    [_stagePopTableView setDelegate:self];
    [_stagePopTableView setScrollEnabled:NO];    
     isSelected = YES;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_noOfStageArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"DealerStagePopCell";
    
    DealerStagePopCell *cell = (DealerStagePopCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
        if (cell == nil) {
            cell = [[DealerStagePopCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            
        }
    
    [_stagePopTableView setSeparatorInset:UIEdgeInsetsZero];
    cell.nameLabel.textColor = DarkGrayTextColor;
    cell.nameLabel.text = [NSString stringWithFormat:@"%@",[_noOfStageArray objectAtIndex:indexPath.row]];
    cell.checkImage.hidden = (BOOL)[[_checkMarkArray objectAtIndex:indexPath.row] boolValue];
    
    
    return cell;
        // Configure the cell...
    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    

}

- (void)reloadRowsAtIndexPaths:(NSArray *)indexPaths withRowAnimation:(UITableViewRowAnimation)animation
{
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Before: %@",_checkMarkArray);

        for (int i=0; i<[_checkMarkArray count]; i++) {
        [_checkMarkArray replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:YES]];
        }
    
    [_checkMarkArray replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithBool:NO]];
    NSLog(@"After: %@",_checkMarkArray);
    [_stagePopTableView reloadData];

    [_delegate updateButtonTitle:[_noOfStageArray objectAtIndex:indexPath.row]];
}

@end
