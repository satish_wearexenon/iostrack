//
//  CheckListRowData.h
//  TrackiOS
//
//  Created by John Paul Ranjith on 25/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 The CheckListRowData class to handle data
 */
@interface CheckListRowData : NSObject
{
    

}

/**
 This property holds a reference data class
 */
@property (nonatomic, strong) NSString *bestPracticeOrActionPlanTitle;

@property (nonatomic, strong) NSString *referenceTitle;
@property (nonatomic, strong) NSString *photoTitle;
@property (nonatomic, strong) NSString *remarkTitle;

@property (nonatomic, assign) BOOL  hasRemarkHistory;


@property (nonatomic, strong) UIImage *selectedImageColor;
@property (nonatomic, assign) NSInteger selectedColor;
@property (nonatomic, assign) NSInteger rowID;

@end
