//
//  CheckListPhotoVC.h
//  TrackiOS
//
//  Created by John Paul Ranjith on 06/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataPopOverVC.h"
#import "CapturePhotoVCViewController.h"
#import "ChkListPhoto.h"
#import "TRAlertView.h"

@protocol CheckListPhotoVCDelegate <NSObject>

-(void)parentCallBack:(id)data;


@end

/**
 The CheckListPhotoVC class is used to present the photo view pop over
 */
@interface CheckListPhotoVC : UIViewController<DataPopOverVCDelegate,CapturedPhotoVCDelegate,UIPopoverControllerDelegate,TRAlertViewDelegate>
{
    UIButton *selectedButton;
    CapturePhotoVCViewController *photoVC;
    
    NSMutableArray *photoArray;
    
    id buttonSender;
    
}

/**
 This property holds a reference of pop over view
 */
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;

@property (weak, nonatomic) IBOutlet UIButton *photoBtn1;
@property (weak, nonatomic) IBOutlet UIButton *photoBtn2;
@property (weak, nonatomic) IBOutlet UIButton *photoBtn3;
@property (weak, nonatomic) IBOutlet UIButton *photoBtn4;
@property (weak, nonatomic) IBOutlet UIButton *photoBtn5;

@property (weak, nonatomic) IBOutlet UIButton *deletePhotoBtn1;
@property (weak, nonatomic) IBOutlet UIButton *deletePhotoBtn2;
@property (weak, nonatomic) IBOutlet UIButton *deletePhotoBtn3;
@property (weak, nonatomic) IBOutlet UIButton *deletePhotoBtn4;
@property (weak, nonatomic) IBOutlet UIButton *deletePhotoBtn5;

@property (nonatomic) ChkListPhoto *photoData;

@property (nonatomic, strong) UIPopoverController *popVC;

@property (nonatomic) id <CheckListPhotoVCDelegate> delegate;

/**
 Method to save the edited data
 */
-(IBAction)saveAction:(id)sender;

/**
 Method to cancel the edited data
 */
-(IBAction)cancelAction:(id)sender;

@end
