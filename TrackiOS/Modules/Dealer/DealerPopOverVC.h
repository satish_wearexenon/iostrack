//
//  DealerPopOverVC.h
//  TrackiOS
//
//  Created by satish sharma on 11/5/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 The DealerPopOverVCDelegate class is contain call back method of PopOver view for dealer list
 */
@protocol DealerPopOverVCDelegate

-(void) updateButtonTitle:(NSString *) title;
@end

/**
 The DealerPopOverVC class is present PopOver view for dealer list
 */

@interface DealerPopOverVC : UIViewController

@property (strong, nonatomic) NSMutableArray *noOfStageArray;
@property (strong, nonatomic) NSMutableArray *checkMarkArray;

@property (nonatomic) id<DealerPopOverVCDelegate> delegate;


@end
