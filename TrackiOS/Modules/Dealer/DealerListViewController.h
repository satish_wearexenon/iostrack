//
//  DealerListViewController.h
//  TrackiOS
//
//  Created by Anish Kumar on 15/10/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GraphView.h"

@interface DealerListViewController : UIViewController <GraphViewViewDelegate>

@property (nonatomic, strong) NSString *currentNavString;
@property (nonatomic, strong) IBOutlet GraphView *graphView;
@property (strong, nonatomic) DealerListViewController *parent;
/**
 Populate Navigation Bar Labels
 Params:
 sectionString : Sub-Section string (Pre-Launch)
 regionStr : Region string (East, West....)
 
 */
- (void) populateNavHeader:(NSString *)sectionString regionStr:(NSString*)regionStr;
- (void) imageCreated:(UIImage *)image;

@end
