//
//  CheckListRemarksHistoryVC.m
//  TrackiOS
//
//  Created by John Paul Ranjith on 19/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "CheckListRemarksHistoryVC.h"
#import "CheckListRemarksHistCell.h"

#define TotalNoOfCharacters 200
#define MaxCharDesc @"Remarks characters should not exceed 200 characters"

@interface CheckListRemarksHistoryVC ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (strong, nonatomic) CheckListRemarksHistCell *stubCell;
@end

@implementation CheckListRemarksHistoryVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = LightGrayBackgroundColor;
    self.remarkHistArray = [[NSMutableArray alloc] init];
    
    [self hardCodedData];
    [self alignUIControls];
}

//Method to position the UI Controls
-(void)alignUIControls
{
    [_clearBtn setTitleColor:CustomTextColor forState:UIControlStateNormal];
    [_cancelBtn setTitleColor:CustomTextColor forState:UIControlStateNormal];
    [_saveBtn setTitleColor:CustomTextColor forState:UIControlStateNormal];
    _titleLabel.textColor = DarkGrayTextColor;
    
    [_cancelBtn.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:25]];
    [_saveBtn.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    [_clearBtn.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    
    [_titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:25]];
    
    
    _remarksTextView.layer.borderWidth = 1.0;
    _remarksTextView.layer.borderColor = CheckListTextViewBorderColor;
    _remarksTextView.textContainerInset = UIEdgeInsetsMake(10.0, 20.0, 10.0, 20.0);
    
    self.remarksTextView.delegate = self;
    
    if([self.remarksTextView.text length] == 0)
    {
        self.remarksTextView.text = @"Remarks characters should not exceed 200 characters";
        self.remarksTextView.textColor = LightGrayTextColor;
    }
    else{
        self.remarksTextView.textColor = BlackColor;
    }
    
    _saveBtn.enabled = NO;
    
    self.remarksTextView.text = self.remarkData.remarksStr;
}

- (void)customAlertView:(TRAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;
{
    UIButton *btn = (UIButton*)buttonSender;
    if(btn.tag == 1000)
    {
        if(buttonIndex == 0)
        {
            NSLog(@"Cancel");
            [self.delegate parentCallBack:nil];
            
        }
        else if(buttonIndex == 1)
        {
            if(_saveBtn.enabled){
                [self insertRemarksData];
            }
        }
        
    }
}

//UITextview delegate methods
- (void)textViewDidBeginEditing:(UITextView *)textView {
    
    NSLog(@"textViewDidBeginEditing");
}

//UITextview delegate methods
- (void)textViewDidEndEditing:(UITextView *)textView{
    
    NSLog(@"textViewDidEndEditing %@",textView.text);
    
}

//Method to cancel edited data
-(IBAction)cancelAction:(id)sender
{
    buttonSender = sender;
    
    if(_saveBtn.enabled)
    {
        TRAlertView *av = [[TRAlertView alloc] initWithTitle:@"Info"
                                                     message:@"Do you want to save the changes."
                                                    delegate:self
                                                      layout:eAlertTypeWithOk
                                               containsEmail:NO
                                                     dateStr:nil
                                           cancelButtonTitle:@"Cancel"
                                           otherButtonTitles:@"OK", nil];
        [av show];
    }
    else
    {
        [self.delegate parentCallBack:nil];
    }
}

//Method to cancel edited data
-(IBAction)clearAction:(id)sender
{
    self.remarksTextView.text = @"";
    //[self.delegate parentCallBack:nil];
}

//Method to save edited data
-(IBAction)saveAction:(id)sender
{
    [self.remarksTextView resignFirstResponder];
    [self insertRemarksData];
}

#pragma mark - UItableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.remarkHistArray count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CheckListRemarksHistCell *remarkHistListCell = (CheckListRemarksHistCell *)[tableView dequeueReusableCellWithIdentifier:@"CheckListRemarksHistCell"];

    NSString *timeStampData = [[self.remarkHistArray objectAtIndex:indexPath.row] objectForKey:@"timestamp"];
    
    [self configureCell:remarkHistListCell atIndexPath:indexPath];
    remarkHistListCell.timeStamp.text = timeStampData;
    
    return remarkHistListCell;

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    CheckListRemarksHistCell *uncheckCell = (CheckListRemarksHistCell *)[tableView dequeueReusableCellWithIdentifier:@"CheckListRemarksHistCell"];
//    
//    //CheckListRemarksHistCell* uncheckCell = (CheckListRemarksHistCell *)[tableView cellForRowAtIndexPath:indexPath];
//    
//    [self configureCell:uncheckCell atIndexPath:indexPath];
//    [uncheckCell layoutSubviews];
//    CGFloat height = [uncheckCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
//    return height +4;
//    
    return 100;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 104;
}

- (void)configureCell:(CheckListRemarksHistCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    cell.histTextLbl.text = [[self.remarkHistArray objectAtIndex:indexPath.row] objectForKey:@"remark"];
}


//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
////    NSString * text = [[self.remarkHistArray objectAtIndex:indexPath.row] objectForKey:@"remark"];
////    return [self heightForTextViewRectWithWidth:640.0 andText:text];
//    
//    static UIFont* dynamicTextFont;
//    static CGRect textFrame;
//    static CGFloat extraHeight;
//    // if( !dynamicTextFont )
//    //{
//        CheckListRemarksHistCell  *cell = (CheckListRemarksHistCell *)[_historyTextTbl dequeueReusableCellWithIdentifier:@"CheckListRemarksHistCell"];
//        dynamicTextFont = cell.histTextLbl.font;
//        CGRect cellFrame = cell.frame;
//        textFrame = cell.histTextLbl.frame;
//        extraHeight = cellFrame.size.height-textFrame.size.height; // The space above and below the growing field
//    //}
//    NSString* text = [[self.remarkHistArray objectAtIndex:indexPath.row] objectForKey:@"remark"];
//    CGSize  size = [text sizeWithFont:dynamicTextFont constrainedToSize:CGSizeMake(textFrame.size.width, 200000.f) lineBreakMode:UILineBreakModeWordWrap];
//    
//    CGSize maximumLabelSize = CGSizeMake(640, FLT_MAX);
//    
//    CGSize expectedLabelSize = [text sizeWithFont:cell.histTextLbl.font constrainedToSize:maximumLabelSize lineBreakMode:cell.histTextLbl.lineBreakMode];
//    
//    //adjust the label the the new height.
//    CGRect newFrame = cell.histTextLbl.frame;
//    newFrame.size.height = expectedLabelSize.height;
//    cell.histTextLbl.frame = newFrame;
//    
//    return size.height+extraHeight+60;
//    
//    
//}

//-(CGFloat)heightForTextViewRectWithWidth:(CGFloat)width andText:(NSString *)text
//{
////    UIFont * font = [UIFont fontWithName:Font_Track_Regular size:16];
////    
////    // this returns us the size of the text for a rect but assumes 0, 0 origin
////    CGSize size = [text sizeWithAttributes:@{NSFontAttributeName: font}];
////    
////    // so we calculate the area
////    CGFloat area = size.height * size.width;
////    
////    CGFloat buffer = 60.0f;
////    
////    // and then return the new height which is the area divided by the width
////    // Basically area = h * w
////    // area / width = h
////    // for w we use the width of the actual text view
////    return floor(area/width) + buffer;
//    
//
//    
//}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    if([self.remarksTextView.text isEqualToString:MaxCharDesc])
    {
        self.remarksTextView.text = @"";
        self.remarksTextView.textColor = BlackColor;
    }
    return YES;
}

-(BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    _saveBtn.enabled = YES;
    
    if([self.remarksTextView.text length] == 0)
    {
        self.remarksTextView.text = @"Remarks characters should not exceed 200 characters";
        self.remarksTextView.textColor = LightGrayTextColor;
    }
    return YES;
}

-(void)textViewDidChange:(UITextView *)textView
{
    
    int len = self.remarksTextView.text.length;
    
    if(len == TotalNoOfCharacters){
        TRAlertView *av = [[TRAlertView alloc] initWithTitle:@"Info"
                                                     message:@"Remarks characters should not exceed 200 characters."
                                                    delegate:self
                                                      layout:eAlertTypeWithOk
                                               containsEmail:NO
                                                     dateStr:nil
                                           cancelButtonTitle:nil
                                           otherButtonTitles:@"OK", nil];
        [av show];
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text length] == 0)
    {
        if([textView.text length] != 0)
        {
            return YES;
        }
    }
    else if([[textView text] length] > TotalNoOfCharacters-1)
    {
        return NO;
    }
    return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


//Method to insert remark data into DB
-(void)insertRemarksData
{
    self.remarkData.remarksStr = self.remarksTextView.text;
    
    [[NSManagedObjectContext defaultContext] saveWithOptions:MRSaveWithoutOptions completion:^(BOOL success, NSError *error) {
        NSLog(@"saved successfully");
        NSLog(@"After inserting %@",self.remarkData);
        
        TRAlertView *av = [[TRAlertView alloc] initWithTitle:@"Info"
                                                     message:@"Remark saved successfully."
                                                    delegate:self
                                                      layout:eAlertTypeWithOk
                                               containsEmail:NO
                                                     dateStr:nil
                                           cancelButtonTitle:nil
                                           otherButtonTitles:@"OK", nil];
        [av show];
        
        [self.delegate parentCallBack:nil];
    }];
    
}


-(void)hardCodedData
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:@"All staffs are well trainied including sales manager, after sales manager All staffs are well trainied including sales manager, after sales manager All staffs are well trainied " forKey:@"remark"];
    [dict setObject:@"2014/10/16 14:22:30" forKey:@"timestamp"];

    [self.remarkHistArray addObject:dict];

    NSMutableDictionary *dict2 = [[NSMutableDictionary alloc] init];
    [dict2 setObject:@"All staffs are well trainied including sales manager, after sales manager All staffs are well trainied including sales manager, after sales manager All staffs are well trainied " forKey:@"remark"];
    [dict2 setObject:@"2014/10/16 14:22:30" forKey:@"timestamp"];
    [self.remarkHistArray addObject:dict2];

    NSMutableDictionary *dict3 = [[NSMutableDictionary alloc] init];
    [dict3 setObject:@"All staffs are well trainied including sales manager, after sales manager All staffs are well trainied including sales manager, after sales manager All staffs are well trainied " forKey:@"remark"];
    [dict3 setObject:@"2014/10/16 14:22:30" forKey:@"timestamp"];
    [self.remarkHistArray addObject:dict3];

    NSMutableDictionary *dict4 = [[NSMutableDictionary alloc] init];
    [dict4 setObject:@"All staffs are well trainied including sales manager, after sales manager All staffs are well trainied including sales manager, after sales manager All staffs are well trainied " forKey:@"remark"];
    [dict4 setObject:@"2014/10/16 14:22:30" forKey:@"timestamp"];
    [self.remarkHistArray addObject:dict4];
}

@end
