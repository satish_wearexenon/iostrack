//
//  DealerStageCell.h
//  TrackiOS
//
//  Created by satish sharma on 11/4/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 The DealerStageCell class is used to maintain the table view cell
 */

@interface DealerStageCell : UITableViewCell

/**
 Properties for the UI Controls
 */
@property (weak, nonatomic) IBOutlet UILabel *dealerStageLabel;
@property (weak, nonatomic) IBOutlet UILabel *itemLabel;
@property (weak, nonatomic) IBOutlet UILabel *noofPhotoLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *percentageLabel;
@property (weak, nonatomic) IBOutlet UIButton *exportButton;

/**
 This property holds a reference of pop over view
 */
@property (nonatomic, strong) UIPopoverController *popVC;

/**
 Method to present the PDF & csv pop Over
 */

- (IBAction)exportPDFButtonClick:(id)sender;

@end
