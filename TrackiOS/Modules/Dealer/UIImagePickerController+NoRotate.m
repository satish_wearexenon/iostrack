//
//  UIImagePickerController+NoRotate.m
//  TrackiOS
//
//  Created by John Paul Ranjith on 24/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "UIImagePickerController+NoRotate.h"

@implementation UIImagePickerController_NoRotate

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end

