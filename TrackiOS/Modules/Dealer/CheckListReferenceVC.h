//
//  CheckListReferenceVC.h
//  TrackiOS
//
//  Created by John Paul Ranjith on 11/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CheckListReferenceVCDelegate <NSObject>

-(void)parentCallBack:(id)data;

@end

/**
 The CheckListReferenceVC class is present the remarks pop over view
 */
@interface CheckListReferenceVC : UIViewController
{
    
}

/**
 This property holds a reference of pop over view
 */
@property (weak, nonatomic) IBOutlet UIButton *okayBtn;
@property (weak, nonatomic) IBOutlet UITextView *referenceTextView;
@property (nonatomic) id <CheckListReferenceVCDelegate> delegate;

/**
 Method to dismiss okay button
 */
-(IBAction)okayAction:(id)sender;

@end
