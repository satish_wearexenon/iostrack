//
//  DealerDetailViewController.m
//  TrackiOS
//
//  Created by Anish Kumar on 03/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "DealerDetailViewController.h"
#import "DealerChecklistViewController.h"
#import "DealerListViewController.h"

@interface DealerDetailViewController ()

@end

@implementation DealerDetailViewController

-(void) viewWillAppear:(BOOL)animated
{
    MainViewController *parentVC = [APP_DELEGATE appBaseViewController];
    
    //Need to test it
    
    parentVC.currentDetailController = self;
    
    [self.view setBackgroundColor:ViewBackgroundColor];
    [_summaryButton setBackgroundImage:[UIImage imageNamed:@"unselectedButton.png"] forState:UIControlStateNormal];
    [_summaryButton setBackgroundImage:[UIImage imageNamed:@"selectedButton.png"] forState:UIControlStateSelected];
    [_summaryButton.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:19.0f]];
    [_summaryButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [_summaryButton setSelected:YES];
    
    [_actionPlanButton setBackgroundImage:[UIImage imageNamed:@"unselectedButton.png"] forState:UIControlStateNormal];
    [_actionPlanButton setBackgroundImage:[UIImage imageNamed:@"selectedButton.png"] forState:UIControlStateSelected];
    [_actionPlanButton.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:19.0f]];
    [_actionPlanButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [_actionPlanButton setSelected:NO];
    
    [_bestPracticeButton setBackgroundImage:[UIImage imageNamed:@"unselectedButton.png"] forState:UIControlStateNormal];
    [_bestPracticeButton setBackgroundImage:[UIImage imageNamed:@"selectedButton.png"] forState:UIControlStateSelected];
    [_bestPracticeButton.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:19.0f]];
    [_bestPracticeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [_bestPracticeButton setSelected:NO];
    
    [_dealerInfoButton setBackgroundImage:[UIImage imageNamed:@"unselectedButton.png"] forState:UIControlStateNormal];
    [_dealerInfoButton setBackgroundImage:[UIImage imageNamed:@"selectedButton.png"] forState:UIControlStateSelected];
    [_dealerInfoButton.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:19.0f]];
    [_dealerInfoButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [_dealerInfoButton setSelected:NO];
    
        //--temporary code ---------------
   // [_actionPlanButton setEnabled:NO];
    //[_bestPracticeButton setEnabled:NO];
  //  [_dealerInfoButton setEnabled:NO];
    //--------------------------------
    
}

- (void) viewDidAppear:(BOOL)animated
{
    MainViewController *parentVC = [APP_DELEGATE appBaseViewController];
    
    parentViewControler = parentVC.currentDetailController;
    //parentViewControlerTitle = [parentVC.backLabel.text uppercaseString];
    parentViewControlerTitle = [parentVC.backButton.titleLabel.text uppercaseString];
    
    [parentVC.backButton setTitle:[parentVC.backLabel.text uppercaseString] forState:UIControlStateNormal];
    
    parentVC.navLabel.hidden = YES;
    //parentVC.backView.hidden = NO;
    parentVC.backButton.hidden = NO;
   // parentVC.backLabel.text = [@"DEALER PROFILE" uppercaseString];
    [parentVC.backButton setTitle:[@"DEALER PROFILE" uppercaseString] forState:UIControlStateNormal];
    parentVC.currentDetailController = self;

    
}


-(void) viewWillDisappear:(BOOL)animated
{
   // [[NSNotificationCenter defaultCenter] removeObserver:self name:@"backPressed" object:nil];
    
    MainViewController *parentVC = [APP_DELEGATE appBaseViewController];
    //parentVC.backView.hidden = YES;
    parentVC.backButton.hidden = YES;
    
    parentVC.currentDetailController = parentViewControler;
    
    if ([parentVC.currentDetailController isKindOfClass: [DealerListViewController class]])
    {
        DealerListViewController *vc = (DealerListViewController*)parentVC.currentDetailController;
        MainViewController *parentVC = [APP_DELEGATE appBaseViewController];
        parentVC.navLabel.text = vc.currentNavString;
    }
    
    parentVC.navLabel.hidden = NO;
    parentVC.navLabel.text = parentVC.leftNavigationString;//parentViewControlerTitle;
    
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.z
   
    [self summaryButtonFilterClick:nil];
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(backPressedLocalAction)  name:@"backPressed" object:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"dealerContaineEmbed"]) {
        _dealercontainerViewController = [segue destinationViewController];
    }
}
/*@"actionPlanEmbed"
#define SegueIdentifierSecond @"summaryEmbed"
#define SegueIdentifierThird  @"bestPracticeEmbed"
#define SegueIdentifierFour   @"dealerInfoEmbed"
*/
- (IBAction)summaryButtonFilterClick:(id)sender {
    
    NSLog(@"summaryButtonFilterClick");
    
    [_actionPlanButton setSelected:NO];
    [_bestPracticeButton setSelected:NO];
    [_dealerInfoButton setSelected:NO];
    
     _dealercontainerViewController.currentSegueIdentifier = @"summaryEmbed";
     _dealercontainerViewController.parent = self;
    [_dealercontainerViewController swapViewController];
}

- (IBAction)actionPlanFilterButtonClick:(id)sender {
    NSLog(@"actionPlanFilterButtonClick");
    [_summaryButton setSelected:NO];
    [_actionPlanButton setSelected:YES];
    [_bestPracticeButton setSelected:NO];
    [_dealerInfoButton setSelected:NO];

    _dealercontainerViewController.currentSegueIdentifier = @"actionPlanEmbed";
    [_dealercontainerViewController swapViewController];
    
}

- (IBAction)bestPracticeFilterButtonClick:(id)sender {
    NSLog(@"bestPracticeFilterButtonClick");
    [_summaryButton setSelected:NO];
    [_actionPlanButton setSelected:NO];
    [_bestPracticeButton setSelected:YES];
    [_dealerInfoButton setSelected:NO];
    
    _dealercontainerViewController.currentSegueIdentifier = @"bestPracticeEmbed";
    _dealercontainerViewController.parent = self;
    [_dealercontainerViewController swapViewController];
}

- (IBAction)dealerInfoFilterButtonClick:(id)sender {
    NSLog(@"dealerInfoFilterButtonClick");
    [_summaryButton setSelected:NO];
    [_actionPlanButton setSelected:NO];
    [_bestPracticeButton setSelected:NO];
    [_dealerInfoButton setSelected:YES];
    
    _dealercontainerViewController.currentSegueIdentifier = @"dealerInfoEmbed";
    _dealercontainerViewController.selectedDealer = _selectedDealer;
    [_dealercontainerViewController swapViewController];
}

- (void)drawCellSeparator {
    
}

- (IBAction)CheckListActionPlanButtonClick:(id)sender
{
    
}

- (void) backPressedLocalAction
{
    MainViewController *parentVC = [APP_DELEGATE appBaseViewController];
    [parentVC proceedBack];
}
@end
