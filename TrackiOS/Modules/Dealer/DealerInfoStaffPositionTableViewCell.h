//
//  DealerInfoStaffPositionTableViewCell.h
//  TrackiOS
//
//  Created by Anish Kumar on 25/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DealerInfoStaffPositionTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *positionLabel;

@end
