//
//  DealerInfoAddDealerStaffVC.m
//  TrackiOS
//
//  Created by Anish Kumar on 24/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "DealerInfoAddDealerStaffVC.h"
#import "DealerGroupPopVC.h"
#import "DealerGroupPopVC.h"
#import "DealerGroupPopVC.h"
#import "UITextField+Validation.h"
#import "StaffRole.h"
#import "JVFloatLabeledTextField.h"

@interface DealerInfoAddDealerStaffVC () <DealerGroupPopVCDelegate>

@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (weak, nonatomic) IBOutlet UIButton *editSavedButton;
@property (weak, nonatomic) IBOutlet UIButton *positionButton;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *positionLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;

@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *nameTextField;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *phoneTextField;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *emailTextField;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *positionTextField;
@property (strong, nonatomic) IBOutletCollection(JVFloatLabeledTextField) NSArray *collectedTextField;

@property (nonatomic, strong) UITextField *activeTextField;

@property (nonatomic, strong) UIPopoverController *popoverControl;
@property (strong, nonatomic) DealerGroupPopVC *dealerInfoStaffPositionVC;
@property (strong, nonatomic) DealerGroupPopVC *dealerGroupPopVC;

@property (weak, nonatomic) IBOutlet UILabel *nameErrorLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneErrorLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailErrorLabel;

@property (weak, nonatomic) IBOutlet UIImageView *dividerImage;

//@property (nonatomic, strong) NSIndexPath* selectedIndexPath;

@property (nonatomic, strong) NSArray *positionArray;

- (IBAction)cancelAction:(id)sender;
- (IBAction)saveAction:(id)sender;
- (IBAction)positionButtonAction:(id)sender;
- (IBAction)deleteAction:(id)sender;
- (IBAction)editAction:(id)sender;

enum TextFieldTag
{
    eNameTextFieldTag=701,
    eEmailTextFieldTag=702,
    ePhoneTextFieldTag=703
};

@end

@implementation DealerInfoAddDealerStaffVC

- (void)viewWillAppear:(BOOL)animated {
    
    //CGSize size = CGSizeMake(675, 2000); // size of view in popover
    //self.contentSizeForViewInPopover = size;
    
    [super viewWillAppear:animated];
    
    if (_addType == eDealerAdd)
    {
        _deleteButton.hidden = YES;
        _dividerImage.hidden = YES;
        _editSavedButton.hidden = YES;
        
        [_saveButton setTitleColor:ButtonDisabledTextColor forState:UIControlStateNormal];
        _nameTextField.text = @"";
        _phoneTextField.text = @"";
        _emailTextField.text = @"";
        //[_positionButton setTitle:@"" forState:UIControlStateNormal];
        StaffRole *staffRole = [_positionArray objectAtIndex:0];
        //[_positionButton setTitle:staffRole.role_name forState:UIControlStateNormal];
        _positionTextField.text = staffRole.role_name;
        _selectedID = staffRole.role_id;
        //NSIndexPath *selectedIndex0 = [NSIndexPath indexPathForRow:0 inSection:0];
        //_selectedIndexPath = _selectedIndex;
        //_dealerGroupPopVC.checkedIndexPath = selectedIndex0;
        
        [_dealerGroupPopVC applyCheckedID:_selectedID];
        
        _saveButton.userInteractionEnabled = NO;
    }
    else
    {
        _deleteButton.hidden = NO;
        _dividerImage.hidden = NO;
        _editSavedButton.hidden = NO;
        _saveButton.hidden = YES;

        [_editSavedButton setTitleColor:CustomTextColor forState:UIControlStateNormal];
        
        //set text
        StaffRole *staffRole = [StaffRole MR_findFirstByAttribute:@"role_id" withValue: _selectedStaff.staff_role_id];
        
        _nameTextField.text = _selectedStaff.first_name; //[_staffDictionary objectForKey:@"addName"];
        //[_positionButton setTitle:[_staffDictionary objectForKey:@"addPosition"] forState:UIControlStateNormal];
       // [_positionButton setTitle: staffRole.role_name forState:UIControlStateNormal];
        if([staffRole.role_name length]>1)
            _positionTextField.text = staffRole.role_name;
        else
        {
            staffRole = [_positionArray objectAtIndex:0];
            _positionTextField.text = staffRole.role_name;
            _selectedID = staffRole.role_id;
        }
        _phoneTextField.text = _selectedStaff.office_phone_number; //[_staffDictionary objectForKey:@"addPhone"];
        _emailTextField.text = _selectedStaff.email; //[_staffDictionary objectForKey:@"addEmail"];
        //_selectedIndexPath = _selectedIndex;
        
        //_dealerGroupPopVC.checkedIndexPath = _selectedIndex;
        //_dealerGroupPopVC.checkedId = _selectedID;
        [_dealerGroupPopVC applyCheckedID:_selectedID];
    }
//    _nameErrorLabel.hidden = YES;
//    _phoneErrorLabel.hidden = YES;
//    _emailErrorLabel.hidden = YES;
    
//    [[TrackUtility sharedUtility] hideErrorMessageViewInView:_nameTextField];
//    [[TrackUtility sharedUtility] hideErrorMessageViewInView:_emailTextField];
//    [[TrackUtility sharedUtility] hideErrorMessageViewInView:_phoneTextField];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // color
    [_cancelButton setTitleColor:CustomTextColor forState:UIControlStateNormal];
    [_saveButton setTitleColor:ButtonDisabledTextColor forState:UIControlStateNormal];
    [_editSavedButton setTitleColor:ButtonDisabledTextColor forState:UIControlStateNormal];
    [_deleteButton setTitleColor:CustomTextColor forState:UIControlStateNormal];
    [_positionButton setTitleColor:DarkGrayTextColor forState:UIControlStateNormal];
    _titleLabel.textColor = DarkGrayTextColor;
//    _nameLabel.textColor = LightGrayTextColor;
//    _positionLabel.textColor = LightGrayTextColor;
//    _phoneLabel.textColor = LightGrayTextColor;
//    _emailLabel.textColor = LightGrayTextColor;
//    _nameErrorLabel.textColor = ErrorTextColor;
//    _phoneErrorLabel.textColor = ErrorTextColor;
//    _emailErrorLabel.textColor = ErrorTextColor;
    self.view.backgroundColor = LightGrayBackgroundColor;
    
    // Font
    [_cancelButton.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:25]];
    [_saveButton.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    [_editSavedButton.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    [_deleteButton.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
//    [_positionButton.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:18]];
    [_titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:25]];
//    [_nameLabel setFont:[UIFont fontWithName:Font_Track_Regular size:18]];
//    [_positionLabel setFont:[UIFont fontWithName:Font_Track_Regular size:18]];
//    [_phoneLabel setFont:[UIFont fontWithName:Font_Track_Regular size:18]];
//    [_emailLabel setFont:[UIFont fontWithName:Font_Track_Regular size:18]];
//    [_nameErrorLabel setFont:[UIFont fontWithName:Font_Track_Regular size:18]];
//    [_phoneErrorLabel setFont:[UIFont fontWithName:Font_Track_Regular size:18]];
//    [_emailErrorLabel setFont:[UIFont fontWithName:Font_Track_Regular size:18]];
//    [_nameTextField setFont:[UIFont fontWithName:Font_Track_Regular size:18]];
//    [_phoneTextField setFont:[UIFont fontWithName:Font_Track_Regular size:18]];
//    [_emailTextField setFont:[UIFont fontWithName:Font_Track_Regular size:18]];
    
    //  Default TEXT
    [_cancelButton setTitle:LOCALIZED_POPOVER_CANCEL forState:UIControlStateNormal];
    [_saveButton setTitle:LOCALIZED_POPOVER_SAVE forState:UIControlStateNormal];
    [_editSavedButton setTitle:LOCALIZED_POPOVER_SAVE forState:UIControlStateNormal];
    [_deleteButton setTitle:LOCALIZED_POPOVER_DELETE forState:UIControlStateNormal];
//    _nameErrorLabel.text = LOCALIZED_ERROR_ADD_DEALER_NAME;
//    _phoneErrorLabel.text = LOCALIZED_ERROR_ADD_DEALER_PHONE;
//    _emailErrorLabel.text = LOCALIZED_ERROR_ADD_DEALER_EMAIL;
//    _nameLabel.text = LOCALIZED_DEALER_ADD_STAFF_NAME;
//    _positionLabel.text = LOCALIZED_DEALER_ADD_STAFF_POSITION;
//    _phoneLabel.text = LOCALIZED_DEALER_ADD_STAFF_PHONE;
//    _emailLabel.text = LOCALIZED_DEALER_ADD_STAFF_EMAIL;
    
    // Textfield Placeholders
    _nameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LOCALIZED_DEALER_ADD_STAFF_NAME
                                                                                 attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    _phoneTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LOCALIZED_DEALER_ADD_STAFF_PHONE
                                                                                  attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    _emailTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LOCALIZED_DEALER_ADD_STAFF_EMAIL
                                                                                 attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    _positionTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LOCALIZED_DEALER_ADD_STAFF_POSITION
                                                                                 attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    
    // Set Textfields Color and font
    for (JVFloatLabeledTextField *textField in _collectedTextField)
    {
        textField.font = [UIFont fontWithName:Font_Track_Regular size:18];
        textField.floatingLabel.font = [UIFont fontWithName:Font_Track_Regular size:12];
        textField.floatingLabelTextColor = LightGrayTextColor;
    }
    
    
    UIButton *buttonPosition = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonPosition addTarget:self action:@selector(positionButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    buttonPosition.frame = CGRectMake(0, 0, 320, 55);
    [_positionTextField addSubview:buttonPosition];
    
    _dealerGroupPopVC = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"DealerInfoStaffPositionVC"];
    _dealerGroupPopVC.popoverType = eDealerPopupStaff;
   // _positionArray = [NSArray arrayWithObjects:@"After Sales Ambassador",@"After Sales Manager",@"General Manager",@"Sales Ambassador",@"Sales Manager",@"Technician",nil];
    _positionArray = nil;
    _positionArray = [[NSArray alloc]initWithArray:(NSArray*)[StaffRole findAll]]; //[StaffRole findAll];
    _dealerGroupPopVC.positionArray = _positionArray;
   // _dealerGroupPopVC.checkedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    _dealerGroupPopVC.delegate = self;
    _popoverControl = nil;
    _popoverControl = [[UIPopoverController alloc] initWithContentViewController:_dealerGroupPopVC];
    
    _nameTextField.delegate = self;
    _phoneTextField.delegate = self;
    _emailTextField.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    // Get the new view controller using [segue destinationViewController].
//    // Pass the selected object to the new view controller.
//    
//    if ([segue.identifier isEqualToString:@"dealerStaffPositionID"])
//    {
//        _dealerInfoStaffPositionVC = [segue destinationViewController];
//        //_calendarVC.delegate = self;
//        //_calendarVC.titleString = LOCALIZED_CALENDAR_POCA_DATE;
//    }
//}






#pragma mark - TextField Delegate callbacks

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.activeTextField=textField;
    
    JVFloatLabeledTextField *textF = (JVFloatLabeledTextField*)textField;
    textF.floatingLabelTextColor = LightGrayTextColor;
    textF.floatingLabel.text = [textField.attributedPlaceholder string];
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSUInteger newLength = (textField.text.length - range.length) + string.length;
    
    NSString *testString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    testString = [testString stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    BOOL isValidField = YES;
    if([testString length] == 0)
    {
        isValidField = NO;
    }
    else
    {
        NSString *nameCharactersString = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ-' ";
        
        switch (textField.tag)
        {
            case eNameTextFieldTag:
            {
                if(nil != _nameTextField && [testString length] > 0 )
                {
                    isValidField = [self checkValidityOfString:testString againstCharacters:nameCharactersString];
                }
                else if(testString.length > kNameTextFieldLimit)
                {
                    isValidField = NO;
                    
                }
                isValidField = (([self validateRegistrationValues:_emailTextField] && [self validateRegistrationValues:_phoneTextField])&& isValidField) ? YES : NO;
                break;
            }
            case eEmailTextFieldTag:
            {
                isValidField = (nil != _emailTextField && [testString length]>0) ? YES : NO;
                if (isValidField)
                {
                    isValidField = [[TrackUtility sharedUtility] isEmailIDValid:testString] ? YES: NO;
                    
                    isValidField = (([self validateRegistrationValues:_nameTextField] && [self validateRegistrationValues:_phoneTextField]) && isValidField) ? YES : NO;
                }
                
                break;
            }
                
            case ePhoneTextFieldTag:
            {
                if ([[TrackUtility sharedUtility] isValidMobileNumber:testString])
                {
                    isValidField = ((testString.length >= kPhoneNumberTextFieldMinLimit) && (testString.length <= kPhoneNumberTextFieldMaxLimit)) ? YES : NO;
                    isValidField = (([self validateRegistrationValues:_nameTextField] && [self validateRegistrationValues:_emailTextField]) && isValidField) ? YES : NO;
                }
                else
                    isValidField = NO;
                
                
                break;
            }
            default:
                break;
        }
    }
   
    if(isValidField)
    {
        _saveButton.userInteractionEnabled=YES;
        _editSavedButton.userInteractionEnabled=YES;
        
        [_saveButton setTitleColor:CustomTextColor forState:UIControlStateNormal];
        [_editSavedButton setTitleColor:CustomTextColor forState:UIControlStateNormal];
        //[_saveButton setBackgroundImage:[UIImage imageNamed:@"btn_next_selected.png"] forState:UIControlStateNormal];
        
    }
    else
    {
        _saveButton.userInteractionEnabled=NO;
        _editSavedButton.userInteractionEnabled=NO;
        
        [_saveButton setTitleColor:ButtonDisabledTextColor forState:UIControlStateNormal];
        [_editSavedButton setTitleColor:ButtonDisabledTextColor forState:UIControlStateNormal];
    }
    
    //[self validateTextFieldValues];
    
    switch (textField.tag) {
        case eNameTextFieldTag:
        {
            return [self isTextFieldEditable:_nameTextField withNewLength:newLength replacementString:string errorMsg:@"FirstNameMaxLimit" withLimit:kNameTextFieldLimit];
            break;
        }
        case eEmailTextFieldTag:
        {
            return [self isTextFieldEditable:_emailTextField withNewLength:newLength replacementString:string errorMsg:@"EmailTextFieldLimit" withLimit:kEmailTextFieldLimit];
            break;
        }
        case ePhoneTextFieldTag:
        {
            
            return [self isTextFieldEditable:_phoneTextField withNewLength:newLength replacementString:string errorMsg:nil withLimit:kPhoneNumberTextFieldMaxLimit];
            break;
        }
        default:
            break;
    }
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    NSString *text = textField.text;
    NSString *newString = [text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    textField.text=newString;
    

    JVFloatLabeledTextField *textF = (JVFloatLabeledTextField*)textField;
    NSString *errorStr = [NSString stringWithFormat:@"%@: %@",LOCALIZED_ERROR_INVALID, [textField.attributedPlaceholder string]];
    
    //Remove the error view from the active textfield if textfield length == max length.
    switch (_activeTextField.tag)
    {
        case eNameTextFieldTag:
        {
            if(_activeTextField.text.length == kNameTextFieldLimit)
            {
                //[[TrackUtility sharedUtility] hideErrorMessageViewInView:_activeTextField];
                textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[textField.attributedPlaceholder string]
                                                                                  attributes:@{NSForegroundColorAttributeName: [UIColor blackColor]}];
                textF.floatingLabelTextColor = LightGrayTextColor;
                textF.floatingLabel.text = [textField.attributedPlaceholder string];
            }
            break;
        }
            
        case eEmailTextFieldTag:
        {
            textField.text = [text stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            if(_activeTextField.text.length ==kEmailTextFieldLimit)
            {
                //[[TrackUtility sharedUtility]hideErrorMessageViewInView:_activeTextField];
                textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[textField.attributedPlaceholder string]
                                                                                  attributes:@{NSForegroundColorAttributeName: [UIColor blackColor]}];
                textF.floatingLabelTextColor = LightGrayTextColor;
                textF.floatingLabel.text = [textField.attributedPlaceholder string];
            }
            
            break;
        }
        case ePhoneTextFieldTag:
        {
            //if(_activeTextField.text.length == kPhoneNumberTextFieldMinLimit)
            if((_activeTextField.text.length >= kPhoneNumberTextFieldMinLimit) && (_activeTextField.text.length <= kPhoneNumberTextFieldMaxLimit))
            {
                //[[TrackUtility sharedUtility] hideErrorMessageViewInView:_activeTextField];
                textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[textField.attributedPlaceholder string]
                                                                                  attributes:@{NSForegroundColorAttributeName: [UIColor blackColor]}];
                textF.floatingLabelTextColor = LightGrayTextColor;
                textF.floatingLabel.text = [textField.attributedPlaceholder string];
            }
            
            break;
        }
            
        default:
            break;
    }
    switch (textField.tag)
    {
        case eNameTextFieldTag:
        {
            if(![_nameTextField requiredText] )
            {
                //[[TrackUtility sharedUtility] showErrorMessageViewInView:textField withErrorMessage:NSLocalizedString(@"Missing:Name", nil)];
                textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[textField.attributedPlaceholder string]
                                                                                  attributes:@{NSForegroundColorAttributeName: [UIColor redColor]}];
            }
            else if (![self checkIfStringContainsAtLeastTwoCharacters:_nameTextField.text andAnySpecialCharacter:@"-' "])
            {
               // [[TrackUtility sharedUtility] showErrorMessageViewInView:textField withErrorMessage:NSLocalizedString(@"Missing:Name", nil)];
                textF.floatingLabelTextColor = [UIColor redColor];
                textF.floatingLabel.text = errorStr;
            }
            else if(![self validateRegistrationValues:_nameTextField])
            {
                //Dispaly Error : Please enter valid first name
                //[[TrackUtility sharedUtility] showErrorMessageViewInView:textField withErrorMessage:NSLocalizedString(@"Invalid:Name", nil)];
                textF.floatingLabelTextColor = [UIColor redColor];
                textF.floatingLabel.text = errorStr;
            }
            else if([self validateTextFieldTextForSalutations:textField.text])
            {
                //remove salutation and save the same
                //[[TrackUtility sharedUtility] showErrorMessageViewInView:textField withErrorMessage:NSLocalizedString(@"Remove:Titles and Salutations", nil)];
                textF.floatingLabelTextColor = [UIColor redColor];
                textF.floatingLabel.text = errorStr;
            }
            break;
        }
            
        case eEmailTextFieldTag:
        {
            if(![_emailTextField requiredText])
            {
                //Dispaly Error : Please enter valid email
                //[[TrackUtility sharedUtility] showErrorMessageViewInView:textField withErrorMessage:NSLocalizedString(@"Missing:EmailAddress", nil)];
                textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[textField.attributedPlaceholder string]
                                                                                  attributes:@{NSForegroundColorAttributeName: [UIColor redColor]}];
            }
            else if(![self validateRegistrationValues:_emailTextField])
            {
                //Dispaly Error : Please enter valid email
                //[[TrackUtility sharedUtility] showErrorMessageViewInView:textField withErrorMessage:NSLocalizedString(@"Invalid:EmailAddress", nil)];
                textF.floatingLabelTextColor = [UIColor redColor];
                textF.floatingLabel.text = errorStr;
            }
            break;
        }
            
        case ePhoneTextFieldTag:
        {
            if(![_phoneTextField requiredText])
            {
                //Dispaly Error : Please enter valid mobilenumber
                //[[TrackUtility sharedUtility] showErrorMessageViewInView:textField withErrorMessage:NSLocalizedString(@"Missing:MobileNumber", nil)];
                textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[textField.attributedPlaceholder string]
                                                                                  attributes:@{NSForegroundColorAttributeName: [UIColor redColor]}];
            }
            else if (!((_activeTextField.text.length >= kPhoneNumberTextFieldMinLimit) && (_activeTextField.text.length <= kPhoneNumberTextFieldMaxLimit)))
            {
                textF.floatingLabelTextColor = [UIColor redColor];
                textF.floatingLabel.text = LOCALIZED_ERROR_ADD_DEALER_PHONE;
            }
            
            else if(![self validateRegistrationValues:_phoneTextField])
            {
                //Dispaly Error : Please enter valid mobilenumber
                //[[TrackUtility sharedUtility] showErrorMessageViewInView:textField withErrorMessage:NSLocalizedString(@"Missing:MobileNumber", nil)];
                textF.floatingLabelTextColor = [UIColor redColor];
                textF.floatingLabel.text = LOCALIZED_ERROR_ADD_DEALER_PHONE;
            }
            
            break;
        }
        default:
            break;
    }
    
    [self validateTextFieldValues];
    [textField resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


//Custom

-(void)validateTextFieldValues
{
    BOOL isValid = YES;

    {
        if([self validateRegistrationValues:_nameTextField] && [self validateRegistrationValues:_emailTextField] && [self validateRegistrationValues:_phoneTextField] &&  ![self validateForMaxLimitOfField:_nameTextField] && ![self validateForMaxLimitOfField:_emailTextField] &&
           ![self validateForMaxLimitOfField:_phoneTextField] && ![self validateTextFieldTextForSalutations:_nameTextField.text])
            isValid = YES;
        else
            isValid = NO;
    }
    if(isValid)
    {
        _saveButton.userInteractionEnabled=YES;
        _editSavedButton.userInteractionEnabled=YES;
        
        [_saveButton setTitleColor:CustomTextColor forState:UIControlStateNormal];
        [_editSavedButton setTitleColor:CustomTextColor forState:UIControlStateNormal];
        //[_saveButton setBackgroundImage:[UIImage imageNamed:@"btn_next_selected.png"] forState:UIControlStateNormal];
        
    }
    else
    {
        _saveButton.userInteractionEnabled=NO;
        _editSavedButton.userInteractionEnabled=NO;
        
        [_saveButton setTitleColor:ButtonDisabledTextColor forState:UIControlStateNormal];
        [_editSavedButton setTitleColor:ButtonDisabledTextColor forState:UIControlStateNormal];
    }
}

-(BOOL)validateTextFieldTextForSalutations:(NSString *)inString
{
    BOOL isContainSalutation = NO;
    
    //Common Salutations and Titles: Dr, Mr, Mrs, Ms, Sr, Jr, Miss, PHD, BA, Sr, Jr, and, 1st, 2nd, 3rd,  4th, 5 th
    
    NSArray * salutationTitlesArray = [NSArray arrayWithObjects:@"Dr",@"Mr",@"Mrs",@"Ms",@"Sr",@"Jr",@"Miss",@"PHD",@"BA",@"1st",@"2nd",@"3rd",@"4th",@"5th", nil];
    
    //Sepearate strings by space
    NSArray *wordsAndEmptyStrings = [inString componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSArray *words = [wordsAndEmptyStrings filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"length > 0"]];
    
    for (NSString* eachWord in words)
    {
        for (NSString *commonStr in salutationTitlesArray)
        {
            if([eachWord caseInsensitiveCompare:commonStr]== NSOrderedSame)
            {
                isContainSalutation=YES;
                break;
            }
        }
        
    }
    
    return isContainSalutation;
}

-(BOOL)validateForMaxLimitOfField:(UITextField *)inTextField
{
    BOOL didCrossMaxLimit = NO;
    
    switch (inTextField.tag)
    {
        case eNameTextFieldTag:
        {
            didCrossMaxLimit = (inTextField.text.length > kNameTextFieldLimit)? YES:NO;
            break;
            
        }
        case eEmailTextFieldTag:
        {
            didCrossMaxLimit = (inTextField.text.length > kEmailTextFieldLimit)? YES:NO;
            break;
        }
        case ePhoneTextFieldTag:
        {
            didCrossMaxLimit = !((inTextField.text.length >= kPhoneNumberTextFieldMinLimit) && (inTextField.text.length <= kPhoneNumberTextFieldMaxLimit)) ? YES:NO;

            //didCrossMaxLimit = (inTextField.text.length > kPhoneNumberTextFieldMaxLimit)? YES:NO;
            break;
        }
            
        default:
            break;
    }
    
    return didCrossMaxLimit;
}

-(BOOL)validateRegistrationValues:(UITextField *)withTextField
{
    BOOL isValidField=YES;
    // Handle checking against first name and last name for the presence of these characters.
    NSString *nameCharactersString = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ-' ";
    
    switch (withTextField.tag)
    {
        case eNameTextFieldTag:
        {
            if([_nameTextField.text length] == 0)
            {
                isValidField = NO;
            }
            
            else if(nil != _nameTextField && [_nameTextField.text length]>0 )
            {
                isValidField = [self checkValidityOfString:_nameTextField.text againstCharacters:nameCharactersString];
            }
            else if(_nameTextField.text.length > kNameTextFieldLimit)
            {
                isValidField = NO;
                
            }
            break;
        }
        case eEmailTextFieldTag:
        {
            if([_emailTextField.text length] == 0)
            {
                isValidField = NO;
            }
            else if(nil != _emailTextField && [_emailTextField.text length]>0)
            {
                isValidField = [[TrackUtility sharedUtility] isEmailIDValid:_emailTextField.text];
                
            }
            else
            {
                isValidField = NO;
            }
            break;
        }
            
        case ePhoneTextFieldTag:
        {
            isValidField = (nil != _phoneTextField && [_phoneTextField.text length]>0) ? YES : NO;
            isValidField = [[TrackUtility sharedUtility] isValidMobileNumber:_phoneTextField.text] && isValidField;
            
            break;
        }
        default:
            break;
    }
    
    return isValidField;
}

-(BOOL)isTextFieldEditable:(UITextField *)textField withNewLength:(NSUInteger)newLength replacementString:(NSString *)string errorMsg:(NSString *)errorMessage withLimit:(NSUInteger)withLimit
{
    BOOL isEditable=YES;
    JVFloatLabeledTextField *textF = (JVFloatLabeledTextField*)textField;
    NSString *errorStr = [NSString stringWithFormat:@"%@: %@",LOCALIZED_ERROR_INVALID, [textField.attributedPlaceholder string]];
    
    if(textField == _phoneTextField)
    {
        int maxLimit= 14;
        
        NSCharacterSet* numberCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; ++i)
        {
            unichar c = [string characterAtIndex:i];
            if (![numberCharSet characterIsMember:c] || (newLength > maxLimit))
            {
                textF.floatingLabelTextColor = [UIColor redColor];
                textF.floatingLabel.text = errorStr;
                return NO;
            }
        }
        textF.floatingLabelTextColor = LightGrayTextColor;
        textF.floatingLabel.text = [textField.attributedPlaceholder string];
        
        return YES;
    }
    else
    {
        if ([string isEqualToString:@""])
        {
            if (withLimit >=textField.text.length)
            {
                //[[TrackUtility sharedUtility] hideErrorMessageViewInView:textField];
                textF.floatingLabelTextColor = LightGrayTextColor;
                textF.floatingLabel.text = [textField.attributedPlaceholder string];
            }
            else
            {
                //[[TrackUtility sharedUtility] showErrorMessageViewInView:textField withErrorMessage:NSLocalizedString(errorMessage, nil)];
                textF.floatingLabelTextColor = [UIColor redColor];
                textF.floatingLabel.text = errorStr;
            }
            return YES;
        }
        if(newLength <= withLimit)
        {
            //[[TrackUtility sharedUtility]hideErrorMessageViewInView:textField];
            textF.floatingLabelTextColor = LightGrayTextColor;
            textF.floatingLabel.text = [textField.attributedPlaceholder string];

            return YES;
        }
        else if([string length]>1)
        {
            //[[TrackUtility sharedUtility]showErrorMessageViewInView:textField withErrorMessage:NSLocalizedString(errorMessage, nil)];
            textF.floatingLabelTextColor = [UIColor redColor];
            textF.floatingLabel.text = errorStr;
            return YES;
        }
        else if(newLength > withLimit)
        {
            //[[TrackUtility sharedUtility]showErrorMessageViewInView:textField withErrorMessage:NSLocalizedString(errorMessage, nil)];
            textF.floatingLabelTextColor = [UIColor redColor];
            textF.floatingLabel.text = errorStr;
            return NO;
        }
        
    }
    return isEditable;
}

/*
 Method to check whether the string contains the specified characters in it.
 @param:
 instring , the string to be checked against the characters
 inCharactersString, string of sharacters to be checked for the presence in the string inString.
 */
-(BOOL)checkValidityOfString:(NSString *)inString againstCharacters:(NSString *)inCharactersString
{
    BOOL isValid = YES;
    NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:inCharactersString] invertedSet];
    
    if ([inString rangeOfCharacterFromSet:set].location != NSNotFound)
    {
        isValid = NO;
        
    }
    return isValid;
}

-(BOOL)checkIfStringContainsAtLeastTwoCharacters:(NSString*)inString andAnySpecialCharacter:(NSString*)specialCharacter
{
    BOOL isValid = YES;
    if ([inString rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:specialCharacter]].location != NSNotFound)
    {
        NSString *alphabetSet = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ";
        NSCharacterSet *alphabetCharacterSet = [NSCharacterSet characterSetWithCharactersInString:alphabetSet];;
        int index = 0;
        int numberOfCharacters = 0;
        for (index =0; index < inString.length; index++)
        {
            unichar c = [inString characterAtIndex:index];
            NSString *string = [NSString stringWithCharacters:&c length:1];
            if ([string rangeOfCharacterFromSet:alphabetCharacterSet].location != NSNotFound)
                numberOfCharacters ++;
        }
        if(numberOfCharacters < 2)
            isValid = NO;
    }
    return isValid;
    
    //    BOOL isValid = NO;
    //    NSString *specialCharString = [inString stringByTrimmingCharactersInSet:[NSCharacterSet letterCharacterSet]];
    //    NSString *alphabetString = [inString stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:specialCharString]];
    //    if(alphabetString.length >= 2)
    //    {
    //        if ([inString rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:specialCharacter]].location != NSNotFound)
    //        {
    //            isValid = YES;
    //        }
    //    }
    //    return isValid;
}

//- (IBAction)saveAction:(id)sender {
    
//    BOOL isValid = TRUE;
//    //NSString *userErrors = @"";
//    _nameErrorLabel.hidden = YES;
//    _phoneErrorLabel.hidden = YES;
//    _emailErrorLabel.hidden = YES;
//    if(![_nameTextField requiredText] || ![_nameTextField validUsername])
//    {
//        isValid = FALSE;
//        [_nameTextField becomeFirstResponder];
//       // userErrors = [userErrors stringByAppendingString:@"A username of at least 3 characters is required. \r\n"];
//        _nameErrorLabel.hidden = NO;
//    }
//    if(![_phoneTextField requiredPhoneText] || ![_phoneTextField validPhoneNumber])
//    {
//        [_phoneTextField becomeFirstResponder];
//        //userErrors = [userErrors stringByAppendingString:@"A phoneTextField of at least 6 characters is required. \r\n"];
//        isValid = FALSE;
//        _phoneErrorLabel.hidden = NO;
//    }
//    if(![_emailTextField requiredText] || ![_emailTextField validEmailAddress])
//    {
//        isValid = FALSE;
//        [_emailTextField becomeFirstResponder];
//        //userErrors = [userErrors stringByAppendingString:@"A valid email address is required. \r\n"];
//        _emailErrorLabel.hidden = NO;
//    }
//    
//    
//    if (isValid)
//    {
//        // TO Service calls
//        // TO Core data calls
//    }
//    else
//    {
//       // UIAlertView *errors = [[UIAlertView alloc] initWithTitle:nil message:userErrors delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        
//        //[errors show];
//    }
    
//}

- (IBAction)saveAction:(id)sender
{
    NSLog(@"seleId = %@", _selectedID);
    NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:
                                 _nameTextField.text, @"addName",
                                 _positionTextField.text, @"addPosition",
                                  _phoneTextField.text,@"addPhone",
                                 _emailTextField.text,@"addEmail",
                                 _selectedID, @"addId",
                                 nil];
    
//    if ([_delegate respondsToSelector:@selector(dataAdded:selectedIndex:)])
//    {
//        [_delegate dataAdded:dict selectedIndex:_selectedIndex];
//    }
    
    if ([_delegate respondsToSelector:@selector(dataAdded:)])
    {
        [_delegate dataAdded:dict];
    }
}

- (IBAction)editAction:(id)sender
{
    NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:
                          _nameTextField.text, @"addName",
                          _positionButton.titleLabel.text, @"addPosition",
                          _phoneTextField.text,@"addPhone",
                          _emailTextField.text,@"addEmail",
                          _selectedID, @"addId",
                          nil];
    
//    if ([_delegate respondsToSelector:@selector(dataAdded:selectedIndex:)])
//    {
//        [_delegate dataEdited:dict selectedIndex:_selectedIndex];
//    }
    if ([_delegate respondsToSelector:@selector(dataEdited:)])
    {
        [_delegate dataEdited:dict];
    }
}

- (IBAction)deleteAction:(id)sender
{
    if ([_delegate respondsToSelector:@selector(dataDeleted)])
    {
        [_delegate dataDeleted];
    }
}

- (IBAction)cancelAction:(id)sender
{
}

- (IBAction)positionButtonAction:(id)sender
{
    [_activeTextField resignFirstResponder];
    
    CGRect positionRect = CGRectMake(500, 0, 10, 180);
    
    _popoverControl.popoverContentSize = CGSizeMake(348, 264);
    [_popoverControl presentPopoverFromRect:positionRect inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

- (void) didPositionChange:(NSString *) position selectedIndex:(NSIndexPath*)selectedIndex
{
    //[_positionButton setTitle:position forState:UIControlStateNormal];
    _positionTextField.text = position;
    //_selectedIndexPath = selectedIndex;
    // _selectedIndex = selectedIndex;
}

- (void) didPositionChangeId:(NSNumber *) selectedId
{
    StaffRole *staffRole = [StaffRole MR_findFirstByAttribute:@"role_id" withValue: selectedId];
    NSString *staffRoleName = staffRole.role_name;
    //[_positionButton setTitle:staffRoleName forState:UIControlStateNormal];
    _positionTextField.text = staffRoleName;
    _selectedID = selectedId;
}

@end
