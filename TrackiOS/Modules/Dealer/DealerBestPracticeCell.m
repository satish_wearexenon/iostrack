//
//  DealerBestPracticeCell.m
//  TrackiOS
//
//  Created by satish sharma on 11/24/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "DealerBestPracticeCell.h"
#import "DealerExportPopOverVC.h"

@interface DealerBestPracticeCell ()

- (IBAction)editBestPracticesButtonClick:(id)sender;


@end

@implementation DealerBestPracticeCell

- (void)awakeFromNib {
    // Initialization code
    
    // Initialization code
    [self.contentView setBackgroundColor:ViewBackgroundColor];
    
    [_exportButton setImage:[UIImage imageNamed:@"edit_icon_green"] forState:UIControlStateNormal];
    
    [_checkButton setBackgroundImage:[UIImage imageNamed:@"check_mark_grey"] forState:UIControlStateNormal];
    [_checkButton setBackgroundImage:[UIImage imageNamed:@"check_mark_green"] forState:UIControlStateSelected];
    [_checkButton setBackgroundImage:[UIImage imageNamed:@"check_mark_grey"] forState:UIControlStateDisabled];
    [_checkButton setSelected:YES];
    
    //set font for uicontrol
    [_titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:23]];
    [_detailLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    
    [_stageTitleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    [_stageLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    
    [_dueDateLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    [_dateLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    
    [_detailLabel setNumberOfLines:2];
    
    //set color
    _titleLabel.textColor = DarkGrayTextColor;
    _detailLabel.textColor = LightGrayTextColor;
    _stageTitleLabel.textColor = CustomTextColor;
    _stageLabel.textColor = CustomTextColor;
    _dueDateLabel.textColor = CustomTextColor;
    _dateLabel.textColor = CustomTextColor;
    
   //  _titleLabel.text = @"title";
   // _stageTitleLabel.text = @"Stagetitle";
   // _dueDateLabel.text = @"DueDate";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)editBestPracticesButtonClick:(id)sender{
    
    NSLog(@"DealerBestPracticeCell: editBestPracticesButtonClick");
    
//    DealerExportPopOverVC *viewController = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"DealerExportPopOverVC"];
//    
//    _popVC = [[UIPopoverController alloc] initWithContentViewController:viewController];
//    
//    _popVC.popoverContentSize = CGSizeMake(189.0, 98.0);
//    
//    [_popVC presentPopoverFromRect:[(UIButton *)sender frame]  inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];

}

@end
