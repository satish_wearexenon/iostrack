//
//  CheckListBestPractiseVC.h
//  TrackiOS
//
//  Created by John Paul Ranjith on 06/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataPopOverVC.h"
#import "CapturePhotoVCViewController.h"
#import "ChkListBestPractice.h"
#import "TRAlertView.h"

@protocol CheckListBestPractiseVCDelegate <NSObject>

-(void)parentCallBack:(id)data;


@end

/**
 The CheckListBestPractiseVC class is used to present the best practice pop over
 */
@interface CheckListBestPractiseVC : UIViewController<UITextViewDelegate,DataPopOverVCDelegate,CapturedPhotoVCDelegate,TRAlertViewDelegate>
{
    UIButton *selectedButton;
    CapturePhotoVCViewController *photoVC;
    NSMutableArray *photoArray;
    id buttonSender;
}

/**
 This property holds a reference of pop over view
 */
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UITextView *titleTextView;
@property (weak, nonatomic) IBOutlet UITextView *descTextView;

@property (weak, nonatomic) IBOutlet UIButton *photoBtn1;
@property (weak, nonatomic) IBOutlet UIButton *photoBtn2;
@property (weak, nonatomic) IBOutlet UIButton *photoBtn3;
@property (weak, nonatomic) IBOutlet UIButton *photoBtn4;
@property (weak, nonatomic) IBOutlet UIButton *photoBtn5;


@property (weak, nonatomic) IBOutlet UIButton *deletePhotoBtn1;
@property (weak, nonatomic) IBOutlet UIButton *deletePhotoBtn2;
@property (weak, nonatomic) IBOutlet UIButton *deletePhotoBtn3;
@property (weak, nonatomic) IBOutlet UIButton *deletePhotoBtn4;
@property (weak, nonatomic) IBOutlet UIButton *deletePhotoBtn5;


@property (nonatomic) ChkListBestPractice *bestPractData;

@property (nonatomic, strong) UIPopoverController *popVC;

@property (nonatomic) id <CheckListBestPractiseVCDelegate> delegate;

/**
 Method to save the edited data
 */
-(IBAction)saveAction:(id)sender;


/**
 Method to cancel the edited data
 */
-(IBAction)cancelAction:(id)sender;

@end
