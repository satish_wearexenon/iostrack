//
//  CheckListPhotoVC.m
//  TrackiOS
//
//  Created by John Paul Ranjith on 06/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "CheckListPhotoVC.h"
#import "DataPopOverVC.h"
#import "CheckListPhotoViewController.h"

@interface CheckListPhotoVC ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation CheckListPhotoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = LightGrayBackgroundColor;
    
    [self.cancelBtn setTitleColor:CustomTextColor forState:UIControlStateNormal];
    [self.saveBtn setTitleColor:CustomTextColor forState:UIControlStateNormal];
    _titleLabel.textColor = DarkGrayTextColor;
   
    _cancelBtn.tag = 1000;
    
    [_cancelBtn.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:25]];
    [_saveBtn.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    [_titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:25]];
    
    _deletePhotoBtn1.hidden = YES;
    _deletePhotoBtn2.hidden = YES;
    _deletePhotoBtn3.hidden = YES;
    _deletePhotoBtn4.hidden = YES;
    _deletePhotoBtn5.hidden = YES;
    
    _saveBtn.enabled = NO;
    
    photoArray = [[NSMutableArray alloc] init];
    
    
    
    
    //Displaying images from local DB
    //NSMutableArray *array = [self fetchPhotoDataArray];
    //if([array count]>0){
    //    ChkListPhoto *photoDat = [array objectAtIndex:0];
    if(self.photoData.photoObj1 != nil){
        
        [self.photoBtn1 setImage:[UIImage imageWithData:self.photoData.photoObj1] forState:UIControlStateNormal];
        
        //Logic to enable delete btn for the retrieved image from local db
        if([self.photoData.deletePhotoBtn1 isEqualToString:@"YES"])
        {
            [photoArray addObject:self.photoBtn1];
            _deletePhotoBtn1.hidden= NO;
        }
        else
        {
            _deletePhotoBtn1.hidden= YES;
        }
    }
    if(self.photoData.photoObj2 != nil){
        
        [self.photoBtn2 setImage:[UIImage imageWithData:self.photoData.photoObj2] forState:UIControlStateNormal];
        
        //Logic to enable delete btn for the retrieved image from local db
        if([self.photoData.deletePhotoBtn2 isEqualToString:@"YES"])
        {
             [photoArray addObject:self.photoBtn2];
            _deletePhotoBtn2.hidden= NO;
        }
        else
        {
            _deletePhotoBtn2.hidden= YES;
        }
        
    }
    if(self.photoData.photoObj3 != nil){
        
        [self.photoBtn3 setImage:[UIImage imageWithData:self.photoData.photoObj3] forState:UIControlStateNormal];
        
        //Logic to enable delete btn for the retrieved image from local db
        if([self.photoData.deletePhotoBtn3 isEqualToString:@"YES"])
        {
             [photoArray addObject:self.photoBtn3];
            _deletePhotoBtn3.hidden= NO;
        }
        else
        {
            _deletePhotoBtn3.hidden= YES;
        }
    }
    if(self.photoData.photoObj4 != nil){
        
        [self.photoBtn4 setImage:[UIImage imageWithData:self.photoData.photoObj4] forState:UIControlStateNormal];
        
        //Logic to enable delete btn for the retrieved image from local db
        if([self.photoData.deletePhotoBtn4 isEqualToString:@"YES"])
        {
             [photoArray addObject:self.photoBtn4];
            _deletePhotoBtn4.hidden= NO;
        }
        else
        {
            _deletePhotoBtn4.hidden= YES;
        }
    }
    if(self.photoData.photoObj5 != nil){
        
        [self.photoBtn5 setImage:[UIImage imageWithData:self.photoData.photoObj5] forState:UIControlStateNormal];
        
        //Logic to enable delete btn for the retrieved image from local db
        if([self.photoData.deletePhotoBtn5 isEqualToString:@"YES"])
        {
             [photoArray addObject:self.photoBtn5];
            _deletePhotoBtn5.hidden= NO;
        }
        else
        {
            _deletePhotoBtn5.hidden= YES;
        }
    }
    //}
    
}

/**
 Method to save the edited data
 */
-(IBAction)saveAction:(id)sender
{
    [self insertPhotosData];
    
}

- (void)customAlertView:(TRAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;
{
    UIButton *btn = (UIButton*)buttonSender;
    if(btn.tag == 1000)
    {
        if(buttonIndex == 0)
        {
            NSLog(@"Cancel");
            [self.delegate parentCallBack:nil];
            
        }
        else if(buttonIndex == 1)
        {
            if(_saveBtn.enabled){
                [self insertPhotosData];
                [self.delegate parentCallBack:nil];
            }
        }
   
    }
    else{
        
        if(buttonIndex == 0)
        {
            NSLog(@"Cancel");
            [self.delegate parentCallBack:nil];
        
        }
        else if(buttonIndex == 1)
        {
            _saveBtn.enabled = YES;
            [self disableDeleteBtn:buttonSender];
        }
    }
}

-(IBAction)deletePhotoAction:(id)sender
{
    TRAlertView *av = [[TRAlertView alloc] initWithTitle:@"Info"
                                                 message:@"Do you want to delete the photo."
                                                delegate:self
                                                  layout:eAlertTypeWithOk
                                           containsEmail:NO
                                                 dateStr:nil
                                       cancelButtonTitle:@"Cancel"
                                       otherButtonTitles:@"OK", nil];
    [av show];
    
    buttonSender = sender;
    
    
}

/**
 Method to cancel the edited data
 */
-(IBAction)cancelAction:(id)sender
{
    buttonSender = sender;
    
    if(_saveBtn.enabled)
    {
        TRAlertView *av = [[TRAlertView alloc] initWithTitle:@"Info"
                                                 message:@"Do you want to save the changes."
                                                delegate:self
                                                  layout:eAlertTypeWithOk
                                           containsEmail:NO
                                                 dateStr:nil
                                       cancelButtonTitle:@"Cancel"
                                       otherButtonTitles:@"OK", nil];
        [av show];
    }
    else
    {
         [self.delegate parentCallBack:nil];
    }
}

-(void)presentDataPopOver:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    
    selectedButton = btn;
    
    
    DataPopOverVC *dataPopOverVC = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"DataPopOverVC"];
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    [array addObject:@"Use Camera"];
    [array addObject:@"Choose from Album"];
    
    dataPopOverVC.delegate = self;
    
    _popVC = [[UIPopoverController alloc] initWithContentViewController:dataPopOverVC];
    
    _popVC.popoverContentSize = CGSizeMake(200, 100.0);
    
    CGRect positionRect = CGRectMake(btn.frame.origin.x, btn.frame.origin.y, btn.frame.size.width, btn.frame.size.height);
    
    [_popVC presentPopoverFromRect:positionRect  inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    
    dataPopOverVC.viewType = eCameraOptionPopOver;
    dataPopOverVC.dataArray = array;
    
}

-(void)capturedPhoto:(UIImage*)imageData
{
    
    NSLog(@"captued...");
    
    [selectedButton setImage:imageData forState:UIControlStateNormal];
    [self enableDeleteBtn:selectedButton];
    
    _saveBtn.enabled = YES;
    
    
    if(![photoArray containsObject:selectedButton])
        [photoArray addObject:selectedButton];
    
    
}
-(void)dataSelectedCallBack:(id)data
{
    [_popVC dismissPopoverAnimated:YES];
    
    NSString *selectedData = [NSString stringWithFormat:@"%@",data];
    
    photoVC = [[CapturePhotoVCViewController alloc] init];
    photoVC.delegate = self;
    //photoVC.parentVC = self.view.superview;
    photoVC.parentVC = self;
    [photoVC showCameraView:selectedData];
    
}

/**
 Method to capture photo
 */
-(IBAction)capturePhotoAction:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    
    //if(btn.imageView.image == [UIImage imageNamed:@"CheckListAddPhotoBtn.png"])
    if(![photoArray containsObject:btn])
    {
        NSLog(@"no image");
        [self presentDataPopOver:sender];
    }
    else{
        NSLog(@"image");
        CheckListPhotoViewController *viewController = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"CheckListPhotoViewController"];
       viewController.capturedImage = btn.imageView.image;
        viewController.modalPresentationStyle = UIModalPresentationFullScreen;
        [self presentViewController:viewController animated:YES completion:NULL];
    }
    
    NSLog(@"capturePhotoAction");
    //[self presentDataPopOver:sender];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) enableDeleteBtn:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    
    if(btn == _photoBtn1){
        _deletePhotoBtn1.hidden= NO;
        self.photoData.deletePhotoBtn1 = @"YES";
    }
    else if(btn == _photoBtn2)
    {
        _deletePhotoBtn2.hidden= NO;
        self.photoData.deletePhotoBtn2 = @"YES";
    }
    else if(btn == _photoBtn3)
    {
        _deletePhotoBtn3.hidden= NO;
        self.photoData.deletePhotoBtn3 = @"YES";
    }
    else if(btn == _photoBtn4)
    {
        _deletePhotoBtn4.hidden= NO;
        self.photoData.deletePhotoBtn4 = @"YES";
    }
    else if(btn == _photoBtn5)
    {
        _deletePhotoBtn5.hidden= NO;
        self.photoData.deletePhotoBtn5 = @"YES";
    }
    
}

-(void) disableDeleteBtn:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    
    if(btn == _deletePhotoBtn1){
        _deletePhotoBtn1.hidden= YES;
        [_photoBtn1 setImage:[UIImage imageNamed:@"CheckListAddPhotoBtn.png"] forState:UIControlStateNormal];
        self.photoData.deletePhotoBtn1 = @"NO";
        [photoArray removeObject:_photoBtn1];
    }
    else if(btn == _deletePhotoBtn2){
        _deletePhotoBtn2.hidden= YES;
        [_photoBtn2 setImage:[UIImage imageNamed:@"CheckListAddPhotoBtn.png"] forState:UIControlStateNormal];
        self.photoData.deletePhotoBtn2 = @"NO";
        [photoArray removeObject:_photoBtn2];
    }
    else if(btn == _deletePhotoBtn3){
        _deletePhotoBtn3.hidden= YES;
        [_photoBtn3 setImage:[UIImage imageNamed:@"CheckListAddPhotoBtn.png"] forState:UIControlStateNormal];
        self.photoData.deletePhotoBtn3 = @"NO";
        [photoArray removeObject:_photoBtn3];
    }
    else if(btn == _deletePhotoBtn4){
        _deletePhotoBtn4.hidden= YES;
        [_photoBtn4 setImage:[UIImage imageNamed:@"CheckListAddPhotoBtn.png"] forState:UIControlStateNormal];
        self.photoData.deletePhotoBtn4 = @"NO";
        [photoArray removeObject:_photoBtn4];
    }
    else if(btn == _deletePhotoBtn5){
        _deletePhotoBtn5.hidden= YES;
        [_photoBtn5 setImage:[UIImage imageNamed:@"CheckListAddPhotoBtn.png"] forState:UIControlStateNormal];
        self.photoData.deletePhotoBtn5 = @"NO";
        [photoArray removeObject:_photoBtn5];
    }
}

//Method to insert photo array
-(void)insertPhotosData
{
    
    self.photoData.photoObj1 = UIImagePNGRepresentation(self.photoBtn1.imageView.image);
    self.photoData.photoObj2 = UIImagePNGRepresentation(self.photoBtn2.imageView.image);
    self.photoData.photoObj3 = UIImagePNGRepresentation(self.photoBtn3.imageView.image);
    self.photoData.photoObj4 = UIImagePNGRepresentation(self.photoBtn4.imageView.image);
    self.photoData.photoObj5 = UIImagePNGRepresentation(self.photoBtn5.imageView.image);
    
    [[NSManagedObjectContext defaultContext] saveWithOptions:MRSaveWithoutOptions completion:^(BOOL success, NSError *error) {
        
        NSLog(@"Inserted");
        
        TRAlertView *av = [[TRAlertView alloc] initWithTitle:@"Info"
                                                     message:@"Photo saved successfully."
                                                    delegate:self
                                                      layout:eAlertTypeWithOk
                                               containsEmail:NO
                                                     dateStr:nil
                                           cancelButtonTitle:nil
                                           otherButtonTitles:@"OK", nil];
        [av show];
        [self.delegate parentCallBack:nil];
        
    }];
}

//Method to fetch photo array
-(NSMutableArray*)fetchPhotoDataArray
{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    for (ChkListPhoto *photo in (NSArray*)[ChkListPhoto findAll]) {
        
        if(self.photoData.mainSectionID == photo.mainSectionID && self.photoData.subSectionID == photo.subSectionID && self.photoData.rowID == photo.rowID)
            {
                if(photo.photoObj1 != nil || photo.photoObj2 != nil || photo.photoObj3 != nil || photo.photoObj4 != nil || photo.photoObj5 != nil)
                    [array addObject:photo];
                    NSLog(@"photo %@",photo);
                    break;
            }
    }
    return array;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
