//
//  CheckListColorSelectorVC.m
//  TrackiOS
//
//  Created by John Paul Ranjith on 06/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "CheckListColorSelectorVC.h"
#import "ChecklistColorTableViewCell.h"

#define NumberOfSelectorColors  3

@interface CheckListColorSelectorVC ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSArray *colorArrayImage;
@property (nonatomic, strong) NSArray *colorArrayImageSelected;


@end

@implementation CheckListColorSelectorVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.greenBtn.backgroundColor = CheckListGreenBtnColor;
    self.redBtn.backgroundColor = CheckListRedBtnColor;
    self.YellowBtn.backgroundColor = CheckListYellowBtnColor;
    
    _colorArrayImage = [NSArray arrayWithObjects:[UIImage imageNamed:@"CheckListColorOptionGreen"] ,
                  [UIImage imageNamed:@"CheckListColorOptionYellow"],
                  [UIImage imageNamed:@"CheckListColorOptionRed"], nil];
    
    _colorArrayImageSelected = [NSArray arrayWithObjects:[UIImage imageNamed:@"CheckListColorOptionGreenArrow"] ,
                        [UIImage imageNamed:@"CheckListColorOptionYellowArrow"],
                        [UIImage imageNamed:@"CheckListColorOptionRedArrow"], nil];
    
}

-(IBAction) colorSelector:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UItableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return NumberOfSelectorColors;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ChecklistColorTableViewCell *cell = (ChecklistColorTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"ChecklistColorTableViewCell"];
    [cell setBackgroundColor:[UIColor clearColor]];
    
    cell.colorImageView.image = [_colorArrayImage objectAtIndex:indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Change according to color selected
    [_delegate selectedColor:[_colorArrayImageSelected objectAtIndex:indexPath.row]:self.index:indexPath];
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
