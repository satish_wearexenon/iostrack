//
//  checkListResponsiblePeopleVC.m
//  TrackiOS
//
//  Created by satish sharma on 11/19/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "CheckListResponsiblePeopleVC.h"
#import "DealerStagePopCell.h"

@interface CheckListResponsiblePeopleVC () <UISearchBarDelegate>

@property (strong, nonatomic) NSMutableArray *noOfStageArray;
@property (strong, nonatomic) NSMutableArray *checkMarkArray;
@property (strong, nonatomic) NSMutableArray *personArray;
@property (strong, nonatomic) NSMutableArray *filterArray;

@end

@implementation CheckListResponsiblePeopleVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _personArray = [[NSMutableArray alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated
{
    [_titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:25.0f]];
    [_titleLabel setTextColor:DarkGrayTextColor];
    
    //[_searchSearchbar sette];
    
    _noOfStageArray = [[NSMutableArray alloc] initWithObjects:@"Ram",@"kumar",@"mac",@"rahra",@"heena",@"kai",@"pete", nil];
    _checkMarkArray = [[NSMutableArray alloc] initWithObjects:[NSNumber numberWithBool:YES],[NSNumber numberWithBool:YES],[NSNumber numberWithBool:YES],[NSNumber numberWithBool:YES],[NSNumber numberWithBool:YES],[NSNumber numberWithBool:YES],[NSNumber numberWithBool:YES], nil];
    
    _filterArray = _noOfStageArray;
    
    [_tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    [_tableView setDataSource:self];
    [_tableView setDelegate:self];
   
    
    _searchSearchbar.delegate = self;
   // [_tableView setScrollEnabled:NO];
    //isSelected = YES;
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (UIView *) tableHeaderView:(NSInteger) section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 22)];
    [view setBackgroundColor:ViewBackgroundColor];
    UILabel *tempLabel=[[UILabel alloc]initWithFrame:CGRectMake(15,0,300,22)];
    tempLabel.backgroundColor=[UIColor clearColor];
    tempLabel.textColor = [UIColor blackColor]; //here you can change the text color of header.
    tempLabel.font = [UIFont fontWithName:Font_Track_Regular size:17.0f];
    tempLabel.text = [NSString stringWithFormat:@"%@",[_noOfStageArray objectAtIndex:section]];
    [view addSubview:tempLabel];
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 22.0f;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    return [self tableHeaderView:section];
}

//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    // fixed font style. use custom view (UILabel) if you want something different
//    
//    NSString *title = [NSString stringWithFormat:@"%@",[_noOfStageArray objectAtIndex:section]];
//    
//    return title;
//}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_noOfStageArray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_filterArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"DealerStagePopCell";
    
    DealerStagePopCell *cell = (DealerStagePopCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[DealerStagePopCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
    }
    
    [_tableView setSeparatorInset:UIEdgeInsetsZero];
    cell.nameLabel.textColor = DarkGrayTextColor;
    cell.nameLabel.text = [NSString stringWithFormat:@"%@",[_filterArray objectAtIndex:indexPath.row]];
    cell.checkImage.hidden = (BOOL)[[_checkMarkArray objectAtIndex:indexPath.row] boolValue];
    
    return cell;
    // Configure the cell...
    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (void)reloadRowsAtIndexPaths:(NSArray *)indexPaths withRowAnimation:(UITableViewRowAnimation)animation
{
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Before: %@",_checkMarkArray);
    
    if ([[_checkMarkArray objectAtIndex:indexPath.row] isEqual:[NSNumber numberWithBool:YES]]) {
        [_checkMarkArray replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithBool:NO]];

    }else
    {
        [_checkMarkArray replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithBool:YES]];
    }
    
//    for (int i=0; i<[_checkMarkArray count]; i++) {
//        [_checkMarkArray replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:YES]];
//    }
//    
//    [_checkMarkArray replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithBool:NO]];
//    
    NSLog(@"After: %@",_checkMarkArray);
    
    [_tableView reloadData];
    
    [_personArray removeAllObjects];
    
    for (int i=0; i<[_checkMarkArray count]; i++) {
       if ([[_checkMarkArray objectAtIndex:i] isEqual:[NSNumber numberWithBool:NO]]) {
           [_personArray addObject:[_filterArray objectAtIndex:i]];
         }
    }
    
    [_delegate addNumberOfResponsiblePerson:_personArray];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{// called when text changes (including clear)
    NSLog(@"searchBar: textDidChange");
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@",_searchSearchbar.text];
    _filterArray = (NSMutableArray *)[_noOfStageArray filteredArrayUsingPredicate:predicate];
    
    if ([_searchSearchbar.text isEqualToString:@""]) {
        _filterArray = _noOfStageArray;
    }
    
    [_tableView reloadData];

}
@end
