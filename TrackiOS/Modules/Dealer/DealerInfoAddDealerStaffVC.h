//
//  DealerInfoAddDealerStaffVC.h
//  TrackiOS
//
//  Created by Anish Kumar on 24/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Staff.h"

@protocol DealerInfoAddDealerStaffVCDelegate <NSObject>

@optional

- (void) dataAdded:(NSDictionary *)data selectedIndex:(NSIndexPath*)selectedIndex;
- (void) dataAdded:(NSDictionary *)data ;

- (void) dataEdited:(NSDictionary *)data selectedIndex:(NSIndexPath*)selectedIndex;
- (void) dataEdited:(NSDictionary *)data;

- (void) dataDeleted;

@end

@interface DealerInfoAddDealerStaffVC : UIViewController <UITextFieldDelegate>

@property (nonatomic, assign) AddActionType addType;
@property (weak, nonatomic) id <DealerInfoAddDealerStaffVCDelegate> delegate;
//@property (nonatomic, strong) NSDictionary *staffDictionary;
@property (nonatomic, strong) Staff *selectedStaff;
//@property (nonatomic, strong) NSIndexPath *selectedIndex;
@property (nonatomic, strong) NSNumber *selectedID;

@end
