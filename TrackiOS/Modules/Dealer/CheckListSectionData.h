//
//  CheckListSectionData.h
//  TrackiOS
//
//  Created by John Paul Ranjith on 25/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CheckListSectionData : NSObject
{
    
}

@property (nonatomic, assign) NSInteger mainSection;
@property (nonatomic, assign) NSInteger subSection;
@property (nonatomic, strong) NSString *mainSectionTitle;
@property (nonatomic, strong) NSString *subSectionTitle;
@property (nonatomic, strong) NSString *sectionType;


@property (nonatomic, strong) NSMutableArray *rowData;



@end
