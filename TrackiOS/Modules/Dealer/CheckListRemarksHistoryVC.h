//
//  CheckListRemarksHistoryVC.h
//  TrackiOS
//
//  Created by John Paul Ranjith on 19/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChkListRemarks.h"
#import "TRAlertView.h"

@protocol CheckListRemarksHistoryVCDelegate <NSObject>

-(void)parentCallBack:(id)data;


@end


/**
 The CheckListRemarksHistoryVC class is present the remarks pop over view
 */
@interface CheckListRemarksHistoryVC : UIViewController<UITextViewDelegate,TRAlertViewDelegate>
{
    id buttonSender;
}

@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UIButton *clearBtn;
@property (weak, nonatomic) IBOutlet UITextView *remarksTextView;
@property (weak, nonatomic) IBOutlet UITableView *historyTextTbl;

@property (nonatomic, strong)  NSMutableArray *remarkHistArray;

@property (nonatomic) id <CheckListRemarksHistoryVCDelegate> delegate;

@property(nonatomic) ChkListRemarks *remarkData;
/**
 Method to save the edited data
 */
-(IBAction)saveAction:(id)sender;

/**
 Method to cancel the edited data
 */
-(IBAction)cancelAction:(id)sender;

/**
 Method to clear the edited data
 */
-(IBAction)clearAction:(id)sender;

@end
