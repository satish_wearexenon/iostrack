//
//  DealerModel.m
//  TrackiOS
//
//  Created by Anish Kumar on 04/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "DealerModel.h"

NSString *const kBaseClassPhotoUpdateCount = @"photoUpdateCount";
NSString *const kBaseClassContactName = @"contactName";
NSString *const kBaseClassCode = @"code";
NSString *const kBaseClassDate = @"date";
NSString *const kBaseClassItemUpdateCount = @"itemUpdateCount";
NSString *const kBaseClassDirection = @"direction";
NSString *const kBaseClassProgress = @"progress";
NSString *const kBaseClassprogressAttributed = @"progressAttributed";
NSString *const kBaseClassLocation = @"location";
NSString *const kBaseClassStage = @"stage";
NSString *const kBaseClassDealerID = @"dealerID";
NSString *const kBaseClassDealer = @"dealer";

@interface DealerModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation DealerModel;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}
- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.photoUpdateCount = [self objectOrNilForKey:kBaseClassPhotoUpdateCount fromDictionary:dict];
        self.contactName = [self objectOrNilForKey:kBaseClassContactName fromDictionary:dict];
        self.code = [self objectOrNilForKey:kBaseClassCode fromDictionary:dict];
        self.date = [self objectOrNilForKey:kBaseClassDate fromDictionary:dict];
        self.itemUpdateCount = [self objectOrNilForKey:kBaseClassItemUpdateCount fromDictionary:dict];
        self.direction = [self objectOrNilForKey:kBaseClassDirection fromDictionary:dict];
        self.progress = [self objectOrNilForKey:kBaseClassProgress fromDictionary:dict];
        self.progressAttributed = [self objectOrNilForKey:kBaseClassprogressAttributed fromDictionary:dict];
        self.location = [self objectOrNilForKey:kBaseClassLocation fromDictionary:dict];
        self.stage = [self objectOrNilForKey:kBaseClassStage fromDictionary:dict];
        self.dealerID = [self objectOrNilForKey:kBaseClassDealerID fromDictionary:dict];
        self.dealer = [self objectOrNilForKey:kBaseClassDealer fromDictionary:dict];
    }
    return self;
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.photoUpdateCount forKey:kBaseClassPhotoUpdateCount];
    [mutableDict setValue:self.contactName forKey:kBaseClassContactName];
    [mutableDict setValue:self.code forKey:kBaseClassCode];
    [mutableDict setValue:self.date forKey:kBaseClassDate];
    [mutableDict setValue:self.itemUpdateCount forKey:kBaseClassItemUpdateCount];
    [mutableDict setValue:self.direction forKey:kBaseClassDirection];
    [mutableDict setValue:self.progress forKey:kBaseClassProgress];
    [mutableDict setValue:self.progressAttributed forKey:kBaseClassprogressAttributed];
    [mutableDict setValue:self.location forKey:kBaseClassLocation];
    [mutableDict setValue:self.stage forKey:kBaseClassStage];
    [mutableDict setValue:self.dealerID forKey:kBaseClassDealerID];
    [mutableDict setValue:self.dealer forKey:kBaseClassDealer];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.photoUpdateCount = [aDecoder decodeObjectForKey:kBaseClassPhotoUpdateCount];
    self.contactName = [aDecoder decodeObjectForKey:kBaseClassContactName];
    self.code = [aDecoder decodeObjectForKey:kBaseClassCode];
    self.date = [aDecoder decodeObjectForKey:kBaseClassDate];
    self.itemUpdateCount = [aDecoder decodeObjectForKey:kBaseClassItemUpdateCount];
    self.direction = [aDecoder decodeObjectForKey:kBaseClassDirection];
    self.progress = [aDecoder decodeObjectForKey:kBaseClassProgress];
    self.progressAttributed = [aDecoder decodeObjectForKey:kBaseClassprogressAttributed];
    self.location = [aDecoder decodeObjectForKey:kBaseClassLocation];
    self.stage = [aDecoder decodeObjectForKey:kBaseClassStage];
    self.dealerID = [aDecoder decodeObjectForKey:kBaseClassDealerID];
    self.dealer = [aDecoder decodeObjectForKey:kBaseClassDealer];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeObject:_photoUpdateCount forKey:kBaseClassPhotoUpdateCount];
    [aCoder encodeObject:_contactName forKey:kBaseClassContactName];
    [aCoder encodeObject:_code forKey:kBaseClassCode];
    [aCoder encodeObject:_date forKey:kBaseClassDate];
    [aCoder encodeObject:_itemUpdateCount forKey:kBaseClassItemUpdateCount];
    [aCoder encodeObject:_direction forKey:kBaseClassDirection];
    [aCoder encodeObject:_progress forKey:kBaseClassProgress];
    [aCoder encodeObject:_progressAttributed forKey:kBaseClassprogressAttributed];
    [aCoder encodeObject:_location forKey:kBaseClassLocation];
    [aCoder encodeObject:_stage forKey:kBaseClassStage];
    [aCoder encodeObject:_dealerID forKey:kBaseClassDealerID];
    [aCoder encodeObject:_dealer forKey:kBaseClassDealer];
}

- (id)copyWithZone:(NSZone *)zone
{
    DealerModel *copy = [[DealerModel alloc] init];
    
    if (copy) {
        
        copy.photoUpdateCount = [self.photoUpdateCount copyWithZone:zone];
        copy.contactName = [self.contactName copyWithZone:zone];
        copy.code = [self.code copyWithZone:zone];
        copy.date = [self.date copyWithZone:zone];
        copy.itemUpdateCount = [self.itemUpdateCount copyWithZone:zone];
        copy.direction = [self.direction copyWithZone:zone];
        copy.progress = [self.progress copyWithZone:zone];
        copy.progressAttributed = [self.progressAttributed copyWithZone:zone];
        copy.location = [self.location copyWithZone:zone];
        copy.stage = [self.stage copyWithZone:zone];
        copy.dealerID = [self.dealerID copyWithZone:zone];
        //copy.dealer = [self.dealer copyWithZone:zone];
    }
    return copy;
}

@end
