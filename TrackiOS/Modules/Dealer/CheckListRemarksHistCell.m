//
//  CheckListRemarksHistCell.m
//  TrackiOS
//
//  Created by John Paul Ranjith on 19/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "CheckListRemarksHistCell.h"

@implementation CheckListRemarksHistCell

- (void)awakeFromNib {
    // Initialization code
    
    [_histTextLbl setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    [_timeStamp setFont:[UIFont fontWithName:Font_Track_Regular size:12]];
    _timeStamp.textColor = LightGrayTextColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
