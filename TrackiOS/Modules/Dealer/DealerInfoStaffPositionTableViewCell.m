//
//  DealerInfoStaffPositionTableViewCell.m
//  TrackiOS
//
//  Created by Anish Kumar on 25/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "DealerInfoStaffPositionTableViewCell.h"

@implementation DealerInfoStaffPositionTableViewCell

- (void)awakeFromNib
{
    [_positionLabel setFont:[UIFont fontWithName:Font_Track_Regular size:19]];
    _positionLabel.textColor = DarkGrayTextColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
