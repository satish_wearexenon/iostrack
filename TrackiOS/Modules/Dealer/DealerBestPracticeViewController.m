//
//  DealerBestPracticeViewController.m
//  TrackiOS
//
//  Created by satish sharma on 11/4/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "DealerBestPracticeViewController.h"
#import "DealerBestPracticeCell.h"
#import "DealerBestPracticePopover.h"
#import "DealerChecklistViewController.h"

@interface DealerBestPracticeViewController () <DealerBestPracticePopoverDelegate>

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;
@property (weak, nonatomic) IBOutlet UIButton *exportButton;

@property (strong, nonatomic) IBOutlet UITableView *bestPracticeTableView;

- (IBAction)tableButtonTapped:(id)sender;

@end

@implementation DealerBestPracticeViewController

- (void) viewDidAppear:(BOOL)animated
{
    // Listen to back Notification Handler
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(backPressedLocalAction)  name:@"backPressed" object:nil];
}

- (void) viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"backPressed" object:nil];
}

- (void) backPressedLocalAction
{
    MainViewController *parentVC = [APP_DELEGATE appBaseViewController];
    [parentVC proceedBack];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated
{
    [self.view setBackgroundColor:ViewBackgroundColor];
    [_bestPracticeTableView setBackgroundColor:ViewBackgroundColor];
    [_bestPracticeTableView setDelegate:self];
    [_bestPracticeTableView setDataSource:self];
    [_bestPracticeTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [_nameLabel setFont:[UIFont fontWithName:Font_Track_Regular size:23]];
    [_detailLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    _nameLabel.textColor = DarkGrayTextColor;
    _detailLabel.textColor = LightGrayTextColor;
    
   // _nameLabel.text = @"name";
   // _detailLabel.text = @"details";
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 153.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"DealerBestPracticeCell";
    
    DealerBestPracticeCell *cell = (DealerBestPracticeCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[DealerBestPracticeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    //cell.detailLabel.text = @"detail";
   //cell.stageLabel.text = @"stage";
   // cell.dateLabel.text = @"date";

    
    return cell;
    // Configure the cell...
    
}


- (IBAction)exportPDFButtonClick:(id)sender
{
    
}

- (IBAction)tableButtonTapped:(id)sender
{
    NSLog(@"DealerBestPracticeViewController: exportPDFButtonClick");
    CGRect positionRect;
    
    DealerBestPracticePopover *viewController = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"DealerBestPracticePopover"];
    viewController.delegate= self;
    positionRect = CGRectMake(self.view.frame.origin.x - 25, -40, 675, 770);
    
    _popVC = [[UIPopoverController alloc] initWithContentViewController:viewController];
    
    _popVC.popoverContentSize = CGSizeMake(675, 770);
    [_popVC presentPopoverFromRect:positionRect  inView:self.view permittedArrowDirections:0 animated:YES];
}

- (IBAction)checkMarkButtonClick:(id)sender
{
    
}

#pragma mark - Navigation
-(void)parentCallBack:(id)data
{
    [_popVC dismissPopoverAnimated:YES];
}

-(void)viewCheckListCallback:(id) sender
{
    NSLog(@"DealerBestPracticeViewController: viewCheckListCallback");
    
    [_popVC dismissPopoverAnimated:YES];
    
    DealerChecklistViewController *viewController = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"DealerChecklistViewController"];
    
    //MainViewController *parentVC = [APP_DELEGATE appBaseViewController];
    
    [_parent addChildViewController:viewController];
    [viewController didMoveToParentViewController:_parent];
    [_parent.view addSubview:viewController.view];
    
}

@end
