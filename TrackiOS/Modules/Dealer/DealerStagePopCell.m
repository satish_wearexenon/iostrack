//
//  DealerStagePopCell.m
//  TrackiOS
//
//  Created by satish sharma on 11/5/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "DealerStagePopCell.h"

@interface  DealerStagePopCell ()



@end


@implementation DealerStagePopCell

- (void)awakeFromNib {
    // Initialization code
    
    [_nameLabel setFont:[UIFont fontWithName:Font_Track_Regular size:18.0f]];
    [_nameLabel setTextColor:DarkGrayTextColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
