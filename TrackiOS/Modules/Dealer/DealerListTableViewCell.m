//
//  DealerListTableViewCell.m
//  TrackiOS
//
//  Created by Anish Kumar on 03/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "DealerListTableViewCell.h"

@implementation DealerListTableViewCell

- (void)awakeFromNib
{
    // Initialization code
    
    // set font
    [_dealerLocationLabel setFont:[UIFont fontWithName:Font_Track_Regular size:23]];
    [_dealerContactsLabel setFont:[UIFont fontWithName:Font_Track_Regular size:19]];
    [_dealerItemsUpdateLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    [_dealerPhotoUpateLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    [_dealerCodeLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    [_dealerDirectionLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    [_dealerDateLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    [_dealerProgressLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];

    // set color
    _dealerLocationLabel.textColor = DarkGrayTextColor;
    _dealerContactsLabel.textColor = LightGrayTextColor;
    _dealerItemsUpdateLabel.textColor = LightGrayTextColor;
    _dealerPhotoUpateLabel.textColor = LightGrayTextColor;
    _dealerCodeLabel.textColor = CustomTextColor;
    _dealerDirectionLabel.textColor = CustomTextColor;
    _dealerDateLabel.textColor = CustomTextColor;
    _dealerProgressLabel.textColor = CustomTextColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
