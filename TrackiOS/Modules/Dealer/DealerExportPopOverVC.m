//
//  DealerExportPopOverVC.m
//  TrackiOS
//
//  Created by satish sharma on 11/5/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "DealerExportPopOverVC.h"

@interface DealerExportPopOverVC ()
@property (weak, nonatomic) IBOutlet UIButton *PdfButton;
@property (weak, nonatomic) IBOutlet UIButton *csvButton;

- (IBAction)exportPDFClick:(id)sender;
- (IBAction)exportCVSClick:(id)sender;
@end

@implementation DealerExportPopOverVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
}

-(void) viewWillAppear:(BOOL)animated
{

    //set button properties
    [_PdfButton setTitle:@"PDF" forState:UIControlStateNormal];
    [_PdfButton setTitleColor:CustomTextColor forState:UIControlStateNormal];
    [_PdfButton setTitleColor:CustomTextColor forState:UIControlStateHighlighted];
    [_PdfButton.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:19]];
    
    [_csvButton.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:19]];
    [_csvButton setTitle:@"CSV" forState:UIControlStateNormal];
    [_csvButton setTitleColor:CustomTextColor forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)exportPDFClick:(id)sender
{
    NSLog(@"DealerExportPopOverVC: exportPDFClick");
    
    [_delegate pdfButtonCallback:sender];
}

- (IBAction)exportCVSClick:(id)sender
{
    NSLog(@"DealerExportPopOverVC:exportCVSClick");
    [_delegate csvButtonCallback:sender];
}


@end
