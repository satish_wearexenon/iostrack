//
//  CheckListRemarksHistCell.h
//  TrackiOS
//
//  Created by John Paul Ranjith on 19/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 The CheckListRemarksHistCell class is present the remarks pop over view
 */
@interface CheckListRemarksHistCell : UITableViewCell
{
    
}

/**
 This property holds a reference of pop over view
 */
@property (weak, nonatomic) IBOutlet UILabel *histTextLbl;
@property (weak, nonatomic) IBOutlet UILabel *timeStamp;

@end
