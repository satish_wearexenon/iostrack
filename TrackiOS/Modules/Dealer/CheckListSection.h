//
//  CheckListSection.h
//  CorePlot-CocoaTouch
//
//  Created by John Paul Ranjith on 05/12/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CheckListRow;

@interface CheckListSection : NSManagedObject

@property (nonatomic, retain) NSNumber * mainSectionID;
@property (nonatomic, retain) NSNumber * subSectionID;
@property (nonatomic, retain) NSNumber * rowID;
@property (nonatomic, retain) NSNumber * checkListID;
@property (nonatomic, retain) NSString * mainSectionName;
@property (nonatomic, retain) NSString * subSectionName;
@property (nonatomic, retain) CheckListRow *checkListRow;

@end
