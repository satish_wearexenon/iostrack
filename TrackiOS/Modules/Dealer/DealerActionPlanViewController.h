//
//  DealerActionPlanViewController.h
//  TrackiOS
//
//  Created by satish sharma on 11/4/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CheckListActionPlanVC.h"
#import "DealerDetailViewController.h"

/**
 The DealerActionPlanViewController class is present the Dealer Action Plan view
 */
//
@interface DealerActionPlanViewController : UIViewController <UITableViewDataSource,UITableViewDelegate, CheckListActionPlanVCDelegate>

@property (nonatomic, strong) UIPopoverController *popVC;

@property (strong, nonatomic) DealerDetailViewController *parent;

@end
