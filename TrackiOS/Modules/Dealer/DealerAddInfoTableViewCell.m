//
//  DealerAddInfoTableViewCell.m
//  TrackiOS
//
//  Created by Anish Kumar on 01/12/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "DealerAddInfoTableViewCell.h"

@implementation DealerAddInfoTableViewCell

- (void)awakeFromNib
{
    for (UILabel *labels in _labelsOutlet)
    {
        [labels setFont:[UIFont fontWithName:Font_Track_Regular size:18]];
        labels.textColor = DarkGrayTextColor;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)editAction:(id)sender {
}
@end
