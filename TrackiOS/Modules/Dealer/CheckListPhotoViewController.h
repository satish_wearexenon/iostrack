//
//  CheckListPhotoViewController.h
//  TrackiOS
//
//  Created by John Paul Ranjith on 26/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 The CheckListPhotoViewController class to display the preview photo
 */
@interface CheckListPhotoViewController : UIViewController
{
    
}

/**
 This property holds a reference data class
 */
@property (weak, nonatomic) IBOutlet UIImageView *imageObj;
@property (weak, nonatomic) IBOutlet UIButton *doneBtn;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (nonatomic, strong)  UIImage *capturedImage;
/**
 Method to cancel the edited data
 */
-(IBAction)doneAction:(id)sender;

@end
