//
//  PageViewController.h
//  TrackiOS
//
//  Created by John Paul Ranjith on 08/12/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PageViewController : UIViewController
{
    
}

@property (assign, nonatomic) NSInteger index;
@property (strong, nonatomic) IBOutlet UILabel *screenNumber;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewObj;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UIImageView *dividerImg;


@property (nonatomic, strong)  UIImage *imageObj;
@property (nonatomic, strong)  NSString *pageNo;


-(IBAction)cancelAction:(id)sender;
-(IBAction)saveAction:(id)sender;

@end
