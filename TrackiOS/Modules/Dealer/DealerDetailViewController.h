//
//  DealerDetailViewController.h
//  TrackiOS
//
//  Created by satish sharma on 03/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DealerProfileContainer.h"
#import "Dealer.h"

/**
 The DealerChecklistViewController class is used to display the Dealer Details.
 */
@interface DealerDetailViewController : UIViewController
{
    id parentViewControler;
    NSString *parentViewControlerTitle;
}
@property (weak, nonatomic) IBOutlet UIButton *summaryButton;
@property (weak, nonatomic) IBOutlet UIButton *actionPlanButton;
@property (weak, nonatomic) IBOutlet UIButton *bestPracticeButton;
@property (weak, nonatomic) IBOutlet UIButton *dealerInfoButton;
//@property (nonatomic, strong) NSNumber *selectedDealerId;
@property (nonatomic, strong) Dealer *selectedDealer;


@property (weak, nonatomic) DealerProfileContainer *dealercontainerViewController;
/**
 Method to display the Summary view
 */
- (IBAction)summaryButtonFilterClick:(id)sender;
/**
 Method to display the Action plan view
 */
- (IBAction)actionPlanFilterButtonClick:(id)sender;
/**
 Method to display the Beat Practice view
 */
- (IBAction)bestPracticeFilterButtonClick:(id)sender;
/**
 Method to display the Dealer Profile view
 */
- (IBAction)dealerInfoFilterButtonClick:(id)sender;


@end
