//
//  CheckListActionPlanVC.h
//  TrackiOS
//
//  Created by John Paul Ranjith on 06/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChkListActionPlan.h"
#import "TRAlertView.h"

@protocol CheckListActionPlanVCDelegate <NSObject>

-(void)parentCallBack:(id)data;


@end
/**
 The CheckListActionPlanVC class is used to present the action plan pop over
 */
@interface CheckListActionPlanVC : UIViewController<UITextViewDelegate,TRAlertViewDelegate>
{
    id buttonSender;
}
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UIButton *completeButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *situationLabel;
@property (weak, nonatomic) IBOutlet UILabel *actionLabel;
@property (weak, nonatomic) IBOutlet UILabel *outcomeLabel;
@property (weak, nonatomic) IBOutlet UILabel *supportLabel;
@property (weak, nonatomic) IBOutlet UILabel *resPersonLabel;
@property (weak, nonatomic) IBOutlet UILabel *dueDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@property (weak, nonatomic) IBOutlet UITextView *situationTextview;
@property (weak, nonatomic) IBOutlet UITextView *actionTextview;
@property (weak, nonatomic) IBOutlet UITextView *outcomeTextview;
@property (weak, nonatomic) IBOutlet UITextView *supportTextview;

@property (nonatomic) id <CheckListActionPlanVCDelegate> delegate;

@property (nonatomic) ChkListActionPlan *actionPlanData;

/**
 Method to cancel the edited data
 */
-(IBAction)cancelAction:(id)sender;

/**
 Method to Complete the edited data
 */
-(IBAction)completeAction:(id)sender;

/**
 Method to save the edited data
 */
-(IBAction)saveAction:(id)sender;

/**
 Method to delete the edited data
 */
-(IBAction)deleteAction:(id)sender;


@end
