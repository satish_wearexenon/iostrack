//
//  CheckListColorSelectorVC.h
//  TrackiOS
//
//  Created by John Paul Ranjith on 06/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SelectedColorVCDelegate <NSObject>

-(void)selectedColor:(UIImage*)imageSelected :(id)path :(NSIndexPath*)indexpath;

@end

@interface CheckListColorSelectorVC : UIViewController
{
    
}

/**
 Properties for the UI Controls
 */
@property (weak, nonatomic) IBOutlet UIButton *redBtn;
@property (weak, nonatomic) IBOutlet UIButton *greenBtn;
@property (weak, nonatomic) IBOutlet UIButton *YellowBtn;

@property (nonatomic, strong) UIButton *selectedBtn;
@property (nonatomic, strong) UITableViewCell *cellObj;
@property (nonatomic, strong) NSIndexPath *index;


/**
 Method to select the color
 */
-(IBAction) colorSelector:(id)sender;

@property (nonatomic) id <SelectedColorVCDelegate> delegate;

@end
