//
//  DealerInfoStaffPositionVC.m
//  TrackiOS
//
//  Created by Anish Kumar on 25/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "DealerGroupPopVC.h"
#import "DealerInfoStaffPositionTableViewCell.h"
#import "StaffRole.h"

@interface DealerGroupPopVC ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;


@end

@implementation DealerGroupPopVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
//   // _checkedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
//    for (StaffRole *staffRole in _positionArray)
//    {
//        if (<#condition#>) {
//            <#statements#>
//        }
//    }
    //NSUInteger index = [_positionArray indexOfObject:_checkedId];
    //NSSet *set1 = [NSSet setWithArray:array1];
    //NSUInteger index = [_positionArray indexOfObject:_checkedId];
    
    // Find the index based on "orderItemId" of the product using indexOfObjectPassingTest block
    
    
}

- (void) applyCheckedID:(NSNumber *)checkedId
{
    if (checkedId)
    {
        NSUInteger indexOfProduct = [_positionArray indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
            if ([[(StaffRole *)obj role_id] isEqualToNumber:checkedId]) {
                *stop = YES;
                return YES;
            }
            return NO;
        }];
        NSLog(@"indexOfProduct = %d", indexOfProduct);
        _checkedIndexPath = [NSIndexPath indexPathForRow:indexOfProduct inSection:0];
    }
    else
    {
        _checkedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    }
    //[tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_positionArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DealerInfoStaffPositionTableViewCell *cell = (DealerInfoStaffPositionTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"DealerInfoStaffPositionTableViewCell"];
    cell.backgroundColor = [UIColor clearColor];
    
    StaffRole *staffRole = [_positionArray objectAtIndex:indexPath.row];
    
    if (_popoverType == eDealerPopupGroup)
        cell.positionLabel.text = [_positionArray objectAtIndex:indexPath.row];
    else
        cell.positionLabel.text = staffRole.role_name;// [_positionArray objectAtIndex:indexPath.row];
    
    [[UITableViewCell appearance] setTintColor:CustomTextColor];
    
    if([_checkedIndexPath isEqual:indexPath])
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    else
        cell.accessoryType = UITableViewCellAccessoryNone;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.checkedIndexPath)
    {
        UITableViewCell* uncheckCell = [tableView cellForRowAtIndexPath:self.checkedIndexPath];
        uncheckCell.accessoryType = UITableViewCellAccessoryNone;
    }
    UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    [[UITableViewCell appearance] setTintColor:CustomTextColor];
    _checkedIndexPath = indexPath;
    cell.backgroundColor = [UIColor clearColor];
    
    if (_popoverType == eDealerPopupGroup)
    {
        if ([_delegate respondsToSelector:@selector(didPositionChange:selectedIndex:)])
            [_delegate didPositionChange:[_positionArray objectAtIndex:indexPath.row ]selectedIndex:indexPath];
    }
    else
    {
        StaffRole *staffRole = [_positionArray objectAtIndex:indexPath.row];
        
        if ([_delegate respondsToSelector:@selector(didPositionChangeId:)])
            [_delegate didPositionChangeId: staffRole.role_id];
    }
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    //dealerPopVC
    
}


@end
