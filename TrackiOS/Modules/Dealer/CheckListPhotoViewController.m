//
//  CheckListPhotoViewController.m
//  TrackiOS
//
//  Created by John Paul Ranjith on 26/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "CheckListPhotoViewController.h"

@interface CheckListPhotoViewController ()

@end

@implementation CheckListPhotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [_doneBtn setTitleColor:CustomTextColor forState:UIControlStateNormal];
    _titleLabel.textColor = DarkGrayTextColor;
    [_doneBtn.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:25]];
    [_titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:25]];
    
    _imageObj.image = _capturedImage;
    
}
-(IBAction)doneAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
