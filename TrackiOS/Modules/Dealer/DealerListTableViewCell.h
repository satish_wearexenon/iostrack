//
//  DealerListTableViewCell.h
//  TrackiOS
//
//  Created by Anish Kumar on 03/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GraphView.h"

@interface DealerListTableViewCell : UITableViewCell

/**
 Populate Navigation Bar Labels
 
 dealerLocationLabel : Location name (eg. Guangzhou Dealer)
 dealerContactsLabel : Contact person
 dealerItemsUpdateLabel : Items update count
 dealerPhotoUpateLabel : Photo count
 dealerCodeLabel : Dealer Code
 dealerDirectionLabel : Directoin/ Region
 dealerDateLabel : Date of Creation/ Last Updated
 dealerProgressLabel : Work progress in percentage
 
 */

@property (weak, nonatomic) IBOutlet UILabel *dealerLocationLabel;
@property (weak, nonatomic) IBOutlet UILabel *dealerContactsLabel;
@property (weak, nonatomic) IBOutlet UILabel *dealerItemsUpdateLabel;
@property (weak, nonatomic) IBOutlet UILabel *dealerPhotoUpateLabel;
@property (weak, nonatomic) IBOutlet UILabel *dealerCodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *dealerDirectionLabel;
@property (weak, nonatomic) IBOutlet UILabel *dealerDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *dealerProgressLabel;
@property (weak, nonatomic) IBOutlet GraphView *graphView;
@property (weak, nonatomic) IBOutlet UIImageView *graphImageView;
@property (weak, nonatomic) IBOutlet UIButton *grpahButton;

@end
