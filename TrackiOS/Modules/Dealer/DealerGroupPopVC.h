//
//  DealerInfoStaffPositionVC.h
//  TrackiOS
//
//  Created by Anish Kumar on 25/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DealerGroupPopVCDelegate <NSObject>

@optional

- (void) didPositionChange:(NSString *) position selectedIndex:(NSIndexPath*)selectedIndex;
- (void) didPositionChangeId:(NSNumber *) selectedId;

@end

@interface DealerGroupPopVC : UIViewController

@property (nonatomic, retain) NSIndexPath* checkedIndexPath;
@property (weak, nonatomic) id <DealerGroupPopVCDelegate> delegate;
@property (nonatomic, strong) NSArray *positionArray;
@property (nonatomic, strong) NSNumber *checkedId;
@property (nonatomic) PopOverType popoverType;

- (void) applyCheckedID:(NSNumber *)checkedId;
@end
