//
//  DealerChecklistViewController.h
//  TrackiOS
//
//  Created by John Paul Ranjith on 04/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CheckListColorSelectorVC.h"
#import "DataPopOverVC.h"
#import "CheckListRemarksVC.h"
#import "CheckListRemarksHistoryVC.h"
#import "CheckListBestPractiseVC.h"
#import "CheckListPhotoVC.h"
#import "CheckListReferenceVC.h"
#import "CheckListActionPlanVC.h"
/**
 The DealerChecklistViewController class is used to display the check list in a tableview.
 */
@interface DealerChecklistViewController : UIViewController<SelectedColorVCDelegate,DataPopOverVCDelegate,CheckListRemarksVCDelegate,CheckListRemarksHistoryVCDelegate,CheckListReferenceVCDelegate,CheckListPhotoVCDelegate,CheckListBestPractiseVCDelegate,CheckListActionPlanVCDelegate,UIPopoverControllerDelegate>
{
   
    NSMutableArray  *mainSectionArray;
    NSString        *parentViewControlerTitle;
    UIView          *mainSectionBackgroundView;
    NSInteger       selectedSection;
    NSMutableArray  *checkLstArray;
    NSInteger       checkListGreenBtnCount;
    NSInteger       checkListYellowBtnCount;
    NSInteger       checkListRedBtnCount;
    NSInteger       checkListGrayBtnCount;
    
    id              parentViewControler;
    
    ChkListRemarks *remark;
    ChkListPhoto *photoData;
    ChkListBestPractice *bestPractData;
    ChkListActionPlan *actionPlanData;
}

/**
 This property holds a reference of pop over view
 */
@property (nonatomic, strong) UIPopoverController *popVC;


/**
 This property holds a reference of check list table
 */
@property (strong, nonatomic) IBOutlet UITableView *checkListTable;

@property (nonatomic, strong) Dealer *selectedDealer;



/**
 Method to invoke different pop over controllers
 */
-(IBAction)tableButtonTapped:(id)sender;

/**
 Method to invoke checklist color selector
 */
-(IBAction)checkListColorSelector:(id)sender;

/**
 Method to invoke checklist action plan
 */
- (IBAction)CheckListActionPlanButtonClick:(id)sender;

@end
