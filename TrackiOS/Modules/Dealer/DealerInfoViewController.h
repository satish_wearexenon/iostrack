//
//  DealerInfoViewController.h
//  TrackiOS
//
//  Created by satish sharma on 11/4/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Dealer.h"
#import "TRAlertView.h"

/**
 The DealerInfoViewController class is present the Dealer Info view
 */
@interface DealerInfoViewController : UIViewController <TRAlertViewDelegate>

//@property (nonatomic, strong) NSNumber *selectedDealerID;
@property (nonatomic, strong) Dealer *selectedDealer;

@end
