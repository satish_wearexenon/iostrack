//
//  DealerSummaryViewController.h
//  TrackiOS
//
//  Created by satish sharma on 11/4/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DealerDetailViewController.h"

/**
  The DealerSummaryViewController class is present the Dealer summary
*/

@interface DealerSummaryViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) DealerDetailViewController *parent;

@property (strong, nonatomic) NSNumber *dealerid;
/**
 Method to hide the PopOver
 */
- (void) hidePopOver;

@end
