//
//  CheckListRemarksVC.m
//  TrackiOS
//
//  Created by John Paul Ranjith on 06/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "CheckListRemarksVC.h"
#import "ChkListRemarks.h"

#define TotalNoOfCharacters 200
#define MaxCharDesc @"Remarks characters should not exceed 200 characters."

@interface CheckListRemarksVC ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation CheckListRemarksVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = LightGrayBackgroundColor;

    [self alignUIControls];
}

//Method to position the UI Controls
-(void)alignUIControls
{
    _remarksTextView.layer.borderWidth = 1.0;
    _remarksTextView.layer.borderColor = CheckListTextViewBorderColor;
    [_remarksTextView setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    
    _cancelBtn.tag = 1000;
    [self.cancelBtn setTitleColor:CustomTextColor forState:UIControlStateNormal];
    [self.saveBtn setTitleColor:CustomTextColor forState:UIControlStateNormal];
    _titleLabel.textColor = DarkGrayTextColor;
    
    [_cancelBtn.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:25]];
    [_saveBtn.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    [_titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:25]];
    
    self.remarksTextView.delegate = self;
    _remarksTextView.textContainerInset = UIEdgeInsetsMake(10.0, 20.0, 10.0, 20.0);
    
    if([self.remarksTextView.text length] == 0)
    {
        self.remarksTextView.text = MaxCharDesc;
        self.remarksTextView.textColor = LightGrayTextColor;
    }
    else{
        self.remarksTextView.textColor = BlackColor;
    }
    
    _saveBtn.enabled= NO;
    
    self.remarksTextView.text = self.remarkData.remarksStr;
    
    if(![self.remarkData.remarksStr length]>0)
       self.remarksTextView.text = MaxCharDesc;
       
       //Displaying the saved remarks
    //[self fetchCheckListData];
    
}

//UITextview delegate methods
- (void)textViewDidBeginEditing:(UITextView *)textView {
    
    NSLog(@"textViewDidBeginEditing");
}

//UITextview delegate methods
- (void)textViewDidEndEditing:(UITextView *)textView{
    
    NSLog(@"textViewDidEndEditing %@",textView.text);

}

//Method to cancel edited data
-(IBAction)cancelAction:(id)sender
{
    buttonSender = sender;
    
    if(_saveBtn.enabled)
    {
        TRAlertView *av = [[TRAlertView alloc] initWithTitle:@"Info"
                                                     message:@"Do you want to save the changes."
                                                    delegate:self
                                                      layout:eAlertTypeWithOk
                                               containsEmail:NO
                                                     dateStr:nil
                                           cancelButtonTitle:@"Cancel"
                                           otherButtonTitles:@"OK", nil];
        [av show];
    }
    else
    {
        [self.delegate parentCallBack:nil];
    }
    NSLog(@"After truncating %@",[ChkListRemarks findAll]);
}

//Method to save edited data
-(IBAction)saveAction:(id)sender
{
    [self.remarksTextView resignFirstResponder];
    [self insertRemarksData];
}

//Method to initialize the controls
-(void)viewWillAppear:(BOOL)animated
{
    self.isPreviousHistExists = NO;
    
    if (self.isPreviousHistExists) {
        [self.view setFrame:CGRectMake(0,0, 600, 400)];
    }
    else
    {
        [self.view setFrame:CGRectMake(0,0, 600, 290)];
    }
    [super viewWillAppear:animated];
}

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    _saveBtn.enabled = YES;
    if([self.remarksTextView.text isEqualToString:MaxCharDesc])
    {
        self.remarksTextView.text = @"";
        self.remarksTextView.textColor = BlackColor;
    }
    
    return YES;
}

-(BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    if([self.remarksTextView.text length] == 0)
    {
        self.remarksTextView.text = MaxCharDesc;
        self.remarksTextView.textColor = LightGrayTextColor;
    }
    return YES;
}

- (void)customAlertView:(TRAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;
{
    UIButton *btn = (UIButton*)buttonSender;
    if(btn.tag == 1000)
    {
        if(buttonIndex == 0)
        {
            NSLog(@"Cancel");
            [self.delegate parentCallBack:nil];
            
        }
        else if(buttonIndex == 1)
        {
            if(_saveBtn.enabled){
                [self insertRemarksData];
            }
        }
        
    }
}

//Method to avoid dismissing popover when the user tap outside
//-(BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
//{
//    return NO;
//}

-(void)textViewDidChange:(UITextView *)textView
{
    
    int len = self.remarksTextView.text.length;
    
    if(len == TotalNoOfCharacters){
        TRAlertView *av = [[TRAlertView alloc] initWithTitle:@"Info"
                                        message:MaxCharDesc
                                        delegate:self
                                        layout:eAlertTypeWithOk
                                        containsEmail:NO
                                        dateStr:nil
                                        cancelButtonTitle:nil
                                        otherButtonTitles:@"OK", nil];
    [av show];
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text length] == 0)
    {
        if([textView.text length] != 0)
        {
            return YES;
        }
    }
    else if([[textView text] length] > TotalNoOfCharacters-1)
    {
        return NO;
    }
    return YES;
}


-(void)insertRemarksData
{
    self.remarkData.remarksStr = self.remarksTextView.text;
    
    [[NSManagedObjectContext defaultContext] saveWithOptions:MRSaveWithoutOptions completion:^(BOOL success, NSError *error) {
        NSLog(@"saved successfully");
        NSLog(@"After inserting %@",self.remarkData);
        TRAlertView *av = [[TRAlertView alloc] initWithTitle:@"Info"
                                                     message:@"Remark saved successfully."
                                                    delegate:self
                                                      layout:eAlertTypeWithOk
                                               containsEmail:NO
                                                     dateStr:nil
                                           cancelButtonTitle:nil
                                           otherButtonTitles:@"OK", nil];
        [av show];
        
        
        [self.delegate parentCallBack:nil];
    }];
    
}

-(void)fetchCheckListData
{
    NSLog(@"findall %@",[ChkListRemarks findAll]);
    
    NSPredicate *recordPredicate = [NSPredicate predicateWithFormat:@"mainSectionID == %@ && subSectionID == %@ && rowID == %@ && remarksStr != nil", self.mainSectionID,self.subSectionID,self.rowID];
   ChkListRemarks *remarks = [ChkListRemarks findFirstWithPredicate:recordPredicate];
    NSLog(@"remarks rowID %@",remarks.rowID);
    if(remarks.remarksStr != nil)
       self.remarksTextView.text = remarks.remarksStr;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
