//
//  calenderViewController.h
//  TrackiOS
//
//  Created by satish sharma on 11/21/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CalenderViewControllerDelegate <NSObject>

@optional
- (void) didDateSelected:(NSString *) date;
- (void) didDateChange:(NSDate *) date;

@end

@interface CalenderViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

@property (nonatomic, strong) NSString *titleString;

- (IBAction)selectDate:(id)sender;

@property (weak, nonatomic) id <CalenderViewControllerDelegate> delegate;

@end
