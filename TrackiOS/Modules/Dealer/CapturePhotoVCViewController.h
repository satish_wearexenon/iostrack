//
//  CapturePhotoVCViewController.h
//  TrackiOS
//
//  Created by John Paul Ranjith on 14/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol CapturedPhotoVCDelegate <NSObject>

-(void)capturedPhoto:(UIImage*)imageData;


@end



@interface CapturePhotoVCViewController : UIViewController<UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIPopoverControllerDelegate>
{
    
}

@property (nonatomic) id <CapturedPhotoVCDelegate> delegate;

@property (nonatomic, strong) UIPopoverController *popVC;

@property (strong, nonatomic) UIImagePickerController * picker;

@property (nonatomic, strong) id parentVC;

-(void)showCameraView:(NSString*)data;

@end
