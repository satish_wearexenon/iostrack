//
//  CheckListActionPlanVC.m
//  TrackiOS
//
//  Created by John Paul Ranjith on 06/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "CheckListActionPlanVC.h"
#import "CheckListResponsiblePeopleVC.h"
#import "CalenderViewController.h"

@interface CheckListActionPlanVC () <UITextViewDelegate, checkListResponsiblePeopleDelegate, CalenderViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *responseScroll;

@property (strong, nonatomic) CheckListResponsiblePeopleVC *checkListResponsible;
@property (strong, nonatomic) CalenderViewController *calendarVC;

@property (strong, nonatomic) NSMutableArray *resPersonArray;

@end

@implementation CheckListActionPlanVC

//- (void) viewDidAppear:(BOOL)animated
//{
//    
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void) viewWillAppear:(BOOL)animated
{
    [self.view setBackgroundColor:LightGrayBackgroundColor];
    
    [_titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:25.0f]];
    [_titleLabel setTextColor:DarkGrayTextColor];
    
    // set title text color
    [_situationLabel setTextColor:LightGrayTextColor];
    [_actionLabel setTextColor:LightGrayTextColor];
    [_outcomeLabel setTextColor:LightGrayTextColor];
    [_supportLabel setTextColor:LightGrayTextColor];
    [_resPersonLabel setTextColor:LightGrayTextColor];
    [_dueDateLabel setTextColor:LightGrayTextColor];
   
    
    //set font
    [_situationLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16.0f]];
    [_actionLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16.0f]];
    [_outcomeLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16.0f]];
    [_supportLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16.0f]];
    [_resPersonLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16.0f]];
    [_dueDateLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16.0f]];
    
    
    
    
    /*
     [_situationLabel setText:@""];
     [_actionLabel setText:@""];
     [_outcomeLabel setText:@""];
     [_supportLabel setText:@""];
     [_resPersonLabel setText:@""];
     [_dueDateLabel setText:@""];
    */
    
    //set textview text color
    [_situationTextview setTextColor:DarkGrayTextColor];
    [_actionTextview setTextColor:DarkGrayTextColor];
    [_outcomeTextview setTextColor:DarkGrayTextColor];
    [_supportTextview setTextColor:DarkGrayTextColor];
    [_dateLabel setTextColor:OrangeTextColor];
    
    [_situationTextview setBackgroundColor:WhiteBGColor];
    [_actionTextview setBackgroundColor:WhiteBGColor];
    [_outcomeTextview setBackgroundColor:WhiteBGColor];
    [_supportTextview setBackgroundColor:WhiteBGColor];
    //[_dateLabel setBackgroundColor:WhiteBGColor];
    //[_responseScroll setBackgroundColor:WhiteBGColor];
    
    [_situationTextview setText:@""];
    [_actionTextview setText:@""];
    [_outcomeTextview setText:@""];
    [_supportTextview setText:@""];
    
    [_situationTextview setFont:[UIFont fontWithName:Font_Track_Regular size:16.0f]];
    [_actionTextview setFont:[UIFont fontWithName:Font_Track_Regular size:16.0f]];
    [_outcomeTextview setFont:[UIFont fontWithName:Font_Track_Regular size:16.0f]];
    [_supportTextview setFont:[UIFont fontWithName:Font_Track_Regular size:16.0f]];
    [_dateLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16.0f]];
    
    [_situationTextview setTextContainerInset:UIEdgeInsetsMake(0, 16, 0, 16)];
    [_actionTextview setTextContainerInset:UIEdgeInsetsMake(0, 16, 0, 16)];
    [_outcomeTextview setTextContainerInset:UIEdgeInsetsMake(0, 16, 0, 16)];
    [_supportTextview setTextContainerInset:UIEdgeInsetsMake(0, 16, 0, 16)];
    // [_dateLabel setTextContainerInset:UIEdgeInsetsMake(0, 16, 0, 16)];
    
    
    [_situationTextview setDelegate:self];
    [_actionTextview setDelegate:self];
    [_outcomeTextview setDelegate:self];
    [_supportTextview setDelegate:self];
    
    // Do any additional setup after loading the view.
    [_saveBtn setTitleColor:CustomTextColor forState:UIControlStateNormal];
    [_saveBtn.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:25]];
    [_cancelBtn setTitleColor:CustomTextColor forState:UIControlStateNormal];
    [_cancelBtn.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:25]];
    [_completeButton setTitleColor:CustomTextColor forState:UIControlStateNormal];
    [_completeButton.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:25]];

    [_deleteButton setTitleColor:CustomTextColor forState:UIControlStateNormal];
    [_deleteButton.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:25]];

    _cancelBtn.tag = 1000;
    
    //[_saveBtn setBackgroundImage:[UIImage imageNamed:@"custom_button_bg"] forState:UIControlStateNormal];
    //[_completeButton setBackgroundImage:[UIImage imageNamed:@"custom_button_bg"] forState:UIControlStateNormal];
    //[_deleteButton setBackgroundImage:[UIImage imageNamed:@"custom_button_bg"] forState:UIControlStateNormal];
    
    //[_saveBtn setBackgroundImage:[UIImage imageNamed:@"01_login_button_press"] forState:UIControlStateSelected];
    //[_completeButton setBackgroundImage:[UIImage imageNamed:@"01_login_button_press"] forState:UIControlStateSelected];
   // [_deleteButton setBackgroundImage:[UIImage imageNamed:@"01_login_button_press"] forState:UIControlStateSelected];
    
    _saveBtn.enabled = NO;
    
    
    NSArray *array = [self.actionPlanData.responsiblePeople componentsSeparatedByString:@","];
    
    [self addNumberOfResponsiblePerson:array];
    self.actionTextview.text = self.actionPlanData.actionStr;
    self.situationTextview.text = self.actionPlanData.situation;
    self.supportTextview.text = self.actionPlanData.support;
    self.outcomeTextview.text = self.actionPlanData.desiredOutcome;
    self.dateLabel.text = self.actionPlanData.dueDate;
    
    [super viewWillAppear:animated];
    
    
   
    
}

-(void)addUIIntoScrollView
{
    UIScrollView *scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    scroll.pagingEnabled = YES;
    
   [self.view addSubview:scroll];
    
}
- (void)customAlertView:(TRAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;
{
    UIButton *btn = (UIButton*)buttonSender;
    if(btn.tag == 1000)
    {
        if(buttonIndex == 0)
        {
            NSLog(@"Cancel");
            [self.delegate parentCallBack:nil];
            
        }
        else if(buttonIndex == 1)
        {
            if(_saveBtn.enabled){
                [self insertActionPlanData];
            }
        }
        
    }
}


/**
 Method to cancel the edited data
 */
-(IBAction)cancelAction:(id)sender
{
    buttonSender = sender;
    
    if(_saveBtn.enabled)
    {
        TRAlertView *av = [[TRAlertView alloc] initWithTitle:@"Info"
                                                     message:@"Do you want to save the changes."
                                                    delegate:self
                                                      layout:eAlertTypeWithOk
                                               containsEmail:NO
                                                     dateStr:nil
                                           cancelButtonTitle:@"Cancel"
                                           otherButtonTitles:@"OK", nil];
        [av show];
    }
    else
    {
        [self.delegate parentCallBack:nil];
    }    
}

/**
 Method to Complete the edited data
 */
-(IBAction)completeAction:(id)sender
{
    
}

/**
 Method to save the edited data
 */
-(IBAction)saveAction:(id)sender
{
    [self insertActionPlanData];
}

/**
 Method to delete the edited data
 */
-(IBAction)deleteAction:(id)sender
{
    
}

/**
 Delegate Method to add responsible Person
 */
- (void) addNumberOfResponsiblePerson:(NSArray *)personArray
{
    _resPersonArray = (NSMutableArray *)personArray;
    NSLog(@"personArray: %@",personArray);
    
    _responseScroll.contentSize = CGSizeMake(([personArray count]*100), 40);
    _responseScroll.showsVerticalScrollIndicator = NO;
    _responseScroll.showsHorizontalScrollIndicator =NO;
    
    [self removeAllSubviews];
    
    for (int i =0; i<[personArray count]; i++) {
        UIButton *personButton;
        personButton = [UIButton buttonWithType:UIButtonTypeCustom];
        personButton.frame = CGRectMake(20 + 102*i, 5, 100, 30);
        personButton.tag = (i+111);
        [personButton setTitle:[personArray objectAtIndex:i] forState:UIControlStateNormal];
        [personButton setBackgroundImage:[UIImage imageNamed:@"shadow_btn_bg"] forState:UIControlStateNormal];
        [personButton addTarget:self action:@selector(deleteResponsiblePerson:) forControlEvents:UIControlEventTouchUpInside];
        [_responseScroll addSubview:personButton];
    }
    
    NSLog(@"addNumberOfResponsiblePerson");
}

-(void) deleteResponsiblePerson:(id) sender
{
    UIButton *btn = (UIButton *)sender;
    
    [self removeAllSubviews];
    
    if (btn.tag>=0) {
        [_resPersonArray removeObjectAtIndex:(btn.tag - 111)];

    }
    
    NSLog(@"addNumberOfResponsiblePerson");
    
    [self addNumberOfResponsiblePerson:_resPersonArray];

}

- (void) removeAllSubviews
{
    NSArray *array = [_responseScroll subviews];
    
    for (int i = 0; i< [array count]; i++) {
        
        if ([[array objectAtIndex:i] isKindOfClass:[UIButton class]]) {
            UIButton *btn1 = [array objectAtIndex:i];
            [btn1 removeFromSuperview];
        }
        
    }

}

//UITextview delegate methods
- (void)textViewDidBeginEditing:(UITextView *)textView {
    
    NSLog(@"textViewDidBeginEditing");
    _saveBtn.enabled = YES;
}

//UITextview delegate methods
- (void)textViewDidEndEditing:(UITextView *)textView{
    
    NSLog(@"textViewDidEndEditing %@",textView.text);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"checkListResponsiblePeopleVC"]) {
        
        _checkListResponsible = [segue destinationViewController];
        _checkListResponsible.delegate = self;
    }else if ([segue.identifier isEqualToString:@"calenderViewController"])
    {
        _calendarVC = [segue destinationViewController];
        _calendarVC.delegate = self;
        _calendarVC.titleString = LOCALIZED_CALENDAR_DUE_DATE;
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSUInteger newLength = [textView.text length] + [text length] - range.length;
    return (newLength > 300) ? NO : YES;
}

- (void) didDateSelected:(NSString *) date
{
    _saveBtn.enabled = YES;
    _dateLabel.text = date;
}

//Method to insert action plan array
-(void)insertActionPlanData
{
    NSString *responsiblePersonStr;
    
    
    responsiblePersonStr = [_resPersonArray componentsJoinedByString:@","];
    
    self.actionPlanData.actionStr = self.actionTextview.text;
    self.actionPlanData.desiredOutcome = self.outcomeTextview.text;
    self.actionPlanData.dueDate = _dateLabel.text;
    self.actionPlanData.responsiblePeople = responsiblePersonStr;
    self.actionPlanData.situation = self.situationTextview.text;
    self.actionPlanData.support = self.supportTextview.text;
    
    
    [[NSManagedObjectContext defaultContext] saveWithOptions:MRSaveWithoutOptions completion:^(BOOL success, NSError *error) {
        
        NSLog(@"Inserted");
        TRAlertView *av = [[TRAlertView alloc] initWithTitle:@"Info"
                                                     message:@"Action plan saved successfully."
                                                    delegate:self
                                                      layout:eAlertTypeWithOk
                                               containsEmail:NO
                                                     dateStr:nil
                                           cancelButtonTitle:nil
                                           otherButtonTitles:@"OK", nil];
        [av show];
        [self.delegate parentCallBack:nil];
    }];
    
    
}

//Method to fetch action plan array
-(NSMutableArray*)fetchActionPlanDataArray
{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    for (ChkListActionPlan *actionPlan in (NSArray*)[ChkListActionPlan findAll]) {
        
        if(self.actionPlanData.mainSectionID == actionPlan.mainSectionID && self.actionPlanData.subSectionID == actionPlan.subSectionID && self.actionPlanData.rowID == actionPlan.rowID)
        {
            [array addObject:actionPlan];
            NSLog(@"actionPlan %@",actionPlan);
        }
    }
    return array;
}


@end
