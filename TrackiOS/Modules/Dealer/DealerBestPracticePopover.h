//
//  DealerBestPracticePopover.h
//  TrackiOS
//
//  Created by satish sharma on 11/26/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataPopOverVC.h"
#import "CapturePhotoVCViewController.h"

@protocol DealerBestPracticePopoverDelegate <NSObject>

-(void)parentCallBack:(id)data;

-(void)viewCheckListCallback:(id) sender;

@end

@interface DealerBestPracticePopover : UIViewController<UITextViewDelegate,DataPopOverVCDelegate,CapturedPhotoVCDelegate>
{
    UIButton *selectedButton;
    CapturePhotoVCViewController *photoVC;
}

@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UIButton *viewChecklistBtn;
@property (weak, nonatomic) IBOutlet UITextView *titleTextView;
@property (weak, nonatomic) IBOutlet UITextView *descTextView;
@property (weak, nonatomic) IBOutlet UITextView *implementationTextView;


@property (weak, nonatomic) IBOutlet UIButton *photoBtn1;
@property (weak, nonatomic) IBOutlet UIButton *photoBtn2;
@property (weak, nonatomic) IBOutlet UIButton *photoBtn3;
@property (weak, nonatomic) IBOutlet UIButton *photoBtn4;
@property (weak, nonatomic) IBOutlet UIButton *photoBtn5;


@property (weak, nonatomic) IBOutlet UIButton *deletePhotoBtn1;
@property (weak, nonatomic) IBOutlet UIButton *deletePhotoBtn2;
@property (weak, nonatomic) IBOutlet UIButton *deletePhotoBtn3;
@property (weak, nonatomic) IBOutlet UIButton *deletePhotoBtn4;
@property (weak, nonatomic) IBOutlet UIButton *deletePhotoBtn5;


@property (nonatomic, strong) UIPopoverController *popVC;

@property (nonatomic) id <DealerBestPracticePopoverDelegate> delegate;

/**
 Method to save the edited data
 */
-(IBAction)saveAction:(id)sender;


/**
 Method to cancel the edited data
 */
-(IBAction)cancelAction:(id)sender;

/**
 Method to viewc Check list the edited data
 */
-(IBAction)viewCheckListAction:(id)sender;

@end
