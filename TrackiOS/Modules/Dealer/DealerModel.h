//
//  DealerModel.h
//  TrackiOS
//
//  Created by Anish Kumar on 04/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Dealer.h"

@interface DealerModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *photoUpdateCount;
@property (nonatomic, strong) NSString *contactName;
@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *itemUpdateCount;
@property (nonatomic, strong) NSString *direction;
@property (nonatomic, strong) NSString *progress;
@property (nonatomic, strong) NSMutableAttributedString *progressAttributed;
@property (nonatomic, strong) NSString *location;
@property (nonatomic, strong) NSString *stage;
@property (nonatomic, strong) NSNumber *dealerID;
@property (nonatomic, strong) Dealer *dealer;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end