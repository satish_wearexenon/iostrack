//
//  DataPopOverVC.m
//  TrackiOS
//
//  Created by John Paul Ranjith on 14/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "DataPopOverVC.h"
#import "DataPopOverCell.h"

@interface DataPopOverVC ()

@end

@implementation DataPopOverVC


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.dataArray = [[NSMutableArray alloc] init];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
     NSLog(@"dataArray %@",self.dataArray);
    
    [super viewWillAppear:animated];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40.0;
}

#pragma mark - UItableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DataPopOverCell *popOverCell = (DataPopOverCell *)[tableView dequeueReusableCellWithIdentifier:@"DataPopOverCell"];
    
    popOverCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    popOverCell.dataLabel.text = [self.dataArray objectAtIndex:indexPath.row];
    
    return popOverCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DataPopOverCell *cell = (DataPopOverCell*)[self.popOverTable cellForRowAtIndexPath:indexPath];
 
    switch (_viewType)
    {
        case eCameraOptionPopOver:
        {
            [_delegate dataSelectedCallBack:cell.dataLabel.text];
        }
            break;
        case eCheckListSectionPopOver:
        {
            NSIndexPath *path = [NSIndexPath indexPathForRow:1 inSection:indexPath.row];
            [_delegate dataSelectedCallBack:path];
        }
            break;
        default:
        {
        }
            break;
    }
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
