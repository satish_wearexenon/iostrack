//
//  DealerExportPopOverVC.h
//  TrackiOS
//
//  Created by satish sharma on 11/5/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 The DealerExportPopOverVCDelegate class is contain call back method
 */

@protocol DealerExportPopOverVCDelegate <NSObject>

-(id)pdfButtonCallback:(id)sender;
-(id)csvButtonCallback:(id)sender;

@end

/**
 The DealerExportPopOverVC class is present PopOver view for PDF & CSV Export
 */
@interface DealerExportPopOverVC : UIViewController
/**
 This property holds a reference of pop over view
 */
@property (nonatomic) id <DealerExportPopOverVCDelegate> delegate;

@end
