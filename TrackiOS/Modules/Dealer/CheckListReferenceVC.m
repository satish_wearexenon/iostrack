//
//  CheckListReferenceVC.m
//  TrackiOS
//
//  Created by John Paul Ranjith on 11/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "CheckListReferenceVC.h"

@interface CheckListReferenceVC ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@end

@implementation CheckListReferenceVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.okayBtn setTitleColor:CustomTextColor forState:UIControlStateNormal];
    _titleLabel.textColor = DarkGrayTextColor;
    
    [_okayBtn.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    [_titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:25]];
    [_referenceTextView setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
}

//Method to handle okay btn
-(IBAction)okayAction:(id)sender
{
    [self.delegate parentCallBack:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
