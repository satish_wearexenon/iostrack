//
//  TTimeline2ViewController.h
//  TrackiOS
//
//  Created by Anish Kumar on 15/10/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface TTimeline2ViewController : UIViewController <MFMailComposeViewControllerDelegate, TRAlertViewDelegate>

- (void) sendEMail;

@end
