//
//  CellMiddle.h
//  CustomTable
//
//  Created by Anish Kumar on 29/09/14.
//  Copyright (c) 2014 xenon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellMiddle : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *lineView;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *resultLabel;
@property (weak, nonatomic) IBOutlet UILabel *itemLabel;
@property (weak, nonatomic) IBOutlet UILabel *photoLabel;
@property (weak, nonatomic) IBOutlet UILabel *codeLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *percentageLabel;
@property (weak, nonatomic) IBOutlet UIButton *imageButton;

- (void)constructCellWithViewColor:(UIColor*)colorSelected;

@end
