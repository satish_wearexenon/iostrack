//
//  CellDay.h
//  CustomTable
//
//  Created by Anish Kumar on 29/09/14.
//  Copyright (c) 2014 xenon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellDay : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *lineViewTop;
@property (weak, nonatomic) IBOutlet UIView *lineViewBottom;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;

@end
