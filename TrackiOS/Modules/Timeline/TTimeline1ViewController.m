//
//  TTimeline1ViewController.m
//  TrackiOS
//
//  Created by Anish Kumar on 15/10/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "TTimeline1ViewController.h"
#import "TTimeline1DetailViewController.h"
#import "DealerDetailViewController.h"

@interface TTimeline1ViewController ()

@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic) int percentage;
- (IBAction)detailViewTapped:(id)sender;

@end

@implementation TTimeline1ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    TRAlertView *av = [[TRAlertView alloc] initWithTitle:@"Hint"
                                                 message:@"You will discard all the changes you have made in this screen."
                                                delegate:self
                                                  layout:eAlertTypeWithOk
                                           containsEmail:NO
                                                dateStr:nil
                                       cancelButtonTitle:nil
                                       otherButtonTitles:@"OK", nil];
    [av show];
    
    _timer = [NSTimer scheduledTimerWithTimeInterval:0.1f
                                              target:self
                                            selector:@selector(timerFired:)
                                            userInfo:nil
                                             repeats:YES];
    
    MainViewController *parentVC = [APP_DELEGATE appBaseViewController];
    parentVC.cloudImageView.image = [UIImage imageNamed:@"cloudProgress"];
    
}

- (void) viewDidAppear:(BOOL)animated
{
    MainViewController *parentVC = [APP_DELEGATE appBaseViewController];
    parentVC.navLabel.text = @"Timeline 1";
    
   // parentVC.backView.hidden = YES;
    parentVC.backButton.hidden = YES;
    parentVC.navLabel.hidden = NO;
    
    parentVC.backLabel.text = @"Timeline 1 Detail";
}


- (void)timerFired:(NSTimer *)timer
{
    if (_percentage==100)
    {
        if ([_timer isValid])
            [_timer invalidate];
        
        _timer = nil;
        MainViewController *parentVC = [APP_DELEGATE appBaseViewController];
        parentVC.progressView.hidden = YES;
        parentVC.progressViewBackground.hidden = YES;
        parentVC.cloudImageView.image = [UIImage imageNamed:@"cloudDone"];
    }
    
    double delayInSeconds = 0.2;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self performSelector:@selector(uploadStatusInPercentage:) withObject:[NSNumber numberWithDouble:_percentage] afterDelay:0.0f];
    });
    _percentage ++;
}

-(void)uploadStatusInPercentage:(NSNumber*)inCompletePercentage
{
    MainViewController *parentVC = [APP_DELEGATE appBaseViewController];
    //parentVC.progressView.hidden = NO;
    
    CGFloat width = 77 * (inCompletePercentage.doubleValue/100);
    [UIView animateWithDuration:0.2f animations:^{
        CGRect frame = parentVC.progressView.frame;
        frame.origin.x = 600.0f;
        frame.size.width = (width>77)? 77:width;
        parentVC.progressView.frame = frame;
    }completion:^(BOOL finished)
     {
         
     }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)detailViewTapped:(id)sender
{
//    TTimeline1DetailViewController *viewController = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"TTimeline1DetailViewController"];
//    
//    [self addChildViewController:viewController];
//    [viewController didMoveToParentViewController:self];
//    [self.view addSubview:viewController.view];
    
        DealerDetailViewController *viewController = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"DealerDetailViewController"];
    
        [self addChildViewController:viewController];
        [viewController didMoveToParentViewController:self];
        [self.view addSubview:viewController.view];
}
@end
