//
//  TTimelineViewController.m
//  TrackiOS
//
//  Created by Anish Kumar on 08/10/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "TTimelineViewController.h"
#import "CellDay.h"
#import "CellMiddle.h"
#import "CellToday.h"
#import "DealerPopOverVC.h"
#import "PhotoDisplayViewController.h"

#import "DealerChecklistViewController.h"

typedef enum  {
    eSectionCellType1 = 3000,
    
    eSectionCellType2,
    
    eSectionCellType3
    
}cellType;


typedef enum  {
    eButtonTypeDealer = 6000,
    
    eButtonTypeChecklist,
    
    eButtonTypeEdit,
    
    eButtonTypeImage
    
}buttonType;

@interface TTimelineViewController () <UITableViewDataSource, UITableViewDelegate, DealerPopOverVCDelegate>

@property (strong, nonatomic) NSMutableArray *dateArray;
@property (strong, nonatomic) NSMutableArray *colorArray;
@property (nonatomic) BOOL isToday;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) UIPopoverController *popVC;

//@property (nonatomic, copy) NSMutableString *model;

@end

@implementation TTimelineViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.view setBackgroundColor:ViewBackgroundColor];

    _isToday = YES;
    
    _dateArray = [[NSMutableArray alloc]init];
    _colorArray = [[NSMutableArray alloc]init];
    
    [_dateArray addObject:[NSDate date]];
    [_dateArray addObject:[NSDate date]];
    
    NSArray *numberArray = @[ @1, @1, @2,
                              @2, @2, @3, @3, @4,
                              @5, @5, @5, @5, @6,
                              @6, @6, @7, @7, @7];
    
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    
    for (int i = 0; i < 18; i++)
    {
        [comps setDay:29- [[numberArray objectAtIndex:i] intValue]];
        [comps setMonth:9];
        [comps setYear:2014];
        NSDate *date = [[NSCalendar currentCalendar] dateFromComponents:comps];
        [_dateArray addObject:date];
    }
    
    for (int i = 0; i < 20; i++)
    {
        NSDate *date1 = [_dateArray objectAtIndex:i];
        NSDate *date2 = [_dateArray objectAtIndex:i];
        
        if (i > 1)
            date2 = [_dateArray objectAtIndex:i -1];
        
        if (_isToday)
            [_colorArray addObject:CustomTextColor];
        else
            [_colorArray addObject:[UIColor grayColor]];
        
        if ([date1 compare:date2] != NSOrderedSame)
            _isToday = NO;
    }
    
    
//    NSMutableString * model = [NSMutableString stringWithString:@"satish"];
//    NSString *str2 = [model copy];
//    //str2 = model;
//    
//    NSLog(@"%@",str2);
//    
//    [model setString:@"kumar"];
//    NSLog(@"%@, %@",model,str2);
    
    [_tableView setBackgroundColor:[UIColor clearColor]];


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//PhotoDisplayViewController *viewController = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"PhotoDisplayViewController"];
//viewController.modalPresentationStyle = UIModalPresentationFullScreen;
//[self presentViewController:viewController animated:YES completion:NULL];

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UItableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    CGFloat height;
    NSDate *date1 = [_dateArray objectAtIndex:indexPath.row];
    NSDate *date2 = [_dateArray objectAtIndex:indexPath.row];
    if (indexPath.row > 1)
        date2 = [_dateArray objectAtIndex:indexPath.row -1];
    
     if (indexPath.row == 0) {
         height = 150.0f;
     }else if ([date1 compare:date2] == NSOrderedSame){
         
         height = 150.0f;
     } else{
         height = 120.0f;
     }
    
    return height;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDate *date1 = [_dateArray objectAtIndex:indexPath.row];
    NSDate *date2 = [_dateArray objectAtIndex:indexPath.row];
    
    if (indexPath.row > 1)
        date2 = [_dateArray objectAtIndex:indexPath.row -1];
    
    if (indexPath.row == 0)
    {
        CellToday *cellToday = (CellToday*)[tableView dequeueReusableCellWithIdentifier:@"CellToday" ];
        [cellToday setBackgroundColor:[UIColor clearColor]];
        cellToday.lineView.backgroundColor = CustomTextColor;
        return cellToday;
    }
    else if ([date1 compare:date2] == NSOrderedSame)
    {
        CellMiddle *cellMiddle = (CellMiddle*)[tableView dequeueReusableCellWithIdentifier:@"CellMiddle" ];
        
        [cellMiddle setBackgroundColor:[UIColor clearColor]];
        cellMiddle.lineView.backgroundColor = (UIColor*)[_colorArray objectAtIndex:indexPath.row];
        
        return cellMiddle;
    }
    else
    {
        CellDay *cellDay = (CellDay*)[tableView dequeueReusableCellWithIdentifier:@"CellDay"];
        
        [cellDay setBackgroundColor:[UIColor clearColor]];
        cellDay.lineViewBottom.backgroundColor = [UIColor grayColor];
        cellDay.lineViewTop.backgroundColor = (UIColor*)[_colorArray objectAtIndex:indexPath.row];
        
        return cellDay;
    }
    
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (IBAction)tableCellButtonTapped:(id)sender
{
    UIButton *button = (UIButton *)sender;
    
    switch (button.tag) {
        case eButtonTypeDealer:
        {
            NSLog(@"eButtonTypeDealer");
            
            DealerPopOverVC *viewController = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"DealerPopOverVC"];
            viewController.delegate = self;
            
            viewController.noOfStageArray = [[NSMutableArray alloc] initWithObjects:@"A",@"B",@"C",@"D",@"F",@"G",@"H",@"I", nil];
            viewController.checkMarkArray = [[NSMutableArray alloc] initWithObjects:[NSNumber numberWithBool:YES],[NSNumber numberWithBool:YES],[NSNumber numberWithBool:YES],[NSNumber numberWithBool:YES],[NSNumber numberWithBool:YES],[NSNumber numberWithBool:YES],[NSNumber numberWithBool:YES],[NSNumber numberWithBool:YES], nil];
            
            [viewController.view setFrame:CGRectMake(viewController.view.frame.origin.x, viewController.view.frame.origin.y, viewController.view.frame.size.width, (viewController.noOfStageArray.count*45))];
            //= CGSizeMake(viewController.view.frame.size.width, (viewController.noOfStageArray.count*45));
            //[viewController.checkMarkArray replaceObjectAtIndex:[self currentStage:sender withTitle:viewController.noOfStageArray] withObject:[NSNumber numberWithBool:NO]];
            
            _popVC = [[UIPopoverController alloc] initWithContentViewController:viewController];
            
            _popVC.popoverContentSize = CGSizeMake(274, 174.0);
            //_popVC.popoverContentSize = CGSizeMake(274, 600.0);
            
            UIButton *button = (UIButton *)sender;
            
            [_popVC presentPopoverFromRect:CGRectMake(button.frame.origin.x + 95, button.frame.origin.y, button.frame.size.width, button.frame.size.height)  inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    }
            break;
            
        case eButtonTypeChecklist:
        {
            NSLog(@"eButtonTypeChecklist");
            
            DealerPopOverVC *viewController = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"DealerPopOverVC"];
            viewController.delegate = self;
            
            viewController.noOfStageArray = [[NSMutableArray alloc] initWithObjects:@"Pre-launch",@"Implementation",@"Operation",@"Additional", @"Hand Over", nil];
            viewController.checkMarkArray = [[NSMutableArray alloc] initWithObjects:[NSNumber numberWithBool:YES],[NSNumber numberWithBool:YES],[NSNumber numberWithBool:YES],[NSNumber numberWithBool:YES],[NSNumber numberWithBool:YES], nil];
            
            //[viewController.checkMarkArray replaceObjectAtIndex:[self currentStage:sender withTitle:viewController.noOfStageArray] withObject:[NSNumber numberWithBool:NO]];
            
            _popVC = [[UIPopoverController alloc] initWithContentViewController:viewController];
            
            _popVC.popoverContentSize = CGSizeMake(274, 174.0);
            
            UIButton *button = (UIButton *)sender;
            
            [_popVC presentPopoverFromRect:CGRectMake(button.frame.origin.x + 95, button.frame.origin.y, button.frame.size.width, button.frame.size.height)  inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
        }
            break;
        case eButtonTypeEdit:
            NSLog(@"eButtonTypeEdit");
            break;
        case eButtonTypeImage:
        {
            NSLog(@"eButtonTypeImage");
        
//            PhotoDisplayViewController *viewController = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"PhotoDisplayViewController"];
//            viewController.modalPresentationStyle = UIModalPresentationFullScreen;
//            
//            [self presentViewController:viewController animated:NO completion:NULL];

//            DealerChecklistViewController *viewController = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"DealerChecklistViewController"];
//            MainViewController *parentVC = [APP_DELEGATE appBaseViewController];
//            
//            [parentVC addChildViewController:viewController];
//            [viewController didMoveToParentViewController:parentVC];
//            [parentVC.view addSubview:viewController.view];
            
            break;
        }
        default:
            break;
    }
}

-(void) updateButtonTitle:(NSString *) title
{
    
}

@end
