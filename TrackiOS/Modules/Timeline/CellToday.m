//
//  CellToday.m
//  CustomTable
//
//  Created by Anish Kumar on 29/09/14.
//  Copyright (c) 2014 xenon. All rights reserved.
//

#import "CellToday.h"

@implementation CellToday

- (void)awakeFromNib {
    // Initialization code
    
    [self.contentView setBackgroundColor:ViewBackgroundColor];
    
    //set font for uicontrol
    [_titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:23]];
    [_detailLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    
    [_codeLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    [_detailLabel setNumberOfLines:3];
    
    //set color
    _titleLabel.textColor = DarkGrayTextColor;
    _detailLabel.textColor = LightGrayTextColor;
    _codeLabel.textColor = CustomTextColor;
    
    [_checklistButton setTitleColor:CustomTextColor forState:UIControlStateNormal];
    [_checklistButton.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16.0f]];
    
    [_dealerButton setTitleColor:CustomTextColor forState:UIControlStateNormal];
    [_dealerButton.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16.0f]];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
