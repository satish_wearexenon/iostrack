//
//  CellMiddle.m
//  CustomTable
//
//  Created by Anish Kumar on 29/09/14.
//  Copyright (c) 2014 xenon. All rights reserved.
//

#import "CellMiddle.h"

@implementation CellMiddle

- (void)awakeFromNib {
    // Initialization code
    
    [self.contentView setBackgroundColor:ViewBackgroundColor];

    //set font for uicontrol
    [_titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:23]];
    [_resultLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    [_itemLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    [_photoLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    [_codeLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    [_nameLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    [_percentageLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    
    //set color
    _titleLabel.textColor = DarkGrayTextColor;
    _resultLabel.textColor = LightGrayTextColor;
    _itemLabel.textColor = LightGrayTextColor;
    _photoLabel.textColor = LightGrayTextColor;
    _codeLabel.textColor = CustomTextColor;
    _nameLabel.textColor = CustomTextColor;
    _percentageLabel.textColor = CustomTextColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) prepareForReuse {
    NSLog(@"prep for reuse");
    //_lineView.backgroundColor = [UIColor greenColor];
}

- (void)constructCellWithViewColor:(UIColor*)colorSelected
{
    _lineView.backgroundColor = colorSelected;
}

@end
