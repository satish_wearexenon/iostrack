//
//  TTimeline1DetailViewController.m
//  TrackiOS
//
//  Created by Anish Kumar on 17/10/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "TTimeline1DetailViewController.h"

@interface TTimeline1DetailViewController ()

@end

@implementation TTimeline1DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void) viewDidAppear:(BOOL)animated
{
    MainViewController *parentVC = [APP_DELEGATE appBaseViewController];
    //parentVC.navLabel.text = @"Timeline 11 Detail";
    
    parentVC.navLabel.hidden = YES;
    //parentVC.backView.hidden = NO;
    parentVC.backButton.hidden = NO;
    parentVC.currentDetailController = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
