//
//  CellToday.h
//  CustomTable
//
//  Created by Anish Kumar on 29/09/14.
//  Copyright (c) 2014 xenon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellToday : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *lineView;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;

@property (weak, nonatomic) IBOutlet UILabel *codeLabel;
@property (weak, nonatomic) IBOutlet UIButton *dealerButton;
@property (weak, nonatomic) IBOutlet UIButton *checklistButton;
@property (weak, nonatomic) IBOutlet UIButton *editButton;


//-(IBAction)selectDealerListSelector:(id)sender;
//-(IBAction)selectCheckListSelector:(id)sender;
//-(IBAction)editTimelisneSelector:(id)sender;
@end
