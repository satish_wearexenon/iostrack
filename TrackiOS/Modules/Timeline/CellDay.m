//
//  CellDay.m
//  CustomTable
//
//  Created by Anish Kumar on 29/09/14.
//  Copyright (c) 2014 xenon. All rights reserved.
//

#import "CellDay.h"

@implementation CellDay

- (void)awakeFromNib {
    // Initialization code
    
    [self.contentView setBackgroundColor:ViewBackgroundColor];
    
    [_nameLabel setFont:[UIFont fontWithName:Font_Track_Regular size:23]];
    [_detailLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    
    _nameLabel.textColor = DarkGrayTextColor;
    _detailLabel.textColor = LightGrayTextColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
