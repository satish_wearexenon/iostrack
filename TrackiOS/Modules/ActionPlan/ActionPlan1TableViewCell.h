//
//  ActionPlan1TableViewCell.h
//  TrackiOS
//
//  Created by John Paul Ranjith on 25/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActionPlan1TableViewCell : UITableViewCell
{
    
}

@property (weak, nonatomic) IBOutlet UILabel *dealerName;
@property (weak, nonatomic) IBOutlet UILabel *dealerNextName;
@property (weak, nonatomic) IBOutlet UILabel *dealerCode;
@property (weak, nonatomic) IBOutlet UILabel *dealerregion;

@property (weak, nonatomic) IBOutlet UILabel *openPlanCount;
@property (weak, nonatomic) IBOutlet UILabel *openPlanLbl;


@end
