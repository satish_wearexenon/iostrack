//
//  ActionPlan1ViewController.m
//  TrackiOS
//
//  Created by Anish Kumar on 15/10/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "ActionPlan1ViewController.h"
#import "DealerDetailViewController.h"
#import "ActionPlan1TableViewCell.h"

@interface ActionPlan1ViewController ()

@property (strong, nonatomic) NSMutableArray *listArray;

@end

@implementation ActionPlan1ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    MainViewController *parentVC = [APP_DELEGATE appBaseViewController];
//    parentVC.navLabel.text = @"ActionPlan 1";
    
    [self.tableViewObj setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [self hardCodedValues];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidAppear:(BOOL)animated
{
    MainViewController *parentVC = [APP_DELEGATE appBaseViewController];
    parentVC.navLabel.text = @"Action Plan All";
    
    //parentVC.backView.hidden = YES;
    parentVC.backButton.hidden = YES;
    parentVC.navLabel.hidden = NO;
    
    
    [_dealerNameBtn setTitle:LOCALIZED_FILTER_DEALER_NAME forState:UIControlStateNormal];
    [_dealerCodeBtn setTitle:LOCALIZED_FILTER_DEALER_CODE forState:UIControlStateNormal];
    
    [_dealerNameBtn.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    [_dealerCodeBtn.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    
    _dealerNameBtn.selected = YES;
    
    [super viewDidAppear:animated];
    
}

- (IBAction)filterButtonTapped:(id)sender
{
    if(_dealerNameBtn.isSelected){
        _dealerNameBtn.selected = NO;
        _dealerCodeBtn.selected = YES;
    }
    else if(_dealerCodeBtn.isSelected){
        _dealerCodeBtn.selected = NO;
        _dealerNameBtn.selected = YES;
    }
    
}

#pragma mark - UItableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dataArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ActionPlan1TableViewCell *actionPlanCell = (ActionPlan1TableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"ActionPlan1TableViewCell"];
    
    
    //[actionPlanCell setBackgroundColor:[UIColor whiteColor]];
    [actionPlanCell setBackgroundColor:[UIColor clearColor]];
    
    actionPlanCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    return actionPlanCell;
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)detailViewTapped:(id)sender
{
    //    TTimeline1DetailViewController *viewController = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"TTimeline1DetailViewController"];
    //
    //    [self addChildViewController:viewController];
    //    [viewController didMoveToParentViewController:self];
    //    [self.view addSubview:viewController.view];
    
    DealerDetailViewController *viewController = [[UIStoryboard mainStoryBoard]instantiateViewControllerWithIdentifier:@"DealerDetailViewController"];
    
    [self addChildViewController:viewController];
    [viewController didMoveToParentViewController:self];
    [self.view addSubview:viewController.view];
}

-(void)hardCodedValues
{
    dataArray = [[NSMutableArray alloc] init];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:@"Shanghai Wansheng" forKey:@"dealerName"];
    [dict setObject:@"Wang Yong" forKey:@"dealerNextName"];
    [dict setObject:@"CN1001" forKey:@"dealerCode"];
    [dict setObject:@"East" forKey:@"dealerRegion"];
    [dict setObject:@"12" forKey:@"openPlanCount"];
    [dict setObject:@"Open Plans" forKey:@"OpenPlanLbl"];
    
    [dataArray addObject:dict];
    [dataArray addObject:dict];
    [dataArray addObject:dict];
    [dataArray addObject:dict];
    [dataArray addObject:dict];
    [dataArray addObject:dict];
    [dataArray addObject:dict];
    [dataArray addObject:dict];
    
    
}

@end
