//
//  ActionPlan1TableViewCell.m
//  TrackiOS
//
//  Created by John Paul Ranjith on 25/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "ActionPlan1TableViewCell.h"

@implementation ActionPlan1TableViewCell

- (void)awakeFromNib {
    // Initialization code
    
    [_dealerName setFont:[UIFont fontWithName:Font_Track_Regular size:21]];
    [_dealerNextName setFont:[UIFont fontWithName:Font_Track_Regular size:18]];
    [_dealerCode setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    [_dealerregion setFont:[UIFont fontWithName:Font_Track_Regular size:16]];
    [_openPlanCount setFont:[UIFont fontWithName:Font_Track_Regular size:33]];
    [_openPlanLbl setFont:[UIFont fontWithName:Font_Track_Regular size:10]];
    
    
    _dealerName.textColor = [UIColor darkGrayColor];
    _dealerNextName.textColor = [UIColor lightGrayColor];
    _dealerCode.textColor = CustomTextColor;
    _dealerregion.textColor = CustomTextColor;
    _openPlanCount.textColor = [UIColor darkGrayColor];
    _openPlanLbl.textColor = [UIColor darkGrayColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
