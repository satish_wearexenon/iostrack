//
//  ActionPlan1ViewController.h
//  TrackiOS
//
//  Created by Anish Kumar on 15/10/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActionPlan1ViewController : UIViewController
{
    NSMutableArray *dataArray;
}

@property (weak, nonatomic) IBOutlet UILabel *sortByLabel;
@property (weak, nonatomic) IBOutlet UIButton *dealerNameBtn;
@property (weak, nonatomic) IBOutlet UIButton *dealerCodeBtn;
@property (weak, nonatomic) IBOutlet UITableView *tableViewObj;


- (IBAction)filterButtonTapped:(id)sender;


@end
