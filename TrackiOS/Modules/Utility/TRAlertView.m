//
//  TRAlertView.m
//  TrackiOS
//
//  Created by Anish Kumar on 14/10/14.
//  Copyright (c) 2014 Xenon Automotive. All rights reserved.

#import "TRAlertView.h"
#import <QuartzCore/QuartzCore.h>

#define kAlertViewPaddingLeft     19
#define kAlertViewPaddingRight    19
#define kAlertViewPaddingTop      24
#define kAlertViewPaddingBottom   15
#define kAlertViewWidth           444
#define kAlertViewMinHeight       120
#define kAlertViewMsgTitleSpacing 5
#define kAlertViewBtnsMsgSpacing  15

#define kAlertViewBtnHeight       44
#define kAlertViewBtnWidth        200
#define kAlertViewBtnsSpacing     0
#define kAlertViewBtnPaddingLeft  0


#define textFieldVertPadding        15
#define textFieldWidth              269
#define textFieldHeight             29
#define textFieldOriginY            90
#define textFieldOriginX            155

#define labelWidth                  140
#define labelOriginX                8
#define labelOriginY                96
#define labelVertPadding            25
#define labelFontSize               16

#define titleFontSize               24
#define changePasswordViewWidth     444.0
#define changePasswordViewHeight    275.0

#define emailButtonWidth            335.0

#define lineColor [UIColor colorWithRed:178.0/255.0 green:178.0/255.0  blue:178.0/255.0  alpha:1.0]
#define textFieldBorderColor [UIColor colorWithRed:161.0/255.0 green:161.0/255.0  blue:161.0/255.0  alpha:1.0]
#define labelColor [UIColor colorWithRed:130.0/255.0 green:138.0/255.0  blue:144.0/255.0  alpha:1.0]
#define xenonGreen [UIColor colorWithRed:0.0/255.0 green:198.0/255.0  blue:186.0/255.0  alpha:1.0]

@interface TRAlertView ()
{
    UIView   * containerView;
    UIButton * crossCloseBtn;
    
    NSString *oldPassword;
    NSString *newPassword;
    NSString *confirmPassword;
    
}

- (void)prepareToShow;
- (void)changeLayoutOfButton:(UIButton *)inButton;
- (UIButton *)buttonWithTitle:(NSString *)inBtnTitle;
@end

@implementation TRAlertView

@synthesize layoutType = layoutType;

@synthesize titleLabel = titleLabel;
@synthesize messageLabel = messageLabel;
@synthesize buttonArray = buttonArray;
@synthesize backgroundImageView = backgroundImageView;
@synthesize showRating = showRating;
@synthesize urlToOpen = urlToOpen;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithTitle:(NSString *)inTitle message:(NSString *)inMessage delegate:(id )inDelegate layout:(TrackAlertSection )inLayoutType containsEmail:(BOOL)hasEmail dateStr:(NSString*)dateString cancelButtonTitle:(NSString *)inCancelButtonTitle otherButtonTitles:(NSString *)inOtherButtonTitles, ...
{
    self = [super init];
    if (self) {
        
        self.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.3];        
        containerView     = [[UIView alloc] initWithFrame:CGRectZero];
        
        containerView.clipsToBounds = YES;
        containerView.layer.borderWidth = 1.0;
        containerView.layer.cornerRadius = 3.0f;
        
        _delegate = inDelegate;
        layoutType    = inLayoutType;
        showRating    = NO;
        
        switch (inLayoutType)
        {

            case eControllerypeChangePassword:
            {
                containerView.backgroundColor = [UIColor whiteColor];
                containerView.layer.borderColor  = [[UIColor clearColor] CGColor];
                
                oldPassword = newPassword = confirmPassword = nil;
            }
                break;
            case eAlertTypeWithOk:
            {
                containerView.backgroundColor = [UIColor whiteColor];
                containerView.layer.borderColor  = [[UIColor clearColor] CGColor];
                
                titleLabel      = [[UILabel alloc] initWithFrame:CGRectZero];
                titleLabel.text = inTitle;
                titleLabel.textColor = [UIColor blackColor];
                [titleLabel setFont:[UIFont fontWithName:Font_Track_Bold size:25]];
                titleLabel.backgroundColor = [UIColor clearColor];
                titleLabel.textAlignment   = NSTextAlignmentCenter;
                titleLabel.numberOfLines   = 0;
                
                if (dateString)
                {
                    _dateLabel      = [[UILabel alloc] initWithFrame:CGRectZero];
                    _dateLabel.text = dateString;
                    [_dateLabel setFont:[UIFont fontWithName:Font_Track_Regular size:21]];
                    _dateLabel.textColor = [UIColor colorWithRed:130.0/255.0 green:138.0/255.0  blue:144.0/255.0  alpha:1.0];
                    _dateLabel.backgroundColor = [UIColor clearColor];
                    _dateLabel.textAlignment   = NSTextAlignmentCenter;
                    _dateLabel.numberOfLines   = 1;
                }
                
                if (hasEmail)
                {
                    _emailButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
                    [_emailButton addTarget:self  action:@selector(emailTapped:) forControlEvents:UIControlEventTouchUpInside];
                    [_emailButton.titleLabel setTextAlignment: NSTextAlignmentCenter];
                    [_emailButton setTitleColor:[UIColor colorWithRed:0/255.0 green:190/255.0 blue:186/255.0 alpha:1.0] forState:UIControlStateNormal];
                    [_emailButton.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:19]];
                    [_emailButton setTitle:@"Report Bug by Email" forState:UIControlStateNormal];
                    _emailButton.layer.cornerRadius = 5.0f;
                    _emailButton.layer.masksToBounds = YES;
                    _emailButton.layer.borderColor = [xenonGreen CGColor];
                    _emailButton.layer.borderWidth = 1.0f;
                }
                
                messageLabel = [[UILabel alloc] initWithFrame:CGRectZero];
                messageLabel.text = inMessage;
                messageLabel.textColor = [UIColor colorWithRed:69.0/255.0 green:73.0/255.0  blue:78.0/255.0  alpha:1.0];
                [messageLabel setFont:[UIFont fontWithName:Font_Track_Regular size:19]];
                messageLabel.backgroundColor = [UIColor clearColor];
                messageLabel.textAlignment = NSTextAlignmentCenter;
                messageLabel.numberOfLines = 0;
                
                buttonArray = [[NSMutableArray alloc] init];
                
                if (inCancelButtonTitle)
                {
                    UIButton * cancelBtn = [self buttonWithTitle:inCancelButtonTitle];
                    [buttonArray addObject:cancelBtn];
                }
                
                va_list args;
                va_start(args, inOtherButtonTitles);
                for (NSString *arg = inOtherButtonTitles; arg != nil; arg = va_arg(args, NSString*))
                {
                    [buttonArray addObject:[self buttonWithTitle:arg]];
                }
                va_end(args);
                
                
            }
                break;
//            case eAlertTypeWithErrorEmail:
//            {
//                containerView.backgroundColor = [UIColor whiteColor];
//                containerView.layer.borderColor  = [[UIColor clearColor] CGColor];
//                
//                titleLabel      = [[UILabel alloc] initWithFrame:CGRectZero];
//                titleLabel.text = inTitle;
//                titleLabel.textColor = [UIColor blackColor];
//                [titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:25]];
//                titleLabel.backgroundColor = [UIColor clearColor];
//                titleLabel.textAlignment   = NSTextAlignmentCenter;
//                titleLabel.numberOfLines   = 0;
//                
//                
//                
//                messageLabel = [[UILabel alloc] initWithFrame:CGRectZero];
//                messageLabel.text = inMessage;
//                messageLabel.textColor = [UIColor colorWithRed:69.0/255.0 green:73.0/255.0  blue:78.0/255.0  alpha:1.0];
//                [messageLabel setFont:[UIFont fontWithName:@"Helvetica" size:19]];
//                messageLabel.backgroundColor = [UIColor clearColor];
//                messageLabel.textAlignment = NSTextAlignmentCenter;
//                messageLabel.numberOfLines = 0;
//                
//                buttonArray = [[NSMutableArray alloc] init];
//                
//                if (inCancelButtonTitle)
//                {
//                    UIButton * cancelBtn = [self buttonWithTitle:inCancelButtonTitle];
//                    [buttonArray addObject:cancelBtn];
//                }
//
//            }
                break;
            case eAlertTypeWithNetworkConnection:
            {
                containerView.backgroundColor = [UIColor redColor];
                containerView.layer.borderColor  = [[UIColor colorWithRed:240.0/255.0 green:165.0/255 blue:26.0/255 alpha:1] CGColor];
            }
                break;
//            case eAlertTypeWithErrorReport:
//            {
//                 containerView.backgroundColor = [UIColor redColor];
//                 containerView.layer.borderColor  = [[UIColor colorWithRed:240.0/255.0 green:165.0/255 blue:26.0/255 alpha:1] CGColor];
//            }
                break;
//            case eControllerTypeError:
//            case eControllerTypeLibrary:
            default:
            {
                containerView.backgroundColor = [UIColor yellowColor];
                containerView.layer.borderColor  = [[UIColor colorWithRed:240.0/255.0 green:165.0/255 blue:26.0/255 alpha:1] CGColor];
            }
                break;
        }
        
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
}

- (void)addButton:(UIButton *)inButton
{
    [inButton addTarget:self action:@selector(didTappedButton:) forControlEvents:UIControlEventTouchUpInside];
    [buttonArray addObject:inButton];
}

- (void)prepareToShow
{
     NSInteger btnsCounts = [buttonArray count];
    if (layoutType == eAlertTypeWithOk)
    {
        CGFloat width  = kAlertViewWidth - (kAlertViewPaddingLeft + kAlertViewPaddingRight);
        CGFloat height = kAlertViewMinHeight;
        CGRect frame = CGRectMake(kAlertViewPaddingLeft, kAlertViewPaddingTop, width, 0.0);
        
        CGSize titleSize = [titleLabel.text sizeWithFont:titleLabel.font
                                       constrainedToSize:CGSizeMake(width, 200)
                                           lineBreakMode:UILineBreakModeTailTruncation];
        frame.size.height = titleSize.height;
        titleLabel.frame = frame;
        [containerView addSubview:titleLabel];
        
        CGSize dateSize   = [_dateLabel.text sizeWithFont:_dateLabel.font
                                         constrainedToSize:CGSizeMake(width, 300)
                                             lineBreakMode:UILineBreakModeTailTruncation];
        
        
        // Update for message label frame
        frame.origin.y  += (dateSize.height + kAlertViewMsgTitleSpacing);
        frame.size.height = dateSize.height;
        _dateLabel.frame = frame;
        [containerView addSubview:_dateLabel];
        
        CGSize msgSize   = [messageLabel.text sizeWithFont:messageLabel.font
                                         constrainedToSize:CGSizeMake(width, 300)
                                             lineBreakMode:UILineBreakModeTailTruncation];
        
        
        // Update for message label frame
        if (_dateLabel)
            frame.origin.y  += (dateSize.height + kAlertViewMsgTitleSpacing);
        else
            frame.origin.y  += (titleSize.height + kAlertViewMsgTitleSpacing);
        
        frame.size.height = msgSize.height;
        messageLabel.frame = frame;
        [containerView addSubview:messageLabel];
        
        //Email Button
        if (_emailButton) {
            frame.origin.y  += (msgSize.height + kAlertViewPaddingTop - 4);
            frame.size.height = kAlertViewBtnHeight;
            frame.size.width = emailButtonWidth;
            frame.origin.x = kAlertViewWidth/2 - frame.size.width/2;
            _emailButton.frame = frame;
            _emailButton.backgroundColor = [UIColor clearColor];
            [containerView addSubview:_emailButton];
        }
        
        
        // Update for buttons
        frame.origin.x  = kAlertViewBtnPaddingLeft;
        frame.origin.y += (frame.size.height + kAlertViewBtnsMsgSpacing + 12);
        frame.size = CGSizeMake(kAlertViewBtnWidth, kAlertViewBtnHeight );
        
        height = frame.origin.y + frame.size.height;
        
        CGRect frameRelative = CGRectZero;
        
        for (int btnIndex = 0; btnIndex < btnsCounts; btnIndex++)
        {
            UIButton *btn = [buttonArray objectAtIndex:btnIndex];
            if ([btn respondsToSelector:@selector(setFrame:)])
            {
                if (btnsCounts == 1)
                {
                    frame.size.width = kAlertViewWidth - (2*kAlertViewBtnPaddingLeft);
                    //[btn.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:19]];
                }
                //height = frame.origin.y + frame.size.height;
                
                //btn.backgroundColor = [UIColor redColor];
                btn.frame = frame;
                [containerView addSubview:btn];
                
                if ((btnIndex%2) == 0)
                {
                    frame.origin.x  = kAlertViewBtnPaddingLeft + kAlertViewBtnWidth + kAlertViewBtnsSpacing;
                    
                }
                else
                {
                    [btn.titleLabel setFont:[UIFont fontWithName:Font_Track_Bold size:19]];
                    frame.origin.x  = kAlertViewBtnPaddingLeft;
                    //frame.origin.y += (kAlertViewBtnHeight + kAlertViewMsgTitleSpacing);
                    
                    if ((btnIndex+1) == (btnsCounts-1))
                    {
                        frame.size.width = kAlertViewWidth - (2*kAlertViewBtnPaddingLeft);
                    }
                }
            }
        }
        
        frame.size = CGSizeMake(kAlertViewWidth, (height ));//+ kAlertViewPaddingBottom));
        //frame.origin = CGPointMake(0, 0);
        containerView.frame = frame;
        backgroundImageView.frame = frame;
        [self addSubview:containerView];
        
        UIView *verticalLineView = [[UIView alloc]init];
        float yPosition = containerView.frame.size.height - 44.0;
        frameRelative.origin = CGPointMake(0, yPosition);
        frameRelative.size   = CGSizeMake(containerView.frame.size.width, 1.0);
        verticalLineView.frame = frameRelative;
        verticalLineView.backgroundColor = lineColor;
        [containerView addSubview:verticalLineView];
        
        if (btnsCounts == 2)
        {
            //Horizontal Line Separator
            float xPosition = containerView.frame.size.width/2;
            frameRelative.origin = CGPointMake(xPosition, yPosition);
            frameRelative.size   = CGSizeMake(1.0, 44.0);
            UIView *lineView = [[UIView alloc] initWithFrame:frameRelative];
            lineView.backgroundColor = lineColor;
            [containerView addSubview:lineView];
        }
        
    }
    else if (layoutType == eControllerypeChangePassword)
    {
        CGRect frame =  CGRectZero;
        frame.size = CGSizeMake(changePasswordViewWidth, changePasswordViewHeight);
        containerView.frame = frame;
        backgroundImageView.frame = frame;
        [self addSubview:containerView];
        
        //Change Password Label
        UILabel *changePasswordLabel = [[UILabel alloc]init];
        NSString *textStr = @"Change Password";
        changePasswordLabel.text = textStr;
        [changePasswordLabel setFont:[UIFont fontWithName:Font_Track_Bold size:titleFontSize]];
        CGSize expectedLabelSize = [changePasswordLabel.text sizeWithAttributes:
                                    @{NSFontAttributeName:
                                          [UIFont systemFontOfSize:titleFontSize]}];
        CGRect frameRelative = CGRectZero;
        float center = (containerView.frame.size.width / 2) - (expectedLabelSize.width / 2);
        frameRelative.origin = CGPointMake(center - 8, 25);
        frameRelative.size   = CGSizeMake(changePasswordViewWidth, expectedLabelSize.height);
        changePasswordLabel.frame = frameRelative;
        [containerView addSubview:changePasswordLabel];
        
        //Old Password Label
        textStr = @"Old Password";
        UILabel *oldPasswordLabel = [self createLabel:textStr withYosition:labelOriginY withFontSize:labelFontSize];
        [containerView addSubview:oldPasswordLabel];
        
        //Old Password Text Field
        float yPosition = textFieldOriginY;
        _oldPasswordTextField = [self createTextField:textStr withYPosition:textFieldOriginY];
        _oldPasswordTextField.delegate = self;
        [containerView addSubview:_oldPasswordTextField];
        
        //New Password Label
        textStr = @"New Password";
        yPosition = oldPasswordLabel.frame.origin.y + oldPasswordLabel.frame.size.height + labelVertPadding;
        UILabel *newPasswordLabel = [self createLabel:textStr withYosition:yPosition withFontSize:labelFontSize];
        [containerView addSubview:newPasswordLabel];
        
        //New Password Text Field
        yPosition = _oldPasswordTextField.frame.origin.y + _oldPasswordTextField.frame.size.height + textFieldVertPadding;
        _nwPasswordTextField = [self createTextField:textStr withYPosition:yPosition];
        _nwPasswordTextField.delegate = self;
        [containerView addSubview:_nwPasswordTextField];
        
        //Confirm Password Label
        textStr = @"Confirm Password";
        yPosition = newPasswordLabel.frame.origin.y + newPasswordLabel.frame.size.height + labelVertPadding;
        UILabel *confirmPasswordLabel = [self createLabel:textStr withYosition:yPosition withFontSize:labelFontSize];
        [containerView addSubview:confirmPasswordLabel];
        
        //Confirm Password Text Field
        yPosition = _nwPasswordTextField.frame.origin.y + _nwPasswordTextField.frame.size.height + textFieldVertPadding;
        _confirmPasswordTextField = [self createTextField:textStr withYPosition:yPosition];
        _confirmPasswordTextField.delegate = self;
        [containerView addSubview:_confirmPasswordTextField];
        
        //Vertical Line Separator
        UIView *verticalLineView = [[UIView alloc]init];
        yPosition = _confirmPasswordTextField.frame.origin.y + _confirmPasswordTextField.frame.size.height + textFieldVertPadding + 8;
        frameRelative.origin = CGPointMake(0, yPosition);
        frameRelative.size   = CGSizeMake(changePasswordViewWidth, 1.0);
        verticalLineView.frame = frameRelative;
        verticalLineView.backgroundColor = lineColor;
        [containerView addSubview:verticalLineView];
        
        //Horizontal Line Separator
        float xPosition = changePasswordViewWidth / 2;
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(xPosition, yPosition, 1.0, 45.0)];
        lineView.backgroundColor = lineColor;
        [containerView addSubview:lineView];
        
        //Cancel Button
        _cancelButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [_cancelButton addTarget:self  action:@selector(cancelTapped:) forControlEvents:UIControlEventTouchUpInside];
        frameRelative.origin = CGPointMake(0, yPosition);
        frameRelative.size   = CGSizeMake(changePasswordViewWidth/2, 44.0);
        [_cancelButton.titleLabel setTextAlignment: NSTextAlignmentCenter];
        [_cancelButton setTitleColor:CustomTextColor forState:UIControlStateNormal];
        [_cancelButton.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:19]];
        [_cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
        _cancelButton.frame = frameRelative;
        [containerView addSubview:_cancelButton];
        
        //Confirm Button
        _confirmButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [_confirmButton addTarget:self  action:@selector(confirmTapped:) forControlEvents:UIControlEventTouchUpInside];
        xPosition = _cancelButton.frame.origin.x + _cancelButton.frame.size.width;
        frameRelative.origin = CGPointMake(xPosition, yPosition);
        frameRelative.size   = CGSizeMake(changePasswordViewWidth/2, 44.0);
        [_confirmButton.titleLabel setTextAlignment: NSTextAlignmentCenter];
        [_confirmButton setTitle:@"Confirm" forState:UIControlStateNormal];
        [_confirmButton setTitleColor:ButtonDisabledTextColor forState:UIControlStateNormal];
        [_confirmButton.titleLabel setFont:[UIFont fontWithName:Font_Track_Bold size:19]];
        _confirmButton.frame = frameRelative;
        _confirmButton.enabled = NO;
        
        [containerView addSubview:_confirmButton];
    }
}

- (UITextField *)createTextField:(NSString *)placeHolderText withYPosition:(float)yPosition
{
    UIImageView *textFieldBorderImageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"TextFieldBorder"]];
    CGRect frameRelative = CGRectZero;
    frameRelative.origin = CGPointMake(textFieldOriginX , yPosition);
    frameRelative.size   = CGSizeMake(textFieldWidth, textFieldHeight);
    textFieldBorderImageView.frame = frameRelative;
    [containerView addSubview:textFieldBorderImageView];
    
    UITextField *textField = [[UITextField alloc]init];
    frameRelative.origin = CGPointMake(textFieldOriginX + 5 , yPosition);
    frameRelative.size   = CGSizeMake(textFieldWidth -5, textFieldHeight);
    textField.frame = frameRelative;
    textField.font = [UIFont systemFontOfSize:15];
    textField.placeholder = placeHolderText;
    textField.autocorrectionType = UITextAutocorrectionTypeNo;
    textField.keyboardType = UIKeyboardTypeDefault;
    textField.returnKeyType = UIReturnKeyDone;
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    textField.borderStyle = UITextBorderStyleNone;
    textField.secureTextEntry = YES;

    
    //Border Layer
//    textField.layer.cornerRadius = 0.0f;
//    textField.layer.masksToBounds = YES;
//    textField.layer.borderColor = [textFieldBorderColor CGColor];
//    textField.layer.borderWidth = 1.0f;
//    textField.delegate = self;

    return textField;
}

- (UILabel *)createLabel:(NSString *)labelText withYosition:(float)yPosition withFontSize:(float)fontSize
{
    UILabel *label = [[UILabel alloc]init];
    label.textAlignment = NSTextAlignmentRight;
    label.text = labelText;
    [label setFont:[UIFont fontWithName:Font_Track_Regular size:fontSize]];
    CGSize expectedLabelSize = [label.text sizeWithAttributes:
                                @{NSFontAttributeName:
                                      [UIFont systemFontOfSize:fontSize]}];
    CGRect frameRelative = CGRectZero;
    frameRelative.origin = CGPointMake(labelOriginX, yPosition);
    frameRelative.size   = CGSizeMake(labelWidth, expectedLabelSize.height);
    label.textColor = labelColor;
    label.frame = frameRelative;
    
    return label;
}

- (void)show
{
    [self prepareToShow];
    
    UIWindow * activeWindow = [APP_DELEGATE window];
    self.frame = [activeWindow bounds];
    self.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.3];
    
    [activeWindow addSubview:self];
    containerView.center = self.center;
    containerView.transform = CGAffineTransformMakeScale( 0.001, 0.001);
    
    NSTimeInterval duration = kTransitionDuration/2;
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^ {
                       containerView.transform = CGAffineTransformMakeScale( 1.1, 1.1);
                     }
                     completion:^(BOOL finished) {
                         [UIView animateWithDuration:duration/2
                                               delay:0
                                             options:UIViewAnimationOptionCurveEaseOut
                                          animations:^ {
                                              containerView.transform = CGAffineTransformMakeScale (0.9, 0.9);                                              
                                          }
                                          completion:^(BOOL finished) {
                                              [UIView animateWithDuration:duration/2
                                                                    delay:0
                                                                  options:UIViewAnimationOptionCurveEaseOut
                                                               animations:^ {
                                                                   containerView.transform = CGAffineTransformMakeScale (1.0, 1.0);
                                                               }
                                                               completion:nil ]; }]; }];
    
}

#pragma mark - Buttons Action Methods 

- (void)cancelTapped:(UIButton *)inTappedBtn
{
//    CGRect frame = CGRectZero;
//    frame.origin = CGPointMake(containerView.frame.origin.x + inTappedBtn.frame.origin.x, containerView.frame.origin.y +  inTappedBtn.frame.origin.y);
//    frame.size   = CGSizeMake(inTappedBtn.frame.size.width, inTappedBtn.frame.size.height);
//    
//    [UIView animateWithDuration:0.25
//                          delay:0
//                        options:UIViewAnimationOptionCurveEaseOut
//                     animations:^ {
//                         inTappedBtn.frame = inTappedBtn.bounds;
//                         containerView.frame = frame;
//                     }
//                     completion:^(BOOL isFinish){[self removeFromSuperview];}];
    
//    [UIView animateWithDuration:0.1 animations:^(void) {
//        self.transform = CGAffineTransformScale(self.transform, 0.1, 0.1);
//    }
//                     completion:^(BOOL isFinish){[self removeFromSuperview];}];
    
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionTransitionCurlUp
                     animations:^ {
                         self.alpha = 0;
                     }
                     completion:^(BOOL isFinish){
                         [self removeFromSuperview];
                         self.alpha = 1;
                     }];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *testString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    testString = [testString stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    BOOL confirm = NO;

    if (textField == _oldPasswordTextField)
    {
        if( testString.length && [newPassword isEqualToString:confirmPassword])
            confirm = YES;
    }
    else if (textField == _nwPasswordTextField)
    {
        if( oldPassword.length && [confirmPassword isEqualToString:testString])
            confirm = YES;
    }
    else if (textField == _confirmPasswordTextField)
    {
        if( oldPassword.length && [newPassword isEqualToString:testString])
            confirm = YES;
    }
    if (confirm)
    {
        _confirmButton.enabled = YES;
        [_confirmButton setTitleColor:CustomTextColor forState:UIControlStateNormal];
    }
    else
    {
        _confirmButton.enabled = NO;
        [_confirmButton setTitleColor:ButtonDisabledTextColor forState:UIControlStateNormal];
    }
    
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == _oldPasswordTextField) {
        oldPassword = textField.text;
    }
    else if (textField == _nwPasswordTextField) {
        newPassword = textField.text;
    }
    else if (textField == _confirmPasswordTextField) {
        confirmPassword = textField.text;
    }
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    if (textField == _oldPasswordTextField) {
        oldPassword = nil;
    }
    else if (textField == _nwPasswordTextField) {
        newPassword = nil;
    }
    else if (textField == _confirmPasswordTextField) {
        confirmPassword = nil;
    }
    _confirmButton.enabled = NO;
    [_confirmButton setTitleColor:ButtonDisabledTextColor forState:UIControlStateNormal];
    return YES;
}

- (void)confirmTapped:(UIButton *)inTappedBtn
{
//    if (_oldPasswordTextField.text.length > 4)
//    {
//        if (_nwPasswordTextField.text.length > 4)
//        {
//        if ([_nwPasswordTextField.text isEqual: _confirmPasswordTextField.text])
//        {
//            if ([_delegate respondsToSelector:@selector(passwordHasChanged)])
//            {
//                [_delegate passwordHasChanged];
//            }
//            
//            [UIView animateWithDuration:0.5
//                                  delay:0
//                                options:UIViewAnimationOptionCurveEaseOut
//                             animations:^ {
//                                 self.alpha = 0;
//                             }
//                             completion:^(BOOL isFinish){
//                                 [self removeFromSuperview];
//                                 self.alpha = 1;
//                             }];
//        }
//        
//    }
//    }
    
    [_delegate passwordHasChanged];
    
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^ {
                         self.alpha = 0;
                     }
                     completion:^(BOOL isFinish){
                         [self removeFromSuperview];
                         self.alpha = 1;
                     }];
    
    
}

- (void)didTappedButton:(UIButton *)inTappedBtn
{
    
    [UIView animateWithDuration:0.1
                          delay:0
                        options:UIViewAnimationOptionTransitionCurlUp
                     animations:^ {
                         self.alpha = 0;
                     }
                     completion:^(BOOL isFinish){
                         [self removeFromSuperview];
                         [self removeFromSuperview];
                         self.alpha = 1;
                     }];
    if ([_delegate respondsToSelector:@selector(customAlertView:clickedButtonAtIndex:)]) {
        [_delegate customAlertView:self clickedButtonAtIndex:[buttonArray indexOfObject:inTappedBtn]];
    }
    
    //[self removeFromSuperview];
    
//    [UIView animateWithDuration:0.5
//                          delay:0
//                        options:UIViewAnimationOptionCurveEaseOut
//                     animations:^ {
//                         self.alpha = 0;
//                     }
//                     completion:^(BOOL isFinish){
//                         [self removeFromSuperview];
//                         self.alpha = 1;
//                     }];
    
    
    
}

#pragma mark - Buttons Creation Methods

- (UIButton *)crossCloseButton
{
    UIButton *crossBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [crossBtn setImage:[UIImage imageNamed:@"cross_btn.png"] forState:UIControlStateNormal];
    [crossBtn sizeToFit];    
    [crossBtn addTarget:self action:@selector(didTappedCrossButton:) forControlEvents:UIControlEventTouchUpInside];
    return crossBtn;
}

- (UIButton *)buttonWithTitle:(NSString *)inBtnTitle
{
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitleColor:xenonGreen forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:19]];
    [btn.titleLabel setTextAlignment: NSTextAlignmentCenter];
    [btn setTitle:inBtnTitle forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(didTappedButton:) forControlEvents:UIControlEventTouchUpInside];
    
    return btn;
}

- (void)changeLayoutOfButton:(UIButton *)inButton
{
    
}

- (void) emailTapped:(UIButton *)inTappedBtn
{
    if ([_delegate respondsToSelector:@selector(sendEMail)])
    {
        [_delegate sendEMail];
    }
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^ {
                         self.alpha = 0;
                     }
                     completion:^(BOOL isFinish){
                         [self removeFromSuperview];
                         self.alpha = 1;
                     }];
    
}

//- (UIImage *)buttonBackgroundImageForLayout:(TrackAlertSection)inLayoutType
//{
//    //UIImage * bgImage = [UIImage imageNamed:@"alert_btn.png"];
//    switch (inLayoutType) {
//            
//        case eControllerTypeDealer:
//        {
//            bgImage = [UIImage imageNamed:@"tips_alert_btn.png"];
//        }
//            break;
//        case eControllerLayoutTypeHome:
//        case eControllerLayoutTypeJournal:
//        default:
//        {
//            
//        }
//            break;
//    }
//    return bgImage;
//}

//- (UILabel *)ratingInfoLabel
//{
//    CGRect frame = CGRectMake(0.0, 0.0, kAlertViewWidth, 40.0);
//    UILabel * infoLabel = [[UILabel alloc] initWithFrame:frame];
//    
//    infoLabel.font = [[FTDUtilities sharedUtility] fontForType:eCustomFontTypeAlertMessage];
//    infoLabel.textColor = [UIColor whiteColor];
//    infoLabel.backgroundColor = [UIColor clearColor];
//    infoLabel.textAlignment   = UITextAlignmentCenter;
//    infoLabel.numberOfLines   = 0;
//    infoLabel.text = @"1 - Poor  2 - Fair  3 - Neutral \n 4 - Good  5 - Great";
//    
//    return infoLabel;
//}

@end







/*
 
 -(void)showAlertView{
 
 
 self.transform = CGAffineTransformMakeScale( 0.001, 0.001);
 mContainerView.frame = [self bounds];
 [UIView beginAnimations:nil context:nil];
 [UIView setAnimationDuration:kTransitionDuration/1.5];
 [UIView setAnimationDelegate:self];
 [UIView setAnimationDidStopSelector:@selector(bounce1AnimationStopped)];
 self.transform = CGAffineTransformMakeScale( 1.1, 1.1);
 mContainerView.frame = [self bounds];
 [UIView commitAnimations];
 }
 
 - (void)bounce1AnimationStopped
 {
 [UIView beginAnimations:nil context:nil];
 [UIView setAnimationDuration:kTransitionDuration/2];
 [UIView setAnimationDelegate:self];
 [UIView setAnimationDidStopSelector:@selector(bounce2AnimationStopped)];
 self.transform = CGAffineTransformMakeScale (0.9, 0.9);
 mContainerView.frame = [self bounds];
 [UIView commitAnimations];
 }
 
 
 
 - (void)bounce2AnimationStopped
 {
 [UIView beginAnimations:nil context:nil];
 [UIView setAnimationDuration:kTransitionDuration/2];
 [UIView setAnimationDelegate:self];
 
 self.transform =  CGAffineTransformIdentity;
 mContainerView.frame = [self bounds];
 [UIView commitAnimations];
 }
 
 */

