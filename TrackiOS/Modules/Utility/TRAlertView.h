//
//  TRAlertView.h
//  TrackiOS
//
//  Created by Anish Kumar 14/10/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

@protocol TRAlertViewDelegate;

@interface TRAlertView : UIView <UITextFieldDelegate>

@property (nonatomic, strong) NSURL * urlToOpen;

@property (nonatomic) BOOL showRating;
@property (nonatomic) TrackAlertSection layoutType;
@property (nonatomic ) id <TRAlertViewDelegate> delegate;

@property (nonatomic, strong) UIImageView * backgroundImageView;

@property (nonatomic, strong) NSMutableArray * buttonArray;

@property (nonatomic, strong) UITextField *passwordField;
@property (nonatomic, strong) UIButton *cancelButton;
@property (nonatomic, strong) UIButton *confirmButton;
@property (nonatomic, strong) UIButton *emailButton;

@property (nonatomic, strong) UILabel *headerLabel;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *messageLabel;
@property (nonatomic, strong) UILabel *dateLabel;

@property (nonatomic, strong) UITextField *oldPasswordTextField;
@property (nonatomic, strong) UITextField *nwPasswordTextField;
@property (nonatomic, strong) UITextField *confirmPasswordTextField;

- (id)initWithTitle:(NSString *)inTitle message:(NSString *)inMessage delegate:(id )inDelegate layout:(TrackAlertSection )inLayoutType containsEmail:(BOOL)hasEmail dateStr:(NSString*)dateString cancelButtonTitle:(NSString *)inCancelButtonTitle  otherButtonTitles:(NSString *)inOtherButtonTitles, ... NS_REQUIRES_NIL_TERMINATION;

- (void)show;
- (void)addButton:(UIButton *)inButton;
@end


@protocol TRAlertViewDelegate <NSObject>
@optional
- (void)customAlertView:(TRAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;
- (void) passwordHasChanged;
- (void) sendEMail;
@end