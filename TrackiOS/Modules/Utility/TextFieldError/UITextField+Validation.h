//
//  UITextField+Validation.h
//  TrackiOS
//
//  Created by Anish Kumar on 26/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UITextField (UITextField_Validation)

- (BOOL)requiredText;
- (BOOL)validZipCode;
- (BOOL)validPhoneNumber;
- (BOOL)validEmailAddress;
- (BOOL)validUsername;
- (BOOL)validPassword;
- (BOOL)requiredPhoneText;

@end
