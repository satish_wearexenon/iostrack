//
//  TrackErrorMessage.h
//  TrackiOS
//
//  Created by Anish Kumar on 26/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TrackErrorMessageView : UIView

@property(nonatomic, weak) IBOutlet UILabel *mErrMsgLabel;

+ (void)showErrorMessageViewInTextField:(UITextField *)inTextField withErrorMessage:(NSString *)inErrMsg;
+ (void)hidePasswordStrengthViewInTextField:(UITextField *)inTextField;

@end
