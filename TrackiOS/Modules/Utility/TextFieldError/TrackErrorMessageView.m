//
//  TrackErrorMessage.m
//  TrackiOS
//
//  Created by Anish Kumar on 26/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "TrackErrorMessageView.h"
#import <QuartzCore/QuartzCore.h>

@implementation TrackErrorMessageView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

+ (void)showErrorMessageViewInTextField:(UITextField *)inTextField withErrorMessage:(NSString *)inErrMsg
{
    UIView *view = [inTextField.superview viewWithTag:(kErrorMessageViewTag + inTextField.tag)];
    TrackErrorMessageView * errorMessageView = nil;
    
    if ([view isKindOfClass:[TrackErrorMessageView class]]) {
        errorMessageView = (TrackErrorMessageView *)view;
    }
    
    if (errorMessageView == nil) {
        
        errorMessageView = (TrackErrorMessageView *)[[[NSBundle mainBundle] loadNibNamed:@"VSErrorMessageView" owner:self options:nil] objectAtIndex:0];
        
        errorMessageView.tag = (kErrorMessageViewTag + inTextField.tag);
        [inTextField.superview addSubview:errorMessageView];
    }
    
    CGSize textSize = [inErrMsg sizeWithFont:[UIFont fontWithName:Font_Track_Regular size:18]];
    CGRect frame = CGRectMake(0, 0, 160, 35);;
    frame.size.width = MAX(50, (textSize.width + 25));
    frame.origin.y = inTextField.frame.origin.y - 28;
    frame.origin.x = (inTextField.frame.size.width - frame.size.width) + inTextField.frame.origin.x + 5;
    errorMessageView.frame = frame;
    
    errorMessageView.mErrMsgLabel.text = inErrMsg;
}

+ (void)hidePasswordStrengthViewInTextField:(UITextField *)inTextField
{
    UIView *view = [inTextField.superview viewWithTag:(kErrorMessageViewTag + inTextField.tag)];
    if ([view isKindOfClass:[TrackErrorMessageView class]]) {
        [view removeFromSuperview];
    }
}

- (void)awakeFromNib
{
    _mErrMsgLabel.font = [UIFont fontWithName:Font_Track_Regular size:18];
    _mErrMsgLabel.textColor = ErrorTextColor;
}

@end
