//
//  TrackThemes.h
//  TrackiOS
//
//  Created by satish sharma on 10/20/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

//#ifndef TrackiOS_TrackThemes_h
//#define TrackiOS_TrackThemes_h
//
//
//#endif

//#define Font_Track_Regular                @"Effra-Regular"
#define Font_Track_Bold                   @"Exo-Regular"

#define Font_Track_Regular                @"Exo-Regular"
//#define Font_Track_Bold                   @"Exo-Bold"


#define ViewBackgroundColor               [UIColor colorWithRed:0.92 green:0.92 blue:0.92 alpha:1]
#define LightGrayBackgroundColor          [UIColor colorWithRed:0.97 green:0.97 blue:0.97 alpha:1]
#define TitleGrayColor                    [UIColor colorWithRed:0.27 green:0.28 blue:0.30 alpha:1]
#define CustomTextColor                   [UIColor colorWithRed:0.0  green:0.74 blue:0.72 alpha:1]
#define ErrorTextColor                    [UIColor colorWithRed:255.0/255  green:102.0/255.0 blue:102.0/255.0 alpha:1]

#define ButtonDisabledTextColor           [UIColor colorWithRed:0.50 green:0.54 blue:0.56 alpha:1]
#define ButtonBgSelectedColor             [UIColor colorWithRed:0.29 green:0.29 blue:0.29 alpha:1]
#define ButtonBgUnselectedColor           [UIColor colorWithRed:0.62 green:0.62 blue:0.62 alpha:1]

//dealer
#define LightGrayTextColor                [UIColor colorWithRed:0.53 green:0.53 blue:0.53 alpha:1]
#define DarkGrayTextColor                 [UIColor colorWithRed:0.14 green:0.14 blue:0.14 alpha:1]
#define OrangeTextColor                   [UIColor colorWithRed:1 green:0.40 blue:0.40 alpha:1]

//action plan text
//#define HeaderTitleColor                  [UIColor colorWithRed:0.97 green:0.97 blue:0.97 alpha:1]
//#define BodyTextColor                     [UIColor colorWithRed:0.80 green:0.80 blue:0.80 alpha:1]

#define WhiteBGColor                        [UIColor whiteColor]

#define CheckListGreenBtnColor              [UIColor colorWithRed:137.0/255.0 green:196.0/255.0 blue:85.0/255.0 alpha:1.0];
#define CheckListRedBtnColor                [UIColor colorWithRed:240.0/255.0 green:76.0/255.0 blue:83.0/255.0 alpha:1.0];
#define CheckListYellowBtnColor             [UIColor colorWithRed:248.0/255.0 green:190.0/255.0 blue:47.0/255.0 alpha:1.0];
#define CheckListGrayBtnColor               [UIColor colorWithRed:181.0/255.0 green:181.0/255.0 blue:181.0/255.0 alpha:1.0];



#define CheckListTblMainSectionColor        [UIColor colorWithRed:83.0/255.0 green:83.0/255.0 blue:83.0/255.0 alpha:1.0];
#define CheckListTblSubSectionColor         [UIColor colorWithRed:144.0/255.0 green:144.0/255.0 blue:144.0/255.0 alpha:1.0];


#define CheckListTextViewBorderColor        [[UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:0.5] CGColor];
#define BlackColor                          [UIColor blackColor]

#define CheckListIconColor                  [UIColor colorWithRed:0.0  green:0.74 blue:0.72 alpha:1]

