//
//  GraphView.h
//  CorePlotBarChartExample
//
//  Created by Anthony Perozzo on 8/06/12.
//  Copyright (c) 2012 Gilthonwe Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"

@protocol GraphViewViewDelegate;

@interface GraphView : CPTGraphHostingView <CPTPlotDataSource, CPTPlotSpaceDelegate>

@property(nonatomic,strong) CPTXYGraph *graph;

@property(nonatomic,strong) NSDictionary *data;
@property(nonatomic,strong) NSDictionary *sets;
@property(nonatomic,strong) NSArray *dataSetNameArray;
@property (nonatomic) id <GraphViewViewDelegate> delegate;

- (void)createGraph;

@end

@protocol GraphViewViewDelegate <NSObject>
@optional
- (void) imageCreated:(UIImage *)image;
@end