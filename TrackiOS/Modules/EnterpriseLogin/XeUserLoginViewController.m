//
//  XeUserLoginViewController.m
//  TrackiOS
//
//  Created by xenon on 08/10/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "XeUserLoginViewController.h"
#import "TRLoginLocalizer.h"
#import "User.h"

#import "UserRegion.h"
#import "Enterprise.h"


@interface XeUserLoginViewController () <UITextFieldDelegate>

@end

@implementation XeUserLoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
     [self addUIcontrolforLogin];
    
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    
    CGFloat leftInset = 10.0f;
    UIView *leftView1 = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, leftInset, _userNameTextfield.bounds.size.height)];
     _userNameTextfield.leftView=leftView1;
    UIView *leftView2 = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, leftInset, _userNameTextfield.bounds.size.height)];
    _passwordTextfield.leftView=leftView2;

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) addUIcontrolforLogin
{
    NSLog(@"addUIcontrolforLogin called");
    
   // [self.view setBackgroundColor:[UIColor colorWithRed:0.92 green:0.92 blue:0.92 alpha:1]];
    [self.view setBackgroundColor:ViewBackgroundColor];

    [_logoImages setFrame:CGRectMake(319, 209, 128, 128)];
    
    [_loginWelcomeLabel setFrame:CGRectMake(0, 300, self.view.frame.size.width, 25)];
    [_loginWelcomeLabel setFont:[UIFont fontWithName:Font_Track_Regular size:25.0]];
    [_loginWelcomeLabel setText:LOCALIZED_LOGIN_WELCOME_TITLE];
    [_loginWelcomeLabel setTextAlignment:NSTextAlignmentCenter];
    [_loginWelcomeLabel setTextColor:TitleGrayColor];
    
    [_pleaseLoginLabel setFrame:CGRectMake(0, 325, self.view.frame.size.width, 25)];
    [_pleaseLoginLabel setFont:[UIFont fontWithName:Font_Track_Regular size:25.0]];
    [_pleaseLoginLabel setText:LOCALIZED_PLEASE_LOGIN_TITLE];
    [_pleaseLoginLabel setTextAlignment:NSTextAlignmentCenter];
    [_pleaseLoginLabel setTextColor:TitleGrayColor];
    
    [_userNameTextfield setFrame:CGRectMake(318/2, 862/2, 450, 100/2)];
    [_userNameTextfield setDelegate:self];
    [_userNameTextfield setFont:[UIFont fontWithName:Font_Track_Regular size:19.0]];
    [_userNameTextfield setPlaceholder:LOCALIZED_USER_NAME_PLACE_HOLDER];
    
   
    _userNameTextfield.leftViewMode = UITextFieldViewModeAlways;
    //_userNameTextfield.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    
    [_passwordTextfield setFrame:CGRectMake(318/2, 862/2 -1, 450, 100/2)];
    [_passwordTextfield setDelegate:self];
    [_passwordTextfield setFont:[UIFont fontWithName:Font_Track_Regular size:19.0]];
    [_passwordTextfield setPlaceholder:LOCALIZED_PASSWORD_PLACE_HOLDER];
    _passwordTextfield.leftViewMode = UITextFieldViewModeAlways;
    
    [_loginButton setFrame:CGRectMake(318/2, 560, 963/2, 80/2)];
    [_loginButton.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:25.0]];
    
    //[_loginButton setTitleColor:[UIColor colorWithRed:0.50 green:0.54 blue:0.56 alpha:1] forState:UIControlStateDisabled];
    [_loginButton setTitleColor:ButtonDisabledTextColor forState:UIControlStateDisabled];

    [_loginButton setBackgroundImage:[UIImage imageNamed:@"01_login_button_disable.png"] forState:UIControlStateDisabled];
    [_loginButton setTitle:LOCALIZED_LOGIN_BUTTON_TITLE forState:UIControlStateDisabled];

    [_loginButton setTitleColor:CustomTextColor forState:UIControlStateNormal];
    [_loginButton setBackgroundImage:[UIImage imageNamed:@"01_login_button.png"] forState:UIControlStateNormal];
    [_loginButton setTitle:LOCALIZED_LOGIN_BUTTON_TITLE forState:UIControlStateNormal];

    [_loginButton setBackgroundImage:[UIImage imageNamed:@"01_login_button_press.png"] forState:UIControlStateSelected];
    
     [self performSelector:@selector(validateUsernameAndPassword) withObject:nil afterDelay:0];
    
}

-(void) validateUsernameAndPassword
{
    _userNameTextfield.text = @"frodo@nineleaps.com";
    _passwordTextfield.text = @"frodo0f0";
    
    NSLog(@"validateUsernameAndPassword");
    
    if ([_userNameTextfield.text isEqualToString:@""] || [_passwordTextfield.text isEqualToString:@""]) {
        
        [_loginButton setEnabled:NO];
    }else
    {
        [_loginButton setEnabled:YES];
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)loginToTrack:(id)sender
{
    [QSActivityIndicator start];
    //Login API call
    
    [NetworkingFactory  loginServiceCall:_userNameTextfield.text withPassword:_passwordTextfield.text withSuccessBlock:^(BOOL success) {
        
        if (success) {
            NSLog(@"ALL COOL login happened");
           
            [NetworkingFactory configServiceCall:^(BOOL success) {
                
                 if (success) {
                     NSLog(@"ALL COOL GOT Config file");
                     
                     [NetworkingFactory dealerListingServiceCall:^(BOOL success) {
                         
                         if (success) {
                             NSLog(@"ALL COOL GOT DEALER LIST");
                        
                             [APP_DELEGATE addControllers];
                             
                             [NetworkingFactory staffRoleAPICall:^(BOOL success) {
                                  if (success) {
                                      NSLog(@"ALL COOL GOT Staff Roles");
                                      
                                      [NetworkingFactory actionPlanGETAPICall:[NSNumber numberWithInt:
                                                                               1] withSuccessCallBack:^(BOOL success){
                                          
                                          
                                          if (success) {
                                              NSLog(@"GOT action plan listing");
                                              
                                              
                                          }
                                          [QSActivityIndicator end];
                                      }];
                                  }
                             }];
                             
                         }
                     }];
                 }
            }];
        }
    }];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    // return NO to disallow editing.
    
    NSLog(@"textFieldDidBeginEditing");
    if (textField == _passwordTextfield)
    {
        _passwordTextfield.text = @"";
        [_loginButton setEnabled:NO];
    }
    
    if (textField == _userNameTextfield)
    {
        if ([_userNameTextfield.text isEqualToString:@""] || [_passwordTextfield.text isEqualToString:@""])
        {
        [_loginButton setEnabled:NO];
        }else{
            [_loginButton setEnabled:YES];
        }
    }
    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    
    NSLog(@"textFieldShouldEndEditing");
    
    if ([_userNameTextfield.text isEqualToString:@""]) {
        
        [_loginButton setEnabled:NO];
    }else
    {
        [_loginButton setEnabled:YES];
    }
    
    if ([_passwordTextfield.text isEqualToString:@""]) {
        
        [_loginButton setEnabled:NO];
    }else
    {
        [_loginButton setEnabled:YES];
    }

    
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    
    if (textField == _userNameTextfield) {
        
        _userNameTextfield.text =@"";
    }
    
    if (textField == _userNameTextfield) {
        _passwordTextfield.text =@"";
    }
    
    [_loginButton setEnabled:NO];
    
    return YES;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // return NO to not change text
    
    NSLog(@"textField shouldChangeCharactersInRange:");
    
//    NSCharacterSet *letterCharacterSet = [NSCharacterSet letterCharacterSet];
//    NSCharacterSet *decimalDigitCharacterSet = [NSCharacterSet decimalDigitCharacterSet];
//    NSCharacterSet *customPunctuationCharacterSet = [NSCharacterSet characterSetWithCharactersInString:@"_-."];
//    
//    NSMutableCharacterSet *validationCharacterSet = [letterCharacterSet mutableCopy];
//    [validationCharacterSet formUnionWithCharacterSet:decimalDigitCharacterSet];
//    [validationCharacterSet formUnionWithCharacterSet:customPunctuationCharacterSet];
    
    
    
     if (textField == _userNameTextfield) {
        
         NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
         
         NSString *expression = @"^([0-9A-Za-z]+)?$";
         
         NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression
                                                                                options:NSRegularExpressionCaseInsensitive
                                                                                  error:nil];
         NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString
                                                             options:0
                                                               range:NSMakeRange(0, [newString length])];
//         if (numberOfMatches == 0)
//             return NO;
        
        if ([_passwordTextfield.text isEqualToString:@""]) {
            
            [_loginButton setEnabled:NO];
            
        }else
        {
            [_loginButton setEnabled:YES];
            
            if ([string isEqualToString:@""] && (textField.text.length == range.length)) {
                [_loginButton setEnabled:NO];
            }
            
        }
    }
    
    if (textField == _passwordTextfield) {
        
        if ([_userNameTextfield.text isEqualToString:@""]) {
            
            [_loginButton setEnabled:NO];
            
        }else
        {
            
            [_loginButton setEnabled:YES];
            
            if ([string isEqualToString:@""] && (textField.text.length == range.length)) {
                
                [_loginButton setEnabled:NO];
            }
            
        }
    }
    
    return YES;
}


@end
