//
//  XeEnterprisLoginViewController.m
//  TrackiOS
//
//  Created by xenon on 08/10/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "XeEnterprisLoginViewController.h"
#import "TRLoginLocalizer.h"
#import "Enterprise.h"
#import "NetworkingFactory.h"

@interface XeEnterprisLoginViewController () <UITextFieldDelegate>
@property (strong, nonatomic)  XeDownloadConfigureFileViewController *configueFileController;
@end

@implementation XeEnterprisLoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
      return self;
}

-(void)addUIcontrolProperties
{
    NSLog(@"addUIcontrolProperties");
    
   // [self.view setBackgroundColor:[UIColor colorWithRed:0.92 green:0.92 blue:0.92 alpha:1]];
    [self.view setBackgroundColor:ViewBackgroundColor];
 
    [_logoImage setFrame:CGRectMake(319, 209, 128, 128)];
    
    [_welcomeLabel setFrame:CGRectMake(0, 345, self.view.frame.size.width, 25)];
    [_welcomeLabel setText:LOCALIZED_ENTERPRISE_LOGIN_SCREEN_WELCOME_TITLE];
    [_welcomeLabel setTextAlignment:NSTextAlignmentCenter];
    [_welcomeLabel setTextColor:TitleGrayColor];
    [_welcomeLabel setFont:[UIFont fontWithName:Font_Track_Regular size:25.0]];

    [_enterCodeLabel setFrame:CGRectMake(0, 372, self.view.frame.size.width, 25)];
    [_enterCodeLabel setFont:[UIFont fontWithName:Font_Track_Regular size:25.0]];
    [_enterCodeLabel setText:LOCALIZED_ENTERPRISE_LOGIN_SCREEN_PLEASE_ENTER_TITLE];
    [_enterCodeLabel setTextAlignment:NSTextAlignmentCenter];
    [_enterCodeLabel setTextColor:TitleGrayColor];
    
    [_enterCodeTextField setFrame:CGRectMake(318/2, 864/2, 900/2, 100/2)];
    [_enterCodeTextField setDelegate:self];
    [_enterCodeTextField setFont:[UIFont fontWithName:Font_Track_Regular size:19.0]];
    [_enterCodeTextField setPlaceholder:LOCALIZED_ENTERPRISE_PLACE_HOLDER];
    CGFloat leftInset = 10.0f;
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, leftInset, _enterCodeTextField.bounds.size.height)];
    _enterCodeTextField.leftView=leftView;
    _enterCodeTextField.leftViewMode = UITextFieldViewModeAlways;
    
    [_continueButton setFrame:CGRectMake(318/2, 1024/2, 900/2, 80/2)];
    [_continueButton.titleLabel setFont:[UIFont fontWithName:Font_Track_Regular size:25.0]];
    [_continueButton setTitleColor:ButtonDisabledTextColor forState:UIControlStateDisabled];
    [_continueButton setTitle:LOCALIZED_CONTINUE_BUTTON_TITLE forState:UIControlStateDisabled];
    [_continueButton setBackgroundImage:[UIImage imageNamed:@"01_login_button_disable.png"] forState:UIControlStateDisabled];
    [_continueButton setTitleColor:CustomTextColor forState:UIControlStateNormal];
    [_continueButton setTitle:LOCALIZED_CONTINUE_BUTTON_TITLE forState:UIControlStateNormal];
    [_continueButton setBackgroundImage:[UIImage imageNamed:@"01_login_button.png"] forState:UIControlStateNormal];
    [_continueButton setBackgroundImage:[UIImage imageNamed:@"01_login_button_press.png"] forState:UIControlStateSelected];
    
    //_enterCodeTextField.text = @"enterprise_secret_key1";
    _enterCodeTextField.text = @"OEM2";

    [self performSelector:@selector(validateEnterpriseCode) withObject:nil afterDelay:0];

    
}

//- (CGRect)textRectForBounds:(CGRect)bounds
//{
//    NSLog(@"textRectForBounds");
//    return CGRectInset( bounds , 30 , 10 );
//}
//- (CGRect)editingRectForBounds:(CGRect)bounds{
//    NSLog(@"editingRectForBounds");
//    return CGRectInset( bounds , 30 , 10 );
//}

-(void)validateEnterpriseCode
{
    NSLog(@"validateEnterpriseCode");
    
    if ([_enterCodeTextField.text isEqualToString:@""]) {
        [_continueButton setEnabled:NO];
    }else{
      
        
        [_continueButton setEnabled:YES];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
//    __block int anInteger = 42;
//    
//    __block XeEnterprisLoginViewController *me = self;
//    
//    void (^testBlock)(void) = ^{
//        NSLog(@"Integer is: %i", anInteger);
//        bool isKind  = [me isKindOfClass:[XeEnterprisLoginViewController class]];
//        bool isMember = [me isMemberOfClass:[UIViewController class]];
//        bool isKind2  = [me isKindOfClass:[UIViewController class]];
//    };
//    
//    anInteger = 84;
//    
//    testBlock();
//    
    [self addUIcontrolProperties];

//    
//    TRAlertView *av = [[TRAlertView alloc] initWithTitle:@"Alert"
//                                                 message:@"erroe, network connectionm,gdl[pgld[lgdrlgolrpolypololtllfhlph,tfght"
//                                                delegate:self
//                                                  layout:eAlertTypeWithNetworkConnection
//                                       cancelButtonTitle:@"Report Bug by Email"
//                                       otherButtonTitles:@"Cancel",@"Retry", nil];
//    [av show];

    
 //   [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone] ;
    
   // [UIApplication sharedApplication].statusBarHidden = YES;
}


-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
//    [NetworkingFactory enterpriseConfigServiceCall:@"enterprise_secret_key1" withSuccess:^(BOOL success) {
//        
//        if (success) {
//            NSLog(@"ALL COOL GOT enterprise code");
//            NSLog(@"Enterprise ********* %@",[Enterprise findFirst].logo);
//            
//            
//            
//            if ([segue.identifier isEqualToString:@"XeDownloadConfigureFileViewController"]) {
//                _configueFileController = [segue destinationViewController];
//                
//                [self downloadConfigurationFile:sender];
//            }
//            // Get the new view controller using [segue destinationViewController].
//            // Pass the selected object to the new view controller.
//            NSLog(@"XeEnterprisLoginViewController: Enterprise code entered :prepareForSegue called");
//            
//            UIViewController *detailViewController = segue.destinationViewController;
//            
//            detailViewController.transitioningDelegate = self;
//            detailViewController.modalPresentationStyle = UIModalPresentationCustom;
//
//        }
//    }];

    
    }

- (IBAction)downloadConfigurationFile:(id)sender {
    
    NSLog(@"XeEnterprisLoginViewController: Enterprise code entered");
    
    //enterprise_secret_key1
    

    [NetworkingFactory getBaseURLforOEMcall:_enterCodeTextField.text withSuccess:^(BOOL success) {
        
        if (success) {
            NSLog(@"ALL COOL GOT enterprise code");
            
           
            NSString *key = [[NSUserDefaults standardUserDefaults] objectForKey:@"EnterprisesecretKey"];
            [NetworkingFactory enterpriseConfigServiceCall:key withSuccess:^(BOOL success) {
                
                if (success) {
                    NSLog(@"ALL COOL GOT enterprise code");
                    
                    
                    _configueFileController = [[UIStoryboard enterpriseStoryBoard] instantiateViewControllerWithIdentifier:@"XeDownloadConfigureFileViewController"];
                    
                    [self.navigationController pushViewController:_configueFileController animated:YES];
                    
                }
            }];

        }
    }];

    
   // XeDownloadConfigureFileViewController *configueFileController = [[UIStoryboard enterpriseStoryBoard] instantiateInitialViewController];
   // UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:configueFileController];
   // [navController.navigationBar setHidden:YES];
    //XeDownloadConfigureFileViewController *configureFileViewController = [[UIStoryboard enterpriseStoryBoard] instantiateViewControllerWithIdentifier:@"XeDownloadConfigureFileViewController"];
    
        //Login API call
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
        
    [_enterCodeTextField resignFirstResponder];
    
}

// UITextField delegate method
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    NSLog(@"textFieldDidBeginEditing");
    // became first responder
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    
    NSLog(@"textFieldShouldEndEditing");

    if ([_enterCodeTextField.text isEqualToString:@""]) {
        
        [_continueButton setEnabled:NO];
    }else
    {
        [_continueButton setEnabled:YES];
    }
        
    return YES;
}
- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    [_continueButton setEnabled:NO];
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // return NO to not change text
    
    NSLog(@"textField shouldChangeCharactersInRange:");

    
    if (range.length > 0)
    {
        if (textField.text.length == range.length) {
            
            [_continueButton setEnabled:NO];
            
        }
        
    }
    else {
        
        if (![_continueButton isEnabled]) {
            
            [_continueButton setEnabled:YES];
            
        }
    }
    
    //----------
    if (textField == _enterCodeTextField) {
        
        if ([_enterCodeTextField.text isEqualToString:@""]) {
            
            [_continueButton setEnabled:NO];
            
            if (![string isEqualToString:@""]) {
                [_continueButton setEnabled:YES];
            }
        }else
        {
            [_continueButton setEnabled:YES];
            
            if ([string isEqualToString:@""] && (textField.text.length == range.length)) {
                [_continueButton setEnabled:NO];
            }
        }
    }
    
    //[self viewDidLoad];
    
    return YES;
}

@end
