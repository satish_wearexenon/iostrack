//
//  XeDownloadConfigureFileViewController.m
//  TrackiOS
//
//  Created by xenon on 09/10/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "XeDownloadConfigureFileViewController.h"
#import "TRLoginLocalizer.h"

@interface XeDownloadConfigureFileViewController ()

@end

@implementation XeDownloadConfigureFileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSLog(@"viewDidLoad");
    
    [_logoImg setFrame:CGRectMake(319, 209, 128, 128)];
    
    _downloadStatusProgressview.progress = 0;
    [self.view setBackgroundColor:ViewBackgroundColor];
    
    [_configLabel setFrame:CGRectMake(0.0, 413, _downloadStatusProgressview.frame.size.width, 30)];
    [_configLabel setText:LOCALIZED_CONFIGURE_DOWNLOAD_TITLE];
    [_configLabel setText:[NSString stringWithFormat:@"%@  %@%@",LOCALIZED_CONFIGURE_DOWNLOAD_TITLE,@"0",@"%"]];
    _configLabel.textAlignment = NSTextAlignmentCenter;
    [_configLabel setTextColor:TitleGrayColor];
    [_configLabel setFont:[UIFont fontWithName:Font_Track_Regular size:25.0]];
    
[self performSelector:@selector(downlodingCompleted) withObject:Nil afterDelay:3];
}

-(void) viewWillAppear:(BOOL)animated
{
    NSLog(@"viewWillAppear");
    
    [super viewWillAppear:animated];
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    NSLog(@"XeDownloadConfigureFileViewController :prepareForSegue called");
    
}

-(void)downlodingCompleted;
{
    NSLog(@"XeDownloadConfigureFileViewController :downlodingCompleted called");

    float currentProgress = _downloadStatusProgressview.progress;
    NSLog(@"%0.2f", currentProgress);
    
    for (int i=0; i<10; i++) {
        
            [_downloadStatusProgressview setProgress:currentProgress+i animated:YES];
        [_configLabel setText:[NSString stringWithFormat:@"%@ %.2f%@",LOCALIZED_CONFIGURE_DOWNLOAD_TITLE,(_downloadStatusProgressview.progress*10),@"%"]];
        
         NSLog(@"i= %i, _percetageLabel.text:%f", i,(_downloadStatusProgressview.progress*10));
    }
    
    
    XeUserLoginViewController *loginViewController = [[UIStoryboard enterpriseStoryBoard] instantiateViewControllerWithIdentifier:@"XeUserLoginViewController"];
    
  //  loginViewController.modalTransitionStyle = UIModalTransitionStylePartialCurl;

   // [self presentViewController:loginViewController animated:YES completion:nil];
    [self.navigationController pushViewController:loginViewController animated:YES];
}

@end
