//
//  XeDownloadConfigureFileViewController.h
//  TrackiOS
//
//  Created by xenon on 09/10/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XeUserLoginViewController.h"

@interface XeDownloadConfigureFileViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView *logoImg;
@property (strong, nonatomic) IBOutlet UILabel *configLabel;
@property (strong, nonatomic) IBOutlet UIProgressView *downloadStatusProgressview;

-(void)downlodingCompleted;

@end
