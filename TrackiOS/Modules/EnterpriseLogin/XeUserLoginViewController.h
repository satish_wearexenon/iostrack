//
//  XeUserLoginViewController.h
//  TrackiOS
//
//  Created by xenon on 08/10/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XeUserLoginViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView *logoImages;
@property (strong, nonatomic) IBOutlet UILabel *loginWelcomeLabel;
@property (strong, nonatomic) IBOutlet UILabel *pleaseLoginLabel;
@property (strong, nonatomic) IBOutlet UITextField *userNameTextfield;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextfield;
@property (strong, nonatomic) IBOutlet UIButton *loginButton;
- (IBAction)loginToTrack:(id)sender;

@end
