//
//  XeEnterprisLoginViewController.h
//  TrackiOS
//
//  Created by xenon on 08/10/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XeDownloadConfigureFileViewController.h"

@interface XeEnterprisLoginViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView *logoImage;
@property (strong, nonatomic) IBOutlet UILabel *welcomeLabel;
@property (strong, nonatomic) IBOutlet UILabel *enterCodeLabel;
@property (strong, nonatomic) IBOutlet UITextField *enterCodeTextField;
@property (strong, nonatomic) IBOutlet UIButton *continueButton;

- (IBAction)downloadConfigurationFile:(id)sender;

@end
