//
//  TRSettingsTableViewCell.h
//  TrackiOS
//
//  Created by Anish Kumar on 06/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TRSettingsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *settingsLabel;
@property (weak, nonatomic) IBOutlet UIImageView *settingsCheckmarkImageView;

@end
