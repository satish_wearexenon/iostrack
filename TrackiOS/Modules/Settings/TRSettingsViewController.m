//
//  SettingsViewController.m
//  TrackiOS
//
//  Created by Anish Kumar on 10/10/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "TRSettingsViewController.h"
//#import "TRChangePasswordViewController.h"

@interface TRSettingsViewController ()
@property (weak, nonatomic) IBOutlet UILabel *versionNumberLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray *settingsArray;
@property (nonatomic, retain) NSIndexPath* checkedIndexPath;
@end

@implementation TRSettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    _settingsArray = [NSArray arrayWithObjects:LOCALIZED_SETTINGS_AFTER_SALES, LOCALIZED_SETTINGS_NETWORK_DEV, LOCALIZED_SETTINGS_CHANGE_PASSWORD, LOCALIZED_SETTINGS_LOG_OUT, nil];
    
    [[NSBundle mainBundle] infoDictionary];
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [infoDict objectForKey:@"CFBundleVersion"];
    //NSLog(@"version = %@", version);
    
    _versionNumberLabel.textColor = LightGrayTextColor;
    [_versionNumberLabel setFont:[UIFont fontWithName:Font_Track_Regular size:13]];
    _versionNumberLabel.text =  [NSString stringWithFormat:@"%@ V%@", LOCALIZED_SETTINGS_XENON_TRACK, version] ; //version;
    _checkedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) populateView
{
    _checkedIndexPath  = _selectedIndex;
    [_tableView reloadData];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_settingsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"settingsCell"];
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.text = [_settingsArray objectAtIndex:indexPath.row];
    cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    
    if ([_settingsArray count] - 1 == indexPath.row || [_settingsArray count] - 2 == indexPath.row)
    {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    else
    {
        [[UITableViewCell appearance] setTintColor:CustomTextColor];
        if([_checkedIndexPath isEqual:indexPath])
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            //[tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   // [_delegate hidePop];
    
    if ([_settingsArray count] - 1 == indexPath.row || [_settingsArray count] - 2 == indexPath.row)
    {
        if ([_settingsArray count] - 2 == indexPath.row)
        {
            TRAlertView *av = [[TRAlertView alloc] initWithTitle:nil
                                                         message:@""
                                                        delegate:self
                                                          layout:eControllerypeChangePassword
                                                   containsEmail:NO
                                                         dateStr:nil
                                               cancelButtonTitle:nil
                                               otherButtonTitles:nil];
            [av show];
        }
        if ([_settingsArray count] -1 == indexPath.row) {
            
            
            [NetworkingFactory logOutWithSuccess:^(BOOL success) {
                
                //Clean Up DB as Log Out happened.
                
                [MagicalRecord cleanUp];
                
                NSString *paramURL = [[NSUserDefaults standardUserDefaults] objectForKey:@"ParamURL"];
             
                if (paramURL) {
                   [APP_DELEGATE moveToUserLogin];
                }
                else
                {
                    [APP_DELEGATE moveToEnterpriseLogin];
                }
               
            }];
        }
    }
    else
    {
        if(self.checkedIndexPath)
        {
            UITableViewCell* uncheckCell = [tableView cellForRowAtIndexPath:self.checkedIndexPath];
            uncheckCell.accessoryType = UITableViewCellAccessoryNone;
        }
        UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        [[UITableViewCell appearance] setTintColor:CustomTextColor];
        self.checkedIndexPath = indexPath;
        cell.backgroundColor = [UIColor clearColor];
    }
    [_delegate hidePop];
}

- (void) passwordHasChanged
{
    NSLog(@"Change Password");
    
    [NetworkingFactory changePasswordFromOldPaddword:@"test123" toNewPassword:@"frodo0f0test123" withSuccess:^(BOOL success) {
        
        if (success) {
            NSLog(@"Change Password changed");
        }
    }];
}

@end
