//
//  TRSettingsTableViewCell.m
//  TrackiOS
//
//  Created by Anish Kumar on 06/11/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import "TRSettingsTableViewCell.h"

@implementation TRSettingsTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [_settingsLabel setFont:[UIFont fontWithName:Font_Track_Regular size:19]];
    _settingsLabel.textColor = DarkGrayTextColor;
}

//- (void)setSelected:(BOOL)selected animated:(BOOL)animated
//{
//    [super setSelected:selected animated:YES];
//    if(selected == YES  )
//    {
//        self.settingsCheckmarkImageView.image = [UIImage imageNamed:@"CellCheckmark"];
//        _settingsLabel.textColor = [UIColor whiteColor];
//    }
//    else
//    {
//        self.settingsCheckmarkImageView.image = nil;
//        _settingsLabel.textColor = [UIColor colorWithRed:183.0/255.0 green:183.0/255.0 blue:183.0/255.0 alpha:1.0];
//    }
//}

@end
