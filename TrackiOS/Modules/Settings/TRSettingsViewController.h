//
//  SettingsViewController.h
//  TrackiOS
//
//  Created by Anish Kumar on 10/10/14.
//  Copyright (c) 2014 Xenon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TRSettingsViewController : UIViewController <TRAlertViewDelegate>
@property (nonatomic, assign) id delegate;
@property (nonatomic, strong) NSIndexPath* selectedIndex;

- (void) populateView;
- (void) passwordHasChanged;
@end
